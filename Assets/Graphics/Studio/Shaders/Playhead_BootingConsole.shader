Shader "_PlayHead/Self-Illumin/BootingConsole" {
	Properties 
	{
		_MainTex ("Diffuse (RGB) ButtonGroups (A)", 2D) = "white" {}
		
		_Illum   ("Illumination (RGB) Glow (A)", 2D) = "white" {}
		
		_BootSequence ("BootSequence (A)", 2D) = "black" {}
		_BootProgress ("Boot progress", Float) = 1
		
		_EmissionLM ("Emission (Lightmapper)", Float) = 1
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		Cull Off
		
		CGPROGRAM
		#pragma surface surf Lambert
		//Standard

		sampler2D _MainTex;
		sampler2D _Illum;

		sampler2D _BootSequence;
		float _BootProgress;

		struct Input {
			float2 uv_MainTex; 
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			// Texture sampling
			fixed4 mTex = tex2D(_MainTex, IN.uv_MainTex);
			fixed4 iTex = tex2D(_Illum, IN.uv_MainTex);
			fixed4 bTex = tex2D(_BootSequence, IN.uv_MainTex);
			
			// Compute Albedo
			o.Albedo = ((1-iTex.a*1.5f) * mTex).rgb;//c.rgb;
			
			// Compute Auto-Illumination during booting
			float th1 = step(bTex.a,_BootProgress);
			o.Emission = iTex.rgb*iTex.rgb*2*th1;
			
			// Compute Glow
			o.Alpha = iTex.a;
		}

//		half4 LightingStandard (SurfaceOutput s, half3 lightDir, half atten) 
//		{
//			half NdotL = dot (s.Normal, lightDir);
//			half4 c; 
//			c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten * 2);
//			c.a = s.Alpha;
//			return c;
//		}

//		inline fixed4 LightingStandard_SingleLightmap (SurfaceOutput s, fixed4 color) 
//		{
//			//half3 lm = _LMintensity*DecodeLightmap (color);
//			half3 lm = DecodeLightmap (color);
//
//			half intensity = (lm.r+lm.g+lm.b)/3;
//			//lm = step(intensity,_BootProgress)*lm+step(_BootProgress,intensity)*lm*_BootProgress;
//			lm.r = step(lm.r,_BootProgress)*lm.r+step(_BootProgress,lm.r)*_BootProgress;
//			lm.g = step(lm.g,_BootProgress)*lm.g+step(_BootProgress,lm.g)*_BootProgress;
//			lm.b = step(lm.b,_BootProgress)*lm.b+step(_BootProgress,lm.b)*_BootProgress;
//			//return fixed4(step(lm.r,_BootProgress)*lm.r+step(_BootProgress,lm.r)*_BootProgress,step(lm.g,_BootProgress)*lm.g+step(_BootProgress,lm.g)*_BootProgress,step(lm.b,_BootProgress)*lm.b+step(_BootProgress,lm.b)*_BootProgress,0);//   (0.5f+0.5f*_BootProgress)*lm, 0);
//			return fixed4(lm,0);
//		}
//
//		inline fixed4 LightingStandard_DualLightmap (SurfaceOutput s, fixed4 totalColor, fixed4 indirectOnlyColor, half indirectFade) 
//		{
//			half3 lm = lerp (DecodeLightmap (indirectOnlyColor), DecodeLightmap (totalColor), indirectFade);
//			return fixed4(lm, 0);
//		}
//
//		inline fixed4 LightingStandard_StandardLightmap (SurfaceOutput s, fixed4 color, fixed4 scale, bool surfFuncWritesNormal) 
//		{
//			UNITY_DIRBASIS
//
//			half3 lm = DecodeLightmap (color);
//			half3 scalePerBasisVector = DecodeLightmap (scale);
//
//			if (surfFuncWritesNormal)
//			{
//			    half3 normalInRnmBasis = saturate (mul (unity_DirBasis, s.Normal));
//			    lm *= dot (normalInRnmBasis, scalePerBasisVector);
//			}
//
//			return fixed4(lm, 0);
//		}




		ENDCG
	} 
	FallBack "Self-Illumin/VertexLit"
}
