﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ScrollScreen : MonoBehaviour {
	
	
	public float line;
	public int lineNr;
	public float lineSpace;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	
	void Update () 
	{
		lineNr = Mathf.FloorToInt(line);
		GetComponent<Renderer>().sharedMaterials[1].mainTextureOffset = new Vector2(0f,lineNr * lineSpace);
		GetComponent<Renderer>().sharedMaterials[1].SetTextureOffset("_Illum",new Vector2(0f,lineNr* lineSpace));
	}
}
