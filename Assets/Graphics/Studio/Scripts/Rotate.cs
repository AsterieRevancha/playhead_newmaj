﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {
	
	public float speed;
	public bool invert;
	
	// Use this for initialization
	void Start () 
	{
		if(invert)
			speed = -1 * speed;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Rotate(new Vector3(speed*Time.deltaTime,0,0));
	}
}
