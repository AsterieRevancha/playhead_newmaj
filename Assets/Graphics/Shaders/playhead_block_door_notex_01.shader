// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:False,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:292,x:33585,y:32697,varname:node_292,prsc:2|emission-5812-OUT;n:type:ShaderForge.SFN_ViewReflectionVector,id:294,x:32389,y:32945,varname:node_294,prsc:2;n:type:ShaderForge.SFN_Cubemap,id:293,x:32683,y:32824,ptovrint:False,ptlb:CubeMap,ptin:_CubeMap,varname:node_1680,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:e2fdbd2b483ee894a8cfa70539ac61d4,pvfc:0|DIR-294-OUT;n:type:ShaderForge.SFN_Multiply,id:5805,x:33158,y:32706,varname:node_5805,prsc:2|A-6956-RGB,B-293-RGB;n:type:ShaderForge.SFN_Add,id:5812,x:33366,y:32639,varname:node_5812,prsc:2|A-5814-OUT,B-5805-OUT;n:type:ShaderForge.SFN_Multiply,id:5814,x:33158,y:32451,varname:node_5814,prsc:2|A-5816-A,B-5818-RGB,C-5821-OUT;n:type:ShaderForge.SFN_VertexColor,id:5816,x:32911,y:32218,varname:node_5816,prsc:2;n:type:ShaderForge.SFN_Color,id:5818,x:32911,y:32364,ptovrint:False,ptlb:LightColor,ptin:_LightColor,varname:node_6178,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:5821,x:32517,y:32506,ptovrint:False,ptlb:Light,ptin:_Light,varname:node_3260,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Color,id:6956,x:32717,y:32638,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_500,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;proporder:6956-293-5818-5821;pass:END;sub:END;*/

Shader "_PlayHead_/blocks/door_notex" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _CubeMap ("CubeMap", Cube) = "_Skybox" {}
        _LightColor ("LightColor", Color) = (0.5,0.5,0.5,1)
        _Light ("Light", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform samplerCUBE _CubeMap;
            uniform float4 _LightColor;
            uniform float _Light;
            uniform float4 _Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
////// Emissive:
                float3 emissive = ((i.vertexColor.a*_LightColor.rgb*_Light)+(_Color.rgb*texCUBE(_CubeMap,viewReflectDirection).rgb));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
