// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Mobile/Particles/Additive,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:20,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:42,x:34846,y:32493,varname:node_42,prsc:2|emission-297-OUT;n:type:ShaderForge.SFN_Tex2d,id:44,x:33970,y:32477,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_8859,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:274b4c17b530da1479fcfa0130367b84,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:60,x:33970,y:32674,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_5405,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Slider,id:123,x:33711,y:32896,ptovrint:False,ptlb:AllLights,ptin:_AllLights,varname:node_8655,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:174,x:33426,y:33757,ptovrint:False,ptlb:Light2,ptin:_Light2,varname:node_2618,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:177,x:33426,y:33875,ptovrint:False,ptlb:Light3,ptin:_Light3,varname:node_4424,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:253,x:33426,y:34279,ptovrint:False,ptlb:Light6,ptin:_Light6,varname:node_8398,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:270,x:33426,y:34011,ptovrint:False,ptlb:Light4,ptin:_Light4,varname:node_2936,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:272,x:33426,y:34151,ptovrint:False,ptlb:Light5,ptin:_Light5,varname:node_7899,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:274,x:33426,y:33627,ptovrint:False,ptlb:Light1,ptin:_Light1,varname:node_3874,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Clamp01,id:291,x:34271,y:32920,varname:node_291,prsc:2|IN-296-OUT;n:type:ShaderForge.SFN_Add,id:296,x:34098,y:32920,varname:node_296,prsc:2|A-123-OUT,B-5427-OUT;n:type:ShaderForge.SFN_Multiply,id:297,x:34484,y:32594,varname:node_297,prsc:2|A-44-A,B-60-RGB,C-60-A,D-291-OUT,E-44-RGB;n:type:ShaderForge.SFN_Multiply,id:5412,x:34034,y:33574,varname:node_5412,prsc:2|A-274-OUT,B-5423-A,C-5423-B;n:type:ShaderForge.SFN_Multiply,id:5414,x:33886,y:33705,varname:node_5414,prsc:2|A-174-OUT,B-5424-OUT,C-5423-R;n:type:ShaderForge.SFN_Multiply,id:5416,x:34034,y:33829,varname:node_5416,prsc:2|A-177-OUT,B-5424-OUT,C-5423-G;n:type:ShaderForge.SFN_Multiply,id:5418,x:34036,y:34092,varname:node_5418,prsc:2|A-272-OUT,B-5423-A,C-5423-R;n:type:ShaderForge.SFN_Multiply,id:5420,x:33872,y:33954,varname:node_5420,prsc:2|A-270-OUT,B-5424-OUT,C-5423-B;n:type:ShaderForge.SFN_Multiply,id:5422,x:33881,y:34229,varname:node_5422,prsc:2|A-253-OUT,B-5423-A,C-5423-G;n:type:ShaderForge.SFN_VertexColor,id:5423,x:33526,y:34390,varname:node_5423,prsc:2;n:type:ShaderForge.SFN_OneMinus,id:5424,x:33708,y:34512,varname:node_5424,prsc:2|IN-5423-A;n:type:ShaderForge.SFN_Add,id:5425,x:34438,y:33826,varname:node_5425,prsc:2|A-5414-OUT,B-5420-OUT,C-5422-OUT;n:type:ShaderForge.SFN_Add,id:5427,x:34438,y:33999,varname:node_5427,prsc:2|A-5425-OUT,B-5412-OUT,C-5416-OUT,D-5418-OUT;proporder:60-44-274-174-177-270-272-253-123;pass:END;sub:END;*/

Shader "_PlayHead_/armature_glow_3" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Light1 ("Light1", Range(0, 1)) = 0
        _Light2 ("Light2", Range(0, 1)) = 0
        _Light3 ("Light3", Range(0, 1)) = 0
        _Light4 ("Light4", Range(0, 1)) = 0
        _Light5 ("Light5", Range(0, 1)) = 0
        _Light6 ("Light6", Range(0, 1)) = 0
        _AllLights ("AllLights", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color;
            uniform float _AllLights;
            uniform float _Light2;
            uniform float _Light3;
            uniform float _Light6;
            uniform float _Light4;
            uniform float _Light5;
            uniform float _Light1;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_5424 = (1.0 - i.vertexColor.a);
                float3 emissive = (_MainTex_var.a*_Color.rgb*_Color.a*saturate((_AllLights+(((_Light2*node_5424*i.vertexColor.r)+(_Light4*node_5424*i.vertexColor.b)+(_Light6*i.vertexColor.a*i.vertexColor.g))+(_Light1*i.vertexColor.a*i.vertexColor.b)+(_Light3*node_5424*i.vertexColor.g)+(_Light5*i.vertexColor.a*i.vertexColor.r))))*_MainTex_var.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Mobile/Particles/Additive"
    CustomEditor "ShaderForgeMaterialInspector"
}
