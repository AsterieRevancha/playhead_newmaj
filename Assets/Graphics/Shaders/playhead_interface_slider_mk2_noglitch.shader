// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:_PlayHead_/interface/slider_mk2_nowave,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:262,x:34820,y:32751,varname:node_262,prsc:2|emission-744-OUT,alpha-653-OUT;n:type:ShaderForge.SFN_Color,id:263,x:33841,y:32499,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_6719,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Tex2d,id:264,x:33703,y:32967,ptovrint:False,ptlb:Slider,ptin:_Slider,varname:node_4768,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False|UVIN-304-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:266,x:33784,y:32661,varname:node_3529,prsc:2,tex:ec636699f97c1bd4b93644a206d7118d,ntxv:1,isnm:False|TEX-693-TEX;n:type:ShaderForge.SFN_Multiply,id:267,x:34276,y:32759,varname:node_267,prsc:2|A-263-RGB,B-732-OUT,C-681-OUT;n:type:ShaderForge.SFN_Color,id:269,x:33651,y:33133,ptovrint:False,ptlb:SliderColor,ptin:_SliderColor,varname:node_8115,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:270,x:34074,y:33063,varname:node_270,prsc:2|A-264-R,B-266-G,C-314-OUT;n:type:ShaderForge.SFN_Panner,id:304,x:33481,y:32967,varname:node_304,prsc:2,spu:1,spv:0|DIST-305-OUT;n:type:ShaderForge.SFN_Slider,id:305,x:33272,y:33216,ptovrint:False,ptlb:SliderPosition,ptin:_SliderPosition,varname:node_8211,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.5,cur:0,max:-0.5;n:type:ShaderForge.SFN_Slider,id:314,x:33494,y:33404,ptovrint:False,ptlb:SliderIntensity,ptin:_SliderIntensity,varname:node_4701,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:629,x:34263,y:33249,ptovrint:False,ptlb:Fader,ptin:_Fader,varname:node_9312,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:-2;n:type:ShaderForge.SFN_Add,id:653,x:34378,y:33047,varname:node_653,prsc:2|A-266-B,B-270-OUT,C-629-OUT;n:type:ShaderForge.SFN_Vector1,id:681,x:34017,y:32927,varname:node_681,prsc:2,v1:2;n:type:ShaderForge.SFN_Tex2dAsset,id:693,x:33198,y:32408,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_5374,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ec636699f97c1bd4b93644a206d7118d,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:702,x:33198,y:32650,varname:node_6494,prsc:2,tex:ec636699f97c1bd4b93644a206d7118d,ntxv:0,isnm:False|UVIN-705-OUT,TEX-693-TEX;n:type:ShaderForge.SFN_TexCoord,id:704,x:32589,y:32653,varname:node_704,prsc:2,uv:0;n:type:ShaderForge.SFN_Add,id:705,x:32978,y:32675,varname:node_705,prsc:2|A-709-OUT,B-706-OUT;n:type:ShaderForge.SFN_Multiply,id:706,x:32797,y:32691,varname:node_706,prsc:2|A-704-UVOUT,B-707-OUT;n:type:ShaderForge.SFN_Vector2,id:707,x:32630,y:32828,varname:node_707,prsc:2,v1:1,v2:0.5;n:type:ShaderForge.SFN_Append,id:709,x:33045,y:32840,varname:node_709,prsc:2|A-710-OUT,B-711-OUT;n:type:ShaderForge.SFN_Vector1,id:710,x:32862,y:32828,varname:node_710,prsc:2,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:711,x:32647,y:32934,ptovrint:False,ptlb:UVOffset,ptin:_UVOffset,varname:node_8658,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:732,x:34032,y:32774,varname:node_732,prsc:2|A-266-R,B-702-A;n:type:ShaderForge.SFN_Lerp,id:744,x:34544,y:32823,varname:node_744,prsc:2|A-267-OUT,B-269-RGB,T-270-OUT;proporder:263-269-693-264-711-305-314-629;pass:END;sub:END;*/

Shader "_PlayHead_/interface/slider_mk2_noglitch" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _SliderColor ("SliderColor", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Slider ("Slider", 2D) = "black" {}
        _UVOffset ("UVOffset", Float ) = 0
        _SliderPosition ("SliderPosition", Range(0.5, -0.5)) = 0
        _SliderIntensity ("SliderIntensity", Range(0, 1)) = 1
        _Fader ("Fader", Range(0, -2)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _Slider; uniform float4 _Slider_ST;
            uniform float4 _SliderColor;
            uniform float _SliderPosition;
            uniform float _SliderIntensity;
            uniform float _Fader;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _UVOffset;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 node_3529 = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float2 node_705 = (float2(0.0,_UVOffset)+(i.uv0*float2(1,0.5)));
                float4 node_6494 = tex2D(_MainTex,TRANSFORM_TEX(node_705, _MainTex));
                float2 node_304 = (i.uv0+_SliderPosition*float2(1,0));
                float4 _Slider_var = tex2D(_Slider,TRANSFORM_TEX(node_304, _Slider));
                float node_270 = (_Slider_var.r*node_3529.g*_SliderIntensity);
                float3 emissive = lerp((_Color.rgb*(node_3529.r+node_6494.a)*2.0),_SliderColor.rgb,node_270);
                float3 finalColor = emissive;
                return fixed4(finalColor,(node_3529.b+node_270+_Fader));
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/interface/slider_mk2_nowave"
    CustomEditor "ShaderForgeMaterialInspector"
}
