// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.05882353,fgcg:0.05882353,fgcb:0.08235294,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:5422,x:34295,y:32710,varname:node_5422,prsc:2|emission-6203-OUT;n:type:ShaderForge.SFN_Color,id:5423,x:33571,y:32688,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_6906,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Tex2d,id:5424,x:33386,y:32864,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_5478,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-5432-UVOUT;n:type:ShaderForge.SFN_Multiply,id:5425,x:33778,y:32824,varname:node_5425,prsc:2|A-5423-RGB,B-5424-RGB,C-5431-B,D-5455-OUT;n:type:ShaderForge.SFN_VertexColor,id:5431,x:32614,y:33190,varname:node_5431,prsc:2;n:type:ShaderForge.SFN_Panner,id:5432,x:33189,y:32864,varname:node_5432,prsc:2,spu:1,spv:0|DIST-5433-OUT;n:type:ShaderForge.SFN_Multiply,id:5433,x:33016,y:32885,varname:node_5433,prsc:2|A-5435-OUT,B-5434-OUT,C-5431-R;n:type:ShaderForge.SFN_Vector1,id:5434,x:32731,y:32913,varname:node_5434,prsc:2,v1:0.2;n:type:ShaderForge.SFN_ValueProperty,id:5435,x:32731,y:32855,ptovrint:False,ptlb:Number,ptin:_Number,varname:node_2091,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Slider,id:5455,x:33200,y:33070,ptovrint:False,ptlb:Alpha,ptin:_Alpha,varname:node_2436,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:5461,x:33669,y:33252,varname:node_5461,prsc:2|A-5462-RGB,B-5463-OUT,C-5424-R,D-5431-A,E-5455-OUT;n:type:ShaderForge.SFN_Color,id:5462,x:33368,y:33185,ptovrint:False,ptlb:color1,ptin:_color1,varname:node_993,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:5463,x:33211,y:33368,ptovrint:False,ptlb:Light1,ptin:_Light1,varname:node_919,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.1,cur:0.1,max:1;n:type:ShaderForge.SFN_Multiply,id:6186,x:33664,y:33542,varname:node_6186,prsc:2|A-6188-RGB,B-6190-OUT,C-5424-G,D-5431-A,E-5455-OUT;n:type:ShaderForge.SFN_Color,id:6188,x:33363,y:33476,ptovrint:False,ptlb:color2,ptin:_color2,varname:node_6118,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:6190,x:33206,y:33659,ptovrint:False,ptlb:Light2,ptin:_Light2,varname:node_5224,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.1,cur:0.1,max:1;n:type:ShaderForge.SFN_Multiply,id:6192,x:33656,y:33843,varname:node_6192,prsc:2|A-6194-RGB,B-6196-OUT,C-5424-B,D-5431-A,E-5455-OUT;n:type:ShaderForge.SFN_Color,id:6194,x:33354,y:33777,ptovrint:False,ptlb:color3,ptin:_color3,varname:node_3147,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:6196,x:33197,y:33960,ptovrint:False,ptlb:Light3,ptin:_Light3,varname:node_1888,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.1,cur:0.1,max:1;n:type:ShaderForge.SFN_Multiply,id:6198,x:33648,y:34150,varname:node_6198,prsc:2|A-6200-RGB,B-6202-OUT,C-5424-A,D-5431-A,E-5455-OUT;n:type:ShaderForge.SFN_Color,id:6200,x:33347,y:34084,ptovrint:False,ptlb:color4,ptin:_color4,varname:node_4230,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:6202,x:33190,y:34267,ptovrint:False,ptlb:Light4,ptin:_Light4,varname:node_3360,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.1,cur:0.1,max:1;n:type:ShaderForge.SFN_Add,id:6203,x:34039,y:32932,varname:node_6203,prsc:2|A-5461-OUT,B-6186-OUT,C-6192-OUT,D-6198-OUT,E-5425-OUT;proporder:5423-5424-5435-5455-5462-5463-6188-6190-6194-6196-6200-6202;pass:END;sub:END;*/

Shader "_PlayHead_/interface/score_01" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Number ("Number", Float ) = 0
        _Alpha ("Alpha", Range(0, 1)) = 1
        _color1 ("color1", Color) = (0.5,0.5,0.5,1)
        _Light1 ("Light1", Range(0.1, 1)) = 0.1
        _color2 ("color2", Color) = (0.5,0.5,0.5,1)
        _Light2 ("Light2", Range(0.1, 1)) = 0.1
        _color3 ("color3", Color) = (0.5,0.5,0.5,1)
        _Light3 ("Light3", Range(0.1, 1)) = 0.1
        _color4 ("color4", Color) = (0.5,0.5,0.5,1)
        _Light4 ("Light4", Range(0.1, 1)) = 0.1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Number;
            uniform float _Alpha;
            uniform float4 _color1;
            uniform float _Light1;
            uniform float4 _color2;
            uniform float _Light2;
            uniform float4 _color3;
            uniform float _Light3;
            uniform float4 _color4;
            uniform float _Light4;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float2 node_5432 = (i.uv0+(_Number*0.2*i.vertexColor.r)*float2(1,0));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_5432, _MainTex));
                float3 emissive = ((_color1.rgb*_Light1*_MainTex_var.r*i.vertexColor.a*_Alpha)+(_color2.rgb*_Light2*_MainTex_var.g*i.vertexColor.a*_Alpha)+(_color3.rgb*_Light3*_MainTex_var.b*i.vertexColor.a*_Alpha)+(_color4.rgb*_Light4*_MainTex_var.a*i.vertexColor.a*_Alpha)+(_Color.rgb*_MainTex_var.rgb*i.vertexColor.b*_Alpha));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
