// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Self-Illumin/Diffuse,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:34389,y:32704,varname:node_1,prsc:2|emission-8-OUT;n:type:ShaderForge.SFN_Color,id:2,x:33341,y:32760,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1057,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Cubemap,id:3,x:32731,y:32579,ptovrint:False,ptlb:Cubemap,ptin:_Cubemap,varname:node_6033,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:bd6f1547ba2bfef45adb489d44ae43b0,pvfc:0|DIR-4-OUT;n:type:ShaderForge.SFN_ViewReflectionVector,id:4,x:32499,y:32579,varname:node_4,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:6,x:32469,y:32897,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_2349,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:7,x:33566,y:32901,varname:node_7,prsc:2|A-2-RGB,B-6-R;n:type:ShaderForge.SFN_Add,id:8,x:34143,y:32804,varname:node_8,prsc:2|A-80-OUT,B-336-OUT,C-224-OUT;n:type:ShaderForge.SFN_Multiply,id:21,x:33566,y:33106,varname:node_21,prsc:2|A-242-RGB,B-6-G,C-320-OUT;n:type:ShaderForge.SFN_Multiply,id:80,x:33168,y:32578,varname:node_80,prsc:2|A-3-RGB,B-6-B;n:type:ShaderForge.SFN_Multiply,id:224,x:33533,y:33363,varname:node_224,prsc:2|A-6-G,B-329-OUT,C-350-OUT;n:type:ShaderForge.SFN_Color,id:242,x:33341,y:32982,ptovrint:False,ptlb:CircuitsColor,ptin:_CircuitsColor,varname:node_6065,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:320,x:33184,y:33184,ptovrint:False,ptlb:Pulse,ptin:_Pulse,varname:node_8336,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.5,cur:1,max:1;n:type:ShaderForge.SFN_Step,id:329,x:32933,y:33358,varname:node_329,prsc:2|A-6-A,B-330-OUT;n:type:ShaderForge.SFN_Slider,id:330,x:32336,y:33376,ptovrint:False,ptlb:Activation,ptin:_Activation,varname:node_6260,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Max,id:336,x:33797,y:32903,varname:node_336,prsc:2|A-7-OUT,B-21-OUT;n:type:ShaderForge.SFN_Slider,id:350,x:33113,y:33515,ptovrint:False,ptlb:ActivationFade,ptin:_ActivationFade,varname:node_3350,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:0;proporder:2-242-6-3-320-330-350;pass:END;sub:END;*/

Shader "_PlayHead_/pistes/piste_01" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _CircuitsColor ("CircuitsColor", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "black" {}
        _Cubemap ("Cubemap", Cube) = "_Skybox" {}
        _Pulse ("Pulse", Range(0.5, 1)) = 1
        _Activation ("Activation", Range(0, 1)) = 0
        _ActivationFade ("ActivationFade", Range(1, 0)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            uniform samplerCUBE _Cubemap;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _CircuitsColor;
            uniform float _Pulse;
            uniform float _Activation;
            uniform float _ActivationFade;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = ((texCUBE(_Cubemap,viewReflectDirection).rgb*_MainTex_var.b)+max((_Color.rgb*_MainTex_var.r),(_CircuitsColor.rgb*_MainTex_var.g*_Pulse))+(_MainTex_var.g*step(_MainTex_var.a,_Activation)*_ActivationFade));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Self-Illumin/Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
