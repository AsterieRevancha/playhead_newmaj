// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:_PlayHead_/inv/m_GhostInvader,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:262,x:33933,y:32773,varname:node_262,prsc:2|emission-945-OUT,alpha-653-OUT;n:type:ShaderForge.SFN_Color,id:263,x:32974,y:32499,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1468,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Tex2d,id:266,x:32917,y:32661,varname:node_7137,prsc:2,tex:ec636699f97c1bd4b93644a206d7118d,ntxv:1,isnm:False|TEX-693-TEX;n:type:ShaderForge.SFN_Multiply,id:267,x:33409,y:32759,varname:node_267,prsc:2|A-263-RGB,B-266-R,C-681-OUT;n:type:ShaderForge.SFN_Slider,id:629,x:33113,y:33293,ptovrint:False,ptlb:Fader,ptin:_Fader,varname:node_3693,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:-2;n:type:ShaderForge.SFN_Add,id:653,x:33560,y:33039,varname:node_653,prsc:2|A-266-B,B-959-OUT,C-629-OUT;n:type:ShaderForge.SFN_Vector1,id:681,x:33258,y:32804,varname:node_681,prsc:2,v1:2;n:type:ShaderForge.SFN_Tex2dAsset,id:693,x:32734,y:32698,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_2883,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ec636699f97c1bd4b93644a206d7118d,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:945,x:33667,y:32797,varname:node_945,prsc:2|A-267-OUT,B-959-OUT;n:type:ShaderForge.SFN_TexCoord,id:952,x:32611,y:32937,varname:node_952,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:959,x:33248,y:33058,varname:node_959,prsc:2|A-266-G,B-962-OUT,C-976-OUT;n:type:ShaderForge.SFN_Step,id:962,x:32890,y:33082,varname:node_962,prsc:2|A-952-U,B-963-OUT;n:type:ShaderForge.SFN_Slider,id:963,x:32454,y:33180,ptovrint:False,ptlb:Jauge,ptin:_Jauge,varname:node_2570,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Vector1,id:976,x:33043,y:33151,varname:node_976,prsc:2,v1:0.5;proporder:263-693-963-629;pass:END;sub:END;*/

Shader "_PlayHead_/interface/jauge_01" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Jauge ("Jauge", Range(0, 1)) = 0
        _Fader ("Fader", Range(0, -2)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float _Fader;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Jauge;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 node_7137 = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_959 = (node_7137.g*step(i.uv0.r,_Jauge)*0.5);
                float3 emissive = ((_Color.rgb*node_7137.r*2.0)+node_959);
                float3 finalColor = emissive;
                return fixed4(finalColor,(node_7137.b+node_959+_Fader));
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/inv/m_GhostInvader"
    CustomEditor "ShaderForgeMaterialInspector"
}
