// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:33223,y:32543,varname:node_1,prsc:2|diff-2-RGB,spec-4-OUT,gloss-5-OUT,emission-39-OUT,clip-5507-OUT;n:type:ShaderForge.SFN_Color,id:2,x:32758,y:32488,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_5457,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:3,x:32492,y:32633,ptovrint:False,ptlb:SpecularColor,ptin:_SpecularColor,varname:node_3824,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:4,x:32764,y:32668,varname:node_4,prsc:2|A-3-RGB,B-6-RGB;n:type:ShaderForge.SFN_Multiply,id:5,x:32729,y:32863,varname:node_5,prsc:2|A-6-R,B-3-A;n:type:ShaderForge.SFN_Tex2d,id:6,x:32492,y:32846,ptovrint:False,ptlb:Specular,ptin:_Specular,varname:node_6904,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Fresnel,id:12,x:32177,y:32364,varname:node_12,prsc:2|EXP-13-OUT;n:type:ShaderForge.SFN_Slider,id:13,x:31815,y:32377,ptovrint:False,ptlb:FresnelPower,ptin:_FresnelPower,varname:node_259,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:10;n:type:ShaderForge.SFN_Slider,id:15,x:31923,y:32651,ptovrint:False,ptlb:FresnelIntensity,ptin:_FresnelIntensity,varname:node_2581,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:16,x:32411,y:32448,varname:node_16,prsc:2|A-12-OUT,B-15-OUT;n:type:ShaderForge.SFN_Multiply,id:18,x:32758,y:32319,varname:node_18,prsc:2|A-16-OUT,B-3-RGB;n:type:ShaderForge.SFN_Add,id:39,x:33022,y:32848,varname:node_39,prsc:2|A-18-OUT,B-60-OUT,C-41-RGB;n:type:ShaderForge.SFN_Color,id:41,x:32710,y:33057,ptovrint:False,ptlb:EmissionColor,ptin:_EmissionColor,varname:node_8177,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_NormalVector,id:59,x:32427,y:33260,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:60,x:32938,y:33179,varname:node_60,prsc:2|A-41-RGB,B-61-OUT;n:type:ShaderForge.SFN_ComponentMask,id:61,x:32633,y:33260,varname:node_61,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-59-OUT;n:type:ShaderForge.SFN_Slider,id:5494,x:32645,y:32053,ptovrint:False,ptlb:Apparition,ptin:_Apparition,varname:node_4255,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:5507,x:33117,y:32094,varname:node_5507,prsc:2|A-5494-OUT,B-5508-R;n:type:ShaderForge.SFN_Tex2d,id:5508,x:32854,y:32156,ptovrint:False,ptlb:Alpha,ptin:_Alpha,varname:node_2781,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;proporder:2-3-6-13-15-41-5508-5494;pass:END;sub:END;*/

Shader "_PlayHead_/metal" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _SpecularColor ("SpecularColor", Color) = (0.5,0.5,0.5,1)
        _Specular ("Specular", 2D) = "white" {}
        _FresnelPower ("FresnelPower", Range(0, 10)) = 1
        _FresnelIntensity ("FresnelIntensity", Range(0, 1)) = 0
        _EmissionColor ("EmissionColor", Color) = (0.5,0.5,0.5,1)
        _Alpha ("Alpha", 2D) = "white" {}
        _Apparition ("Apparition", Range(0, 1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform float4 _SpecularColor;
            uniform sampler2D _Specular; uniform float4 _Specular_ST;
            uniform float _FresnelPower;
            uniform float _FresnelIntensity;
            uniform float4 _EmissionColor;
            uniform float _Apparition;
            uniform sampler2D _Alpha; uniform float4 _Alpha_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 _Alpha_var = tex2D(_Alpha,TRANSFORM_TEX(i.uv0, _Alpha));
                clip((_Apparition*_Alpha_var.r) - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float4 _Specular_var = tex2D(_Specular,TRANSFORM_TEX(i.uv0, _Specular));
                float gloss = (_Specular_var.r*_SpecularColor.a);
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = (_SpecularColor.rgb*_Specular_var.rgb);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = _Color.rgb;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = (((pow(1.0-max(0,dot(normalDirection, viewDirection)),_FresnelPower)*_FresnelIntensity)*_SpecularColor.rgb)+(_EmissionColor.rgb*i.normalDir.g)+_EmissionColor.rgb);
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float _Apparition;
            uniform sampler2D _Alpha; uniform float4 _Alpha_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float4 _Alpha_var = tex2D(_Alpha,TRANSFORM_TEX(i.uv0, _Alpha));
                clip((_Apparition*_Alpha_var.r) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
