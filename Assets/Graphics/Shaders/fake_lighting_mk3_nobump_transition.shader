// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:_PlayHead_/fake_lighting_mk1_nobump,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:False,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.003,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:56,x:30706,y:32814,varname:node_56,prsc:2|spec-4952-OUT,gloss-4954-OUT,emission-308-OUT;n:type:ShaderForge.SFN_Color,id:292,x:29541,y:32862,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_6729,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:293,x:29851,y:33167,varname:node_293,prsc:2|A-292-RGB,B-995-RGB,C-292-A,D-5041-OUT;n:type:ShaderForge.SFN_VertexColor,id:301,x:29247,y:33377,varname:node_301,prsc:2;n:type:ShaderForge.SFN_Color,id:306,x:29451,y:33588,ptovrint:False,ptlb:Glow,ptin:_Glow,varname:node_8206,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Add,id:308,x:30355,y:33243,varname:node_308,prsc:2|A-1013-OUT,B-293-OUT,C-309-OUT,D-3343-OUT;n:type:ShaderForge.SFN_Multiply,id:309,x:29819,y:33503,varname:node_309,prsc:2|A-301-R,B-306-RGB,C-306-A,D-5006-OUT;n:type:ShaderForge.SFN_Tex2d,id:995,x:29496,y:33045,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_7794,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1004,x:29826,y:32654,ptovrint:False,ptlb:Emission,ptin:_Emission,varname:node_1508,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1013,x:30151,y:32937,varname:node_1013,prsc:2|A-1004-RGB,B-1019-RGB,C-1019-A,D-5006-OUT;n:type:ShaderForge.SFN_Color,id:1019,x:29826,y:32898,ptovrint:False,ptlb:EmissionColor,ptin:_EmissionColor,varname:node_4983,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Vector1,id:3343,x:30140,y:33399,varname:node_3343,prsc:2,v1:0.008;n:type:ShaderForge.SFN_Color,id:4942,x:30128,y:32711,ptovrint:False,ptlb:SpecColor,ptin:_SpecColor,varname:node_4577,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:4952,x:30398,y:32648,varname:node_4952,prsc:2|A-995-RGB,B-4942-RGB,C-301-G;n:type:ShaderForge.SFN_Multiply,id:4954,x:30377,y:32814,varname:node_4954,prsc:2|A-4942-A,B-995-R;n:type:ShaderForge.SFN_OneMinus,id:5006,x:29808,y:33335,varname:node_5006,prsc:2|IN-301-G;n:type:ShaderForge.SFN_Add,id:5039,x:29473,y:33243,varname:node_5039,prsc:2|A-5040-OUT,B-301-G;n:type:ShaderForge.SFN_Vector1,id:5040,x:29283,y:33242,varname:node_5040,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Clamp01,id:5041,x:29640,y:33243,varname:node_5041,prsc:2|IN-5039-OUT;proporder:292-1019-4942-306-995-1004;pass:END;sub:END;*/

Shader "_PlayHead_/fake_lighting_mk3_nobump_trans" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _EmissionColor ("EmissionColor", Color) = (0.5,0.5,0.5,1)
        _SpecColor ("SpecColor", Color) = (0.5,0.5,0.5,1)
        _Glow ("Glow", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Emission ("Emission", 2D) = "black" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform float4 _Glow;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _Emission; uniform float4 _Emission_ST;
            uniform float4 _EmissionColor;
            uniform float4 _SpecColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float gloss = (_SpecColor.a*_MainTex_var.r);
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = (_MainTex_var.rgb*_SpecColor.rgb*i.vertexColor.g);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
////// Emissive:
                float4 _Emission_var = tex2D(_Emission,TRANSFORM_TEX(i.uv0, _Emission));
                float node_5006 = (1.0 - i.vertexColor.g);
                float3 emissive = ((_Emission_var.rgb*_EmissionColor.rgb*_EmissionColor.a*node_5006)+(_Color.rgb*_MainTex_var.rgb*_Color.a*saturate((0.5+i.vertexColor.g)))+(i.vertexColor.r*_Glow.rgb*_Glow.a*node_5006)+0.008);
/// Final Color:
                float3 finalColor = specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/fake_lighting_mk1_nobump"
    CustomEditor "ShaderForgeMaterialInspector"
}
