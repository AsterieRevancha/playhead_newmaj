// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:False,igpj:False,qofs:-997,qpre:0,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:33250,y:32644,varname:node_1,prsc:2|emission-72-OUT;n:type:ShaderForge.SFN_Color,id:4,x:32460,y:32694,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_6509,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.07843138,c3:0.07843138,c4:1;n:type:ShaderForge.SFN_Tex2d,id:13,x:32460,y:32880,ptovrint:False,ptlb:FogMap,ptin:_FogMap,varname:node_4134,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Lerp,id:72,x:32927,y:32746,varname:node_72,prsc:2|A-351-RGB,B-4-RGB,T-13-B;n:type:ShaderForge.SFN_Color,id:351,x:32460,y:32514,ptovrint:False,ptlb:FogColor,ptin:_FogColor,varname:node_4469,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.07843138,c3:0.07843138,c4:1;proporder:4-351-13;pass:END;sub:END;*/

Shader "_PlayHead_/fog/zenith" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.07843138,0.07843138,1)
        _FogColor ("FogColor", Color) = (0.07843138,0.07843138,0.07843138,1)
        _FogMap ("FogMap", 2D) = "black" {}
    }
    SubShader {
        Tags {
            "Queue"="Background-997"
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _FogMap; uniform float4 _FogMap_ST;
            uniform float4 _FogColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 _FogMap_var = tex2D(_FogMap,TRANSFORM_TEX(i.uv0, _FogMap));
                float3 emissive = lerp(_FogColor.rgb,_Color.rgb,_FogMap_var.b);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
