// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:_PlayHead_/skysphere_01_noramp,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:False,igpj:False,qofs:-999,qpre:0,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:34343,y:32993,varname:node_1,prsc:2|emission-2810-OUT;n:type:ShaderForge.SFN_TexCoord,id:7,x:32519,y:33125,varname:node_7,prsc:2,uv:1;n:type:ShaderForge.SFN_Lerp,id:27,x:33065,y:33034,varname:node_27,prsc:2|A-38-RGB,B-40-RGB,T-7-V;n:type:ShaderForge.SFN_Color,id:38,x:32818,y:32755,ptovrint:False,ptlb:Horizon,ptin:_Horizon,varname:node_6275,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:40,x:32818,y:32941,ptovrint:False,ptlb:Sky,ptin:_Sky,varname:node_8854,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:2555,x:33065,y:33186,varname:node_2555,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:2631,x:32340,y:33746,varname:node_2631,prsc:2,uv:0;n:type:ShaderForge.SFN_Append,id:2633,x:33140,y:33374,varname:node_2633,prsc:2|A-2635-OUT,B-6149-OUT;n:type:ShaderForge.SFN_Slider,id:2635,x:32776,y:33314,ptovrint:False,ptlb:FogValue,ptin:_FogValue,varname:node_9327,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:2641,x:32216,y:33490,ptovrint:False,ptlb:HeightAdjust,ptin:_HeightAdjust,varname:node_691,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:0.6,max:0;n:type:ShaderForge.SFN_Tex2d,id:2651,x:33327,y:33374,ptovrint:False,ptlb:FogRamp,ptin:_FogRamp,varname:node_5897,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False|UVIN-2633-OUT;n:type:ShaderForge.SFN_Add,id:2803,x:33577,y:32946,varname:node_2803,prsc:2|A-27-OUT,B-2804-OUT,C-2827-OUT;n:type:ShaderForge.SFN_Multiply,id:2804,x:33667,y:33210,varname:node_2804,prsc:2|A-2809-OUT,B-2651-RGB,C-2828-RGB,D-2828-A;n:type:ShaderForge.SFN_RemapRange,id:2808,x:33277,y:33168,varname:node_2808,prsc:2,frmn:0.7,frmx:1,tomn:0,tomx:0.8|IN-2555-B;n:type:ShaderForge.SFN_Clamp01,id:2809,x:33446,y:33168,varname:node_2809,prsc:2|IN-2808-OUT;n:type:ShaderForge.SFN_Multiply,id:2810,x:33958,y:33007,varname:node_2810,prsc:2|A-2803-OUT,B-2824-OUT;n:type:ShaderForge.SFN_RemapRange,id:2823,x:33222,y:33641,varname:node_2823,prsc:2,frmn:0.4,frmx:0.5,tomn:0.7,tomx:1|IN-2631-V;n:type:ShaderForge.SFN_Clamp01,id:2824,x:33400,y:33641,varname:node_2824,prsc:2|IN-2823-OUT;n:type:ShaderForge.SFN_Color,id:2826,x:33096,y:32775,ptovrint:False,ptlb:SunInfluence,ptin:_SunInfluence,varname:node_2567,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:2827,x:33295,y:32807,varname:node_2827,prsc:2|A-2826-RGB,B-2555-G,C-2826-A;n:type:ShaderForge.SFN_Color,id:2828,x:33474,y:33440,ptovrint:False,ptlb:FogColor,ptin:_FogColor,varname:node_4270,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:0.4;n:type:ShaderForge.SFN_Clamp01,id:6147,x:32753,y:33572,varname:node_6147,prsc:2|IN-6148-OUT;n:type:ShaderForge.SFN_RemapRange,id:6148,x:32575,y:33610,varname:node_6148,prsc:2,frmn:0.5,frmx:0.53,tomn:0,tomx:1|IN-2631-V;n:type:ShaderForge.SFN_Multiply,id:6149,x:32922,y:33519,varname:node_6149,prsc:2|A-2641-OUT,B-6147-OUT;proporder:38-40-2651-2635-2641-2826-2828;pass:END;sub:END;*/

Shader "_PlayHead_/skysphere_01" {
    Properties {
        _Horizon ("Horizon", Color) = (1,1,1,1)
        _Sky ("Sky", Color) = (0.5,0.5,1,1)
        _FogRamp ("FogRamp", 2D) = "black" {}
        _FogValue ("FogValue", Range(0, 1)) = 1
        _HeightAdjust ("HeightAdjust", Range(1, 0)) = 0.6
        _SunInfluence ("SunInfluence", Color) = (1,1,1,1)
        _FogColor ("FogColor", Color) = (1,1,1,0.4)
    }
    SubShader {
        Tags {
            "Queue"="Background-999"
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Horizon;
            uniform float4 _Sky;
            uniform float _FogValue;
            uniform float _HeightAdjust;
            uniform sampler2D _FogRamp; uniform float4 _FogRamp_ST;
            uniform float4 _SunInfluence;
            uniform float4 _FogColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float2 node_2633 = float2(_FogValue,(_HeightAdjust*saturate((i.uv0.g*33.33337+-16.66668))));
                float4 _FogRamp_var = tex2D(_FogRamp,TRANSFORM_TEX(node_2633, _FogRamp));
                float3 emissive = ((lerp(_Horizon.rgb,_Sky.rgb,i.uv1.g)+(saturate((i.vertexColor.b*2.666667+-1.866667))*_FogRamp_var.rgb*_FogColor.rgb*_FogColor.a)+(_SunInfluence.rgb*i.vertexColor.g*_SunInfluence.a))*saturate((i.uv0.g*3.0+-0.5000001)));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/skysphere_01_noramp"
    CustomEditor "ShaderForgeMaterialInspector"
}
