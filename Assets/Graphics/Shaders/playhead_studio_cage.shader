// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Self-Illumin/Diffuse,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:2840,x:33466,y:32456,varname:node_2840,prsc:2|emission-5207-OUT;n:type:ShaderForge.SFN_Tex2d,id:2841,x:32554,y:31901,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_3407,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:3025,x:32770,y:32381,varname:node_3025,prsc:2|A-3156-RGB,B-4759-OUT;n:type:ShaderForge.SFN_Color,id:3156,x:32146,y:32592,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_924,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_NormalVector,id:4758,x:32146,y:32160,prsc:2,pt:False;n:type:ShaderForge.SFN_ConstantClamp,id:4759,x:32496,y:32160,varname:node_4759,prsc:2,min:0.5,max:1|IN-4760-OUT;n:type:ShaderForge.SFN_ComponentMask,id:4760,x:32335,y:32160,varname:node_4760,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-4758-OUT;n:type:ShaderForge.SFN_Fresnel,id:5193,x:32611,y:32633,varname:node_5193,prsc:2|EXP-5194-OUT;n:type:ShaderForge.SFN_Vector1,id:5194,x:32445,y:32696,varname:node_5194,prsc:2,v1:3;n:type:ShaderForge.SFN_Add,id:5207,x:33021,y:32552,varname:node_5207,prsc:2|A-3025-OUT,B-5220-OUT,C-5620-OUT;n:type:ShaderForge.SFN_Multiply,id:5220,x:32830,y:32695,varname:node_5220,prsc:2|A-5193-OUT,B-5221-RGB;n:type:ShaderForge.SFN_Color,id:5221,x:32646,y:32785,ptovrint:False,ptlb:FresnelColor,ptin:_FresnelColor,varname:node_5195,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:5620,x:32788,y:32084,varname:node_5620,prsc:2|A-5621-RGB,B-2841-RGB,C-5623-OUT;n:type:ShaderForge.SFN_Color,id:5621,x:32554,y:31735,ptovrint:False,ptlb:GlowColor,ptin:_GlowColor,varname:node_2293,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Slider,id:5623,x:32339,y:32064,ptovrint:False,ptlb:Glow,ptin:_Glow,varname:node_1095,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;proporder:3156-5221-2841-5621-5623;pass:END;sub:END;*/

Shader "_PlayHead_/studio/cage" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _FresnelColor ("FresnelColor", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "black" {}
        _GlowColor ("GlowColor", Color) = (1,1,1,1)
        _Glow ("Glow", Range(0, 1)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color;
            uniform float4 _FresnelColor;
            uniform float4 _GlowColor;
            uniform float _Glow;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = ((_Color.rgb*clamp(i.normalDir.g,0.5,1))+(pow(1.0-max(0,dot(normalDirection, viewDirection)),3.0)*_FresnelColor.rgb)+(_GlowColor.rgb*_MainTex_var.rgb*_Glow));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #include "UnityCG.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color;
            uniform float4 _FresnelColor;
            uniform float4 _GlowColor;
            uniform float _Glow;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                o.Emission = ((_Color.rgb*clamp(i.normalDir.g,0.5,1))+(pow(1.0-max(0,dot(normalDirection, viewDirection)),3.0)*_FresnelColor.rgb)+(_GlowColor.rgb*_MainTex_var.rgb*_Glow));
                
                float3 diffColor = float3(0,0,0);
                o.Albedo = diffColor;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Self-Illumin/Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
