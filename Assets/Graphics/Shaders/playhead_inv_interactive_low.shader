// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Mobile/Particles/Additive,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:6,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:493,x:33446,y:32990,varname:node_493,prsc:2|emission-4575-OUT,voffset-2690-OUT;n:type:ShaderForge.SFN_Color,id:495,x:31553,y:32735,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_6537,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.3,c3:0.3,c4:1;n:type:ShaderForge.SFN_Slider,id:554,x:31409,y:32621,ptovrint:False,ptlb:Transition,ptin:_Transition,varname:node_9242,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:564,x:31860,y:32841,varname:node_564,prsc:2|A-495-RGB,B-566-RGB,T-554-OUT;n:type:ShaderForge.SFN_Color,id:566,x:31543,y:32925,ptovrint:False,ptlb:ColorActive,ptin:_ColorActive,varname:node_9234,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_NormalVector,id:2688,x:32264,y:33217,prsc:2,pt:False;n:type:ShaderForge.SFN_Slider,id:2689,x:32310,y:33482,ptovrint:False,ptlb:Explosion,ptin:_Explosion,varname:node_8025,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:2690,x:32798,y:33318,varname:node_2690,prsc:2|A-2688-OUT,B-2689-OUT,C-4762-G,D-2754-OUT;n:type:ShaderForge.SFN_Vector1,id:2754,x:32264,y:33388,varname:node_2754,prsc:2,v1:0.66;n:type:ShaderForge.SFN_OneMinus,id:2898,x:32877,y:33484,varname:node_2898,prsc:2|IN-2689-OUT;n:type:ShaderForge.SFN_Tex2d,id:4467,x:32042,y:32577,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_4306,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:4575,x:33083,y:32893,varname:node_4575,prsc:2|A-5136-OUT,B-564-OUT,C-2898-OUT,D-4762-R;n:type:ShaderForge.SFN_VertexColor,id:4762,x:32799,y:32989,varname:node_4762,prsc:2;n:type:ShaderForge.SFN_Max,id:5136,x:32755,y:32629,varname:node_5136,prsc:2|A-4467-R,B-4467-G,C-5365-OUT;n:type:ShaderForge.SFN_Step,id:5244,x:32396,y:32979,varname:node_5244,prsc:2|A-4467-B,B-5402-OUT;n:type:ShaderForge.SFN_Clamp01,id:5365,x:32597,y:32979,varname:node_5365,prsc:2|IN-5244-OUT;n:type:ShaderForge.SFN_Multiply,id:5402,x:32130,y:32921,varname:node_5402,prsc:2|A-554-OUT,B-5403-OUT;n:type:ShaderForge.SFN_Vector1,id:5403,x:31898,y:33018,varname:node_5403,prsc:2,v1:0.42;proporder:4467-495-566-554-2689;pass:END;sub:END;*/

Shader "_PlayHead_/inv/interactive_low" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _Color ("Color", Color) = (1,0.3,0.3,1)
        _ColorActive ("ColorActive", Color) = (1,0,0,1)
        _Transition ("Transition", Range(0, 1)) = 0
        _Explosion ("Explosion", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One OneMinusSrcColor
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float _Transition;
            uniform float4 _ColorActive;
            uniform float _Explosion;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (v.normal*_Explosion*o.vertexColor.g*0.66);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = (max(max(_MainTex_var.r,_MainTex_var.g),saturate(step(_MainTex_var.b,(_Transition*0.42))))*lerp(_Color.rgb,_ColorActive.rgb,_Transition)*(1.0 - _Explosion)*i.vertexColor.r);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float _Explosion;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float3 normalDir : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (v.normal*_Explosion*o.vertexColor.g*0.66);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Mobile/Particles/Additive"
    CustomEditor "ShaderForgeMaterialInspector"
}
