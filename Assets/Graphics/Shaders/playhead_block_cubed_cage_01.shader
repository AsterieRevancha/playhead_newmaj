// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:292,x:33249,y:32620,varname:node_292,prsc:2|emission-5835-OUT;n:type:ShaderForge.SFN_ViewReflectionVector,id:294,x:32273,y:32773,varname:node_294,prsc:2;n:type:ShaderForge.SFN_Cubemap,id:293,x:32468,y:32773,ptovrint:False,ptlb:CubeMap,ptin:_CubeMap,varname:node_7167,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:e2fdbd2b483ee894a8cfa70539ac61d4,pvfc:0|DIR-294-OUT;n:type:ShaderForge.SFN_Tex2d,id:5806,x:32468,y:32567,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_8134,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Color,id:5833,x:32487,y:32238,ptovrint:False,ptlb:GlowColor,ptin:_GlowColor,varname:node_9676,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9960784,c2:0.9333333,c3:0.7960784,c4:1;n:type:ShaderForge.SFN_Slider,id:5834,x:32311,y:32449,ptovrint:False,ptlb:Glow,ptin:_Glow,varname:node_1797,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Add,id:5835,x:32987,y:32718,varname:node_5835,prsc:2|A-5836-OUT,B-5842-OUT;n:type:ShaderForge.SFN_Multiply,id:5836,x:32745,y:32525,varname:node_5836,prsc:2|A-5833-RGB,B-5834-OUT,C-5806-R;n:type:ShaderForge.SFN_Multiply,id:5842,x:32712,y:32864,varname:node_5842,prsc:2|A-293-RGB,B-5843-RGB;n:type:ShaderForge.SFN_Color,id:5843,x:32489,y:32972,ptovrint:False,ptlb:color,ptin:_color,varname:node_546,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;proporder:5843-293-5806-5833-5834;pass:END;sub:END;*/

Shader "_PlayHead_/blocks/cubed_cage" {
    Properties {
        _color ("color", Color) = (0.5,0.5,0.5,1)
        _CubeMap ("CubeMap", Cube) = "_Skybox" {}
        _MainTex ("MainTex", 2D) = "black" {}
        _GlowColor ("GlowColor", Color) = (0.9960784,0.9333333,0.7960784,1)
        _Glow ("Glow", Range(0, 1)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform samplerCUBE _CubeMap;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _GlowColor;
            uniform float _Glow;
            uniform float4 _color;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = ((_GlowColor.rgb*_Glow*_MainTex_var.r)+(texCUBE(_CubeMap,viewReflectDirection).rgb*_color.rgb));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
