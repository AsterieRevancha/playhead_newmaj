// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:_PlayHead_/studio_consoles_nocube,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:True;n:type:ShaderForge.SFN_Final,id:2840,x:33620,y:32478,varname:node_2840,prsc:2|emission-3025-OUT;n:type:ShaderForge.SFN_Tex2d,id:2841,x:32308,y:32437,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_2716,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:1,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:2927,x:32889,y:32641,ptovrint:False,ptlb:Emission,ptin:_Emission,varname:node_5351,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Cubemap,id:3014,x:32451,y:32155,ptovrint:False,ptlb:Cubemap,ptin:_Cubemap,varname:node_4385,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,pvfc:0|DIR-3020-OUT;n:type:ShaderForge.SFN_NormalVector,id:3020,x:32169,y:32155,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:3025,x:32975,y:32138,varname:node_3025,prsc:2|A-3077-OUT,B-3014-RGB,C-2841-RGB,D-3156-RGB;n:type:ShaderForge.SFN_Vector1,id:3077,x:32205,y:31975,varname:node_3077,prsc:2,v1:2;n:type:ShaderForge.SFN_Color,id:3156,x:32518,y:32525,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_4369,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Add,id:3165,x:33385,y:32557,varname:node_3165,prsc:2;n:type:ShaderForge.SFN_Color,id:3174,x:32889,y:32882,ptovrint:False,ptlb:EmissionColor,ptin:_EmissionColor,varname:node_1271,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:3175,x:33137,y:32697,varname:node_3175,prsc:2|A-2927-RGB,B-3174-RGB;n:type:ShaderForge.SFN_Blend,id:4706,x:33154,y:32408,varname:node_4706,prsc:2,blmd:6,clmp:True;proporder:3156-2841-2927-3174-3014;pass:END;sub:END;*/

Shader "_PlayHead_/studio_consoles" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("MainTex", 2D) = "gray" {}
        _Emission ("Emission", 2D) = "black" {}
        _EmissionColor ("EmissionColor", Color) = (0.5,0.5,0.5,1)
        _Cubemap ("Cubemap", Cube) = "_Skybox" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 2.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform samplerCUBE _Cubemap;
            uniform float4 _Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = (2.0*texCUBE(_Cubemap,i.normalDir).rgb*_MainTex_var.rgb*_Color.rgb);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/studio_consoles_nocube"
    CustomEditor "ShaderForgeMaterialInspector"
}
