// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:4060,x:34627,y:32712,varname:node_4060,prsc:2|diff-4061-RGB,spec-4062-RGB,gloss-4063-OUT,emission-4068-OUT;n:type:ShaderForge.SFN_Color,id:4061,x:34202,y:32310,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1585,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4062,x:34014,y:32570,ptovrint:False,ptlb:Specular,ptin:_Specular,varname:node_9135,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:4063,x:33872,y:32765,ptovrint:False,ptlb:Shininess,ptin:_Shininess,varname:node_3369,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_VertexColor,id:4064,x:33834,y:33014,varname:node_4064,prsc:2;n:type:ShaderForge.SFN_Color,id:4065,x:33992,y:32874,ptovrint:False,ptlb:SideGlow,ptin:_SideGlow,varname:node_5599,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4067,x:33989,y:33172,ptovrint:False,ptlb:HeadGlow,ptin:_HeadGlow,varname:node_1261,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Add,id:4068,x:34446,y:32856,varname:node_4068,prsc:2|A-4073-OUT,B-4070-OUT,C-4072-OUT,D-4077-OUT;n:type:ShaderForge.SFN_Multiply,id:4070,x:34260,y:32903,varname:node_4070,prsc:2|A-4065-RGB,B-4064-R,C-4082-RGB;n:type:ShaderForge.SFN_Multiply,id:4072,x:34260,y:33063,varname:node_4072,prsc:2|A-4064-G,B-4067-RGB,C-4086-RGB;n:type:ShaderForge.SFN_Vector1,id:4073,x:34241,y:32822,varname:node_4073,prsc:2,v1:0.008;n:type:ShaderForge.SFN_NormalVector,id:4075,x:33635,y:32368,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:4076,x:33817,y:32356,varname:node_4076,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-4075-OUT;n:type:ShaderForge.SFN_Multiply,id:4077,x:34288,y:32477,varname:node_4077,prsc:2|A-4061-A,B-4076-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:4081,x:33171,y:32703,ptovrint:False,ptlb:GlowRamp,ptin:_GlowRamp,varname:node_6694,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:4082,x:33656,y:32779,varname:node_9340,prsc:2,ntxv:0,isnm:False|UVIN-4098-OUT,TEX-4081-TEX;n:type:ShaderForge.SFN_Tex2d,id:4086,x:33653,y:33128,varname:node_1103,prsc:2,ntxv:0,isnm:False|UVIN-4090-OUT,TEX-4081-TEX;n:type:ShaderForge.SFN_TexCoord,id:4087,x:33207,y:32934,varname:node_4087,prsc:2,uv:0;n:type:ShaderForge.SFN_TexCoord,id:4089,x:33207,y:33077,varname:node_4089,prsc:2,uv:1;n:type:ShaderForge.SFN_Add,id:4090,x:33426,y:33177,varname:node_4090,prsc:2|A-4089-UVOUT,B-4092-OUT;n:type:ShaderForge.SFN_Time,id:4091,x:32961,y:33225,varname:node_4091,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4092,x:33156,y:33273,varname:node_4092,prsc:2|A-4091-T,B-4093-OUT,C-4099-OUT;n:type:ShaderForge.SFN_Vector1,id:4093,x:32961,y:33372,varname:node_4093,prsc:2,v1:-1.8755;n:type:ShaderForge.SFN_Add,id:4098,x:33415,y:32985,varname:node_4098,prsc:2|A-4087-UVOUT,B-4092-OUT,C-4100-OUT;n:type:ShaderForge.SFN_Vector1,id:4099,x:32961,y:33432,varname:node_4099,prsc:2,v1:0.25;n:type:ShaderForge.SFN_Vector1,id:4100,x:33019,y:33006,varname:node_4100,prsc:2,v1:0.25;proporder:4061-4062-4063-4065-4067-4081;pass:END;sub:END;*/

Shader "_PlayHead_/cockpit_01_automatic" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _Specular ("Specular", Color) = (0.5,0.5,0.5,1)
        _Shininess ("Shininess", Range(0, 1)) = 0
        _SideGlow ("SideGlow", Color) = (0.5,0.5,0.5,1)
        _HeadGlow ("HeadGlow", Color) = (0.5,0.5,0.5,1)
        _GlowRamp ("GlowRamp", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform float4 _Specular;
            uniform float _Shininess;
            uniform float4 _SideGlow;
            uniform float4 _HeadGlow;
            uniform sampler2D _GlowRamp; uniform float4 _GlowRamp_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(4,5)
                UNITY_FOG_COORDS(6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Shininess;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = _Specular.rgb;
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = _Color.rgb;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float4 node_4091 = _Time + _TimeEditor;
                float node_4092 = (node_4091.g*(-1.8755)*0.25);
                float2 node_4098 = (i.uv0+node_4092+0.25);
                float4 node_9340 = tex2D(_GlowRamp,TRANSFORM_TEX(node_4098, _GlowRamp));
                float2 node_4090 = (i.uv1+node_4092);
                float4 node_1103 = tex2D(_GlowRamp,TRANSFORM_TEX(node_4090, _GlowRamp));
                float3 emissive = (0.008+(_SideGlow.rgb*i.vertexColor.r*node_9340.rgb)+(i.vertexColor.g*_HeadGlow.rgb*node_1103.rgb)+(_Color.a*i.normalDir.g));
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform float4 _Specular;
            uniform float _Shininess;
            uniform float4 _SideGlow;
            uniform float4 _HeadGlow;
            uniform sampler2D _GlowRamp; uniform float4 _GlowRamp_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(4,5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Shininess;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = _Specular.rgb;
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 diffuseColor = _Color.rgb;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
