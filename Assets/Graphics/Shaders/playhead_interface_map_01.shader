// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:140,x:34116,y:32640,varname:node_140,prsc:2|emission-177-OUT,alpha-141-A;n:type:ShaderForge.SFN_Tex2d,id:141,x:33491,y:32599,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_2010,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:143,x:33515,y:32830,varname:node_1734,prsc:2,ntxv:2,isnm:False|UVIN-223-UVOUT,TEX-207-TEX;n:type:ShaderForge.SFN_Add,id:177,x:33833,y:32624,varname:node_177,prsc:2|A-141-RGB,B-143-A,C-254-OUT;n:type:ShaderForge.SFN_TexCoord,id:194,x:33008,y:32769,varname:node_194,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:198,x:32713,y:32988,ptovrint:False,ptlb:Phase,ptin:_Phase,varname:node_4297,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Tex2dAsset,id:207,x:33233,y:32656,ptovrint:False,ptlb:MapElements,ptin:_MapElements,varname:node_850,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:209,x:33515,y:33003,varname:node_9032,prsc:2,ntxv:2,isnm:False|UVIN-229-UVOUT,TEX-207-TEX;n:type:ShaderForge.SFN_Panner,id:223,x:33257,y:32869,varname:node_223,prsc:2,spu:0,spv:1|UVIN-194-UVOUT,DIST-198-OUT;n:type:ShaderForge.SFN_Panner,id:229,x:33257,y:33051,varname:node_229,prsc:2,spu:0,spv:1|UVIN-194-UVOUT,DIST-231-OUT;n:type:ShaderForge.SFN_Slider,id:231,x:32696,y:33129,ptovrint:False,ptlb:Look,ptin:_Look,varname:node_6527,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:253,x:32674,y:33253,ptovrint:False,ptlb:LookIntensity,ptin:_LookIntensity,varname:node_5312,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:254,x:33768,y:33122,varname:node_254,prsc:2|A-209-RGB,B-253-OUT;proporder:141-207-231-198-253;pass:END;sub:END;*/

Shader "_PlayHead_/interface/map" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _MapElements ("MapElements", 2D) = "white" {}
        _Look ("Look", Range(0, 1)) = 0
        _Phase ("Phase", Range(0, 1)) = 0
        _LookIntensity ("LookIntensity", Range(0, 1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Phase;
            uniform sampler2D _MapElements; uniform float4 _MapElements_ST;
            uniform float _Look;
            uniform float _LookIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float2 node_223 = (i.uv0+_Phase*float2(0,1));
                float4 node_1734 = tex2D(_MapElements,TRANSFORM_TEX(node_223, _MapElements));
                float2 node_229 = (i.uv0+_Look*float2(0,1));
                float4 node_9032 = tex2D(_MapElements,TRANSFORM_TEX(node_229, _MapElements));
                float3 emissive = (_MainTex_var.rgb+node_1734.a+(node_9032.rgb*_LookIntensity));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,_MainTex_var.a);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
