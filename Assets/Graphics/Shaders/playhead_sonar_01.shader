// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Mobile/Particles/Additive,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:6034,x:33984,y:32712,varname:node_6034,prsc:2|emission-6037-OUT;n:type:ShaderForge.SFN_Color,id:6036,x:33536,y:32584,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_8442,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:6037,x:33762,y:32741,varname:node_6037,prsc:2|A-6036-RGB,B-6038-RGB,C-6048-OUT,D-6074-A,E-6078-OUT;n:type:ShaderForge.SFN_Tex2d,id:6038,x:33494,y:32775,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_3039,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-6056-UVOUT;n:type:ShaderForge.SFN_Slider,id:6048,x:33311,y:32979,ptovrint:False,ptlb:Intensity,ptin:_Intensity,varname:node_127,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Panner,id:6056,x:33280,y:32754,varname:node_6056,prsc:2,spu:0,spv:1|UVIN-6068-UVOUT,DIST-6065-OUT;n:type:ShaderForge.SFN_Multiply,id:6065,x:33073,y:32845,varname:node_6065,prsc:2|A-6067-T,B-6073-OUT;n:type:ShaderForge.SFN_Time,id:6067,x:32874,y:32814,varname:node_6067,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:6068,x:33088,y:32669,varname:node_6068,prsc:2,uv:0;n:type:ShaderForge.SFN_ValueProperty,id:6073,x:32850,y:32986,ptovrint:False,ptlb:Speed,ptin:_Speed,varname:node_3606,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_VertexColor,id:6074,x:33471,y:33065,varname:node_6074,prsc:2;n:type:ShaderForge.SFN_Fresnel,id:6075,x:33354,y:33234,varname:node_6075,prsc:2;n:type:ShaderForge.SFN_RemapRange,id:6077,x:33525,y:33234,varname:node_6077,prsc:2,frmn:0.2,frmx:0.8,tomn:1,tomx:0|IN-6075-OUT;n:type:ShaderForge.SFN_Clamp01,id:6078,x:33695,y:33234,varname:node_6078,prsc:2|IN-6077-OUT;proporder:6036-6048-6038-6073;pass:END;sub:END;*/

Shader "_PlayHead_/sonar" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _Intensity ("Intensity", Range(0, 1)) = 1
        _MainTex ("MainTex", 2D) = "white" {}
        _Speed ("Speed", Float ) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Intensity;
            uniform float _Speed;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_6067 = _Time + _TimeEditor;
                float2 node_6056 = (i.uv0+(node_6067.g*_Speed)*float2(0,1));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_6056, _MainTex));
                float3 emissive = (_Color.rgb*_MainTex_var.rgb*_Intensity*i.vertexColor.a*saturate(((1.0-max(0,dot(normalDirection, viewDirection)))*-1.666667+1.333333)));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Mobile/Particles/Additive"
    CustomEditor "ShaderForgeMaterialInspector"
}
