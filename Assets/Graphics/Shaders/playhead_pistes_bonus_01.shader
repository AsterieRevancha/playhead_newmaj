// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:_PlayHead_/pistes/bonus_01,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:34412,y:32704,varname:node_1,prsc:2|emission-8-OUT,voffset-6225-OUT;n:type:ShaderForge.SFN_Color,id:2,x:33364,y:32760,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_2865,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Cubemap,id:3,x:32754,y:32579,ptovrint:False,ptlb:Cubemap,ptin:_Cubemap,varname:node_8878,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:bd6f1547ba2bfef45adb489d44ae43b0,pvfc:0|DIR-4-OUT;n:type:ShaderForge.SFN_ViewReflectionVector,id:4,x:32522,y:32579,varname:node_4,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:6,x:32492,y:32897,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_3501,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:7,x:33589,y:32901,varname:node_7,prsc:2|A-2-RGB,B-6-R;n:type:ShaderForge.SFN_Add,id:8,x:34166,y:32804,varname:node_8,prsc:2|A-80-OUT,B-336-OUT,C-224-OUT;n:type:ShaderForge.SFN_Multiply,id:21,x:33589,y:33106,varname:node_21,prsc:2|A-242-RGB,B-6-G,C-320-OUT;n:type:ShaderForge.SFN_Multiply,id:80,x:33191,y:32578,varname:node_80,prsc:2|A-3-RGB,B-6-B;n:type:ShaderForge.SFN_Multiply,id:224,x:33556,y:33363,varname:node_224,prsc:2|A-6-G,B-329-OUT,C-350-OUT;n:type:ShaderForge.SFN_Color,id:242,x:33364,y:32982,ptovrint:False,ptlb:CircuitsColor,ptin:_CircuitsColor,varname:node_4310,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:320,x:33207,y:33184,ptovrint:False,ptlb:Pulse,ptin:_Pulse,varname:node_9106,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.5,cur:1,max:1;n:type:ShaderForge.SFN_Step,id:329,x:32956,y:33358,varname:node_329,prsc:2|A-6-A,B-330-OUT;n:type:ShaderForge.SFN_Slider,id:330,x:32359,y:33376,ptovrint:False,ptlb:Activation,ptin:_Activation,varname:node_6602,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Max,id:336,x:33820,y:32903,varname:node_336,prsc:2|A-7-OUT,B-21-OUT;n:type:ShaderForge.SFN_Slider,id:350,x:33136,y:33515,ptovrint:False,ptlb:ActivationFade,ptin:_ActivationFade,varname:node_2894,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:0;n:type:ShaderForge.SFN_VertexColor,id:6198,x:32839,y:33911,varname:node_6198,prsc:2;n:type:ShaderForge.SFN_Multiply,id:6199,x:33369,y:33727,varname:node_6199,prsc:2|A-6200-OUT,B-6202-OUT,C-6198-B;n:type:ShaderForge.SFN_Vector3,id:6200,x:33049,y:33711,varname:node_6200,prsc:2,v1:2,v2:2,v3:0;n:type:ShaderForge.SFN_Add,id:6202,x:33065,y:33829,varname:node_6202,prsc:2|A-6203-OUT,B-6198-RGB;n:type:ShaderForge.SFN_Vector1,id:6203,x:32850,y:33829,varname:node_6203,prsc:2,v1:-0.5;n:type:ShaderForge.SFN_ValueProperty,id:6205,x:33331,y:33622,ptovrint:False,ptlb:ActivationOffset,ptin:_ActivationOffset,varname:node_1375,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:-0.2;n:type:ShaderForge.SFN_Multiply,id:6215,x:33562,y:33622,varname:node_6215,prsc:2|A-6205-OUT,B-330-OUT,C-6199-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6221,x:33331,y:33907,ptovrint:False,ptlb:StartOffset,ptin:_StartOffset,varname:node_3916,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:-0.2;n:type:ShaderForge.SFN_Multiply,id:6223,x:33562,y:33808,varname:node_6223,prsc:2|A-6199-OUT,B-6221-OUT;n:type:ShaderForge.SFN_Add,id:6225,x:33813,y:33841,varname:node_6225,prsc:2|A-6215-OUT,B-6223-OUT,C-6231-OUT;n:type:ShaderForge.SFN_Multiply,id:6231,x:33331,y:34038,varname:node_6231,prsc:2|A-6200-OUT,B-6202-OUT,C-6198-A,D-6232-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6232,x:33137,y:34141,ptovrint:False,ptlb:TubeShortening,ptin:_TubeShortening,varname:node_8023,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;proporder:2-242-6-3-320-330-350-6221-6205-6232;pass:END;sub:END;*/

Shader "_PlayHead_/pistes/bonus_01" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _CircuitsColor ("CircuitsColor", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "black" {}
        _Cubemap ("Cubemap", Cube) = "_Skybox" {}
        _Pulse ("Pulse", Range(0.5, 1)) = 1
        _Activation ("Activation", Range(0, 1)) = 0
        _ActivationFade ("ActivationFade", Range(1, 0)) = 1
        _StartOffset ("StartOffset", Float ) = -0.2
        _ActivationOffset ("ActivationOffset", Float ) = -0.2
        _TubeShortening ("TubeShortening", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            uniform samplerCUBE _Cubemap;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _CircuitsColor;
            uniform float _Pulse;
            uniform float _Activation;
            uniform float _ActivationFade;
            uniform float _ActivationOffset;
            uniform float _StartOffset;
            uniform float _TubeShortening;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float3 node_6200 = float3(2,2,0);
                float3 node_6202 = ((-0.5)+o.vertexColor.rgb);
                float3 node_6199 = (node_6200*node_6202*o.vertexColor.b);
                v.vertex.xyz += ((_ActivationOffset*_Activation*node_6199)+(node_6199*_StartOffset)+(node_6200*node_6202*o.vertexColor.a*_TubeShortening));
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = ((texCUBE(_Cubemap,viewReflectDirection).rgb*_MainTex_var.b)+max((_Color.rgb*_MainTex_var.r),(_CircuitsColor.rgb*_MainTex_var.g*_Pulse))+(_MainTex_var.g*step(_MainTex_var.a,_Activation)*_ActivationFade));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _Activation;
            uniform float _ActivationOffset;
            uniform float _StartOffset;
            uniform float _TubeShortening;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.vertexColor = v.vertexColor;
                float3 node_6200 = float3(2,2,0);
                float3 node_6202 = ((-0.5)+o.vertexColor.rgb);
                float3 node_6199 = (node_6200*node_6202*o.vertexColor.b);
                v.vertex.xyz += ((_ActivationOffset*_Activation*node_6199)+(node_6199*_StartOffset)+(node_6200*node_6202*o.vertexColor.a*_TubeShortening));
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/pistes/bonus_01"
    CustomEditor "ShaderForgeMaterialInspector"
}
