// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Specular,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:33728,y:32719,varname:node_1,prsc:2|diff-6298-OUT,spec-15-OUT,gloss-14-OUT,emission-13-RGB,voffset-6711-OUT;n:type:ShaderForge.SFN_Color,id:2,x:33137,y:32682,ptovrint:False,ptlb:MainColor,ptin:_MainColor,varname:node_6985,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Tex2d,id:3,x:33137,y:32873,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_3907,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-7-OUT;n:type:ShaderForge.SFN_Multiply,id:4,x:33325,y:32781,varname:node_4,prsc:2|A-2-RGB,B-3-R;n:type:ShaderForge.SFN_Add,id:7,x:32946,y:32873,varname:node_7,prsc:2|A-8-UVOUT,B-9-OUT;n:type:ShaderForge.SFN_TexCoord,id:8,x:32710,y:32873,varname:node_8,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:9,x:32866,y:33053,varname:node_9,prsc:2|A-11-OUT,B-10-OUT,C-12-T;n:type:ShaderForge.SFN_ValueProperty,id:10,x:32684,y:33178,ptovrint:False,ptlb:Speed,ptin:_Speed,varname:node_4672,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Vector2,id:11,x:32684,y:33053,varname:node_11,prsc:2,v1:0,v2:1;n:type:ShaderForge.SFN_Time,id:12,x:32684,y:33255,varname:node_12,prsc:2;n:type:ShaderForge.SFN_Color,id:13,x:33401,y:33163,ptovrint:False,ptlb:IlluminationColor,ptin:_IlluminationColor,varname:node_2786,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:14,x:33289,y:32460,ptovrint:False,ptlb:Glossiness,ptin:_Glossiness,varname:node_5212,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:15,x:33401,y:32984,varname:node_15,prsc:2|A-3-G,B-16-G;n:type:ShaderForge.SFN_VertexColor,id:16,x:33137,y:33056,varname:node_16,prsc:2;n:type:ShaderForge.SFN_Lerp,id:6298,x:33520,y:32648,varname:node_6298,prsc:2|A-6299-RGB,B-4-OUT,T-16-R;n:type:ShaderForge.SFN_Color,id:6299,x:33288,y:32563,ptovrint:False,ptlb:SideColor,ptin:_SideColor,varname:node_1320,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Vector4Property,id:6641,x:33401,y:33335,ptovrint:False,ptlb:VertexOffset,ptin:_VertexOffset,varname:node_820,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_Multiply,id:6711,x:33620,y:33335,varname:node_6711,prsc:2|A-6641-XYZ,B-6712-OUT;n:type:ShaderForge.SFN_Slider,id:6712,x:33282,y:33512,ptovrint:False,ptlb:Offset,ptin:_Offset,varname:node_8737,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;proporder:2-6299-13-14-3-10-6641-6712;pass:END;sub:END;*/

Shader "_PlayHead_/paysage/river_01" {
    Properties {
        _MainColor ("MainColor", Color) = (0.5,0.5,0.5,1)
        _SideColor ("SideColor", Color) = (0.5,0.5,0.5,1)
        _IlluminationColor ("IlluminationColor", Color) = (0.5,0.5,0.5,1)
        _Glossiness ("Glossiness", Range(0, 1)) = 0
        _MainTex ("MainTex", 2D) = "white" {}
        _Speed ("Speed", Float ) = 1
        _VertexOffset ("VertexOffset", Vector) = (0,0,0,0)
        _Offset ("Offset", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float4 _MainColor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Speed;
            uniform float4 _IlluminationColor;
            uniform float _Glossiness;
            uniform float4 _SideColor;
            uniform float4 _VertexOffset;
            uniform float _Offset;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (_VertexOffset.rgb*_Offset);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Glossiness;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float4 node_12 = _Time + _TimeEditor;
                float2 node_7 = (i.uv0+(float2(0,1)*_Speed*node_12.g));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_7, _MainTex));
                float node_15 = (_MainTex_var.g*i.vertexColor.g);
                float3 specularColor = float3(node_15,node_15,node_15);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = lerp(_SideColor.rgb,(_MainColor.rgb*_MainTex_var.r),i.vertexColor.r);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = _IlluminationColor.rgb;
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _VertexOffset;
            uniform float _Offset;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                v.vertex.xyz += (_VertexOffset.rgb*_Offset);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Specular"
    CustomEditor "ShaderForgeMaterialInspector"
}
