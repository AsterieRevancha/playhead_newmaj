// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Particles/Additive,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:20,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:42,x:34431,y:32493,varname:node_42,prsc:2|emission-3953-OUT;n:type:ShaderForge.SFN_Tex2d,id:44,x:33551,y:32477,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_2360,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:274b4c17b530da1479fcfa0130367b84,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:60,x:33551,y:32674,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_197,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_TexCoord,id:120,x:33188,y:33430,varname:node_120,prsc:2,uv:1;n:type:ShaderForge.SFN_Slider,id:123,x:33292,y:32896,ptovrint:False,ptlb:AllLights,ptin:_AllLights,varname:node_9395,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:-1;n:type:ShaderForge.SFN_Slider,id:174,x:33007,y:33749,ptovrint:False,ptlb:Light2,ptin:_Light2,varname:node_400,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:0,max:0;n:type:ShaderForge.SFN_Slider,id:177,x:33007,y:33943,ptovrint:False,ptlb:Light3,ptin:_Light3,varname:node_1333,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:0;n:type:ShaderForge.SFN_RemapRange,id:205,x:33503,y:34601,varname:node_205,prsc:2,frmn:0.67,frmx:0.81,tomn:-1,tomx:1|IN-120-V;n:type:ShaderForge.SFN_Abs,id:206,x:33679,y:34601,varname:node_206,prsc:2|IN-205-OUT;n:type:ShaderForge.SFN_RemapRange,id:208,x:33503,y:33603,varname:node_208,prsc:2,frmn:0.06,frmx:0.185,tomn:-1,tomx:1|IN-120-V;n:type:ShaderForge.SFN_Abs,id:210,x:33679,y:33603,varname:node_210,prsc:2|IN-208-OUT;n:type:ShaderForge.SFN_Floor,id:211,x:33852,y:33603,varname:node_211,prsc:2|IN-210-OUT;n:type:ShaderForge.SFN_Floor,id:213,x:33852,y:34601,varname:node_213,prsc:2|IN-206-OUT;n:type:ShaderForge.SFN_Floor,id:215,x:33852,y:33800,varname:node_215,prsc:2|IN-217-OUT;n:type:ShaderForge.SFN_Abs,id:217,x:33679,y:33800,varname:node_217,prsc:2|IN-219-OUT;n:type:ShaderForge.SFN_RemapRange,id:219,x:33503,y:33800,varname:node_219,prsc:2,frmn:0.185,frmx:0.31,tomn:-1,tomx:1|IN-120-V;n:type:ShaderForge.SFN_RemapRange,id:221,x:33503,y:33995,varname:node_221,prsc:2,frmn:0.31,frmx:0.44,tomn:-1,tomx:1|IN-120-V;n:type:ShaderForge.SFN_Abs,id:223,x:33679,y:33995,varname:node_223,prsc:2|IN-221-OUT;n:type:ShaderForge.SFN_Floor,id:225,x:33852,y:33995,varname:node_225,prsc:2|IN-223-OUT;n:type:ShaderForge.SFN_RemapRange,id:227,x:33503,y:34187,varname:node_227,prsc:2,frmn:0.44,frmx:0.56,tomn:-1,tomx:1|IN-120-V;n:type:ShaderForge.SFN_Abs,id:229,x:33679,y:34187,varname:node_229,prsc:2|IN-227-OUT;n:type:ShaderForge.SFN_Floor,id:231,x:33852,y:34187,varname:node_231,prsc:2|IN-229-OUT;n:type:ShaderForge.SFN_RemapRange,id:233,x:33503,y:34397,varname:node_233,prsc:2,frmn:0.56,frmx:0.67,tomn:-1,tomx:1|IN-120-V;n:type:ShaderForge.SFN_Abs,id:235,x:33679,y:34397,varname:node_235,prsc:2|IN-233-OUT;n:type:ShaderForge.SFN_Floor,id:237,x:33852,y:34397,varname:node_237,prsc:2|IN-235-OUT;n:type:ShaderForge.SFN_Slider,id:253,x:33007,y:34544,ptovrint:False,ptlb:Light6,ptin:_Light6,varname:node_8797,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:0;n:type:ShaderForge.SFN_Slider,id:270,x:33007,y:34140,ptovrint:False,ptlb:Light4,ptin:_Light4,varname:node_8779,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:0;n:type:ShaderForge.SFN_Slider,id:272,x:33007,y:34345,ptovrint:False,ptlb:Light5,ptin:_Light5,varname:node_3155,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:0;n:type:ShaderForge.SFN_Slider,id:274,x:33007,y:34745,ptovrint:False,ptlb:Light1,ptin:_Light1,varname:node_2826,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:0;n:type:ShaderForge.SFN_Add,id:279,x:34032,y:34325,varname:node_279,prsc:2|A-231-OUT,B-272-OUT;n:type:ShaderForge.SFN_Add,id:281,x:34032,y:34528,varname:node_281,prsc:2|A-237-OUT,B-253-OUT;n:type:ShaderForge.SFN_Add,id:283,x:34032,y:34727,varname:node_283,prsc:2|A-213-OUT,B-274-OUT;n:type:ShaderForge.SFN_Add,id:285,x:34032,y:34118,varname:node_285,prsc:2|A-225-OUT,B-270-OUT;n:type:ShaderForge.SFN_Add,id:287,x:34032,y:33927,varname:node_287,prsc:2|A-215-OUT,B-177-OUT;n:type:ShaderForge.SFN_Add,id:289,x:34032,y:33721,varname:node_289,prsc:2|A-211-OUT,B-174-OUT;n:type:ShaderForge.SFN_Clamp01,id:291,x:33880,y:32920,varname:node_291,prsc:2|IN-296-OUT;n:type:ShaderForge.SFN_OneMinus,id:292,x:34049,y:32920,varname:node_292,prsc:2|IN-291-OUT;n:type:ShaderForge.SFN_Min,id:293,x:34346,y:33929,varname:node_293,prsc:2|A-289-OUT,B-287-OUT,C-285-OUT;n:type:ShaderForge.SFN_Min,id:295,x:34547,y:34031,varname:node_295,prsc:2|A-293-OUT,B-279-OUT,C-281-OUT,D-283-OUT;n:type:ShaderForge.SFN_Add,id:296,x:33707,y:32920,varname:node_296,prsc:2|A-123-OUT,B-295-OUT;n:type:ShaderForge.SFN_Multiply,id:297,x:34050,y:32594,varname:node_297,prsc:2|A-44-A,B-60-RGB,C-60-A,D-292-OUT,E-44-RGB;n:type:ShaderForge.SFN_Add,id:3953,x:34228,y:32508,varname:node_3953,prsc:2|A-3955-OUT,B-297-OUT;n:type:ShaderForge.SFN_Vector1,id:3955,x:34040,y:32481,varname:node_3955,prsc:2,v1:0.008;proporder:60-44-274-174-177-270-272-253-123;pass:END;sub:END;*/

Shader "_PlayHead_/armature_frame_2" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Light1 ("Light1", Range(1, 0)) = 1
        _Light2 ("Light2", Range(1, 0)) = 0
        _Light3 ("Light3", Range(1, 0)) = 1
        _Light4 ("Light4", Range(1, 0)) = 1
        _Light5 ("Light5", Range(1, 0)) = 1
        _Light6 ("Light6", Range(1, 0)) = 1
        _AllLights ("AllLights", Range(0, -1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color;
            uniform float _AllLights;
            uniform float _Light2;
            uniform float _Light3;
            uniform float _Light6;
            uniform float _Light4;
            uniform float _Light5;
            uniform float _Light1;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = (0.008+(_MainTex_var.a*_Color.rgb*_Color.a*(1.0 - saturate((_AllLights+min(min(min(min(min((floor(abs((i.uv1.g*16.0+-1.96)))+_Light2),(floor(abs((i.uv1.g*16.0+-3.96)))+_Light3)),(floor(abs((i.uv1.g*15.38462+-5.769231)))+_Light4)),(floor(abs((i.uv1.g*16.66667+-8.333333)))+_Light5)),(floor(abs((i.uv1.g*18.18182+-11.18182)))+_Light6)),(floor(abs((i.uv1.g*14.28572+-10.57143)))+_Light1)))))*_MainTex_var.rgb));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Particles/Additive"
    CustomEditor "ShaderForgeMaterialInspector"
}
