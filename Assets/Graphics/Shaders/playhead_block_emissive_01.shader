// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:False,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:292,x:33425,y:32690,varname:node_292,prsc:2|emission-5819-OUT;n:type:ShaderForge.SFN_ViewReflectionVector,id:294,x:32048,y:32824,varname:node_294,prsc:2;n:type:ShaderForge.SFN_Cubemap,id:293,x:32260,y:32824,ptovrint:False,ptlb:CubeMap,ptin:_CubeMap,varname:node_2351,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:e2fdbd2b483ee894a8cfa70539ac61d4,pvfc:0|DIR-294-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:298,x:32492,y:32778,varname:node_298,prsc:2,min:0.1,max:1|IN-293-RGB;n:type:ShaderForge.SFN_Multiply,id:5805,x:32735,y:32706,varname:node_5805,prsc:2|A-5806-RGB,B-298-OUT;n:type:ShaderForge.SFN_Tex2d,id:5806,x:32479,y:32592,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_7102,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:5816,x:32735,y:32548,varname:node_5816,prsc:2|A-5817-OUT,B-5806-RGB;n:type:ShaderForge.SFN_Vector1,id:5817,x:32479,y:32482,varname:node_5817,prsc:2,v1:-0.5;n:type:ShaderForge.SFN_Clamp01,id:5818,x:32914,y:32548,varname:node_5818,prsc:2|IN-5816-OUT;n:type:ShaderForge.SFN_Add,id:5819,x:33152,y:32702,varname:node_5819,prsc:2|A-5828-OUT,B-5805-OUT;n:type:ShaderForge.SFN_Multiply,id:5828,x:33109,y:32491,varname:node_5828,prsc:2|A-5830-OUT,B-5818-OUT,C-5839-RGB;n:type:ShaderForge.SFN_Slider,id:5830,x:32757,y:32461,ptovrint:False,ptlb:Emission,ptin:_Emission,varname:node_1013,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:3;n:type:ShaderForge.SFN_Color,id:5839,x:32914,y:32288,ptovrint:False,ptlb:EmissionColor,ptin:_EmissionColor,varname:node_6630,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;proporder:293-5806-5839-5830;pass:END;sub:END;*/

Shader "_PlayHead_/blocks/emissive" {
    Properties {
        _CubeMap ("CubeMap", Cube) = "_Skybox" {}
        _MainTex ("MainTex", 2D) = "white" {}
        _EmissionColor ("EmissionColor", Color) = (0.5,0.5,0.5,1)
        _Emission ("Emission", Range(0, 3)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform samplerCUBE _CubeMap;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Emission;
            uniform float4 _EmissionColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = ((_Emission*saturate(((-0.5)+_MainTex_var.rgb))*_EmissionColor.rgb)+(_MainTex_var.rgb*clamp(texCUBE(_CubeMap,viewReflectDirection).rgb,0.1,1)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
