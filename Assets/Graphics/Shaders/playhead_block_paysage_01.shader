// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Diffuse,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.001,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1082,x:34098,y:32771,varname:node_1082,prsc:2|emission-3267-OUT,voffset-2840-OUT;n:type:ShaderForge.SFN_Color,id:1083,x:32559,y:32542,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_3647,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:1084,x:32559,y:32700,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_6095,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1085,x:33539,y:32703,varname:node_1085,prsc:2|A-2874-RGB,B-1944-OUT,C-2240-OUT;n:type:ShaderForge.SFN_TexCoord,id:1548,x:31162,y:32922,varname:node_1548,prsc:2,uv:1;n:type:ShaderForge.SFN_Tex2d,id:1549,x:32025,y:33089,ptovrint:False,ptlb:FogRamp,ptin:_FogRamp,varname:node_2389,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False|UVIN-1557-OUT;n:type:ShaderForge.SFN_Append,id:1557,x:31838,y:33089,varname:node_1557,prsc:2|A-1558-OUT,B-1609-OUT;n:type:ShaderForge.SFN_Slider,id:1558,x:31464,y:33040,ptovrint:False,ptlb:FogValue,ptin:_FogValue,varname:node_4762,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_ValueProperty,id:1567,x:31001,y:33245,ptovrint:False,ptlb:FogHeight,ptin:_FogHeight,varname:node_4997,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:1568,x:31232,y:33150,varname:node_1568,prsc:2|A-1596-Y,B-1567-OUT;n:type:ShaderForge.SFN_Multiply,id:1575,x:32453,y:33051,varname:node_1575,prsc:2|A-1549-RGB,B-1576-RGB,C-1576-A,D-2502-OUT;n:type:ShaderForge.SFN_Color,id:1576,x:32025,y:33287,ptovrint:False,ptlb:FogColor,ptin:_FogColor,varname:node_6936,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:1583,x:30844,y:33351,ptovrint:False,ptlb:HeightAdjust,ptin:_HeightAdjust,varname:node_8618,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:0,max:0;n:type:ShaderForge.SFN_Multiply,id:1584,x:31445,y:33241,varname:node_1584,prsc:2|A-1568-OUT,B-1583-OUT,C-2466-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:1596,x:31001,y:33072,varname:node_1596,prsc:2;n:type:ShaderForge.SFN_Clamp01,id:1609,x:31644,y:33166,varname:node_1609,prsc:2|IN-1584-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:1682,x:31910,y:33462,varname:node_1682,prsc:2;n:type:ShaderForge.SFN_Color,id:1771,x:32569,y:31981,ptovrint:False,ptlb:SpecColor,ptin:_SpecColor,varname:node_1437,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Fresnel,id:1795,x:32347,y:32294,varname:node_1795,prsc:2|EXP-1803-OUT;n:type:ShaderForge.SFN_Slider,id:1803,x:31969,y:32270,ptovrint:False,ptlb:FresnelPower,ptin:_FresnelPower,varname:node_8576,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:2,max:20;n:type:ShaderForge.SFN_Slider,id:1853,x:32209,y:32431,ptovrint:False,ptlb:FresnelIntensity,ptin:_FresnelIntensity,varname:node_2636,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2,max:1;n:type:ShaderForge.SFN_Multiply,id:1869,x:33002,y:32433,varname:node_1869,prsc:2|A-1795-OUT,B-1853-OUT,C-3065-OUT,D-3135-OUT;n:type:ShaderForge.SFN_NormalVector,id:1884,x:32855,y:33433,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:1912,x:33024,y:33484,varname:node_1912,prsc:2,cc1:2,cc2:-1,cc3:-1,cc4:-1|IN-1884-OUT;n:type:ShaderForge.SFN_Multiply,id:1925,x:33367,y:33610,varname:node_1925,prsc:2|A-1942-OUT,B-1926-OUT,C-2904-OUT,D-2874-RGB,E-3065-OUT;n:type:ShaderForge.SFN_Vector3,id:1926,x:33010,y:33674,varname:node_1926,prsc:2,v1:0.5294118,v2:0.5294118,v3:0.5294118;n:type:ShaderForge.SFN_RemapRange,id:1942,x:33188,y:33484,varname:node_1942,prsc:2,frmn:0.05,frmx:0.2,tomn:0,tomx:1|IN-1912-OUT;n:type:ShaderForge.SFN_LightVector,id:1943,x:32855,y:33255,varname:node_1943,prsc:2;n:type:ShaderForge.SFN_Dot,id:1944,x:33093,y:33283,varname:node_1944,prsc:2,dt:1|A-1943-OUT,B-1884-OUT;n:type:ShaderForge.SFN_Clamp01,id:2159,x:33540,y:33610,varname:node_2159,prsc:2|IN-1925-OUT;n:type:ShaderForge.SFN_OneMinus,id:2240,x:32439,y:33229,varname:node_2240,prsc:2|IN-2502-OUT;n:type:ShaderForge.SFN_Add,id:2241,x:33807,y:32858,varname:node_2241,prsc:2|A-1085-OUT,B-1575-OUT,C-2159-OUT,D-3089-OUT,E-1869-OUT;n:type:ShaderForge.SFN_Vector1,id:2466,x:31232,y:33388,varname:node_2466,prsc:2,v1:0.02;n:type:ShaderForge.SFN_Length,id:2498,x:32104,y:33634,varname:node_2498,prsc:2|IN-1682-XYZ;n:type:ShaderForge.SFN_RemapRange,id:2500,x:32383,y:33634,varname:node_2500,prsc:2,frmn:50,frmx:750,tomn:0.2,tomx:1|IN-2498-OUT;n:type:ShaderForge.SFN_Clamp01,id:2502,x:32543,y:33634,varname:node_2502,prsc:2|IN-2500-OUT;n:type:ShaderForge.SFN_TexCoord,id:2837,x:33787,y:33723,varname:node_2837,prsc:2,uv:1;n:type:ShaderForge.SFN_Slider,id:2838,x:33630,y:33893,ptovrint:False,ptlb:VertexOffset,ptin:_VertexOffset,varname:node_7826,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:200;n:type:ShaderForge.SFN_Vector3,id:2839,x:33776,y:33975,varname:node_2839,prsc:2,v1:0,v2:-1,v3:0;n:type:ShaderForge.SFN_Multiply,id:2840,x:34121,y:33862,varname:node_2840,prsc:2|A-2853-OUT,B-2838-OUT,C-2839-OUT;n:type:ShaderForge.SFN_Clamp01,id:2853,x:33954,y:33751,varname:node_2853,prsc:2|IN-2837-V;n:type:ShaderForge.SFN_LightColor,id:2874,x:32981,y:33101,varname:node_2874,prsc:2;n:type:ShaderForge.SFN_Relay,id:2903,x:32596,y:33003,varname:node_2903,prsc:2|IN-1558-OUT;n:type:ShaderForge.SFN_OneMinus,id:2904,x:32924,y:33778,varname:node_2904,prsc:2|IN-2903-OUT;n:type:ShaderForge.SFN_VertexColor,id:2934,x:31928,y:32782,varname:node_2934,prsc:2;n:type:ShaderForge.SFN_Vector1,id:2938,x:32117,y:32958,varname:node_2938,prsc:2,v1:0.2;n:type:ShaderForge.SFN_OneMinus,id:2955,x:32168,y:32753,varname:node_2955,prsc:2|IN-2934-G;n:type:ShaderForge.SFN_Add,id:3063,x:32373,y:32852,varname:node_3063,prsc:2|A-2955-OUT,B-2938-OUT;n:type:ShaderForge.SFN_Relay,id:3065,x:32744,y:32875,varname:node_3065,prsc:2|IN-3063-OUT;n:type:ShaderForge.SFN_ComponentMask,id:3088,x:33304,y:33309,varname:node_3088,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-1884-OUT;n:type:ShaderForge.SFN_Multiply,id:3089,x:33531,y:33093,varname:node_3089,prsc:2|A-2874-RGB,B-3088-OUT,C-3095-OUT;n:type:ShaderForge.SFN_Vector1,id:3095,x:33380,y:33201,varname:node_3095,prsc:2,v1:0.2;n:type:ShaderForge.SFN_VertexColor,id:3118,x:33152,y:31922,varname:node_3118,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:3134,x:32554,y:32152,ptovrint:False,ptlb:Spec,ptin:_Spec,varname:node_1122,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:3135,x:32821,y:32068,varname:node_3135,prsc:2|A-1771-RGB,B-3134-RGB;n:type:ShaderForge.SFN_Multiply,id:3144,x:32969,y:32627,varname:node_3144,prsc:2|A-1083-RGB,B-1084-RGB,C-3224-OUT;n:type:ShaderForge.SFN_Tex2d,id:3221,x:33136,y:32106,ptovrint:False,ptlb:SecondaryTex,ptin:_SecondaryTex,varname:node_8729,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:3222,x:33332,y:32287,varname:node_3222,prsc:2|A-3221-RGB,B-3223-RGB,C-3118-G;n:type:ShaderForge.SFN_Color,id:3223,x:33136,y:32331,ptovrint:False,ptlb:SecondaryColor,ptin:_SecondaryColor,varname:node_666,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_OneMinus,id:3224,x:33428,y:32059,varname:node_3224,prsc:2|IN-3118-G;n:type:ShaderForge.SFN_Add,id:3229,x:33471,y:32478,varname:node_3229,prsc:2|A-3222-OUT,B-3144-OUT;n:type:ShaderForge.SFN_Vector1,id:3252,x:33289,y:32689,varname:node_3252,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:3267,x:33914,y:32691,varname:node_3267,prsc:2|A-3229-OUT,B-2241-OUT;proporder:1083-3223-1771-1084-3221-3134-1549-1558-1567-1583-1576-1803-1853-2838;pass:END;sub:END;*/

Shader "_PlayHead_/block_paysage_01" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _SecondaryColor ("SecondaryColor", Color) = (0.5,0.5,0.5,1)
        _SpecColor ("SpecColor", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _SecondaryTex ("SecondaryTex", 2D) = "white" {}
        _Spec ("Spec", 2D) = "white" {}
        _FogRamp ("FogRamp", 2D) = "black" {}
        _FogValue ("FogValue", Range(0, 1)) = 1
        _FogHeight ("FogHeight", Float ) = 0
        _HeightAdjust ("HeightAdjust", Range(1, 0)) = 0
        _FogColor ("FogColor", Color) = (0.5,0.5,0.5,1)
        _FresnelPower ("FresnelPower", Range(0, 20)) = 2
        _FresnelIntensity ("FresnelIntensity", Range(0, 1)) = 0.2
        _VertexOffset ("VertexOffset", Range(0, 200)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _FogRamp; uniform float4 _FogRamp_ST;
            uniform float _FogValue;
            uniform float _FogHeight;
            uniform float4 _FogColor;
            uniform float _HeightAdjust;
            uniform float4 _SpecColor;
            uniform float _FresnelPower;
            uniform float _FresnelIntensity;
            uniform float _VertexOffset;
            uniform sampler2D _Spec; uniform float4 _Spec_ST;
            uniform sampler2D _SecondaryTex; uniform float4 _SecondaryTex_ST;
            uniform float4 _SecondaryColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(4,5)
                UNITY_FOG_COORDS(6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (saturate(o.uv1.g)*_VertexOffset*float3(0,-1,0));
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
////// Emissive:
                float4 _SecondaryTex_var = tex2D(_SecondaryTex,TRANSFORM_TEX(i.uv0, _SecondaryTex));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_2502 = saturate((length(i.posWorld.rgb)*0.001142857+0.1428571));
                float2 node_1557 = float2(_FogValue,saturate(((i.posWorld.g+_FogHeight)*_HeightAdjust*0.02)));
                float4 _FogRamp_var = tex2D(_FogRamp,TRANSFORM_TEX(node_1557, _FogRamp));
                float node_3065 = ((1.0 - i.vertexColor.g)+0.2);
                float4 _Spec_var = tex2D(_Spec,TRANSFORM_TEX(i.uv0, _Spec));
                float3 emissive = (((_SecondaryTex_var.rgb*_SecondaryColor.rgb*i.vertexColor.g)+(_Color.rgb*_MainTex_var.rgb*(1.0 - i.vertexColor.g)))*((_LightColor0.rgb*max(0,dot(lightDirection,i.normalDir))*(1.0 - node_2502))+(_FogRamp_var.rgb*_FogColor.rgb*_FogColor.a*node_2502)+saturate(((i.normalDir.b*6.666667+-0.3333333)*float3(0.5294118,0.5294118,0.5294118)*(1.0 - _FogValue)*_LightColor0.rgb*node_3065))+(_LightColor0.rgb*i.normalDir.g*0.2)+(pow(1.0-max(0,dot(normalDirection, viewDirection)),_FresnelPower)*_FresnelIntensity*node_3065*(_SpecColor.rgb*_Spec_var.rgb))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float _VertexOffset;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv1 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                v.vertex.xyz += (saturate(o.uv1.g)*_VertexOffset*float3(0,-1,0));
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
