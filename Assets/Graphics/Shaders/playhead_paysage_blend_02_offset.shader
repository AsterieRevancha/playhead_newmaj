// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:_PlayHead_/paysage/blend_02,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:False,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.05882353,fgcg:0.05882353,fgcb:0.08235294,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:5834,x:33723,y:32761,varname:node_5834,prsc:2|diff-5903-OUT,emission-6469-OUT,voffset-6622-OUT;n:type:ShaderForge.SFN_VertexColor,id:5835,x:32972,y:32853,varname:node_5835,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:5836,x:32699,y:32436,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_8204,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:5842,x:32993,y:32564,varname:node_5842,prsc:2|A-5836-RGB,B-5850-RGB;n:type:ShaderForge.SFN_Color,id:5850,x:32699,y:32626,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_3112,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:5856,x:32682,y:32817,ptovrint:False,ptlb:BlendColor,ptin:_BlendColor,varname:node_9549,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Lerp,id:5903,x:33224,y:32724,varname:node_5903,prsc:2|A-5842-OUT,B-5856-RGB,T-5835-G;n:type:ShaderForge.SFN_Color,id:6417,x:32972,y:33032,ptovrint:False,ptlb:SkyColor,ptin:_SkyColor,varname:node_9218,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_NormalVector,id:6463,x:32284,y:33111,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:6464,x:32474,y:33111,varname:node_6464,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-6463-OUT;n:type:ShaderForge.SFN_Multiply,id:6465,x:32707,y:33166,varname:node_6465,prsc:2|A-6464-OUT,B-6466-OUT;n:type:ShaderForge.SFN_Vector1,id:6466,x:32474,y:33290,varname:node_6466,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Add,id:6467,x:32915,y:33266,varname:node_6467,prsc:2|A-6465-OUT,B-6466-OUT,C-6417-A;n:type:ShaderForge.SFN_Multiply,id:6469,x:33435,y:33009,varname:node_6469,prsc:2|A-5903-OUT,B-6417-RGB,C-6477-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:6477,x:33119,y:33266,varname:node_6477,prsc:2,min:0.3,max:1|IN-6467-OUT;n:type:ShaderForge.SFN_Vector4Property,id:6617,x:33341,y:33207,ptovrint:False,ptlb:VertexOffset,ptin:_VertexOffset,varname:node_8768,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_Multiply,id:6622,x:33664,y:33308,varname:node_6622,prsc:2|A-6617-XYZ,B-6625-OUT;n:type:ShaderForge.SFN_Slider,id:6625,x:33277,y:33412,ptovrint:False,ptlb:Offset,ptin:_Offset,varname:node_4096,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;proporder:5850-5856-6417-5836-6617-6625;pass:END;sub:END;*/

Shader "_PlayHead_/paysage/blend_02_offset" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _BlendColor ("BlendColor", Color) = (0.5,0.5,0.5,1)
        _SkyColor ("SkyColor", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _VertexOffset ("VertexOffset", Vector) = (0,0,0,0)
        _Offset ("Offset", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color;
            uniform float4 _BlendColor;
            uniform float4 _SkyColor;
            uniform float4 _VertexOffset;
            uniform float _Offset;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (_VertexOffset.rgb*_Offset);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 node_5903 = lerp((_MainTex_var.rgb*_Color.rgb),_BlendColor.rgb,i.vertexColor.g);
                float3 diffuseColor = node_5903;
                float3 diffuse = directDiffuse * diffuseColor;
////// Emissive:
                float node_6466 = 0.5;
                float3 emissive = (node_5903*_SkyColor.rgb*clamp(((i.normalDir.g*node_6466)+node_6466+_SkyColor.a),0.3,1));
/// Final Color:
                float3 finalColor = diffuse + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color;
            uniform float4 _BlendColor;
            uniform float4 _SkyColor;
            uniform float4 _VertexOffset;
            uniform float _Offset;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (_VertexOffset.rgb*_Offset);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 node_5903 = lerp((_MainTex_var.rgb*_Color.rgb),_BlendColor.rgb,i.vertexColor.g);
                float3 diffuseColor = node_5903;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _VertexOffset;
            uniform float _Offset;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                v.vertex.xyz += (_VertexOffset.rgb*_Offset);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/paysage/blend_02"
    CustomEditor "ShaderForgeMaterialInspector"
}
