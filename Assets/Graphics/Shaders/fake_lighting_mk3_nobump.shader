// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:_PlayHead_/fake_lighting_mk1_nobump,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:False,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.003,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:56,x:34036,y:32814,varname:node_56,prsc:2|emission-308-OUT;n:type:ShaderForge.SFN_NormalVector,id:57,x:29437,y:32389,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector4Property,id:58,x:29454,y:32746,ptovrint:False,ptlb:LightDirection1,ptin:_LightDirection1,varname:node_1907,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:-0.5,v2:0.866,v3:0,v4:0;n:type:ShaderForge.SFN_Distance,id:60,x:30033,y:32186,varname:node_60,prsc:2|A-57-OUT,B-58-XYZ;n:type:ShaderForge.SFN_Multiply,id:61,x:30333,y:32154,varname:node_61,prsc:2|A-62-OUT,B-60-OUT;n:type:ShaderForge.SFN_Vector1,id:62,x:30134,y:32097,varname:node_62,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:81,x:32161,y:32931,varname:node_81,prsc:2|A-287-OUT,B-177-RGB,C-856-OUT;n:type:ShaderForge.SFN_Vector4Property,id:100,x:29454,y:32948,ptovrint:False,ptlb:LightDirection2,ptin:_LightDirection2,varname:node_8383,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5,v2:0.866,v3:0,v4:0;n:type:ShaderForge.SFN_Vector4Property,id:102,x:29454,y:33159,ptovrint:False,ptlb:LightDirection3,ptin:_LightDirection3,varname:node_9844,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_OneMinus,id:155,x:30567,y:32260,varname:node_155,prsc:2|IN-61-OUT;n:type:ShaderForge.SFN_Color,id:177,x:31930,y:33201,ptovrint:False,ptlb:LightColor,ptin:_LightColor,varname:node_1819,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:186,x:30536,y:32763,ptovrint:False,ptlb:Light4,ptin:_Light4,varname:node_4944,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:198,x:30464,y:32856,ptovrint:False,ptlb:Light5,ptin:_Light5,varname:node_630,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Distance,id:205,x:30033,y:32352,varname:node_205,prsc:2|A-57-OUT,B-100-XYZ;n:type:ShaderForge.SFN_Multiply,id:207,x:30333,y:32347,varname:node_207,prsc:2|A-62-OUT,B-205-OUT;n:type:ShaderForge.SFN_OneMinus,id:211,x:30543,y:32440,varname:node_211,prsc:2|IN-207-OUT;n:type:ShaderForge.SFN_Add,id:214,x:31405,y:32518,varname:node_214,prsc:2|A-244-OUT,B-246-OUT,C-260-OUT;n:type:ShaderForge.SFN_Slider,id:239,x:30536,y:32944,ptovrint:False,ptlb:Light6,ptin:_Light6,varname:node_2363,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Distance,id:243,x:30033,y:32528,varname:node_243,prsc:2|A-57-OUT,B-102-XYZ;n:type:ShaderForge.SFN_Multiply,id:244,x:31011,y:32088,varname:node_244,prsc:2|A-61-OUT,B-186-OUT;n:type:ShaderForge.SFN_Multiply,id:246,x:31010,y:32357,varname:node_246,prsc:2|A-207-OUT,B-198-OUT;n:type:ShaderForge.SFN_Multiply,id:256,x:30333,y:32514,varname:node_256,prsc:2|A-62-OUT,B-243-OUT;n:type:ShaderForge.SFN_OneMinus,id:258,x:30543,y:32607,varname:node_258,prsc:2|IN-256-OUT;n:type:ShaderForge.SFN_Multiply,id:260,x:31010,y:32614,varname:node_260,prsc:2|A-256-OUT,B-239-OUT;n:type:ShaderForge.SFN_Multiply,id:263,x:31114,y:32224,varname:node_263,prsc:2|A-155-OUT,B-281-OUT;n:type:ShaderForge.SFN_Multiply,id:265,x:31111,y:32482,varname:node_265,prsc:2|A-211-OUT,B-283-OUT;n:type:ShaderForge.SFN_Multiply,id:267,x:31094,y:32762,varname:node_267,prsc:2|A-258-OUT,B-285-OUT;n:type:ShaderForge.SFN_Slider,id:281,x:30464,y:33041,ptovrint:False,ptlb:Light1,ptin:_Light1,varname:node_2461,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:283,x:30546,y:33127,ptovrint:False,ptlb:Light2,ptin:_Light2,varname:node_2913,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:285,x:30435,y:33226,ptovrint:False,ptlb:Light3,ptin:_Light3,varname:node_1252,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:287,x:31405,y:32690,varname:node_287,prsc:2|A-214-OUT,B-263-OUT,C-265-OUT,D-267-OUT;n:type:ShaderForge.SFN_Color,id:292,x:32871,y:32862,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_2067,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:293,x:33128,y:33240,varname:node_293,prsc:2|A-292-RGB,B-995-RGB,C-81-OUT,D-3865-OUT;n:type:ShaderForge.SFN_VertexColor,id:301,x:32781,y:33379,varname:node_301,prsc:2;n:type:ShaderForge.SFN_Color,id:306,x:32781,y:33588,ptovrint:False,ptlb:Glow,ptin:_Glow,varname:node_7840,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Add,id:308,x:33685,y:33243,varname:node_308,prsc:2|A-1013-OUT,B-293-OUT,C-309-OUT,D-3343-OUT;n:type:ShaderForge.SFN_Multiply,id:309,x:33149,y:33503,varname:node_309,prsc:2|A-301-R,B-306-RGB,C-306-A;n:type:ShaderForge.SFN_Tex2d,id:684,x:32292,y:32477,varname:node_8237,prsc:2,ntxv:0,isnm:False|UVIN-704-OUT,TEX-879-TEX;n:type:ShaderForge.SFN_FragmentPosition,id:692,x:31341,y:32140,varname:node_692,prsc:2;n:type:ShaderForge.SFN_Append,id:693,x:31740,y:32295,varname:node_693,prsc:2|A-692-Y,B-1029-OUT;n:type:ShaderForge.SFN_Vector2,id:703,x:31740,y:32432,varname:node_703,prsc:2,v1:0.038,v2:0.125;n:type:ShaderForge.SFN_Multiply,id:704,x:31925,y:32295,varname:node_704,prsc:2|A-693-OUT,B-703-OUT;n:type:ShaderForge.SFN_Append,id:765,x:31740,y:32531,varname:node_765,prsc:2|A-692-X,B-1029-OUT;n:type:ShaderForge.SFN_Multiply,id:767,x:31925,y:32475,varname:node_767,prsc:2|A-703-OUT,B-765-OUT;n:type:ShaderForge.SFN_ChannelBlend,id:856,x:32589,y:32493,varname:node_856,prsc:2,chbt:0|M-896-OUT,R-684-R,G-884-G,B-909-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:879,x:32052,y:32615,ptovrint:False,ptlb:Cookie,ptin:_Cookie,varname:node_5704,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:884,x:32292,y:32632,varname:node_9857,prsc:2,ntxv:0,isnm:False|UVIN-767-OUT,TEX-879-TEX;n:type:ShaderForge.SFN_NormalVector,id:894,x:31740,y:32139,prsc:2,pt:False;n:type:ShaderForge.SFN_Abs,id:895,x:31925,y:32139,varname:node_895,prsc:2|IN-894-OUT;n:type:ShaderForge.SFN_Multiply,id:896,x:32121,y:32139,varname:node_896,prsc:2|A-895-OUT,B-895-OUT;n:type:ShaderForge.SFN_Vector1,id:909,x:32170,y:32395,varname:node_909,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Tex2d,id:995,x:32826,y:33045,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_8273,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1004,x:33156,y:32654,ptovrint:False,ptlb:Emission,ptin:_Emission,varname:node_4385,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1013,x:33481,y:32937,varname:node_1013,prsc:2|A-1004-RGB,B-1019-RGB,C-1019-A;n:type:ShaderForge.SFN_Color,id:1019,x:33156,y:32898,ptovrint:False,ptlb:EmissionColor,ptin:_EmissionColor,varname:node_4849,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Subtract,id:1029,x:31555,y:32345,varname:node_1029,prsc:2|A-692-Z,B-1030-Z;n:type:ShaderForge.SFN_ObjectPosition,id:1030,x:31358,y:32370,varname:node_1030,prsc:2;n:type:ShaderForge.SFN_Length,id:1043,x:31740,y:31977,varname:node_1043,prsc:2|IN-1055-OUT;n:type:ShaderForge.SFN_Append,id:1055,x:31563,y:31977,varname:node_1055,prsc:2|A-692-X,B-692-Y;n:type:ShaderForge.SFN_RemapRange,id:1057,x:32631,y:32069,varname:node_1057,prsc:2,frmn:2,frmx:6,tomn:1,tomx:0.1|IN-1043-OUT;n:type:ShaderForge.SFN_Vector1,id:3343,x:33470,y:33399,varname:node_3343,prsc:2,v1:0.008;n:type:ShaderForge.SFN_ConstantClamp,id:3865,x:32864,y:32069,varname:node_3865,prsc:2,min:0.1,max:1|IN-1057-OUT;proporder:292-177-281-283-285-186-198-239-58-100-102-306-879-995-1004-1019;pass:END;sub:END;*/

Shader "_PlayHead_/fake_lighting_mk3_nobump" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _LightColor ("LightColor", Color) = (0.5,0.5,0.5,1)
        _Light1 ("Light1", Range(0, 1)) = 0
        _Light2 ("Light2", Range(0, 1)) = 0
        _Light3 ("Light3", Range(0, 1)) = 0
        _Light4 ("Light4", Range(0, 1)) = 1
        _Light5 ("Light5", Range(0, 1)) = 0
        _Light6 ("Light6", Range(0, 1)) = 0
        _LightDirection1 ("LightDirection1", Vector) = (-0.5,0.866,0,0)
        _LightDirection2 ("LightDirection2", Vector) = (0.5,0.866,0,0)
        _LightDirection3 ("LightDirection3", Vector) = (1,0,0,0)
        _Glow ("Glow", Color) = (0.5,0.5,0.5,1)
        _Cookie ("Cookie", 2D) = "white" {}
        _MainTex ("MainTex", 2D) = "white" {}
        _Emission ("Emission", 2D) = "black" {}
        _EmissionColor ("EmissionColor", Color) = (0.5,0.5,0.5,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightDirection1;
            uniform float4 _LightDirection2;
            uniform float4 _LightDirection3;
            uniform float4 _LightColor;
            uniform float _Light4;
            uniform float _Light5;
            uniform float _Light6;
            uniform float _Light1;
            uniform float _Light2;
            uniform float _Light3;
            uniform float4 _Color;
            uniform float4 _Glow;
            uniform sampler2D _Cookie; uniform float4 _Cookie_ST;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _Emission; uniform float4 _Emission_ST;
            uniform float4 _EmissionColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
/////// Vectors:
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _Emission_var = tex2D(_Emission,TRANSFORM_TEX(i.uv0, _Emission));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_62 = 0.5;
                float node_61 = (node_62*distance(i.normalDir,_LightDirection1.rgb));
                float node_207 = (node_62*distance(i.normalDir,_LightDirection2.rgb));
                float node_256 = (node_62*distance(i.normalDir,_LightDirection3.rgb));
                float3 node_895 = abs(i.normalDir);
                float3 node_896 = (node_895*node_895);
                float node_1029 = (i.posWorld.b-objPos.b);
                float2 node_703 = float2(0.038,0.125);
                float2 node_704 = (float2(i.posWorld.g,node_1029)*node_703);
                float4 node_8237 = tex2D(_Cookie,TRANSFORM_TEX(node_704, _Cookie));
                float2 node_767 = (node_703*float2(i.posWorld.r,node_1029));
                float4 node_9857 = tex2D(_Cookie,TRANSFORM_TEX(node_767, _Cookie));
                float3 emissive = ((_Emission_var.rgb*_EmissionColor.rgb*_EmissionColor.a)+(_Color.rgb*_MainTex_var.rgb*((((node_61*_Light4)+(node_207*_Light5)+(node_256*_Light6))+((1.0 - node_61)*_Light1)+((1.0 - node_207)*_Light2)+((1.0 - node_256)*_Light3))*_LightColor.rgb*(node_896.r*node_8237.r + node_896.g*node_9857.g + node_896.b*0.1))*clamp((length(float2(i.posWorld.r,i.posWorld.g))*-0.225+1.45),0.1,1))+(i.vertexColor.r*_Glow.rgb*_Glow.a)+0.008);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/fake_lighting_mk1_nobump"
    CustomEditor "ShaderForgeMaterialInspector"
}
