// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:_PlayHead_/fake_lighting_mk1_nobump,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:False,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.003,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:56,x:34340,y:32814,varname:node_56,prsc:2|emission-308-OUT;n:type:ShaderForge.SFN_NormalVector,id:57,x:29741,y:32389,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector4Property,id:58,x:29758,y:32746,ptovrint:False,ptlb:LightDirection1,ptin:_LightDirection1,varname:node_4837,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:-0.5,v2:0.866,v3:0,v4:0;n:type:ShaderForge.SFN_Distance,id:60,x:30337,y:32186,varname:node_60,prsc:2|A-57-OUT,B-58-XYZ;n:type:ShaderForge.SFN_Multiply,id:61,x:30637,y:32154,varname:node_61,prsc:2|A-62-OUT,B-60-OUT;n:type:ShaderForge.SFN_Vector1,id:62,x:30438,y:32097,varname:node_62,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:81,x:32465,y:32931,varname:node_81,prsc:2|A-3888-OUT,B-177-RGB,C-5964-OUT;n:type:ShaderForge.SFN_Vector4Property,id:100,x:29758,y:32948,ptovrint:False,ptlb:LightDirection2,ptin:_LightDirection2,varname:node_5758,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5,v2:0.866,v3:0,v4:0;n:type:ShaderForge.SFN_Vector4Property,id:102,x:29758,y:33159,ptovrint:False,ptlb:LightDirection3,ptin:_LightDirection3,varname:node_8523,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_OneMinus,id:155,x:30871,y:32260,varname:node_155,prsc:2|IN-61-OUT;n:type:ShaderForge.SFN_Color,id:177,x:32025,y:33021,ptovrint:False,ptlb:LightColor,ptin:_LightColor,varname:node_7946,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:186,x:30840,y:32763,ptovrint:False,ptlb:Light4,ptin:_Light4,varname:node_3751,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:198,x:30768,y:32856,ptovrint:False,ptlb:Light5,ptin:_Light5,varname:node_1118,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Distance,id:205,x:30337,y:32352,varname:node_205,prsc:2|A-57-OUT,B-100-XYZ;n:type:ShaderForge.SFN_Multiply,id:207,x:30637,y:32347,varname:node_207,prsc:2|A-62-OUT,B-205-OUT;n:type:ShaderForge.SFN_OneMinus,id:211,x:30847,y:32440,varname:node_211,prsc:2|IN-207-OUT;n:type:ShaderForge.SFN_Add,id:214,x:31709,y:32518,varname:node_214,prsc:2|A-244-OUT,B-246-OUT,C-260-OUT;n:type:ShaderForge.SFN_Slider,id:239,x:30840,y:32944,ptovrint:False,ptlb:Light6,ptin:_Light6,varname:node_8255,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Distance,id:243,x:30337,y:32528,varname:node_243,prsc:2|A-57-OUT,B-102-XYZ;n:type:ShaderForge.SFN_Multiply,id:244,x:31315,y:32088,varname:node_244,prsc:2|A-61-OUT,B-186-OUT;n:type:ShaderForge.SFN_Multiply,id:246,x:31314,y:32357,varname:node_246,prsc:2|A-207-OUT,B-198-OUT;n:type:ShaderForge.SFN_Multiply,id:256,x:30637,y:32514,varname:node_256,prsc:2|A-62-OUT,B-243-OUT;n:type:ShaderForge.SFN_OneMinus,id:258,x:30847,y:32607,varname:node_258,prsc:2|IN-256-OUT;n:type:ShaderForge.SFN_Multiply,id:260,x:31314,y:32614,varname:node_260,prsc:2|A-256-OUT,B-239-OUT;n:type:ShaderForge.SFN_Multiply,id:263,x:31418,y:32224,varname:node_263,prsc:2|A-155-OUT,B-281-OUT;n:type:ShaderForge.SFN_Multiply,id:265,x:31415,y:32482,varname:node_265,prsc:2|A-211-OUT,B-283-OUT;n:type:ShaderForge.SFN_Multiply,id:267,x:31398,y:32762,varname:node_267,prsc:2|A-258-OUT,B-285-OUT;n:type:ShaderForge.SFN_Slider,id:281,x:30768,y:33041,ptovrint:False,ptlb:Light1,ptin:_Light1,varname:node_4095,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:283,x:30850,y:33127,ptovrint:False,ptlb:Light2,ptin:_Light2,varname:node_7436,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:285,x:30739,y:33226,ptovrint:False,ptlb:Light3,ptin:_Light3,varname:node_4827,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:287,x:31709,y:32690,varname:node_287,prsc:2|A-214-OUT,B-263-OUT,C-265-OUT,D-267-OUT;n:type:ShaderForge.SFN_Color,id:292,x:33175,y:32862,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_8426,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:293,x:33432,y:33240,varname:node_293,prsc:2|A-292-RGB,B-995-RGB,C-5107-OUT;n:type:ShaderForge.SFN_VertexColor,id:301,x:33085,y:33379,varname:node_301,prsc:2;n:type:ShaderForge.SFN_Color,id:306,x:33085,y:33588,ptovrint:False,ptlb:GlowColor,ptin:_GlowColor,varname:node_7204,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Add,id:308,x:34061,y:33182,varname:node_308,prsc:2|A-1013-OUT,B-293-OUT,C-309-OUT;n:type:ShaderForge.SFN_Multiply,id:309,x:33604,y:33520,varname:node_309,prsc:2|A-5128-OUT,B-306-RGB,C-306-A,D-301-A,E-5907-RGB;n:type:ShaderForge.SFN_Tex2d,id:995,x:33130,y:33045,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_3362,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1004,x:33460,y:32654,ptovrint:False,ptlb:Emission,ptin:_Emission,varname:node_436,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1013,x:33809,y:32952,varname:node_1013,prsc:2|A-1004-RGB,B-1019-RGB,C-1019-A,D-5128-OUT;n:type:ShaderForge.SFN_Color,id:1019,x:33460,y:32898,ptovrint:False,ptlb:EmissionColor,ptin:_EmissionColor,varname:node_3579,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Clamp01,id:3888,x:31996,y:32861,varname:node_3888,prsc:2|IN-287-OUT;n:type:ShaderForge.SFN_TexCoord,id:3891,x:30627,y:33456,varname:node_3891,prsc:2,uv:1;n:type:ShaderForge.SFN_ConstantClamp,id:5107,x:32696,y:33047,varname:node_5107,prsc:2,min:0.02,max:1|IN-81-OUT;n:type:ShaderForge.SFN_OneMinus,id:5128,x:33432,y:33409,varname:node_5128,prsc:2|IN-5925-OUT;n:type:ShaderForge.SFN_Tex2d,id:5907,x:33384,y:33690,ptovrint:False,ptlb:GlowTex,ptin:_GlowTex,varname:node_2760,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:5925,x:33264,y:33363,varname:node_5925,prsc:2|A-301-R,B-301-B;n:type:ShaderForge.SFN_Vector2,id:5960,x:31110,y:33444,varname:node_5960,prsc:2,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_Distance,id:5962,x:31349,y:33394,varname:node_5962,prsc:2|A-3891-UVOUT,B-5960-OUT;n:type:ShaderForge.SFN_RemapRange,id:5963,x:31541,y:33394,varname:node_5963,prsc:2,frmn:0.05,frmx:0.4,tomn:1,tomx:0|IN-5962-OUT;n:type:ShaderForge.SFN_Clamp01,id:5964,x:31723,y:33394,varname:node_5964,prsc:2|IN-5963-OUT;proporder:292-177-281-283-285-186-198-239-58-100-102-995-1004-5907-1019-306;pass:END;sub:END;*/

Shader "_PlayHead_/fake_lighting_mk4_low" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _LightColor ("LightColor", Color) = (0.5,0.5,0.5,1)
        _Light1 ("Light1", Range(0, 1)) = 0
        _Light2 ("Light2", Range(0, 1)) = 0
        _Light3 ("Light3", Range(0, 1)) = 0
        _Light4 ("Light4", Range(0, 1)) = 1
        _Light5 ("Light5", Range(0, 1)) = 0
        _Light6 ("Light6", Range(0, 1)) = 0
        _LightDirection1 ("LightDirection1", Vector) = (-0.5,0.866,0,0)
        _LightDirection2 ("LightDirection2", Vector) = (0.5,0.866,0,0)
        _LightDirection3 ("LightDirection3", Vector) = (1,0,0,0)
        _MainTex ("MainTex", 2D) = "white" {}
        _Emission ("Emission", 2D) = "black" {}
        _GlowTex ("GlowTex", 2D) = "white" {}
        _EmissionColor ("EmissionColor", Color) = (0.5,0.5,0.5,1)
        _GlowColor ("GlowColor", Color) = (0.5,0.5,0.5,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightDirection1;
            uniform float4 _LightDirection2;
            uniform float4 _LightDirection3;
            uniform float4 _LightColor;
            uniform float _Light4;
            uniform float _Light5;
            uniform float _Light6;
            uniform float _Light1;
            uniform float _Light2;
            uniform float _Light3;
            uniform float4 _Color;
            uniform float4 _GlowColor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _Emission; uniform float4 _Emission_ST;
            uniform float4 _EmissionColor;
            uniform sampler2D _GlowTex; uniform float4 _GlowTex_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _Emission_var = tex2D(_Emission,TRANSFORM_TEX(i.uv0, _Emission));
                float node_5128 = (1.0 - (i.vertexColor.r+i.vertexColor.b));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_62 = 0.5;
                float node_61 = (node_62*distance(i.normalDir,_LightDirection1.rgb));
                float node_207 = (node_62*distance(i.normalDir,_LightDirection2.rgb));
                float node_256 = (node_62*distance(i.normalDir,_LightDirection3.rgb));
                float4 _GlowTex_var = tex2D(_GlowTex,TRANSFORM_TEX(i.uv0, _GlowTex));
                float3 emissive = ((_Emission_var.rgb*_EmissionColor.rgb*_EmissionColor.a*node_5128)+(_Color.rgb*_MainTex_var.rgb*clamp((saturate((((node_61*_Light4)+(node_207*_Light5)+(node_256*_Light6))+((1.0 - node_61)*_Light1)+((1.0 - node_207)*_Light2)+((1.0 - node_256)*_Light3)))*_LightColor.rgb*saturate((distance(i.uv1,float2(0.5,0.5))*-2.857143+1.142857))),0.02,1))+(node_5128*_GlowColor.rgb*_GlowColor.a*i.vertexColor.a*_GlowTex_var.rgb));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/fake_lighting_mk1_nobump"
    CustomEditor "ShaderForgeMaterialInspector"
}
