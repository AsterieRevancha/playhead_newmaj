// Shader created with Shader Forge Beta 0.36 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.36;sub:START;pass:START;ps:flbk:_PlayHead_/paysage/blend_02_notex,lico:1,lgpr:1,nrmq:0,limd:1,uamb:False,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:True,rmgx:True,rpth:0,hqsc:False,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:True,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.05882353,fgcg:0.05882353,fgcb:0.08235294,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:5834,x:32183,y:32761|diff-5903-OUT,emission-6469-OUT;n:type:ShaderForge.SFN_VertexColor,id:5835,x:33566,y:32974;n:type:ShaderForge.SFN_Color,id:5850,x:33290,y:32309,ptlb:Color,ptin:_Color,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:5856,x:33276,y:32815,ptlb:BlendColor,ptin:_BlendColor,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Lerp,id:5903,x:32751,y:32724|A-6719-OUT,B-5856-RGB,T-5835-G;n:type:ShaderForge.SFN_Color,id:6417,x:33003,y:33032,ptlb:SkyColor,ptin:_SkyColor,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_NormalVector,id:6463,x:33691,y:33111,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:6464,x:33501,y:33111,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-6463-OUT;n:type:ShaderForge.SFN_Multiply,id:6465,x:33268,y:33166|A-6464-OUT,B-6466-OUT;n:type:ShaderForge.SFN_Vector1,id:6466,x:33501,y:33290,v1:0.5;n:type:ShaderForge.SFN_Add,id:6467,x:33060,y:33266|A-6465-OUT,B-6466-OUT,C-6417-A;n:type:ShaderForge.SFN_Multiply,id:6469,x:32540,y:33009|A-5903-OUT,B-6417-RGB,C-6477-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:6477,x:32856,y:33266,min:0.3,max:1|IN-6467-OUT;n:type:ShaderForge.SFN_Color,id:6718,x:33290,y:32492,ptlb:Grass,ptin:_Grass,glob:False,c1:0.671504,c2:0.8088235,c3:0.487673,c4:1;n:type:ShaderForge.SFN_Lerp,id:6719,x:32999,y:32559|A-5850-RGB,B-6718-RGB,T-6730-OUT;n:type:ShaderForge.SFN_TexCoord,id:6721,x:33755,y:32762,uv:1;n:type:ShaderForge.SFN_Slider,id:6723,x:33755,y:32650,ptlb:Growing,ptin:_Growing,min:1,cur:1,max:0;n:type:ShaderForge.SFN_Step,id:6730,x:33512,y:32669|A-6723-OUT,B-6721-V;proporder:5850-5856-6417-6718-6723;pass:END;sub:END;*/

Shader "_PlayHead_/paysage/blend_02_grass" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _BlendColor ("BlendColor", Color) = (0.5,0.5,0.5,1)
        _SkyColor ("SkyColor", Color) = (0.5,0.5,0.5,1)
        _Grass ("Grass", Color) = (0.671504,0.8088235,0.487673,1)
        _Growing ("Growing", Range(1, 0)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform float4 _BlendColor;
            uniform float4 _SkyColor;
            uniform float4 _Grass;
            uniform float _Growing;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
/////// Normals:
                float3 normalDirection =  i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i)*2;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor;
////// Emissive:
                float3 node_5903 = lerp(lerp(_Color.rgb,_Grass.rgb,step(_Growing,i.uv1.g)),_BlendColor.rgb,i.vertexColor.g);
                float node_6466 = 0.5;
                float3 emissive = (node_5903*_SkyColor.rgb*clamp(((i.normalDir.g*node_6466)+node_6466+_SkyColor.a),0.3,1));
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * node_5903;
                finalColor += emissive;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform float4 _BlendColor;
            uniform float4 _SkyColor;
            uniform float4 _Grass;
            uniform float _Growing;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
/////// Normals:
                float3 normalDirection =  i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i)*2;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float3 node_5903 = lerp(lerp(_Color.rgb,_Grass.rgb,step(_Growing,i.uv1.g)),_BlendColor.rgb,i.vertexColor.g);
                finalColor += diffuseLight * node_5903;
/// Final Color:
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/paysage/blend_02_notex"
    CustomEditor "ShaderForgeMaterialInspector"
}
