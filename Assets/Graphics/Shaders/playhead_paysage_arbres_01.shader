// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Diffuse,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.03921569,fgcg:0.03921569,fgcb:0.03921569,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:364,x:34197,y:32768,varname:node_364,prsc:2|diff-365-RGB,emission-366-OUT,voffset-374-OUT;n:type:ShaderForge.SFN_Color,id:365,x:33632,y:32574,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_4014,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:366,x:33864,y:32790,varname:node_366,prsc:2|A-365-RGB,B-433-OUT,C-439-OUT,D-6280-RGB,E-6280-A;n:type:ShaderForge.SFN_Slider,id:372,x:33164,y:33037,ptovrint:False,ptlb:Growing,ptin:_Growing,varname:node_5874,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:0,max:2;n:type:ShaderForge.SFN_NormalVector,id:373,x:33405,y:33351,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:374,x:33900,y:33161,varname:node_374,prsc:2|A-6274-OUT,B-419-OUT;n:type:ShaderForge.SFN_Add,id:419,x:33687,y:33390,varname:node_419,prsc:2|A-373-OUT,B-420-OUT;n:type:ShaderForge.SFN_Vector3,id:420,x:33434,y:33497,varname:node_420,prsc:2,v1:0,v2:3,v3:0;n:type:ShaderForge.SFN_Fresnel,id:433,x:33235,y:32584,varname:node_433,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:435,x:33028,y:32801,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:436,x:33235,y:32801,varname:node_436,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-435-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:439,x:33453,y:32801,varname:node_439,prsc:2,min:0.2,max:1|IN-436-OUT;n:type:ShaderForge.SFN_Add,id:6270,x:33549,y:33111,varname:node_6270,prsc:2|A-372-OUT,B-6279-OUT;n:type:ShaderForge.SFN_TexCoord,id:6271,x:32878,y:33140,varname:node_6271,prsc:2,uv:1;n:type:ShaderForge.SFN_ConstantClamp,id:6274,x:33717,y:33111,varname:node_6274,prsc:2,min:-1,max:0|IN-6270-OUT;n:type:ShaderForge.SFN_Vector1,id:6277,x:33171,y:33291,varname:node_6277,prsc:2,v1:-2;n:type:ShaderForge.SFN_Multiply,id:6279,x:33361,y:33181,varname:node_6279,prsc:2|A-6755-OUT,B-6277-OUT;n:type:ShaderForge.SFN_Color,id:6280,x:33640,y:32869,ptovrint:False,ptlb:SkyColor,ptin:_SkyColor,varname:node_1305,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:6281,x:33837,y:32582,varname:node_6281,prsc:2|A-365-RGB,B-6280-RGB;n:type:ShaderForge.SFN_OneMinus,id:6755,x:33129,y:33140,varname:node_6755,prsc:2|IN-6271-V;proporder:365-6280-372;pass:END;sub:END;*/

Shader "_PlayHead_/paysage/arbres" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _SkyColor ("SkyColor", Color) = (0.5,0.5,0.5,1)
        _Growing ("Growing", Range(-1, 2)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform float _Growing;
            uniform float4 _SkyColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (clamp((_Growing+((1.0 - o.uv1.g)*(-2.0))),-1,0)*(v.normal+float3(0,3,0)));
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = _Color.rgb;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = (_Color.rgb*(1.0-max(0,dot(normalDirection, viewDirection)))*clamp(i.normalDir.g,0.2,1)*_SkyColor.rgb*_SkyColor.a);
/// Final Color:
                float3 finalColor = diffuse + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float _Growing;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv1 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (clamp((_Growing+((1.0 - o.uv1.g)*(-2.0))),-1,0)*(v.normal+float3(0,3,0)));
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
