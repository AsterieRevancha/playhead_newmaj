// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:4060,x:36017,y:32686,varname:node_4060,prsc:2|emission-5330-OUT;n:type:ShaderForge.SFN_Color,id:4061,x:34667,y:32327,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_9027,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4062,x:35329,y:33086,ptovrint:False,ptlb:Specular,ptin:_Specular,varname:node_8425,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:4063,x:34385,y:32765,ptovrint:False,ptlb:Shininess,ptin:_Shininess,varname:node_9185,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Color,id:4067,x:34502,y:33172,ptovrint:False,ptlb:HeadGlow,ptin:_HeadGlow,varname:node_2237,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Add,id:4068,x:35433,y:32827,varname:node_4068,prsc:2|A-4073-OUT,B-4072-OUT,C-4077-OUT,D-4630-OUT,E-5349-OUT;n:type:ShaderForge.SFN_Multiply,id:4072,x:34773,y:33063,varname:node_4072,prsc:2|A-4236-RGB,B-4067-RGB,C-4086-RGB;n:type:ShaderForge.SFN_Vector1,id:4073,x:34754,y:32822,varname:node_4073,prsc:2,v1:0.04;n:type:ShaderForge.SFN_NormalVector,id:4075,x:33738,y:32289,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:4076,x:33943,y:32278,varname:node_4076,prsc:2,cc1:2,cc2:1,cc3:0,cc4:-1|IN-4075-OUT;n:type:ShaderForge.SFN_Multiply,id:4077,x:34994,y:32543,varname:node_4077,prsc:2|A-4061-A,B-4076-G,C-4353-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:4081,x:33828,y:32975,ptovrint:False,ptlb:GlowRamp,ptin:_GlowRamp,varname:node_3513,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:4086,x:34166,y:33128,varname:node_7934,prsc:2,ntxv:0,isnm:False|UVIN-4137-OUT,TEX-4081-TEX;n:type:ShaderForge.SFN_TexCoord,id:4089,x:33532,y:33089,varname:node_4089,prsc:2,uv:1;n:type:ShaderForge.SFN_Add,id:4137,x:33947,y:33128,varname:node_4137,prsc:2|A-4089-UVOUT,B-4140-OUT;n:type:ShaderForge.SFN_Slider,id:4140,x:33521,y:33314,ptovrint:False,ptlb:GlowAnim,ptin:_GlowAnim,varname:node_3807,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:0;n:type:ShaderForge.SFN_Multiply,id:4169,x:35505,y:32560,varname:node_4169,prsc:2|A-4170-RGB,B-4061-RGB;n:type:ShaderForge.SFN_Tex2d,id:4170,x:34938,y:32184,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_6442,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:4236,x:34509,y:32888,ptovrint:False,ptlb:Illumination,ptin:_Illumination,varname:node_9124,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Vector1,id:4353,x:34742,y:32577,varname:node_4353,prsc:2,v1:0.15;n:type:ShaderForge.SFN_Color,id:4600,x:33082,y:33883,ptovrint:False,ptlb:ColorLight1,ptin:_ColorLight1,varname:node_9190,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4602,x:32947,y:34029,ptovrint:False,ptlb:ColorLight2,ptin:_ColorLight2,varname:node_2723,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4604,x:33084,y:34173,ptovrint:False,ptlb:ColorLight3,ptin:_ColorLight3,varname:node_7218,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4606,x:32942,y:34316,ptovrint:False,ptlb:ColorLight4,ptin:_ColorLight4,varname:node_248,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4608,x:33078,y:34464,ptovrint:False,ptlb:ColorLight5,ptin:_ColorLight5,varname:node_7070,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4610,x:32937,y:34603,ptovrint:False,ptlb:ColorLight6,ptin:_ColorLight6,varname:node_3177,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_VertexColor,id:4612,x:32980,y:34802,varname:node_4612,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4614,x:33498,y:33882,varname:node_4614,prsc:2|A-4600-RGB,B-4636-OUT,C-4612-B,D-4612-A;n:type:ShaderForge.SFN_Add,id:4616,x:33903,y:34202,varname:node_4616,prsc:2|A-4614-OUT,B-4618-OUT,C-4622-OUT;n:type:ShaderForge.SFN_Multiply,id:4618,x:33398,y:34027,varname:node_4618,prsc:2|A-4602-RGB,B-4638-OUT,C-4612-R,D-4628-OUT;n:type:ShaderForge.SFN_Multiply,id:4620,x:33389,y:34311,varname:node_4620,prsc:2|A-4606-RGB,B-4642-OUT,C-4612-B,D-4628-OUT;n:type:ShaderForge.SFN_Multiply,id:4622,x:33506,y:34173,varname:node_4622,prsc:2|A-4604-RGB,B-4640-OUT,C-4612-G,D-4628-OUT;n:type:ShaderForge.SFN_Multiply,id:4624,x:33377,y:34601,varname:node_4624,prsc:2|A-4610-RGB,B-4646-OUT,C-4612-G,D-4612-A;n:type:ShaderForge.SFN_Multiply,id:4626,x:33499,y:34459,varname:node_4626,prsc:2|A-4608-RGB,B-4644-OUT,C-4612-R,D-4612-A;n:type:ShaderForge.SFN_OneMinus,id:4628,x:33037,y:34991,varname:node_4628,prsc:2|IN-4612-A;n:type:ShaderForge.SFN_Multiply,id:4630,x:34210,y:34230,varname:node_4630,prsc:2|A-4236-RGB,B-4634-OUT;n:type:ShaderForge.SFN_Add,id:4634,x:33963,y:34374,varname:node_4634,prsc:2|A-4616-OUT,B-4620-OUT,C-4626-OUT,D-4624-OUT;n:type:ShaderForge.SFN_Slider,id:4636,x:32680,y:33937,ptovrint:False,ptlb:Light1,ptin:_Light1,varname:node_9349,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:4638,x:32606,y:34089,ptovrint:False,ptlb:Light2,ptin:_Light2,varname:node_5155,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:4640,x:32678,y:34217,ptovrint:False,ptlb:Light3,ptin:_Light3,varname:node_371,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:4642,x:32595,y:34370,ptovrint:False,ptlb:Light4,ptin:_Light4,varname:node_9553,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:4644,x:32657,y:34507,ptovrint:False,ptlb:Light5,ptin:_Light5,varname:node_5166,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:4646,x:32585,y:34656,ptovrint:False,ptlb:Light6,ptin:_Light6,varname:node_7575,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Blend,id:5330,x:35790,y:32791,varname:node_5330,prsc:2,blmd:10,clmp:True|SRC-4169-OUT,DST-4068-OUT;n:type:ShaderForge.SFN_Fresnel,id:5339,x:35329,y:33229,varname:node_5339,prsc:2|EXP-5344-OUT;n:type:ShaderForge.SFN_Vector1,id:5344,x:35097,y:33229,varname:node_5344,prsc:2,v1:5;n:type:ShaderForge.SFN_Multiply,id:5349,x:35555,y:33096,varname:node_5349,prsc:2|A-4062-RGB,B-5339-OUT;proporder:4061-4062-4063-4067-4081-4140-4170-4236-4600-4636-4602-4638-4604-4640-4606-4642-4608-4644-4610-4646;pass:END;sub:END;*/

Shader "_PlayHead_/cockpit_03" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _Specular ("Specular", Color) = (0.5,0.5,0.5,1)
        _Shininess ("Shininess", Range(0, 1)) = 0
        _HeadGlow ("HeadGlow", Color) = (0.5,0.5,0.5,1)
        _GlowRamp ("GlowRamp", 2D) = "white" {}
        _GlowAnim ("GlowAnim", Range(1, 0)) = 1
        _MainTex ("MainTex", 2D) = "white" {}
        _Illumination ("Illumination", 2D) = "white" {}
        _ColorLight1 ("ColorLight1", Color) = (0.5,0.5,0.5,1)
        _Light1 ("Light1", Range(0, 1)) = 0
        _ColorLight2 ("ColorLight2", Color) = (0.5,0.5,0.5,1)
        _Light2 ("Light2", Range(0, 1)) = 0
        _ColorLight3 ("ColorLight3", Color) = (0.5,0.5,0.5,1)
        _Light3 ("Light3", Range(0, 1)) = 0
        _ColorLight4 ("ColorLight4", Color) = (0.5,0.5,0.5,1)
        _Light4 ("Light4", Range(0, 1)) = 0
        _ColorLight5 ("ColorLight5", Color) = (0.5,0.5,0.5,1)
        _Light5 ("Light5", Range(0, 1)) = 0
        _ColorLight6 ("ColorLight6", Color) = (0.5,0.5,0.5,1)
        _Light6 ("Light6", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float4 _Specular;
            uniform float4 _HeadGlow;
            uniform sampler2D _GlowRamp; uniform float4 _GlowRamp_ST;
            uniform float _GlowAnim;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _Illumination; uniform float4 _Illumination_ST;
            uniform float4 _ColorLight1;
            uniform float4 _ColorLight2;
            uniform float4 _ColorLight3;
            uniform float4 _ColorLight4;
            uniform float4 _ColorLight5;
            uniform float4 _ColorLight6;
            uniform float _Light1;
            uniform float _Light2;
            uniform float _Light3;
            uniform float _Light4;
            uniform float _Light5;
            uniform float _Light6;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 _Illumination_var = tex2D(_Illumination,TRANSFORM_TEX(i.uv0, _Illumination));
                float2 node_4137 = (i.uv1+_GlowAnim);
                float4 node_7934 = tex2D(_GlowRamp,TRANSFORM_TEX(node_4137, _GlowRamp));
                float node_4628 = (1.0 - i.vertexColor.a);
                float3 emissive = saturate(( (0.04+(_Illumination_var.rgb*_HeadGlow.rgb*node_7934.rgb)+(_Color.a*i.normalDir.bgr.g*0.15)+(_Illumination_var.rgb*(((_ColorLight1.rgb*_Light1*i.vertexColor.b*i.vertexColor.a)+(_ColorLight2.rgb*_Light2*i.vertexColor.r*node_4628)+(_ColorLight3.rgb*_Light3*i.vertexColor.g*node_4628))+(_ColorLight4.rgb*_Light4*i.vertexColor.b*node_4628)+(_ColorLight5.rgb*_Light5*i.vertexColor.r*i.vertexColor.a)+(_ColorLight6.rgb*_Light6*i.vertexColor.g*i.vertexColor.a)))+(_Specular.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),5.0))) > 0.5 ? (1.0-(1.0-2.0*((0.04+(_Illumination_var.rgb*_HeadGlow.rgb*node_7934.rgb)+(_Color.a*i.normalDir.bgr.g*0.15)+(_Illumination_var.rgb*(((_ColorLight1.rgb*_Light1*i.vertexColor.b*i.vertexColor.a)+(_ColorLight2.rgb*_Light2*i.vertexColor.r*node_4628)+(_ColorLight3.rgb*_Light3*i.vertexColor.g*node_4628))+(_ColorLight4.rgb*_Light4*i.vertexColor.b*node_4628)+(_ColorLight5.rgb*_Light5*i.vertexColor.r*i.vertexColor.a)+(_ColorLight6.rgb*_Light6*i.vertexColor.g*i.vertexColor.a)))+(_Specular.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),5.0)))-0.5))*(1.0-(_MainTex_var.rgb*_Color.rgb))) : (2.0*(0.04+(_Illumination_var.rgb*_HeadGlow.rgb*node_7934.rgb)+(_Color.a*i.normalDir.bgr.g*0.15)+(_Illumination_var.rgb*(((_ColorLight1.rgb*_Light1*i.vertexColor.b*i.vertexColor.a)+(_ColorLight2.rgb*_Light2*i.vertexColor.r*node_4628)+(_ColorLight3.rgb*_Light3*i.vertexColor.g*node_4628))+(_ColorLight4.rgb*_Light4*i.vertexColor.b*node_4628)+(_ColorLight5.rgb*_Light5*i.vertexColor.r*i.vertexColor.a)+(_ColorLight6.rgb*_Light6*i.vertexColor.g*i.vertexColor.a)))+(_Specular.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),5.0)))*(_MainTex_var.rgb*_Color.rgb)) ));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
