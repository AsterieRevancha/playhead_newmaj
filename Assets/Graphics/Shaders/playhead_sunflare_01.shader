// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:0,dpts:0,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:False,igpj:True,qofs:-998,qpre:0,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:2513,x:33736,y:32757,varname:node_2513,prsc:2|emission-2560-OUT;n:type:ShaderForge.SFN_Tex2d,id:2514,x:33108,y:32734,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_985,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:11d02730ae6ea524886d1014e2ce95f4,ntxv:2,isnm:False|UVIN-2779-OUT;n:type:ShaderForge.SFN_Color,id:2546,x:33108,y:32566,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_5933,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:2547,x:33331,y:32628,varname:node_2547,prsc:2|A-2546-RGB,B-2514-RGB,C-2546-A;n:type:ShaderForge.SFN_Add,id:2560,x:33545,y:32856,varname:node_2560,prsc:2|A-2547-OUT,B-2706-OUT;n:type:ShaderForge.SFN_VertexColor,id:2704,x:33108,y:32911,varname:node_2704,prsc:2;n:type:ShaderForge.SFN_Color,id:2705,x:33108,y:33070,ptovrint:False,ptlb:FlareColor,ptin:_FlareColor,varname:node_7864,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:2706,x:33331,y:32976,varname:node_2706,prsc:2|A-2704-R,B-2705-RGB,C-2705-A;n:type:ShaderForge.SFN_TexCoord,id:2778,x:32660,y:32698,varname:node_2778,prsc:2,uv:1;n:type:ShaderForge.SFN_Multiply,id:2779,x:32911,y:32784,varname:node_2779,prsc:2|A-2778-UVOUT,B-2780-OUT;n:type:ShaderForge.SFN_Slider,id:2780,x:32582,y:32904,ptovrint:False,ptlb:SunSize,ptin:_SunSize,varname:node_3747,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.5,cur:1,max:2.5;proporder:2514-2546-2705-2780;pass:END;sub:END;*/

Shader "_PlayHead_/sunflare" {
    Properties {
        _MainTex ("MainTex", 2D) = "black" {}
        _Color ("Color", Color) = (1,1,1,1)
        _FlareColor ("FlareColor", Color) = (0.5,0.5,0.5,1)
        _SunSize ("SunSize", Range(0.5, 2.5)) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Background-998"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZTest Less
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color;
            uniform float4 _FlareColor;
            uniform float _SunSize;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float2 node_2779 = (i.uv1*_SunSize);
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_2779, _MainTex));
                float3 emissive = ((_Color.rgb*_MainTex_var.rgb*_Color.a)+(i.vertexColor.r*_FlareColor.rgb*_FlareColor.a));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
