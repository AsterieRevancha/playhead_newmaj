// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:False,igpj:True,qofs:25,qpre:4,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.03921569,fgcg:0.03921569,fgcb:0.03921569,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:2,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:34983,y:32684,varname:node_1,prsc:2|emission-532-OUT,alpha-17-OUT;n:type:ShaderForge.SFN_Color,id:2,x:34396,y:32294,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_6018,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Slider,id:3,x:34340,y:33266,ptovrint:False,ptlb:Alpha,ptin:_Alpha,varname:node_7406,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Tex2d,id:12,x:33431,y:32881,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_6122,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-583-UVOUT;n:type:ShaderForge.SFN_Multiply,id:17,x:34419,y:32984,varname:node_17,prsc:2|A-151-OUT,B-3-OUT;n:type:ShaderForge.SFN_Color,id:102,x:33931,y:32633,ptovrint:False,ptlb:HoverColor,ptin:_HoverColor,varname:node_5345,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9960784,c2:0.7803922,c3:0.3254902,c4:1;n:type:ShaderForge.SFN_Add,id:151,x:34164,y:32877,varname:node_151,prsc:2|A-12-R,B-284-OUT,C-343-OUT;n:type:ShaderForge.SFN_Tex2d,id:186,x:33767,y:33264,ptovrint:False,ptlb:Cursor,ptin:_Cursor,varname:node_6302,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False|UVIN-196-UVOUT;n:type:ShaderForge.SFN_Slider,id:195,x:32912,y:33464,ptovrint:False,ptlb:CursorPosition,ptin:_CursorPosition,varname:node_679,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Panner,id:196,x:33472,y:33304,varname:node_196,prsc:2,spu:1,spv:0|DIST-206-OUT;n:type:ShaderForge.SFN_Multiply,id:206,x:33294,y:33334,varname:node_206,prsc:2|A-216-OUT,B-195-OUT;n:type:ShaderForge.SFN_Vector1,id:216,x:33059,y:33304,varname:node_216,prsc:2,v1:-0.941;n:type:ShaderForge.SFN_Multiply,id:284,x:33943,y:33093,varname:node_284,prsc:2|A-12-G,B-186-A;n:type:ShaderForge.SFN_Vector1,id:285,x:33592,y:32759,varname:node_285,prsc:2,v1:0.8;n:type:ShaderForge.SFN_Multiply,id:343,x:33943,y:32933,varname:node_343,prsc:2|A-285-OUT,B-12-B;n:type:ShaderForge.SFN_Color,id:376,x:33317,y:31520,ptovrint:False,ptlb:TracksColor,ptin:_TracksColor,varname:node_1841,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2205882,c2:0.2205882,c3:0.2205882,c4:1;n:type:ShaderForge.SFN_Tex2d,id:400,x:33667,y:32550,ptovrint:False,ptlb:Separator,ptin:_Separator,varname:node_2458,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:8e643748d03f49d458845363d5f6953b,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Color,id:438,x:33385,y:31775,ptovrint:False,ptlb:Color1,ptin:_Color1,varname:node_1532,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:440,x:33385,y:31968,ptovrint:False,ptlb:Color2,ptin:_Color2,varname:node_2113,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:442,x:33385,y:32143,ptovrint:False,ptlb:Color3,ptin:_Color3,varname:node_2845,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:444,x:33385,y:32350,ptovrint:False,ptlb:Color4,ptin:_Color4,varname:node_4608,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Lerp,id:445,x:33667,y:31877,varname:node_445,prsc:2|A-376-RGB,B-438-RGB,T-450-OUT;n:type:ShaderForge.SFN_Slider,id:450,x:33068,y:31903,ptovrint:False,ptlb:Active1,ptin:_Active1,varname:node_1607,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:452,x:33667,y:32034,varname:node_452,prsc:2|A-376-RGB,B-440-RGB,T-458-OUT;n:type:ShaderForge.SFN_Lerp,id:454,x:33667,y:32208,varname:node_454,prsc:2|A-376-RGB,B-442-RGB,T-460-OUT;n:type:ShaderForge.SFN_Lerp,id:456,x:33667,y:32379,varname:node_456,prsc:2|A-376-RGB,B-444-RGB,T-462-OUT;n:type:ShaderForge.SFN_Slider,id:458,x:33068,y:32070,ptovrint:False,ptlb:Active2,ptin:_Active2,varname:node_5460,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:460,x:33068,y:32228,ptovrint:False,ptlb:Active3,ptin:_Active3,varname:node_4909,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:462,x:33068,y:32387,ptovrint:False,ptlb:Active4,ptin:_Active4,varname:node_3890,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:484,x:33931,y:31970,varname:node_484,prsc:2|A-445-OUT,B-400-R;n:type:ShaderForge.SFN_Multiply,id:486,x:33931,y:32132,varname:node_486,prsc:2|A-452-OUT,B-400-G;n:type:ShaderForge.SFN_Multiply,id:488,x:33931,y:32295,varname:node_488,prsc:2|A-454-OUT,B-400-B;n:type:ShaderForge.SFN_Multiply,id:490,x:33931,y:32473,varname:node_490,prsc:2|A-456-OUT,B-400-A;n:type:ShaderForge.SFN_Add,id:491,x:34396,y:32484,varname:node_491,prsc:2|A-484-OUT,B-486-OUT,C-488-OUT,D-490-OUT;n:type:ShaderForge.SFN_Lerp,id:522,x:34660,y:32544,varname:node_522,prsc:2|A-2-RGB,B-491-OUT,T-12-G;n:type:ShaderForge.SFN_Lerp,id:532,x:34761,y:32786,varname:node_532,prsc:2|A-522-OUT,B-102-RGB,T-554-OUT;n:type:ShaderForge.SFN_Multiply,id:554,x:34430,y:32823,varname:node_554,prsc:2|A-12-G,B-186-A;n:type:ShaderForge.SFN_ValueProperty,id:580,x:32822,y:33088,ptovrint:False,ptlb:Phase,ptin:_Phase,varname:node_7837,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:581,x:33049,y:32956,varname:node_581,prsc:2|A-582-OUT,B-580-OUT;n:type:ShaderForge.SFN_Vector1,id:582,x:32872,y:32956,varname:node_582,prsc:2,v1:-0.25;n:type:ShaderForge.SFN_Panner,id:583,x:33220,y:32857,varname:node_583,prsc:2,spu:0,spv:1|DIST-581-OUT;proporder:12-186-400-2-102-376-3-195-438-440-442-444-450-458-460-462-580;pass:END;sub:END;*/

Shader "_PlayHead_/interface/transbar_map_01" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _Cursor ("Cursor", 2D) = "black" {}
        _Separator ("Separator", 2D) = "black" {}
        _Color ("Color", Color) = (1,1,1,1)
        _HoverColor ("HoverColor", Color) = (0.9960784,0.7803922,0.3254902,1)
        _TracksColor ("TracksColor", Color) = (0.2205882,0.2205882,0.2205882,1)
        _Alpha ("Alpha", Range(0, 1)) = 0
        _CursorPosition ("CursorPosition", Range(0, 1)) = 0
        _Color1 ("Color1", Color) = (0.5,0.5,0.5,1)
        _Color2 ("Color2", Color) = (0.5,0.5,0.5,1)
        _Color3 ("Color3", Color) = (0.5,0.5,0.5,1)
        _Color4 ("Color4", Color) = (0.5,0.5,0.5,1)
        _Active1 ("Active1", Range(0, 1)) = 0
        _Active2 ("Active2", Range(0, 1)) = 0
        _Active3 ("Active3", Range(0, 1)) = 0
        _Active4 ("Active4", Range(0, 1)) = 0
        _Phase ("Phase", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Overlay+25"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float _Alpha;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _HoverColor;
            uniform sampler2D _Cursor; uniform float4 _Cursor_ST;
            uniform float _CursorPosition;
            uniform float4 _TracksColor;
            uniform sampler2D _Separator; uniform float4 _Separator_ST;
            uniform float4 _Color1;
            uniform float4 _Color2;
            uniform float4 _Color3;
            uniform float4 _Color4;
            uniform float _Active1;
            uniform float _Active2;
            uniform float _Active3;
            uniform float _Active4;
            uniform float _Phase;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 _Separator_var = tex2D(_Separator,TRANSFORM_TEX(i.uv0, _Separator));
                float2 node_583 = (i.uv0+((-0.25)*_Phase)*float2(0,1));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_583, _MainTex));
                float2 node_196 = (i.uv0+((-0.941)*_CursorPosition)*float2(1,0));
                float4 _Cursor_var = tex2D(_Cursor,TRANSFORM_TEX(node_196, _Cursor));
                float3 emissive = lerp(lerp(_Color.rgb,((lerp(_TracksColor.rgb,_Color1.rgb,_Active1)*_Separator_var.r)+(lerp(_TracksColor.rgb,_Color2.rgb,_Active2)*_Separator_var.g)+(lerp(_TracksColor.rgb,_Color3.rgb,_Active3)*_Separator_var.b)+(lerp(_TracksColor.rgb,_Color4.rgb,_Active4)*_Separator_var.a)),_MainTex_var.g),_HoverColor.rgb,(_MainTex_var.g*_Cursor_var.a));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,((_MainTex_var.r+(_MainTex_var.g*_Cursor_var.a)+(0.8*_MainTex_var.b))*_Alpha));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
