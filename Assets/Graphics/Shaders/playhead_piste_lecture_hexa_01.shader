// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Particles/Additive,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:153,x:34435,y:32718,varname:node_153,prsc:2|emission-156-OUT,voffset-184-OUT;n:type:ShaderForge.SFN_Color,id:154,x:33694,y:32313,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1580,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:156,x:34016,y:32566,varname:node_156,prsc:2|A-154-RGB,B-185-RGB,C-926-OUT;n:type:ShaderForge.SFN_Multiply,id:157,x:33356,y:33026,varname:node_157,prsc:2|A-175-R,B-161-OUT,C-175-A;n:type:ShaderForge.SFN_Multiply,id:159,x:33355,y:33168,varname:node_159,prsc:2|A-175-G,B-163-OUT,C-175-A;n:type:ShaderForge.SFN_Slider,id:161,x:32696,y:33188,ptovrint:False,ptlb:Lecture1,ptin:_Lecture1,varname:node_8404,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:163,x:32696,y:33307,ptovrint:False,ptlb:Lecture2,ptin:_Lecture2,varname:node_1022,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:165,x:32696,y:33419,ptovrint:False,ptlb:Lecture3,ptin:_Lecture3,varname:node_2604,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:167,x:32696,y:33540,ptovrint:False,ptlb:Lecture4,ptin:_Lecture4,varname:node_6542,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:171,x:33355,y:33308,varname:node_171,prsc:2|A-175-B,B-165-OUT,C-175-A;n:type:ShaderForge.SFN_Multiply,id:173,x:33355,y:33452,varname:node_173,prsc:2|A-175-R,B-167-OUT,C-638-OUT;n:type:ShaderForge.SFN_Add,id:174,x:33705,y:33041,varname:node_174,prsc:2|A-157-OUT,B-159-OUT,C-171-OUT;n:type:ShaderForge.SFN_VertexColor,id:175,x:32840,y:32880,varname:node_175,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:176,x:33698,y:33547,prsc:2,pt:False;n:type:ShaderForge.SFN_TexCoord,id:181,x:33698,y:33371,varname:node_181,prsc:2,uv:1;n:type:ShaderForge.SFN_Multiply,id:184,x:33977,y:33041,varname:node_184,prsc:2|A-934-OUT,B-181-U,C-176-OUT,D-524-OUT;n:type:ShaderForge.SFN_Tex2d,id:185,x:33706,y:32500,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_2601,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-742-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:524,x:33977,y:33208,ptovrint:False,ptlb:Distance,ptin:_Distance,varname:node_2937,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.77;n:type:ShaderForge.SFN_Slider,id:533,x:32704,y:32185,ptovrint:False,ptlb:Fader1,ptin:_Fader1,varname:node_4892,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:535,x:32704,y:32308,ptovrint:False,ptlb:Fader2,ptin:_Fader2,varname:node_286,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:537,x:32704,y:32437,ptovrint:False,ptlb:Fader3,ptin:_Fader3,varname:node_6690,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:539,x:32704,y:32556,ptovrint:False,ptlb:Fader4,ptin:_Fader4,varname:node_8386,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:546,x:33356,y:32613,varname:node_546,prsc:2|A-539-OUT,B-175-R,C-638-OUT;n:type:ShaderForge.SFN_Multiply,id:548,x:33356,y:32487,varname:node_548,prsc:2|A-537-OUT,B-175-B,C-175-A;n:type:ShaderForge.SFN_Multiply,id:550,x:33360,y:32342,varname:node_550,prsc:2|A-535-OUT,B-175-G,C-175-A;n:type:ShaderForge.SFN_Multiply,id:552,x:33360,y:32204,varname:node_552,prsc:2|A-533-OUT,B-175-R,C-175-A;n:type:ShaderForge.SFN_Add,id:563,x:33706,y:32706,varname:node_563,prsc:2|A-552-OUT,B-550-OUT,C-548-OUT;n:type:ShaderForge.SFN_Slider,id:627,x:32704,y:32674,ptovrint:False,ptlb:Fader5,ptin:_Fader5,varname:node_3288,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:629,x:32696,y:33665,ptovrint:False,ptlb:Lecture5,ptin:_Lecture5,varname:node_7917,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:631,x:33356,y:32751,varname:node_631,prsc:2|A-627-OUT,B-175-G,C-638-OUT;n:type:ShaderForge.SFN_Multiply,id:633,x:33355,y:33594,varname:node_633,prsc:2|A-175-G,B-629-OUT,C-638-OUT;n:type:ShaderForge.SFN_OneMinus,id:638,x:32840,y:33028,varname:node_638,prsc:2|IN-175-A;n:type:ShaderForge.SFN_TexCoord,id:697,x:33177,y:31947,varname:node_697,prsc:2,uv:1;n:type:ShaderForge.SFN_TexCoord,id:741,x:33486,y:31824,varname:node_741,prsc:2,uv:0;n:type:ShaderForge.SFN_Panner,id:742,x:33729,y:31910,varname:node_742,prsc:2,spu:0,spv:1|UVIN-741-UVOUT,DIST-1010-OUT;n:type:ShaderForge.SFN_Add,id:926,x:33706,y:32834,varname:node_926,prsc:2|A-563-OUT,B-546-OUT,C-631-OUT,D-938-OUT;n:type:ShaderForge.SFN_Slider,id:928,x:32704,y:32785,ptovrint:False,ptlb:Fader6,ptin:_Fader6,varname:node_8626,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:930,x:32696,y:33778,ptovrint:False,ptlb:Lecture6,ptin:_Lecture6,varname:node_8763,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:932,x:33355,y:33743,varname:node_932,prsc:2|A-175-B,B-930-OUT,C-638-OUT;n:type:ShaderForge.SFN_Add,id:934,x:33698,y:33191,varname:node_934,prsc:2|A-174-OUT,B-173-OUT,C-633-OUT,D-932-OUT;n:type:ShaderForge.SFN_Multiply,id:938,x:33356,y:32882,varname:node_938,prsc:2|A-928-OUT,B-175-B,C-638-OUT;n:type:ShaderForge.SFN_Multiply,id:1010,x:33547,y:31999,varname:node_1010,prsc:2|A-697-V,B-934-OUT;proporder:154-185-161-533-163-535-165-537-167-539-629-627-930-928-524;pass:END;sub:END;*/

Shader "_PlayHead_/pistes/hexa_01" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Lecture1 ("Lecture1", Range(0, 1)) = 0
        _Fader1 ("Fader1", Range(0, 1)) = 1
        _Lecture2 ("Lecture2", Range(0, 1)) = 0
        _Fader2 ("Fader2", Range(0, 1)) = 1
        _Lecture3 ("Lecture3", Range(0, 1)) = 0
        _Fader3 ("Fader3", Range(0, 1)) = 1
        _Lecture4 ("Lecture4", Range(0, 1)) = 0
        _Fader4 ("Fader4", Range(0, 1)) = 1
        _Lecture5 ("Lecture5", Range(0, 1)) = 0
        _Fader5 ("Fader5", Range(0, 1)) = 1
        _Lecture6 ("Lecture6", Range(0, 1)) = 0
        _Fader6 ("Fader6", Range(0, 1)) = 1
        _Distance ("Distance", Float ) = 0.77
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float _Lecture1;
            uniform float _Lecture2;
            uniform float _Lecture3;
            uniform float _Lecture4;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Distance;
            uniform float _Fader1;
            uniform float _Fader2;
            uniform float _Fader3;
            uniform float _Fader4;
            uniform float _Fader5;
            uniform float _Lecture5;
            uniform float _Fader6;
            uniform float _Lecture6;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float node_638 = (1.0 - o.vertexColor.a);
                float node_934 = (((o.vertexColor.r*_Lecture1*o.vertexColor.a)+(o.vertexColor.g*_Lecture2*o.vertexColor.a)+(o.vertexColor.b*_Lecture3*o.vertexColor.a))+(o.vertexColor.r*_Lecture4*node_638)+(o.vertexColor.g*_Lecture5*node_638)+(o.vertexColor.b*_Lecture6*node_638));
                v.vertex.xyz += (node_934*o.uv1.r*v.normal*_Distance);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                
                float nSign = sign( dot( viewDirection, i.normalDir ) ); // Reverse normal if this is a backface
                i.normalDir *= nSign;
                normalDirection *= nSign;
                
////// Lighting:
////// Emissive:
                float node_638 = (1.0 - i.vertexColor.a);
                float node_934 = (((i.vertexColor.r*_Lecture1*i.vertexColor.a)+(i.vertexColor.g*_Lecture2*i.vertexColor.a)+(i.vertexColor.b*_Lecture3*i.vertexColor.a))+(i.vertexColor.r*_Lecture4*node_638)+(i.vertexColor.g*_Lecture5*node_638)+(i.vertexColor.b*_Lecture6*node_638));
                float2 node_742 = (i.uv0+(i.uv1.g*node_934)*float2(0,1));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_742, _MainTex));
                float3 emissive = (_Color.rgb*_MainTex_var.rgb*(((_Fader1*i.vertexColor.r*i.vertexColor.a)+(_Fader2*i.vertexColor.g*i.vertexColor.a)+(_Fader3*i.vertexColor.b*i.vertexColor.a))+(_Fader4*i.vertexColor.r*node_638)+(_Fader5*i.vertexColor.g*node_638)+(_Fader6*i.vertexColor.b*node_638)));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _Lecture1;
            uniform float _Lecture2;
            uniform float _Lecture3;
            uniform float _Lecture4;
            uniform float _Distance;
            uniform float _Lecture5;
            uniform float _Lecture6;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float node_638 = (1.0 - o.vertexColor.a);
                float node_934 = (((o.vertexColor.r*_Lecture1*o.vertexColor.a)+(o.vertexColor.g*_Lecture2*o.vertexColor.a)+(o.vertexColor.b*_Lecture3*o.vertexColor.a))+(o.vertexColor.r*_Lecture4*node_638)+(o.vertexColor.g*_Lecture5*node_638)+(o.vertexColor.b*_Lecture6*node_638));
                v.vertex.xyz += (node_934*o.uv1.r*v.normal*_Distance);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                
                float nSign = sign( dot( viewDirection, i.normalDir ) ); // Reverse normal if this is a backface
                i.normalDir *= nSign;
                normalDirection *= nSign;
                
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Particles/Additive"
    CustomEditor "ShaderForgeMaterialInspector"
}
