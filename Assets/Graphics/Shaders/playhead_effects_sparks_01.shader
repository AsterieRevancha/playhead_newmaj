// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.05882353,fgcg:0.05882353,fgcb:0.08235294,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:34126,y:32631,varname:node_1,prsc:2|emission-4-OUT;n:type:ShaderForge.SFN_Color,id:2,x:33445,y:32594,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_9276,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Tex2d,id:3,x:33445,y:32771,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_2057,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-181-OUT;n:type:ShaderForge.SFN_Multiply,id:4,x:33840,y:32694,varname:node_4,prsc:2|A-2-RGB,B-186-OUT,C-15-R,D-21-OUT,E-134-OUT;n:type:ShaderForge.SFN_VertexColor,id:15,x:33445,y:32946,varname:node_15,prsc:2;n:type:ShaderForge.SFN_Vector1,id:21,x:33445,y:33091,varname:node_21,prsc:2,v1:2;n:type:ShaderForge.SFN_Panner,id:27,x:33161,y:32582,varname:node_27,prsc:2,spu:0,spv:4;n:type:ShaderForge.SFN_Slider,id:134,x:33288,y:33186,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_6471,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:177,x:33161,y:32956,varname:node_177,prsc:2|A-180-T,B-184-OUT,C-185-OUT;n:type:ShaderForge.SFN_Time,id:180,x:32919,y:32936,varname:node_180,prsc:2;n:type:ShaderForge.SFN_Add,id:181,x:33161,y:32771,varname:node_181,prsc:2|A-182-UVOUT,B-177-OUT;n:type:ShaderForge.SFN_TexCoord,id:182,x:32919,y:32771,varname:node_182,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector2,id:184,x:32919,y:33083,varname:node_184,prsc:2,v1:0,v2:1;n:type:ShaderForge.SFN_ValueProperty,id:185,x:32919,y:33210,ptovrint:False,ptlb:Speed,ptin:_Speed,varname:node_3673,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:4;n:type:ShaderForge.SFN_Add,id:186,x:33775,y:32874,varname:node_186,prsc:2|A-3-R,B-15-B;proporder:2-3-134-185;pass:END;sub:END;*/

Shader "_PlayHead_/effects/sparks_01" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Opacity ("Opacity", Range(0, 1)) = 0
        _Speed ("Speed", Float ) = 4
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Opacity;
            uniform float _Speed;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 node_180 = _Time + _TimeEditor;
                float2 node_181 = (i.uv0+(node_180.g*float2(0,1)*_Speed));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_181, _MainTex));
                float3 emissive = (_Color.rgb*(_MainTex_var.r+i.vertexColor.b)*i.vertexColor.r*2.0*_Opacity);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
