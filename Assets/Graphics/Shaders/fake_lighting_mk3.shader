// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:_PlayHead_/fake_lighting_mk3_nobump,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:False,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.003,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:56,x:34535,y:32814,varname:node_56,prsc:2|normal-295-RGB,emission-308-OUT;n:type:ShaderForge.SFN_NormalVector,id:57,x:29936,y:32389,prsc:2,pt:True;n:type:ShaderForge.SFN_Vector4Property,id:58,x:29953,y:32746,ptovrint:False,ptlb:LightDirection1,ptin:_LightDirection1,varname:node_2102,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:-0.5,v2:0.866,v3:0,v4:0;n:type:ShaderForge.SFN_Distance,id:60,x:30532,y:32186,varname:node_60,prsc:2|A-57-OUT,B-58-XYZ;n:type:ShaderForge.SFN_Multiply,id:61,x:30832,y:32154,varname:node_61,prsc:2|A-62-OUT,B-60-OUT;n:type:ShaderForge.SFN_Vector1,id:62,x:30633,y:32097,varname:node_62,prsc:2,v1:0.5;n:type:ShaderForge.SFN_FragmentPosition,id:65,x:29543,y:33568,varname:node_65,prsc:2;n:type:ShaderForge.SFN_Append,id:68,x:29758,y:33568,varname:node_68,prsc:2|A-65-X,B-65-Y;n:type:ShaderForge.SFN_Normalize,id:70,x:29957,y:33568,varname:node_70,prsc:2|IN-68-OUT;n:type:ShaderForge.SFN_Distance,id:76,x:30572,y:33340,varname:node_76,prsc:2|A-58-XYZ,B-70-OUT;n:type:ShaderForge.SFN_Subtract,id:79,x:30840,y:33320,varname:node_79,prsc:2|A-234-OUT,B-76-OUT;n:type:ShaderForge.SFN_Multiply,id:81,x:32660,y:32931,varname:node_81,prsc:2|A-287-OUT,B-177-RGB,C-856-OUT,D-1532-OUT,E-289-OUT;n:type:ShaderForge.SFN_Clamp01,id:89,x:31000,y:33320,varname:node_89,prsc:2|IN-79-OUT;n:type:ShaderForge.SFN_Vector4Property,id:100,x:29953,y:32948,ptovrint:False,ptlb:LightDirection2,ptin:_LightDirection2,varname:node_6818,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5,v2:0.866,v3:0,v4:0;n:type:ShaderForge.SFN_Vector4Property,id:102,x:29953,y:33159,ptovrint:False,ptlb:LightDirection3,ptin:_LightDirection3,varname:node_2304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_OneMinus,id:155,x:31066,y:32260,varname:node_155,prsc:2|IN-61-OUT;n:type:ShaderForge.SFN_OneMinus,id:171,x:31188,y:33385,varname:node_171,prsc:2|IN-89-OUT;n:type:ShaderForge.SFN_Color,id:177,x:32429,y:33201,ptovrint:False,ptlb:LightColor,ptin:_LightColor,varname:node_3784,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:179,x:32414,y:33471,ptovrint:False,ptlb:BounceColor,ptin:_BounceColor,varname:node_2434,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_RemapRange,id:180,x:32421,y:33789,varname:node_180,prsc:2,frmn:0.75,frmx:0,tomn:0,tomx:0.5|IN-289-OUT;n:type:ShaderForge.SFN_Multiply,id:181,x:32638,y:33620,varname:node_181,prsc:2|A-179-RGB,B-180-OUT,C-985-OUT;n:type:ShaderForge.SFN_Max,id:185,x:32972,y:33288,varname:node_185,prsc:2|B-181-OUT;n:type:ShaderForge.SFN_Slider,id:186,x:31035,y:32763,ptovrint:False,ptlb:Light4,ptin:_Light4,varname:node_3006,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:187,x:31380,y:33300,varname:node_187,prsc:2|A-186-OUT,B-89-OUT;n:type:ShaderForge.SFN_Distance,id:190,x:30572,y:33503,varname:node_190,prsc:2|A-100-XYZ,B-70-OUT;n:type:ShaderForge.SFN_Subtract,id:192,x:30839,y:33529,varname:node_192,prsc:2|A-234-OUT,B-190-OUT;n:type:ShaderForge.SFN_Clamp01,id:194,x:31000,y:33529,varname:node_194,prsc:2|IN-192-OUT;n:type:ShaderForge.SFN_Multiply,id:196,x:31380,y:33543,varname:node_196,prsc:2|A-198-OUT,B-194-OUT;n:type:ShaderForge.SFN_Slider,id:198,x:30963,y:32856,ptovrint:False,ptlb:Light5,ptin:_Light5,varname:node_5792,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.6317097,max:1;n:type:ShaderForge.SFN_Distance,id:205,x:30532,y:32352,varname:node_205,prsc:2|A-57-OUT,B-100-XYZ;n:type:ShaderForge.SFN_Multiply,id:207,x:30832,y:32347,varname:node_207,prsc:2|A-62-OUT,B-205-OUT;n:type:ShaderForge.SFN_OneMinus,id:211,x:31042,y:32440,varname:node_211,prsc:2|IN-207-OUT;n:type:ShaderForge.SFN_Add,id:214,x:31904,y:32518,varname:node_214,prsc:2|A-244-OUT,B-246-OUT,C-260-OUT;n:type:ShaderForge.SFN_Add,id:215,x:31872,y:33646,varname:node_215,prsc:2|A-187-OUT,B-196-OUT,C-254-OUT;n:type:ShaderForge.SFN_OneMinus,id:228,x:31166,y:33601,varname:node_228,prsc:2|IN-194-OUT;n:type:ShaderForge.SFN_Vector1,id:234,x:30649,y:33254,varname:node_234,prsc:2,v1:1.85;n:type:ShaderForge.SFN_Slider,id:239,x:31035,y:32944,ptovrint:False,ptlb:Light6,ptin:_Light6,varname:node_4592,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2070584,max:1;n:type:ShaderForge.SFN_Distance,id:241,x:30561,y:33693,varname:node_241,prsc:2|A-102-XYZ,B-70-OUT;n:type:ShaderForge.SFN_Distance,id:243,x:30532,y:32528,varname:node_243,prsc:2|A-57-OUT,B-102-XYZ;n:type:ShaderForge.SFN_Multiply,id:244,x:31510,y:32088,varname:node_244,prsc:2|A-61-OUT,B-186-OUT;n:type:ShaderForge.SFN_Multiply,id:246,x:31509,y:32357,varname:node_246,prsc:2|A-207-OUT,B-198-OUT;n:type:ShaderForge.SFN_Subtract,id:248,x:30823,y:33741,varname:node_248,prsc:2|A-234-OUT,B-241-OUT;n:type:ShaderForge.SFN_Clamp01,id:250,x:30984,y:33741,varname:node_250,prsc:2|IN-248-OUT;n:type:ShaderForge.SFN_OneMinus,id:252,x:31153,y:33827,varname:node_252,prsc:2|IN-250-OUT;n:type:ShaderForge.SFN_Multiply,id:254,x:31380,y:33767,varname:node_254,prsc:2|A-239-OUT,B-250-OUT;n:type:ShaderForge.SFN_Multiply,id:256,x:30832,y:32514,varname:node_256,prsc:2|A-62-OUT,B-243-OUT;n:type:ShaderForge.SFN_OneMinus,id:258,x:31042,y:32607,varname:node_258,prsc:2|IN-256-OUT;n:type:ShaderForge.SFN_Multiply,id:260,x:31509,y:32614,varname:node_260,prsc:2|A-256-OUT,B-239-OUT;n:type:ShaderForge.SFN_Multiply,id:263,x:31613,y:32224,varname:node_263,prsc:2|A-155-OUT,B-281-OUT;n:type:ShaderForge.SFN_Multiply,id:265,x:31610,y:32482,varname:node_265,prsc:2|A-211-OUT,B-283-OUT;n:type:ShaderForge.SFN_Multiply,id:267,x:31593,y:32762,varname:node_267,prsc:2|A-258-OUT,B-285-OUT;n:type:ShaderForge.SFN_Multiply,id:269,x:31509,y:33427,varname:node_269,prsc:2|A-281-OUT,B-171-OUT;n:type:ShaderForge.SFN_Multiply,id:271,x:31521,y:33647,varname:node_271,prsc:2|A-283-OUT,B-228-OUT;n:type:ShaderForge.SFN_Multiply,id:273,x:31521,y:33892,varname:node_273,prsc:2|A-285-OUT,B-252-OUT;n:type:ShaderForge.SFN_Slider,id:281,x:30963,y:33041,ptovrint:False,ptlb:Light1,ptin:_Light1,varname:node_3056,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:283,x:31045,y:33127,ptovrint:False,ptlb:Light2,ptin:_Light2,varname:node_5597,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:285,x:30934,y:33226,ptovrint:False,ptlb:Light3,ptin:_Light3,varname:node_8520,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:287,x:31904,y:32690,varname:node_287,prsc:2|A-214-OUT,B-263-OUT,C-265-OUT,D-267-OUT;n:type:ShaderForge.SFN_Add,id:289,x:31872,y:33792,varname:node_289,prsc:2|A-215-OUT,B-269-OUT,C-271-OUT,D-273-OUT;n:type:ShaderForge.SFN_Color,id:292,x:33370,y:32862,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_8958,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:293,x:33627,y:33240,varname:node_293,prsc:2|A-292-RGB,B-995-RGB,C-81-OUT;n:type:ShaderForge.SFN_Tex2d,id:295,x:33425,y:32430,ptovrint:False,ptlb:BumpMap,ptin:_BumpMap,varname:node_7988,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1d19c24fe0ff370449f9381936a0bf16,ntxv:3,isnm:True;n:type:ShaderForge.SFN_VertexColor,id:301,x:33280,y:33379,varname:node_301,prsc:2;n:type:ShaderForge.SFN_Color,id:306,x:33280,y:33588,ptovrint:False,ptlb:Glow,ptin:_Glow,varname:node_7415,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:0;n:type:ShaderForge.SFN_Add,id:308,x:34184,y:33243,varname:node_308,prsc:2|A-1013-OUT,B-293-OUT,C-309-OUT,D-3555-OUT;n:type:ShaderForge.SFN_Multiply,id:309,x:33648,y:33503,varname:node_309,prsc:2|A-301-R,B-306-RGB,C-306-A;n:type:ShaderForge.SFN_Tex2d,id:684,x:32791,y:32477,varname:node_3445,prsc:2,ntxv:0,isnm:False|UVIN-704-OUT,TEX-879-TEX;n:type:ShaderForge.SFN_FragmentPosition,id:692,x:31840,y:32140,varname:node_692,prsc:2;n:type:ShaderForge.SFN_Append,id:693,x:32239,y:32295,varname:node_693,prsc:2|A-692-Y,B-1029-OUT;n:type:ShaderForge.SFN_Vector2,id:703,x:32239,y:32432,varname:node_703,prsc:2,v1:0.038,v2:0.125;n:type:ShaderForge.SFN_Multiply,id:704,x:32424,y:32295,varname:node_704,prsc:2|A-693-OUT,B-703-OUT;n:type:ShaderForge.SFN_Append,id:765,x:32239,y:32531,varname:node_765,prsc:2|A-692-X,B-1029-OUT;n:type:ShaderForge.SFN_Multiply,id:767,x:32424,y:32475,varname:node_767,prsc:2|A-703-OUT,B-765-OUT;n:type:ShaderForge.SFN_ChannelBlend,id:856,x:33088,y:32493,varname:node_856,prsc:2,chbt:0|M-896-OUT,R-684-R,G-884-G,B-909-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:879,x:32551,y:32615,ptovrint:False,ptlb:Cookie,ptin:_Cookie,varname:node_2235,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:884,x:32791,y:32632,varname:node_1048,prsc:2,ntxv:0,isnm:False|UVIN-767-OUT,TEX-879-TEX;n:type:ShaderForge.SFN_NormalVector,id:894,x:32239,y:32139,prsc:2,pt:False;n:type:ShaderForge.SFN_Abs,id:895,x:32424,y:32139,varname:node_895,prsc:2|IN-894-OUT;n:type:ShaderForge.SFN_Multiply,id:896,x:32620,y:32139,varname:node_896,prsc:2|A-895-OUT,B-895-OUT;n:type:ShaderForge.SFN_Vector1,id:909,x:32669,y:32395,varname:node_909,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:985,x:32026,y:33084,varname:node_985,prsc:2|A-856-OUT,B-986-OUT;n:type:ShaderForge.SFN_Vector1,id:986,x:31723,y:33063,varname:node_986,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Tex2d,id:995,x:33325,y:33045,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_7853,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:d7a9fdd65ca1e354584b3060380d8b95,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1004,x:33655,y:32654,ptovrint:False,ptlb:Emission,ptin:_Emission,varname:node_6138,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1bb83221e3bb1274ba32f7f2b1f8c543,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1013,x:33980,y:32937,varname:node_1013,prsc:2|A-1004-RGB,B-1019-RGB,C-1019-A;n:type:ShaderForge.SFN_Color,id:1019,x:33655,y:32898,ptovrint:False,ptlb:EmissionColor,ptin:_EmissionColor,varname:node_5098,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Subtract,id:1029,x:32054,y:32345,varname:node_1029,prsc:2|A-692-Z,B-1030-Z;n:type:ShaderForge.SFN_ObjectPosition,id:1030,x:31857,y:32370,varname:node_1030,prsc:2;n:type:ShaderForge.SFN_Length,id:1043,x:32239,y:31977,varname:node_1043,prsc:2|IN-1055-OUT;n:type:ShaderForge.SFN_Append,id:1055,x:32062,y:31987,varname:node_1055,prsc:2|A-692-X,B-692-Y;n:type:ShaderForge.SFN_RemapRange,id:1057,x:32442,y:31959,varname:node_1057,prsc:2,frmn:3,frmx:6,tomn:1,tomx:0.2|IN-1043-OUT;n:type:ShaderForge.SFN_Clamp01,id:1532,x:33088,y:32378,varname:node_1532,prsc:2|IN-1057-OUT;n:type:ShaderForge.SFN_Vector1,id:3555,x:34149,y:33430,varname:node_3555,prsc:2,v1:0.008;proporder:292-177-179-281-283-285-186-198-239-58-100-102-295-306-879-995-1004-1019;pass:END;sub:END;*/

Shader "_PlayHead_/fake_lighting_mk3" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _LightColor ("LightColor", Color) = (0.5,0.5,0.5,1)
        _BounceColor ("BounceColor", Color) = (0.5,0.5,0.5,1)
        _Light1 ("Light1", Range(0, 1)) = 0
        _Light2 ("Light2", Range(0, 1)) = 0
        _Light3 ("Light3", Range(0, 1)) = 0
        _Light4 ("Light4", Range(0, 1)) = 1
        _Light5 ("Light5", Range(0, 1)) = 0.6317097
        _Light6 ("Light6", Range(0, 1)) = 0.2070584
        _LightDirection1 ("LightDirection1", Vector) = (-0.5,0.866,0,0)
        _LightDirection2 ("LightDirection2", Vector) = (0.5,0.866,0,0)
        _LightDirection3 ("LightDirection3", Vector) = (1,0,0,0)
        _BumpMap ("BumpMap", 2D) = "bump" {}
        _Glow ("Glow", Color) = (0.5,0.5,0.5,0)
        _Cookie ("Cookie", 2D) = "white" {}
        _MainTex ("MainTex", 2D) = "white" {}
        _Emission ("Emission", 2D) = "black" {}
        _EmissionColor ("EmissionColor", Color) = (0.5,0.5,0.5,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _LightDirection1;
            uniform float4 _LightDirection2;
            uniform float4 _LightDirection3;
            uniform float4 _LightColor;
            uniform float _Light4;
            uniform float _Light5;
            uniform float _Light6;
            uniform float _Light1;
            uniform float _Light2;
            uniform float _Light3;
            uniform float4 _Color;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float4 _Glow;
            uniform sampler2D _Cookie; uniform float4 _Cookie_ST;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _Emission; uniform float4 _Emission_ST;
            uniform float4 _EmissionColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 objPos = mul ( _Object2World, float4(0,0,0,1) );
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/////// Vectors:
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 normalLocal = _BumpMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
////// Lighting:
////// Emissive:
                float4 _Emission_var = tex2D(_Emission,TRANSFORM_TEX(i.uv0, _Emission));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_62 = 0.5;
                float node_61 = (node_62*distance(normalDirection,_LightDirection1.rgb));
                float node_207 = (node_62*distance(normalDirection,_LightDirection2.rgb));
                float node_256 = (node_62*distance(normalDirection,_LightDirection3.rgb));
                float3 node_895 = abs(i.normalDir);
                float3 node_896 = (node_895*node_895);
                float node_1029 = (i.posWorld.b-objPos.b);
                float2 node_703 = float2(0.038,0.125);
                float2 node_704 = (float2(i.posWorld.g,node_1029)*node_703);
                float4 node_3445 = tex2D(_Cookie,TRANSFORM_TEX(node_704, _Cookie));
                float2 node_767 = (node_703*float2(i.posWorld.r,node_1029));
                float4 node_1048 = tex2D(_Cookie,TRANSFORM_TEX(node_767, _Cookie));
                float node_856 = (node_896.r*node_3445.r + node_896.g*node_1048.g + node_896.b*0.1);
                float node_234 = 1.85;
                float2 node_70 = normalize(float2(i.posWorld.r,i.posWorld.g));
                float node_89 = saturate((node_234-distance(_LightDirection1.rgb,node_70)));
                float node_194 = saturate((node_234-distance(_LightDirection2.rgb,node_70)));
                float node_250 = saturate((node_234-distance(_LightDirection3.rgb,node_70)));
                float node_289 = (((_Light4*node_89)+(_Light5*node_194)+(_Light6*node_250))+(_Light1*(1.0 - node_89))+(_Light2*(1.0 - node_194))+(_Light3*(1.0 - node_250)));
                float3 emissive = ((_Emission_var.rgb*_EmissionColor.rgb*_EmissionColor.a)+(_Color.rgb*_MainTex_var.rgb*((((node_61*_Light4)+(node_207*_Light5)+(node_256*_Light6))+((1.0 - node_61)*_Light1)+((1.0 - node_207)*_Light2)+((1.0 - node_256)*_Light3))*_LightColor.rgb*node_856*saturate((length(float2(i.posWorld.r,i.posWorld.g))*-0.2666667+1.8))*node_289))+(i.vertexColor.r*_Glow.rgb*_Glow.a)+0.008);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/fake_lighting_mk3_nobump"
    CustomEditor "ShaderForgeMaterialInspector"
}
