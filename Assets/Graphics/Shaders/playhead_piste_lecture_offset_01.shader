// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Particles/Additive,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:153,x:34062,y:32719,varname:node_153,prsc:2|emission-156-OUT,voffset-651-OUT;n:type:ShaderForge.SFN_Color,id:154,x:33414,y:32335,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_4121,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:156,x:33672,y:32571,varname:node_156,prsc:2|A-154-RGB,B-185-RGB,C-563-OUT;n:type:ShaderForge.SFN_Multiply,id:157,x:33169,y:32970,varname:node_157,prsc:2|A-175-R,B-161-OUT;n:type:ShaderForge.SFN_Multiply,id:159,x:33169,y:33122,varname:node_159,prsc:2|A-175-G,B-163-OUT;n:type:ShaderForge.SFN_Slider,id:161,x:32527,y:33098,ptovrint:False,ptlb:Lecture1,ptin:_Lecture1,varname:node_9669,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:163,x:32527,y:33217,ptovrint:False,ptlb:Lecture2,ptin:_Lecture2,varname:node_6090,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:165,x:32527,y:33329,ptovrint:False,ptlb:Lecture3,ptin:_Lecture3,varname:node_2750,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:167,x:32527,y:33450,ptovrint:False,ptlb:Lecture4,ptin:_Lecture4,varname:node_9706,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:171,x:33169,y:33267,varname:node_171,prsc:2|A-175-B,B-165-OUT;n:type:ShaderForge.SFN_Multiply,id:173,x:33169,y:33427,varname:node_173,prsc:2|A-175-A,B-167-OUT;n:type:ShaderForge.SFN_Add,id:174,x:33406,y:33041,varname:node_174,prsc:2|A-157-OUT,B-159-OUT,C-171-OUT,D-173-OUT;n:type:ShaderForge.SFN_VertexColor,id:175,x:32684,y:32925,varname:node_175,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:176,x:33406,y:33350,prsc:2,pt:False;n:type:ShaderForge.SFN_TexCoord,id:181,x:33406,y:33183,varname:node_181,prsc:2,uv:1;n:type:ShaderForge.SFN_Multiply,id:184,x:33661,y:33041,varname:node_184,prsc:2|A-174-OUT,B-181-U,C-176-OUT,D-524-OUT;n:type:ShaderForge.SFN_Tex2d,id:185,x:33390,y:32500,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_8882,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ValueProperty,id:524,x:33661,y:33208,ptovrint:False,ptlb:Distance,ptin:_Distance,varname:node_2310,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.77;n:type:ShaderForge.SFN_Slider,id:533,x:32527,y:32445,ptovrint:False,ptlb:Fader1,ptin:_Fader1,varname:node_5696,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:535,x:32527,y:32568,ptovrint:False,ptlb:Fader2,ptin:_Fader2,varname:node_2823,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:537,x:32527,y:32697,ptovrint:False,ptlb:Fader3,ptin:_Fader3,varname:node_3097,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:539,x:32527,y:32816,ptovrint:False,ptlb:Fader4,ptin:_Fader4,varname:node_2062,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:546,x:33040,y:32813,varname:node_546,prsc:2|A-539-OUT,B-175-A;n:type:ShaderForge.SFN_Multiply,id:548,x:33040,y:32672,varname:node_548,prsc:2|A-537-OUT,B-175-B;n:type:ShaderForge.SFN_Multiply,id:550,x:33040,y:32531,varname:node_550,prsc:2|A-535-OUT,B-175-G;n:type:ShaderForge.SFN_Multiply,id:552,x:33040,y:32393,varname:node_552,prsc:2|A-533-OUT,B-175-R;n:type:ShaderForge.SFN_Add,id:563,x:33350,y:32705,varname:node_563,prsc:2|A-552-OUT,B-550-OUT,C-548-OUT,D-546-OUT;n:type:ShaderForge.SFN_Add,id:651,x:33859,y:32948,varname:node_651,prsc:2|A-652-OUT,B-184-OUT;n:type:ShaderForge.SFN_Multiply,id:652,x:33661,y:32864,varname:node_652,prsc:2|A-653-OUT,B-181-U,C-176-OUT;n:type:ShaderForge.SFN_ValueProperty,id:653,x:33406,y:32864,ptovrint:False,ptlb:StartOffset,ptin:_StartOffset,varname:node_827,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:-0.77;proporder:154-185-161-533-163-535-165-537-167-539-524-653;pass:END;sub:END;*/

Shader "_PlayHead_/pistes/lecture_offset_01" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Lecture1 ("Lecture1", Range(0, 1)) = 0
        _Fader1 ("Fader1", Range(0, 1)) = 1
        _Lecture2 ("Lecture2", Range(0, 1)) = 0
        _Fader2 ("Fader2", Range(0, 1)) = 1
        _Lecture3 ("Lecture3", Range(0, 1)) = 0
        _Fader3 ("Fader3", Range(0, 1)) = 1
        _Lecture4 ("Lecture4", Range(0, 1)) = 0
        _Fader4 ("Fader4", Range(0, 1)) = 1
        _Distance ("Distance", Float ) = 0.77
        _StartOffset ("StartOffset", Float ) = -0.77
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float _Lecture1;
            uniform float _Lecture2;
            uniform float _Lecture3;
            uniform float _Lecture4;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Distance;
            uniform float _Fader1;
            uniform float _Fader2;
            uniform float _Fader3;
            uniform float _Fader4;
            uniform float _StartOffset;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += ((_StartOffset*o.uv1.r*v.normal)+(((o.vertexColor.r*_Lecture1)+(o.vertexColor.g*_Lecture2)+(o.vertexColor.b*_Lecture3)+(o.vertexColor.a*_Lecture4))*o.uv1.r*v.normal*_Distance));
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = (_Color.rgb*_MainTex_var.rgb*((_Fader1*i.vertexColor.r)+(_Fader2*i.vertexColor.g)+(_Fader3*i.vertexColor.b)+(_Fader4*i.vertexColor.a)));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _Lecture1;
            uniform float _Lecture2;
            uniform float _Lecture3;
            uniform float _Lecture4;
            uniform float _Distance;
            uniform float _StartOffset;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv1 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += ((_StartOffset*o.uv1.r*v.normal)+(((o.vertexColor.r*_Lecture1)+(o.vertexColor.g*_Lecture2)+(o.vertexColor.b*_Lecture3)+(o.vertexColor.a*_Lecture4))*o.uv1.r*v.normal*_Distance));
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Particles/Additive"
    CustomEditor "ShaderForgeMaterialInspector"
}
