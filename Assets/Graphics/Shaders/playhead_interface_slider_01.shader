// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Mobile/Particles/Additive,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:262,x:34033,y:32723,varname:node_262,prsc:2|emission-268-OUT;n:type:ShaderForge.SFN_Color,id:263,x:33367,y:32499,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_5694,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Tex2d,id:264,x:33229,y:32967,ptovrint:False,ptlb:Slider,ptin:_Slider,varname:node_7695,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False|UVIN-304-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:266,x:33344,y:32689,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_6991,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:fcf4e838ab87b3c438fb172ec5d2cffd,ntxv:1,isnm:False;n:type:ShaderForge.SFN_Multiply,id:267,x:33615,y:32784,varname:node_267,prsc:2|A-263-RGB,B-266-RGB,C-266-A,D-629-OUT;n:type:ShaderForge.SFN_Add,id:268,x:33831,y:32894,varname:node_268,prsc:2|A-267-OUT,B-270-OUT;n:type:ShaderForge.SFN_Color,id:269,x:33023,y:33195,ptovrint:False,ptlb:SliderColor,ptin:_SliderColor,varname:node_3396,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:270,x:33615,y:32973,varname:node_270,prsc:2|A-264-RGB,B-269-RGB,C-266-A,D-314-OUT,E-629-OUT;n:type:ShaderForge.SFN_Panner,id:304,x:33007,y:32967,varname:node_304,prsc:2,spu:1,spv:0|DIST-305-OUT;n:type:ShaderForge.SFN_Slider,id:305,x:32649,y:32991,ptovrint:False,ptlb:SliderPosition,ptin:_SliderPosition,varname:node_8266,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.5,cur:0,max:-0.5;n:type:ShaderForge.SFN_Slider,id:314,x:33020,y:33404,ptovrint:False,ptlb:SliderIntensity,ptin:_SliderIntensity,varname:node_3459,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:629,x:33072,y:32870,ptovrint:False,ptlb:Fader,ptin:_Fader,varname:node_9931,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;proporder:263-269-266-264-305-314-629;pass:END;sub:END;*/

Shader "_PlayHead_/interface/slider_01" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _SliderColor ("SliderColor", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "gray" {}
        _Slider ("Slider", 2D) = "black" {}
        _SliderPosition ("SliderPosition", Range(0.5, -0.5)) = 0
        _SliderIntensity ("SliderIntensity", Range(0, 1)) = 1
        _Fader ("Fader", Range(0, 1)) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _Slider; uniform float4 _Slider_ST;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _SliderColor;
            uniform float _SliderPosition;
            uniform float _SliderIntensity;
            uniform float _Fader;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float2 node_304 = (i.uv0+_SliderPosition*float2(1,0));
                float4 _Slider_var = tex2D(_Slider,TRANSFORM_TEX(node_304, _Slider));
                float3 emissive = ((_Color.rgb*_MainTex_var.rgb*_MainTex_var.a*_Fader)+(_Slider_var.rgb*_SliderColor.rgb*_MainTex_var.a*_SliderIntensity*_Fader));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Mobile/Particles/Additive"
    CustomEditor "ShaderForgeMaterialInspector"
}
