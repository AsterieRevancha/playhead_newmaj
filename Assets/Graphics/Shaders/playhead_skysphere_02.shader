// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:0,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:False,igpj:False,qofs:-999,qpre:0,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:33792,y:32993,varname:node_1,prsc:2|emission-6184-OUT;n:type:ShaderForge.SFN_Color,id:40,x:32267,y:32941,ptovrint:False,ptlb:SkyTop,ptin:_SkyTop,varname:node_5224,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.04705882,c2:0.05882353,c3:0.1333333,c4:1;n:type:ShaderForge.SFN_VertexColor,id:2555,x:32208,y:33155,varname:node_2555,prsc:2;n:type:ShaderForge.SFN_Color,id:6175,x:32681,y:32789,ptovrint:False,ptlb:FogColor,ptin:_FogColor,varname:node_4503,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.05882353,c2:0.05882353,c3:0.08235294,c4:1;n:type:ShaderForge.SFN_Lerp,id:6183,x:33102,y:33019,varname:node_6183,prsc:2|A-6175-RGB,B-6192-OUT,T-2555-A;n:type:ShaderForge.SFN_Add,id:6184,x:33284,y:33229,varname:node_6184,prsc:2|A-6183-OUT,B-6185-OUT;n:type:ShaderForge.SFN_Multiply,id:6185,x:33076,y:33312,varname:node_6185,prsc:2|A-2555-B,B-2555-B,C-6186-RGB;n:type:ShaderForge.SFN_Color,id:6186,x:32807,y:33482,ptovrint:False,ptlb:Horizon,ptin:_Horizon,varname:node_8602,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:6188,x:32843,y:33194,varname:node_6188,prsc:2|A-2555-R,B-2555-R;n:type:ShaderForge.SFN_Color,id:6190,x:32522,y:33383,ptovrint:False,ptlb:Atmosphere,ptin:_Atmosphere,varname:node_4010,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Lerp,id:6192,x:32856,y:33042,varname:node_6192,prsc:2|A-40-RGB,B-6190-RGB,T-6188-OUT;proporder:40-6190-6186-6175;pass:END;sub:END;*/

Shader "_PlayHead_/paysage/skysphere_02" {
    Properties {
        _SkyTop ("SkyTop", Color) = (0.04705882,0.05882353,0.1333333,1)
        _Atmosphere ("Atmosphere", Color) = (0.5,0.5,0.5,1)
        _Horizon ("Horizon", Color) = (0.5,0.5,0.5,1)
        _FogColor ("FogColor", Color) = (0.05882353,0.05882353,0.08235294,1)
    }
    SubShader {
        Tags {
            "Queue"="Background-999"
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZTest Less
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _SkyTop;
            uniform float4 _FogColor;
            uniform float4 _Horizon;
            uniform float4 _Atmosphere;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float3 emissive = (lerp(_FogColor.rgb,lerp(_SkyTop.rgb,_Atmosphere.rgb,(i.vertexColor.r*i.vertexColor.r)),i.vertexColor.a)+(i.vertexColor.b*i.vertexColor.b*_Horizon.rgb));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
