// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:5130,x:33558,y:32712,varname:node_5130,prsc:2|emission-5136-OUT;n:type:ShaderForge.SFN_Color,id:5131,x:32908,y:32689,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_3677,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:5132,x:33187,y:32533,ptovrint:False,ptlb:FogColor,ptin:_FogColor,varname:node_7023,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.05882353,c2:0.05882353,c3:0.08235294,c4:1;n:type:ShaderForge.SFN_Tex2d,id:5133,x:32908,y:32879,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_4003,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:5134,x:33175,y:32756,varname:node_5134,prsc:2|A-5131-RGB,B-5133-RGB;n:type:ShaderForge.SFN_VertexColor,id:5135,x:33037,y:33005,varname:node_5135,prsc:2;n:type:ShaderForge.SFN_Lerp,id:5136,x:33385,y:32712,varname:node_5136,prsc:2|A-5132-RGB,B-5134-OUT,T-5165-OUT;n:type:ShaderForge.SFN_Slider,id:5160,x:32893,y:33180,ptovrint:False,ptlb:FogPower,ptin:_FogPower,varname:node_4261,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:5;n:type:ShaderForge.SFN_Clamp01,id:5165,x:33282,y:32927,varname:node_5165,prsc:2|IN-5166-OUT;n:type:ShaderForge.SFN_Multiply,id:5166,x:33282,y:33084,varname:node_5166,prsc:2|A-5135-A,B-5160-OUT;proporder:5131-5133-5132-5160;pass:END;sub:END;*/

Shader "_PlayHead_/studio/opaque_fog" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _FogColor ("FogColor", Color) = (0.05882353,0.05882353,0.08235294,1)
        _FogPower ("FogPower", Range(0, 5)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float4 _FogColor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _FogPower;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = lerp(_FogColor.rgb,(_Color.rgb*_MainTex_var.rgb),saturate((i.vertexColor.a*_FogPower)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
