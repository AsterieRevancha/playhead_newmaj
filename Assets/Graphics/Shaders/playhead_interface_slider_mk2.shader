// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:_PlayHead_/interface/slider_mk2_noglitch,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:262,x:35278,y:32751,varname:node_262,prsc:2|emission-744-OUT,alpha-653-OUT;n:type:ShaderForge.SFN_Color,id:263,x:34299,y:32499,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_8891,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Tex2d,id:264,x:34161,y:32967,ptovrint:False,ptlb:Slider,ptin:_Slider,varname:node_7935,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False|UVIN-304-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:266,x:34242,y:32661,varname:node_5649,prsc:2,tex:ec636699f97c1bd4b93644a206d7118d,ntxv:1,isnm:False|TEX-693-TEX;n:type:ShaderForge.SFN_Multiply,id:267,x:34734,y:32759,varname:node_267,prsc:2|A-263-RGB,B-732-OUT,C-681-OUT;n:type:ShaderForge.SFN_Color,id:269,x:34109,y:33133,ptovrint:False,ptlb:SliderColor,ptin:_SliderColor,varname:node_4782,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:270,x:34532,y:33063,varname:node_270,prsc:2|A-264-R,B-266-G,C-314-OUT;n:type:ShaderForge.SFN_Panner,id:304,x:33939,y:32967,varname:node_304,prsc:2,spu:1,spv:0|DIST-305-OUT;n:type:ShaderForge.SFN_Slider,id:305,x:33730,y:33216,ptovrint:False,ptlb:SliderPosition,ptin:_SliderPosition,varname:node_1930,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.5,cur:0,max:-0.5;n:type:ShaderForge.SFN_Slider,id:314,x:33952,y:33404,ptovrint:False,ptlb:SliderIntensity,ptin:_SliderIntensity,varname:node_6421,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:629,x:34721,y:33249,ptovrint:False,ptlb:Fader,ptin:_Fader,varname:node_5646,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:-2;n:type:ShaderForge.SFN_Add,id:653,x:34911,y:33043,varname:node_653,prsc:2|A-266-B,B-270-OUT,C-629-OUT;n:type:ShaderForge.SFN_Vector1,id:681,x:34475,y:32927,varname:node_681,prsc:2,v1:2;n:type:ShaderForge.SFN_Tex2dAsset,id:693,x:33656,y:32408,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_6419,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ec636699f97c1bd4b93644a206d7118d,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:702,x:33656,y:32650,varname:node_3462,prsc:2,tex:ec636699f97c1bd4b93644a206d7118d,ntxv:0,isnm:False|UVIN-705-OUT,TEX-693-TEX;n:type:ShaderForge.SFN_TexCoord,id:704,x:33047,y:32653,varname:node_704,prsc:2,uv:0;n:type:ShaderForge.SFN_Add,id:705,x:33436,y:32675,varname:node_705,prsc:2|A-709-OUT,B-706-OUT;n:type:ShaderForge.SFN_Multiply,id:706,x:33255,y:32691,varname:node_706,prsc:2|A-704-UVOUT,B-707-OUT;n:type:ShaderForge.SFN_Vector2,id:707,x:33088,y:32828,varname:node_707,prsc:2,v1:1,v2:0.5;n:type:ShaderForge.SFN_Append,id:709,x:33503,y:32840,varname:node_709,prsc:2|A-710-OUT,B-787-OUT;n:type:ShaderForge.SFN_Vector1,id:710,x:33320,y:32828,varname:node_710,prsc:2,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:711,x:33105,y:32934,ptovrint:False,ptlb:UVOffset,ptin:_UVOffset,varname:node_9806,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:732,x:34490,y:32774,varname:node_732,prsc:2|A-266-R,B-702-A;n:type:ShaderForge.SFN_Lerp,id:744,x:35002,y:32823,varname:node_744,prsc:2|A-267-OUT,B-269-RGB,T-270-OUT;n:type:ShaderForge.SFN_Panner,id:758,x:32758,y:33233,varname:node_758,prsc:2,spu:0,spv:-0.5;n:type:ShaderForge.SFN_Slider,id:785,x:32669,y:33116,ptovrint:False,ptlb:Glitch,ptin:_Glitch,varname:node_911,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.1;n:type:ShaderForge.SFN_Multiply,id:786,x:33296,y:33158,varname:node_786,prsc:2|A-827-OUT,B-785-OUT;n:type:ShaderForge.SFN_Add,id:787,x:33320,y:32934,varname:node_787,prsc:2|A-711-OUT,B-868-OUT,C-786-OUT;n:type:ShaderForge.SFN_Noise,id:827,x:33069,y:33212,varname:node_827,prsc:2|XY-758-UVOUT;n:type:ShaderForge.SFN_Multiply,id:868,x:33069,y:33013,varname:node_868,prsc:2|A-871-OUT,B-785-OUT;n:type:ShaderForge.SFN_Vector1,id:871,x:32801,y:33001,varname:node_871,prsc:2,v1:-0.5;proporder:263-269-693-264-711-305-314-629-785;pass:END;sub:END;*/

Shader "_PlayHead_/interface/slider_mk2" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _SliderColor ("SliderColor", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Slider ("Slider", 2D) = "black" {}
        _UVOffset ("UVOffset", Float ) = 0
        _SliderPosition ("SliderPosition", Range(0.5, -0.5)) = 0
        _SliderIntensity ("SliderIntensity", Range(0, 1)) = 1
        _Fader ("Fader", Range(0, -2)) = 0
        _Glitch ("Glitch", Range(0, 0.1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _Slider; uniform float4 _Slider_ST;
            uniform float4 _SliderColor;
            uniform float _SliderPosition;
            uniform float _SliderIntensity;
            uniform float _Fader;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _UVOffset;
            uniform float _Glitch;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 node_5649 = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 node_8078 = _Time + _TimeEditor;
                float2 node_758 = (i.uv0+node_8078.g*float2(0,-0.5));
                float2 node_827_skew = node_758 + 0.2127+node_758.x*0.3713*node_758.y;
                float2 node_827_rnd = 4.789*sin(489.123*(node_827_skew));
                float node_827 = frac(node_827_rnd.x*node_827_rnd.y*(1+node_827_skew.x));
                float2 node_705 = (float2(0.0,(_UVOffset+((-0.5)*_Glitch)+(node_827*_Glitch)))+(i.uv0*float2(1,0.5)));
                float4 node_3462 = tex2D(_MainTex,TRANSFORM_TEX(node_705, _MainTex));
                float2 node_304 = (i.uv0+_SliderPosition*float2(1,0));
                float4 _Slider_var = tex2D(_Slider,TRANSFORM_TEX(node_304, _Slider));
                float node_270 = (_Slider_var.r*node_5649.g*_SliderIntensity);
                float3 emissive = lerp((_Color.rgb*(node_5649.r+node_3462.a)*2.0),_SliderColor.rgb,node_270);
                float3 finalColor = emissive;
                return fixed4(finalColor,(node_5649.b+node_270+_Fader));
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/interface/slider_mk2_noglitch"
    CustomEditor "ShaderForgeMaterialInspector"
}
