// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:False,igpj:False,qofs:200,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:5494,x:33898,y:32732,varname:node_5494,prsc:2|emission-5670-OUT;n:type:ShaderForge.SFN_Tex2d,id:5495,x:32892,y:32721,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_965,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:1,isnm:False|UVIN-5496-UVOUT;n:type:ShaderForge.SFN_Panner,id:5496,x:32640,y:32721,varname:node_5496,prsc:2,spu:1,spv:-0.5|DIST-5647-OUT;n:type:ShaderForge.SFN_Desaturate,id:5608,x:33075,y:32856,varname:node_5608,prsc:2|COL-5495-RGB,DES-5609-OUT;n:type:ShaderForge.SFN_Slider,id:5609,x:32753,y:32970,ptovrint:False,ptlb:Saturation,ptin:_Saturation,varname:node_7291,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:0;n:type:ShaderForge.SFN_Slider,id:5632,x:32058,y:32919,ptovrint:False,ptlb:Speed,ptin:_Speed,varname:node_948,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:5;n:type:ShaderForge.SFN_Time,id:5643,x:32286,y:32695,varname:node_5643,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5647,x:32453,y:32840,varname:node_5647,prsc:2|A-5643-T,B-5632-OUT;n:type:ShaderForge.SFN_Add,id:5652,x:33477,y:33001,varname:node_5652,prsc:2|A-5654-OUT,B-5655-OUT;n:type:ShaderForge.SFN_Slider,id:5653,x:32834,y:33131,ptovrint:False,ptlb:Contrast,ptin:_Contrast,varname:node_1302,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Multiply,id:5654,x:33281,y:32931,varname:node_5654,prsc:2|A-5608-OUT,B-5653-OUT;n:type:ShaderForge.SFN_Slider,id:5655,x:32956,y:33266,ptovrint:False,ptlb:Luminosity,ptin:_Luminosity,varname:node_9635,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Color,id:5660,x:33055,y:32512,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_5288,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:5670,x:33659,y:32872,varname:node_5670,prsc:2|A-5660-RGB,B-5652-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5679,x:33699,y:33182,ptovrint:False,ptlb:EmissionLM,ptin:_EmissionLM,varname:node_6461,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;proporder:5495-5660-5609-5653-5655-5632-5679;pass:END;sub:END;*/

Shader "_PlayHead_/studio/screens" {
    Properties {
        _MainTex ("MainTex", 2D) = "gray" {}
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _Saturation ("Saturation", Range(1, 0)) = 1
        _Contrast ("Contrast", Range(0, 1)) = 0.5
        _Luminosity ("Luminosity", Range(0, 1)) = 0.5
        _Speed ("Speed", Range(0, 5)) = 0
        _EmissionLM ("EmissionLM", Float ) = 0
    }
    SubShader {
        Tags {
            "Queue"="Geometry+200"
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Saturation;
            uniform float _Speed;
            uniform float _Contrast;
            uniform float _Luminosity;
            uniform float4 _Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 node_5643 = _Time + _TimeEditor;
                float2 node_5496 = (i.uv0+(node_5643.g*_Speed)*float2(1,-0.5));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_5496, _MainTex));
                float3 emissive = (_Color.rgb*((lerp(_MainTex_var.rgb,dot(_MainTex_var.rgb,float3(0.3,0.59,0.11)),_Saturation)*_Contrast)+_Luminosity));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
