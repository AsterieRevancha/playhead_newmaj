// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:4467,x:34489,y:32680,varname:node_4467,prsc:2|emission-4493-OUT;n:type:ShaderForge.SFN_Color,id:4468,x:33156,y:32476,ptovrint:False,ptlb:ColorLight1,ptin:_ColorLight1,varname:node_9765,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4470,x:33021,y:32622,ptovrint:False,ptlb:ColorLight2,ptin:_ColorLight2,varname:node_3615,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4472,x:33158,y:32766,ptovrint:False,ptlb:ColorLight3,ptin:_ColorLight3,varname:node_9844,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4474,x:33016,y:32909,ptovrint:False,ptlb:ColorLight4,ptin:_ColorLight4,varname:node_7286,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4476,x:33152,y:33057,ptovrint:False,ptlb:ColorLight5,ptin:_ColorLight5,varname:node_5353,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4478,x:33011,y:33196,ptovrint:False,ptlb:ColorLight6,ptin:_ColorLight6,varname:node_7703,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_VertexColor,id:4479,x:33054,y:33395,varname:node_4479,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4480,x:33572,y:32475,varname:node_4480,prsc:2|A-4468-RGB,B-4518-OUT,C-4479-B,D-4479-A;n:type:ShaderForge.SFN_Add,id:4481,x:33977,y:32795,varname:node_4481,prsc:2|A-4480-OUT,B-4483-OUT,C-4487-OUT;n:type:ShaderForge.SFN_Multiply,id:4483,x:33472,y:32620,varname:node_4483,prsc:2|A-4470-RGB,B-4520-OUT,C-4479-R,D-4492-OUT;n:type:ShaderForge.SFN_Multiply,id:4485,x:33463,y:32904,varname:node_4485,prsc:2|A-4474-RGB,B-4524-OUT,C-4479-B,D-4492-OUT;n:type:ShaderForge.SFN_Multiply,id:4487,x:33580,y:32766,varname:node_4487,prsc:2|A-4472-RGB,B-4522-OUT,C-4479-G,D-4492-OUT;n:type:ShaderForge.SFN_Multiply,id:4489,x:33451,y:33194,varname:node_4489,prsc:2|A-4478-RGB,B-4528-OUT,C-4479-G,D-4479-A;n:type:ShaderForge.SFN_Multiply,id:4491,x:33573,y:33052,varname:node_4491,prsc:2|A-4476-RGB,B-4526-OUT,C-4479-R,D-4479-A;n:type:ShaderForge.SFN_OneMinus,id:4492,x:33111,y:33584,varname:node_4492,prsc:2|IN-4479-A;n:type:ShaderForge.SFN_Multiply,id:4493,x:34284,y:32825,varname:node_4493,prsc:2|A-4494-RGB,B-4505-OUT;n:type:ShaderForge.SFN_Tex2d,id:4494,x:34082,y:32629,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_9882,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:4505,x:34037,y:32967,varname:node_4505,prsc:2|A-4481-OUT,B-4485-OUT,C-4491-OUT,D-4489-OUT;n:type:ShaderForge.SFN_Slider,id:4518,x:32754,y:32530,ptovrint:False,ptlb:Light1,ptin:_Light1,varname:node_4449,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:4520,x:32680,y:32682,ptovrint:False,ptlb:Light2,ptin:_Light2,varname:node_6493,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:4522,x:32752,y:32810,ptovrint:False,ptlb:Light3,ptin:_Light3,varname:node_2716,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:4524,x:32669,y:32963,ptovrint:False,ptlb:Light4,ptin:_Light4,varname:node_253,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:4526,x:32731,y:33100,ptovrint:False,ptlb:Light5,ptin:_Light5,varname:node_1552,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:4528,x:32659,y:33249,ptovrint:False,ptlb:Light6,ptin:_Light6,varname:node_8534,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;proporder:4494-4468-4518-4470-4520-4472-4522-4474-4524-4476-4526-4478-4528;pass:END;sub:END;*/

Shader "_PlayHead_/cockpit_flares" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _ColorLight1 ("ColorLight1", Color) = (0.5,0.5,0.5,1)
        _Light1 ("Light1", Range(0, 1)) = 0
        _ColorLight2 ("ColorLight2", Color) = (0.5,0.5,0.5,1)
        _Light2 ("Light2", Range(0, 1)) = 0
        _ColorLight3 ("ColorLight3", Color) = (0.5,0.5,0.5,1)
        _Light3 ("Light3", Range(0, 1)) = 0
        _ColorLight4 ("ColorLight4", Color) = (0.5,0.5,0.5,1)
        _Light4 ("Light4", Range(0, 1)) = 0
        _ColorLight5 ("ColorLight5", Color) = (0.5,0.5,0.5,1)
        _Light5 ("Light5", Range(0, 1)) = 0
        _ColorLight6 ("ColorLight6", Color) = (0.5,0.5,0.5,1)
        _Light6 ("Light6", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _ColorLight1;
            uniform float4 _ColorLight2;
            uniform float4 _ColorLight3;
            uniform float4 _ColorLight4;
            uniform float4 _ColorLight5;
            uniform float4 _ColorLight6;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Light1;
            uniform float _Light2;
            uniform float _Light3;
            uniform float _Light4;
            uniform float _Light5;
            uniform float _Light6;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_4492 = (1.0 - i.vertexColor.a);
                float3 emissive = (_MainTex_var.rgb*(((_ColorLight1.rgb*_Light1*i.vertexColor.b*i.vertexColor.a)+(_ColorLight2.rgb*_Light2*i.vertexColor.r*node_4492)+(_ColorLight3.rgb*_Light3*i.vertexColor.g*node_4492))+(_ColorLight4.rgb*_Light4*i.vertexColor.b*node_4492)+(_ColorLight5.rgb*_Light5*i.vertexColor.r*i.vertexColor.a)+(_ColorLight6.rgb*_Light6*i.vertexColor.g*i.vertexColor.a)));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
