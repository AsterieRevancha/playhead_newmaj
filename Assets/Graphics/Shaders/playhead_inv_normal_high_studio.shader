// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:_PlayHead_/inv/normal_low,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:6,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:False,igpj:True,qofs:0,qpre:2,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:493,x:33540,y:32990,varname:node_493,prsc:2|emission-4575-OUT;n:type:ShaderForge.SFN_Color,id:495,x:31646,y:32735,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_6584,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.3,c3:0.3,c4:1;n:type:ShaderForge.SFN_Slider,id:554,x:31502,y:32621,ptovrint:False,ptlb:Transition,ptin:_Transition,varname:node_9519,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:564,x:31953,y:32841,varname:node_564,prsc:2|A-495-RGB,B-566-RGB,T-554-OUT;n:type:ShaderForge.SFN_Color,id:566,x:31636,y:32925,ptovrint:False,ptlb:ColorActive,ptin:_ColorActive,varname:node_1826,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Tex2d,id:4467,x:32135,y:32577,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_933,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:4575,x:33176,y:32893,varname:node_4575,prsc:2|A-5136-OUT,B-564-OUT,C-5442-R;n:type:ShaderForge.SFN_Fresnel,id:4902,x:32135,y:32404,varname:node_4902,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5023,x:32536,y:32671,varname:node_5023,prsc:2|A-4467-G,B-5411-OUT;n:type:ShaderForge.SFN_Max,id:5136,x:32848,y:32629,varname:node_5136,prsc:2|A-4467-R,B-5023-OUT;n:type:ShaderForge.SFN_Clamp01,id:5411,x:32551,y:32404,varname:node_5411,prsc:2|IN-5412-OUT;n:type:ShaderForge.SFN_RemapRange,id:5412,x:32351,y:32404,varname:node_5412,prsc:2,frmn:0.5,frmx:0.8,tomn:1,tomx:0|IN-4902-OUT;n:type:ShaderForge.SFN_VertexColor,id:5442,x:32969,y:32984,varname:node_5442,prsc:2;proporder:4467-495-566-554;pass:END;sub:END;*/

Shader "_PlayHead_/inv/normal_high_studio" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _Color ("Color", Color) = (1,0.3,0.3,1)
        _ColorActive ("ColorActive", Color) = (1,0,0,1)
        _Transition ("Transition", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="AlphaTest"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One OneMinusSrcColor
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float _Transition;
            uniform float4 _ColorActive;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = (max(_MainTex_var.r,(_MainTex_var.g*saturate(((1.0-max(0,dot(normalDirection, viewDirection)))*-3.333333+2.666667))))*lerp(_Color.rgb,_ColorActive.rgb,_Transition)*i.vertexColor.r);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/inv/normal_low"
    CustomEditor "ShaderForgeMaterialInspector"
}
