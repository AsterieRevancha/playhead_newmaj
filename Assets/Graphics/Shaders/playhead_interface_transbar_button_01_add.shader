// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:False,igpj:True,qofs:25,qpre:4,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.03921569,fgcg:0.03921569,fgcb:0.03921569,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:2,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:34012,y:32699,varname:node_1,prsc:2|emission-182-OUT,voffset-157-OUT;n:type:ShaderForge.SFN_Color,id:2,x:33151,y:32172,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_108,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Slider,id:3,x:33107,y:33072,ptovrint:False,ptlb:Alpha,ptin:_Alpha,varname:node_1090,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Tex2d,id:12,x:32890,y:32941,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_7444,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:17,x:33556,y:32967,varname:node_17,prsc:2|A-151-OUT,B-3-OUT,C-174-OUT;n:type:ShaderForge.SFN_Color,id:102,x:33065,y:32349,ptovrint:False,ptlb:HoverColor,ptin:_HoverColor,varname:node_4345,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9960784,c2:0.7803922,c3:0.3254902,c4:1;n:type:ShaderForge.SFN_Lerp,id:103,x:33405,y:32433,varname:node_103,prsc:2|A-2-RGB,B-102-RGB,T-104-OUT;n:type:ShaderForge.SFN_Slider,id:104,x:32120,y:32489,ptovrint:False,ptlb:Hover,ptin:_Hover,varname:node_6396,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Color,id:113,x:32919,y:32599,ptovrint:False,ptlb:ActiveColor,ptin:_ActiveColor,varname:node_7557,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.8078431,c2:0.9960784,c3:0.2,c4:1;n:type:ShaderForge.SFN_Lerp,id:114,x:33611,y:32608,varname:node_114,prsc:2|A-103-OUT,B-113-RGB,T-116-OUT;n:type:ShaderForge.SFN_Slider,id:116,x:33063,y:32667,ptovrint:False,ptlb:Activation,ptin:_Activation,varname:node_4400,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:150,x:33079,y:32792,varname:node_150,prsc:2|A-104-OUT,B-12-G;n:type:ShaderForge.SFN_Add,id:151,x:33321,y:32877,varname:node_151,prsc:2|A-150-OUT,B-12-R;n:type:ShaderForge.SFN_NormalVector,id:156,x:33218,y:33404,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:157,x:33600,y:33398,varname:node_157,prsc:2|A-156-OUT,B-158-OUT,C-104-OUT;n:type:ShaderForge.SFN_Vector3,id:158,x:33355,y:33524,varname:node_158,prsc:2,v1:0.3,v2:0,v3:0.3;n:type:ShaderForge.SFN_Slider,id:174,x:33301,y:33207,ptovrint:False,ptlb:Activable,ptin:_Activable,varname:node_1091,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.33,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:182,x:33822,y:32760,varname:node_182,prsc:2|A-114-OUT,B-17-OUT;proporder:12-2-3-102-104-113-116-174;pass:END;sub:END;*/

Shader "_PlayHead_/interface/transbar_button_01_add" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _Alpha ("Alpha", Range(0, 1)) = 0
        _HoverColor ("HoverColor", Color) = (0.9960784,0.7803922,0.3254902,1)
        _Hover ("Hover", Range(0, 1)) = 0
        _ActiveColor ("ActiveColor", Color) = (0.8078431,0.9960784,0.2,1)
        _Activation ("Activation", Range(0, 1)) = 0
        _Activable ("Activable", Range(0.33, 1)) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Overlay+25"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float _Alpha;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _HoverColor;
            uniform float _Hover;
            uniform float4 _ActiveColor;
            uniform float _Activation;
            uniform float _Activable;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (v.normal*float3(0.3,0,0.3)*_Hover);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = (lerp(lerp(_Color.rgb,_HoverColor.rgb,_Hover),_ActiveColor.rgb,_Activation)*(((_Hover*_MainTex_var.g)+_MainTex_var.r)*_Alpha*_Activable));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float _Hover;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (v.normal*float3(0.3,0,0.3)*_Hover);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
