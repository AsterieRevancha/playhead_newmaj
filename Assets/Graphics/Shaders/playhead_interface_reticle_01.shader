// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:_PlayHead_/inv/m_GhostInvader,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:6,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:False,qofs:2000,qpre:4,rntp:5,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.05882353,fgcg:0.05882353,fgcb:0.08235294,fgca:1,fgde:0.026,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:5691,x:34260,y:32620,varname:node_5691,prsc:2|emission-5810-OUT,alpha-5698-OUT,voffset-5837-OUT;n:type:ShaderForge.SFN_Tex2d,id:5692,x:33079,y:32967,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_7786,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:5694,x:33226,y:32356,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_4906,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:5698,x:34032,y:32841,varname:node_5698,prsc:2|A-5734-OUT,B-5704-OUT,C-5799-OUT;n:type:ShaderForge.SFN_Add,id:5704,x:33800,y:32947,varname:node_5704,prsc:2|A-5692-R,B-5721-OUT,C-5742-OUT;n:type:ShaderForge.SFN_Multiply,id:5721,x:33427,y:32836,varname:node_5721,prsc:2|A-5692-G,B-5765-OUT,C-5824-OUT;n:type:ShaderForge.SFN_Slider,id:5734,x:33501,y:32756,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_9396,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Step,id:5739,x:33307,y:33149,varname:node_5739,prsc:2|A-5692-A,B-5740-OUT;n:type:ShaderForge.SFN_Slider,id:5740,x:32922,y:33172,ptovrint:False,ptlb:Activation,ptin:_Activation,varname:node_1807,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:5742,x:33561,y:33071,varname:node_5742,prsc:2|A-5692-B,B-5739-OUT,C-5760-OUT;n:type:ShaderForge.SFN_Slider,id:5760,x:33150,y:33352,ptovrint:False,ptlb:ActivationFader,ptin:_ActivationFader,varname:node_573,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:0;n:type:ShaderForge.SFN_Slider,id:5765,x:32890,y:32782,ptovrint:False,ptlb:Hover,ptin:_Hover,varname:node_2476,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Color,id:5780,x:32969,y:32548,ptovrint:False,ptlb:HoverColor,ptin:_HoverColor,varname:node_1037,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.7403075,c3:0.1838235,c4:1;n:type:ShaderForge.SFN_Lerp,id:5785,x:33656,y:32428,varname:node_5785,prsc:2|A-5694-RGB,B-5780-RGB,T-5765-OUT;n:type:ShaderForge.SFN_Lerp,id:5799,x:33656,y:32591,varname:node_5799,prsc:2|A-5694-A,B-5780-A,T-5765-OUT;n:type:ShaderForge.SFN_Vector1,id:5804,x:32983,y:32895,varname:node_5804,prsc:2,v1:0.33;n:type:ShaderForge.SFN_Add,id:5809,x:33856,y:32410,varname:node_5809,prsc:2|A-5815-OUT,B-5785-OUT;n:type:ShaderForge.SFN_Clamp01,id:5810,x:34027,y:32410,varname:node_5810,prsc:2|IN-5809-OUT;n:type:ShaderForge.SFN_Slider,id:5815,x:33509,y:32320,ptovrint:False,ptlb:ActivationFlash,ptin:_ActivationFlash,varname:node_9802,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:5824,x:33250,y:32876,varname:node_5824,prsc:2|A-5815-OUT,B-5804-OUT;n:type:ShaderForge.SFN_NormalVector,id:5836,x:33786,y:33128,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:5837,x:33974,y:33170,varname:node_5837,prsc:2|A-5836-OUT,B-5838-OUT;n:type:ShaderForge.SFN_Slider,id:5838,x:33629,y:33324,ptovrint:False,ptlb:Separation,ptin:_Separation,varname:node_3812,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;proporder:5694-5780-5692-5734-5838-5765-5740-5815-5760;pass:END;sub:END;*/

Shader "_PlayHead_/interface/reticle_02" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _HoverColor ("HoverColor", Color) = (1,0.7403075,0.1838235,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Opacity ("Opacity", Range(0, 1)) = 1
        _Separation ("Separation", Range(0, 1)) = 0
        _Hover ("Hover", Range(0, 1)) = 0.5
        _Activation ("Activation", Range(0, 1)) = 0
        _ActivationFlash ("ActivationFlash", Range(0, 1)) = 0
        _ActivationFader ("ActivationFader", Range(1, 0)) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="Overlay+2000"
            "RenderType"="Overlay"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZTest Always
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color;
            uniform float _Opacity;
            uniform float _Activation;
            uniform float _ActivationFader;
            uniform float _Hover;
            uniform float4 _HoverColor;
            uniform float _ActivationFlash;
            uniform float _Separation;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (v.normal*_Separation);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = saturate((_ActivationFlash+lerp(_Color.rgb,_HoverColor.rgb,_Hover)));
                float3 finalColor = emissive;
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                return fixed4(finalColor,(_Opacity*(_MainTex_var.r+(_MainTex_var.g*_Hover*(_ActivationFlash+0.33))+(_MainTex_var.b*step(_MainTex_var.a,_Activation)*_ActivationFader))*lerp(_Color.a,_HoverColor.a,_Hover)));
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float _Separation;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (v.normal*_Separation);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "_PlayHead_/inv/m_GhostInvader"
    CustomEditor "ShaderForgeMaterialInspector"
}
