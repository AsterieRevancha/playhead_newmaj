// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Mobile/Particles/Additive,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.05882353,fgcg:0.05882353,fgcb:0.08235294,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:6094,x:33996,y:32968,varname:node_6094,prsc:2|emission-6097-OUT,voffset-6116-OUT;n:type:ShaderForge.SFN_Tex2d,id:6095,x:33461,y:32976,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_5032,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-6110-OUT;n:type:ShaderForge.SFN_Color,id:6096,x:33461,y:32804,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_9244,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:6097,x:33713,y:32996,varname:node_6097,prsc:2|A-6096-RGB,B-6095-RGB,C-6114-OUT,D-6115-R;n:type:ShaderForge.SFN_ValueProperty,id:6108,x:32848,y:33159,ptovrint:False,ptlb:Speed,ptin:_Speed,varname:node_6160,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:6109,x:33039,y:33083,varname:node_6109,prsc:2|A-6112-OUT,B-6108-OUT,C-6113-T;n:type:ShaderForge.SFN_Add,id:6110,x:33271,y:32976,varname:node_6110,prsc:2|A-6111-UVOUT,B-6109-OUT;n:type:ShaderForge.SFN_TexCoord,id:6111,x:33039,y:32905,varname:node_6111,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector2,id:6112,x:32848,y:33033,varname:node_6112,prsc:2,v1:0,v2:-1;n:type:ShaderForge.SFN_Time,id:6113,x:32848,y:33227,varname:node_6113,prsc:2;n:type:ShaderForge.SFN_Slider,id:6114,x:33232,y:33186,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_4940,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_VertexColor,id:6115,x:33366,y:33273,varname:node_6115,prsc:2;n:type:ShaderForge.SFN_Multiply,id:6116,x:33760,y:33446,varname:node_6116,prsc:2|A-6117-OUT,B-6115-G,C-6121-OUT,D-6120-OUT;n:type:ShaderForge.SFN_Add,id:6117,x:33683,y:33229,varname:node_6117,prsc:2|A-6095-A,B-6118-OUT;n:type:ShaderForge.SFN_Vector1,id:6118,x:33366,y:33427,varname:node_6118,prsc:2,v1:-0.5;n:type:ShaderForge.SFN_NormalVector,id:6120,x:33552,y:33566,prsc:2,pt:False;n:type:ShaderForge.SFN_ValueProperty,id:6121,x:33366,y:33518,ptovrint:False,ptlb:Offset,ptin:_Offset,varname:node_2549,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;proporder:6095-6096-6108-6121-6114;pass:END;sub:END;*/

Shader "_PlayHead_/effects/lightning_01" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _Speed ("Speed", Float ) = 1
        _Offset ("Offset", Float ) = 2
        _Opacity ("Opacity", Range(0, 1)) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color;
            uniform float _Speed;
            uniform float _Opacity;
            uniform float _Offset;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_6113 = _Time + _TimeEditor;
                float2 node_6110 = (o.uv0+(float2(0,-1)*_Speed*node_6113.g));
                float4 _MainTex_var = tex2Dlod(_MainTex,float4(TRANSFORM_TEX(node_6110, _MainTex),0.0,0));
                v.vertex.xyz += ((_MainTex_var.a+(-0.5))*o.vertexColor.g*_Offset*v.normal);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_6113 = _Time + _TimeEditor;
                float2 node_6110 = (i.uv0+(float2(0,-1)*_Speed*node_6113.g));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_6110, _MainTex));
                float3 emissive = (_Color.rgb*_MainTex_var.rgb*_Opacity*i.vertexColor.r);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Speed;
            uniform float _Offset;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_6113 = _Time + _TimeEditor;
                float2 node_6110 = (o.uv0+(float2(0,-1)*_Speed*node_6113.g));
                float4 _MainTex_var = tex2Dlod(_MainTex,float4(TRANSFORM_TEX(node_6110, _MainTex),0.0,0));
                v.vertex.xyz += ((_MainTex_var.a+(-0.5))*o.vertexColor.g*_Offset*v.normal);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Mobile/Particles/Additive"
    CustomEditor "ShaderForgeMaterialInspector"
}
