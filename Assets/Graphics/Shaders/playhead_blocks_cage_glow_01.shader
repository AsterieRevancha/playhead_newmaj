// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Particles/Additive,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:153,x:33534,y:32600,varname:node_153,prsc:2|emission-5988-OUT,voffset-5902-OUT;n:type:ShaderForge.SFN_Color,id:5888,x:32607,y:32696,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_297,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_VertexColor,id:5890,x:32340,y:32935,varname:node_5890,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5891,x:33172,y:32698,varname:node_5891,prsc:2|A-5893-R,B-5888-RGB,C-5903-OUT,D-5890-A,E-5888-A;n:type:ShaderForge.SFN_Tex2d,id:5893,x:32721,y:32402,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_3114,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:5900,x:32747,y:33013,varname:node_5900,prsc:2|A-5890-RGB,B-5904-OUT;n:type:ShaderForge.SFN_Add,id:5901,x:32927,y:33074,varname:node_5901,prsc:2|A-5900-OUT,B-5906-OUT;n:type:ShaderForge.SFN_Multiply,id:5902,x:33153,y:33110,varname:node_5902,prsc:2|A-6019-OUT,B-5951-OUT,C-5919-OUT,D-5890-R;n:type:ShaderForge.SFN_Slider,id:5903,x:32523,y:33320,ptovrint:False,ptlb:Apparition,ptin:_Apparition,varname:node_3955,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Vector1,id:5904,x:32536,y:33074,varname:node_5904,prsc:2,v1:2;n:type:ShaderForge.SFN_Vector1,id:5906,x:32747,y:33157,varname:node_5906,prsc:2,v1:-1;n:type:ShaderForge.SFN_OneMinus,id:5919,x:32911,y:33353,varname:node_5919,prsc:2|IN-5903-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5951,x:32927,y:33239,ptovrint:False,ptlb:Scale,ptin:_Scale,varname:node_1056,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_TexCoord,id:5980,x:32805,y:32810,varname:node_5980,prsc:2,uv:1;n:type:ShaderForge.SFN_Multiply,id:5988,x:33350,y:32634,varname:node_5988,prsc:2|A-5990-OUT,B-5891-OUT;n:type:ShaderForge.SFN_Slider,id:5990,x:33172,y:32505,ptovrint:False,ptlb:Pulse,ptin:_Pulse,varname:node_463,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.25,cur:1,max:1;n:type:ShaderForge.SFN_NormalVector,id:6019,x:32550,y:33145,prsc:2,pt:False;proporder:5888-5893-5951-5903-5990;pass:END;sub:END;*/

Shader "_PlayHead_/blocks/cage_glow_01" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Scale ("Scale", Float ) = 2
        _Apparition ("Apparition", Range(0, 1)) = 0
        _Pulse ("Pulse", Range(0.25, 1)) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Apparition;
            uniform float _Scale;
            uniform float _Pulse;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (v.normal*_Scale*(1.0 - _Apparition)*o.vertexColor.r);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = (_Pulse*(_MainTex_var.r*_Color.rgb*_Apparition*i.vertexColor.a*_Color.a));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float _Apparition;
            uniform float _Scale;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float3 normalDir : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                v.vertex.xyz += (v.normal*_Scale*(1.0 - _Apparition)*o.vertexColor.r);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Particles/Additive"
    CustomEditor "ShaderForgeMaterialInspector"
}
