// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:33775,y:32847,varname:node_1,prsc:2|emission-18-OUT,alpha-16-OUT;n:type:ShaderForge.SFN_Tex2d,id:2,x:33407,y:32976,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_6008,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-10-OUT;n:type:ShaderForge.SFN_TexCoord,id:9,x:33000,y:32908,varname:node_9,prsc:2,uv:0;n:type:ShaderForge.SFN_Add,id:10,x:33200,y:32976,varname:node_10,prsc:2|A-9-UVOUT,B-12-OUT;n:type:ShaderForge.SFN_ValueProperty,id:11,x:32759,y:33110,ptovrint:False,ptlb:Speed,ptin:_Speed,varname:node_8944,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:12,x:33000,y:33066,varname:node_12,prsc:2|A-13-T,B-11-OUT,C-14-OUT,D-15-R;n:type:ShaderForge.SFN_Time,id:13,x:32759,y:32972,varname:node_13,prsc:2;n:type:ShaderForge.SFN_Vector2,id:14,x:32759,y:33177,varname:node_14,prsc:2,v1:1,v2:0;n:type:ShaderForge.SFN_VertexColor,id:15,x:32759,y:33277,varname:node_15,prsc:2;n:type:ShaderForge.SFN_Multiply,id:16,x:33598,y:33099,varname:node_16,prsc:2|A-2-A,B-17-OUT;n:type:ShaderForge.SFN_Slider,id:17,x:33250,y:33186,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_5117,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:18,x:33598,y:32900,varname:node_18,prsc:2|A-19-RGB,B-2-RGB;n:type:ShaderForge.SFN_Color,id:19,x:33407,y:32800,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_9696,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;proporder:19-2-17-11;pass:END;sub:END;*/

Shader "_PlayHead_/studio/sequencer_01" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _Opacity ("Opacity", Range(0, 1)) = 1
        _Speed ("Speed", Float ) = 0.1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Speed;
            uniform float _Opacity;
            uniform float4 _Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 node_13 = _Time + _TimeEditor;
                float2 node_10 = (i.uv0+(node_13.g*_Speed*float2(1,0)*i.vertexColor.r));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_10, _MainTex));
                float3 emissive = (_Color.rgb*_MainTex_var.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,(_MainTex_var.a*_Opacity));
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
