// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:4060,x:34494,y:32686,varname:node_4060,prsc:2|emission-5330-OUT;n:type:ShaderForge.SFN_Color,id:4061,x:33144,y:32327,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_5949,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:4062,x:33806,y:33086,ptovrint:False,ptlb:Specular,ptin:_Specular,varname:node_1450,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:4063,x:32862,y:32765,ptovrint:False,ptlb:Shininess,ptin:_Shininess,varname:node_6730,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:5,max:10;n:type:ShaderForge.SFN_Color,id:4067,x:32979,y:33172,ptovrint:False,ptlb:HeadGlow,ptin:_HeadGlow,varname:node_8897,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Add,id:4068,x:33910,y:32827,varname:node_4068,prsc:2|A-4073-OUT,B-4072-OUT,C-4077-OUT,D-5349-OUT;n:type:ShaderForge.SFN_Multiply,id:4072,x:33250,y:33063,varname:node_4072,prsc:2|A-4170-A,B-4067-RGB,C-4086-RGB;n:type:ShaderForge.SFN_Vector1,id:4073,x:33231,y:32822,varname:node_4073,prsc:2,v1:0.04;n:type:ShaderForge.SFN_NormalVector,id:4075,x:32215,y:32289,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:4076,x:32420,y:32278,varname:node_4076,prsc:2,cc1:2,cc2:1,cc3:0,cc4:-1|IN-4075-OUT;n:type:ShaderForge.SFN_Multiply,id:4077,x:33471,y:32543,varname:node_4077,prsc:2|A-4061-A,B-4076-G,C-4353-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:4081,x:32305,y:32975,ptovrint:False,ptlb:GlowRamp,ptin:_GlowRamp,varname:node_793,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:4086,x:32643,y:33128,varname:node_1032,prsc:2,ntxv:0,isnm:False|UVIN-4137-OUT,TEX-4081-TEX;n:type:ShaderForge.SFN_TexCoord,id:4089,x:32009,y:33089,varname:node_4089,prsc:2,uv:1;n:type:ShaderForge.SFN_Add,id:4137,x:32424,y:33128,varname:node_4137,prsc:2|A-4089-UVOUT,B-4140-OUT;n:type:ShaderForge.SFN_Slider,id:4140,x:31998,y:33314,ptovrint:False,ptlb:GlowAnim,ptin:_GlowAnim,varname:node_1227,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:0;n:type:ShaderForge.SFN_Multiply,id:4169,x:33982,y:32560,varname:node_4169,prsc:2|A-4170-RGB,B-4061-RGB;n:type:ShaderForge.SFN_Tex2d,id:4170,x:33415,y:32184,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_6190,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Vector1,id:4353,x:33219,y:32577,varname:node_4353,prsc:2,v1:0.15;n:type:ShaderForge.SFN_Blend,id:5330,x:34267,y:32791,varname:node_5330,prsc:2,blmd:10,clmp:True|SRC-4169-OUT,DST-4068-OUT;n:type:ShaderForge.SFN_Fresnel,id:5339,x:33806,y:33229,varname:node_5339,prsc:2|EXP-4063-OUT;n:type:ShaderForge.SFN_Vector1,id:5344,x:33574,y:33229,varname:node_5344,prsc:2,v1:5;n:type:ShaderForge.SFN_Multiply,id:5349,x:34032,y:33096,varname:node_5349,prsc:2|A-4062-RGB,B-5339-OUT;proporder:4061-4062-4063-4067-4081-4140-4170;pass:END;sub:END;*/

Shader "_PlayHead_/cockpit/04_base" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _Specular ("Specular", Color) = (0.5,0.5,0.5,1)
        _Shininess ("Shininess", Range(0, 10)) = 5
        _HeadGlow ("HeadGlow", Color) = (0.5,0.5,0.5,1)
        _GlowRamp ("GlowRamp", 2D) = "white" {}
        _GlowAnim ("GlowAnim", Range(1, 0)) = 1
        _MainTex ("MainTex", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float4 _Specular;
            uniform float _Shininess;
            uniform float4 _HeadGlow;
            uniform sampler2D _GlowRamp; uniform float4 _GlowRamp_ST;
            uniform float _GlowAnim;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float2 node_4137 = (i.uv1+_GlowAnim);
                float4 node_1032 = tex2D(_GlowRamp,TRANSFORM_TEX(node_4137, _GlowRamp));
                float3 emissive = saturate(( (0.04+(_MainTex_var.a*_HeadGlow.rgb*node_1032.rgb)+(_Color.a*i.normalDir.bgr.g*0.15)+(_Specular.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),_Shininess))) > 0.5 ? (1.0-(1.0-2.0*((0.04+(_MainTex_var.a*_HeadGlow.rgb*node_1032.rgb)+(_Color.a*i.normalDir.bgr.g*0.15)+(_Specular.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),_Shininess)))-0.5))*(1.0-(_MainTex_var.rgb*_Color.rgb))) : (2.0*(0.04+(_MainTex_var.a*_HeadGlow.rgb*node_1032.rgb)+(_Color.a*i.normalDir.bgr.g*0.15)+(_Specular.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),_Shininess)))*(_MainTex_var.rgb*_Color.rgb)) ));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
