// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:False,qofs:-997,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32983,y:32680,varname:node_1,prsc:2|emission-105-OUT;n:type:ShaderForge.SFN_Color,id:4,x:32360,y:32756,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_5981,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.07843138,c3:0.07843138,c4:1;n:type:ShaderForge.SFN_Lerp,id:105,x:32670,y:32779,varname:node_105,prsc:2|A-321-RGB,B-4-RGB,T-334-OUT;n:type:ShaderForge.SFN_Color,id:321,x:32360,y:32580,ptovrint:False,ptlb:FogColor,ptin:_FogColor,varname:node_2504,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.07843138,c3:0.07843138,c4:1;n:type:ShaderForge.SFN_VertexColor,id:331,x:32247,y:32916,varname:node_331,prsc:2;n:type:ShaderForge.SFN_Multiply,id:334,x:32479,y:32960,varname:node_334,prsc:2|A-331-G,B-331-G;proporder:4-321;pass:END;sub:END;*/

Shader "_PlayHead_/fog/horizon_2" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.07843138,0.07843138,1)
        _FogColor ("FogColor", Color) = (0.07843138,0.07843138,0.07843138,1)
    }
    SubShader {
        Tags {
            "Queue"="Geometry-997"
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float4 _FogColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float3 emissive = lerp(_FogColor.rgb,_Color.rgb,(i.vertexColor.g*i.vertexColor.g));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
