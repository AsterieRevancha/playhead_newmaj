// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Mobile/Diffuse,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:False,igpj:False,qofs:-999,qpre:0,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:33920,y:32993,varname:node_1,prsc:2|emission-2829-OUT;n:type:ShaderForge.SFN_TexCoord,id:7,x:32069,y:33082,varname:node_7,prsc:2,uv:1;n:type:ShaderForge.SFN_Lerp,id:27,x:32568,y:33034,varname:node_27,prsc:2|A-38-RGB,B-40-RGB,T-7-V;n:type:ShaderForge.SFN_Color,id:38,x:32321,y:32755,ptovrint:False,ptlb:Horizon,ptin:_Horizon,varname:node_9590,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:40,x:32321,y:32941,ptovrint:False,ptlb:Sky,ptin:_Sky,varname:node_827,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:2555,x:32568,y:33186,varname:node_2555,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:2631,x:32398,y:33597,varname:node_2631,prsc:2,uv:0;n:type:ShaderForge.SFN_Add,id:2803,x:33080,y:32946,varname:node_2803,prsc:2|A-27-OUT,B-2827-OUT,C-2804-OUT;n:type:ShaderForge.SFN_Multiply,id:2804,x:33170,y:33210,varname:node_2804,prsc:2|A-2835-OUT,B-2828-RGB,C-2828-A;n:type:ShaderForge.SFN_RemapRange,id:2823,x:32725,y:33641,varname:node_2823,prsc:2,frmn:0.46,frmx:0.5,tomn:1,tomx:0|IN-2631-V;n:type:ShaderForge.SFN_Clamp01,id:2824,x:32903,y:33641,varname:node_2824,prsc:2|IN-2823-OUT;n:type:ShaderForge.SFN_Color,id:2826,x:32599,y:32775,ptovrint:False,ptlb:SunInfluence,ptin:_SunInfluence,varname:node_4899,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:2827,x:32798,y:32807,varname:node_2827,prsc:2|A-2826-RGB,B-2555-G,C-2826-A;n:type:ShaderForge.SFN_Color,id:2828,x:32677,y:33403,ptovrint:False,ptlb:FogColor,ptin:_FogColor,varname:node_7510,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:0.4;n:type:ShaderForge.SFN_Lerp,id:2829,x:33661,y:33135,varname:node_2829,prsc:2|A-2838-OUT,B-2830-RGB,T-3206-OUT;n:type:ShaderForge.SFN_Color,id:2830,x:33341,y:33391,ptovrint:False,ptlb:VoidColor,ptin:_VoidColor,varname:node_2673,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.07843138,c3:0.07843138,c4:1;n:type:ShaderForge.SFN_Vector1,id:2833,x:32715,y:33313,varname:node_2833,prsc:2,v1:3;n:type:ShaderForge.SFN_Power,id:2835,x:32909,y:33179,varname:node_2835,prsc:2|VAL-2555-B,EXP-2833-OUT;n:type:ShaderForge.SFN_Multiply,id:2838,x:33446,y:32969,varname:node_2838,prsc:2|A-38-A,B-2803-OUT;n:type:ShaderForge.SFN_Slider,id:3204,x:32825,y:33889,ptovrint:False,ptlb:Nooooo,ptin:_Nooooo,varname:node_7453,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:3205,x:33122,y:33717,varname:node_3205,prsc:2|A-2824-OUT,B-3204-OUT;n:type:ShaderForge.SFN_Clamp01,id:3206,x:33290,y:33717,varname:node_3206,prsc:2|IN-3205-OUT;proporder:38-40-2826-2828-2830-3204;pass:END;sub:END;*/

Shader "_PlayHead_/skysphere_01_noramp" {
    Properties {
        _Horizon ("Horizon", Color) = (1,1,1,1)
        _Sky ("Sky", Color) = (0.5,0.5,1,1)
        _SunInfluence ("SunInfluence", Color) = (1,1,1,1)
        _FogColor ("FogColor", Color) = (1,1,1,0.4)
        _VoidColor ("VoidColor", Color) = (0.07843138,0.07843138,0.07843138,1)
        _Nooooo ("Nooooo", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "Queue"="Background-999"
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _Horizon;
            uniform float4 _Sky;
            uniform float4 _SunInfluence;
            uniform float4 _FogColor;
            uniform float4 _VoidColor;
            uniform float _Nooooo;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float3 emissive = lerp((_Horizon.a*(lerp(_Horizon.rgb,_Sky.rgb,i.uv1.g)+(_SunInfluence.rgb*i.vertexColor.g*_SunInfluence.a)+(pow(i.vertexColor.b,3.0)*_FogColor.rgb*_FogColor.a))),_VoidColor.rgb,saturate((saturate((i.uv0.g*-25.00001+12.5))+_Nooooo)));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Mobile/Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
