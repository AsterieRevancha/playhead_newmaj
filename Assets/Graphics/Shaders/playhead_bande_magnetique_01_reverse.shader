// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:6,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.03,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:6161,x:33554,y:32704,varname:node_6161,prsc:2|emission-6207-OUT;n:type:ShaderForge.SFN_Tex2d,id:6162,x:32525,y:32636,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_1146,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False|UVIN-6313-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:6163,x:32525,y:32870,ptovrint:False,ptlb:ScrollingTex,ptin:_ScrollingTex,varname:node_7641,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False|UVIN-6214-UVOUT;n:type:ShaderForge.SFN_Multiply,id:6207,x:33382,y:32837,varname:node_6207,prsc:2|A-6550-OUT,B-6209-A,C-6760-OUT;n:type:ShaderForge.SFN_VertexColor,id:6209,x:32484,y:33145,varname:node_6209,prsc:2;n:type:ShaderForge.SFN_Panner,id:6214,x:32250,y:32878,varname:node_6214,prsc:2,spu:-0.1,spv:1;n:type:ShaderForge.SFN_Multiply,id:6237,x:33000,y:32760,varname:node_6237,prsc:2|A-6163-RGB,B-6162-A,C-6162-R,D-6745-OUT;n:type:ShaderForge.SFN_Color,id:6263,x:32701,y:32420,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_4183,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9843137,c2:0.8196079,c3:0.7058824,c4:1;n:type:ShaderForge.SFN_Panner,id:6313,x:32250,y:32645,varname:node_6313,prsc:2,spu:0,spv:2;n:type:ShaderForge.SFN_Min,id:6531,x:33000,y:32552,varname:node_6531,prsc:2|A-6263-RGB,B-6263-A,C-6162-A,D-6162-R,E-6209-R;n:type:ShaderForge.SFN_Add,id:6550,x:33209,y:32696,varname:node_6550,prsc:2|A-6531-OUT,B-6237-OUT;n:type:ShaderForge.SFN_OneMinus,id:6745,x:32810,y:33007,varname:node_6745,prsc:2|IN-6209-B;n:type:ShaderForge.SFN_Slider,id:6760,x:33106,y:33021,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_6699,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;proporder:6263-6162-6163-6760;pass:END;sub:END;*/

Shader "_PlayHead_/bande_magnetique_reverse" {
    Properties {
        _Color ("Color", Color) = (0.9843137,0.8196079,0.7058824,1)
        _MainTex ("MainTex", 2D) = "black" {}
        _ScrollingTex ("ScrollingTex", 2D) = "black" {}
        _Opacity ("Opacity", Range(0, 1)) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One OneMinusSrcColor
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _ScrollingTex; uniform float4 _ScrollingTex_ST;
            uniform float4 _Color;
            uniform float _Opacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 node_3480 = _Time + _TimeEditor;
                float2 node_6313 = (i.uv0+node_3480.g*float2(0,2));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_6313, _MainTex));
                float2 node_6214 = (i.uv0+node_3480.g*float2(-0.1,1));
                float4 _ScrollingTex_var = tex2D(_ScrollingTex,TRANSFORM_TEX(node_6214, _ScrollingTex));
                float3 emissive = ((min(min(min(min(_Color.rgb,_Color.a),_MainTex_var.a),_MainTex_var.r),i.vertexColor.r)+(_ScrollingTex_var.rgb*_MainTex_var.a*_MainTex_var.r*(1.0 - i.vertexColor.b)))*i.vertexColor.a*_Opacity);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
