﻿Shader "_PlayHead_/inv/m_GhostInvader" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_MainTex ("Main Texture", 2D) = "white" {}
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Blend SrcAlpha One
	Cull Back Lighting Off ZWrite Off Fog { Mode Off }
	
	BindChannels {
		Bind "Vertex", vertex
		Bind "TexCoord", texcoord
	}
    
	SubShader {
		Pass {
			SetTexture [_MainTex] {
				constantColor [_Color]
				combine texture * constant
			}
		}
	}
}
}