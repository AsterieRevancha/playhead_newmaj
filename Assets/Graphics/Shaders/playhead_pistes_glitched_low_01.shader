// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Self-Illumin/Diffuse,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.026,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:35613,y:32682,varname:node_1,prsc:2|emission-8-OUT;n:type:ShaderForge.SFN_Color,id:2,x:33795,y:32739,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_3660,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Cubemap,id:3,x:33678,y:32579,ptovrint:False,ptlb:Cubemap,ptin:_Cubemap,varname:node_3276,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:bd6f1547ba2bfef45adb489d44ae43b0,pvfc:0|DIR-4-OUT;n:type:ShaderForge.SFN_ViewReflectionVector,id:4,x:33446,y:32579,varname:node_4,prsc:2;n:type:ShaderForge.SFN_Multiply,id:7,x:34513,y:32901,varname:node_7,prsc:2|A-2-RGB,B-378-R;n:type:ShaderForge.SFN_Add,id:8,x:35090,y:32804,varname:node_8,prsc:2|A-80-OUT,B-336-OUT,C-719-OUT;n:type:ShaderForge.SFN_Multiply,id:21,x:34513,y:33106,varname:node_21,prsc:2|A-242-RGB,B-378-G,C-320-OUT;n:type:ShaderForge.SFN_Multiply,id:80,x:34115,y:32578,varname:node_80,prsc:2|A-3-RGB,B-378-B;n:type:ShaderForge.SFN_Color,id:242,x:34288,y:32982,ptovrint:False,ptlb:CircuitsColor,ptin:_CircuitsColor,varname:node_272,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:320,x:34131,y:33184,ptovrint:False,ptlb:Pulse,ptin:_Pulse,varname:node_3443,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.5,cur:1,max:1;n:type:ShaderForge.SFN_Max,id:336,x:34744,y:32903,varname:node_336,prsc:2|A-7-OUT,B-21-OUT;n:type:ShaderForge.SFN_Tex2d,id:369,x:32927,y:33186,varname:node_9026,prsc:2,ntxv:0,isnm:False|UVIN-388-UVOUT,TEX-704-TEX;n:type:ShaderForge.SFN_Tex2d,id:378,x:33479,y:32988,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_662,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False|UVIN-391-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:384,x:32478,y:33082,varname:node_384,prsc:2,uv:0;n:type:ShaderForge.SFN_Panner,id:388,x:32712,y:33186,varname:node_388,prsc:2,spu:0,spv:0.5|UVIN-384-UVOUT;n:type:ShaderForge.SFN_Panner,id:391,x:33212,y:33079,varname:node_391,prsc:2,spu:1,spv:1|UVIN-384-UVOUT,DIST-465-OUT;n:type:ShaderForge.SFN_Slider,id:452,x:32773,y:33657,ptovrint:False,ptlb:GlitchIntensity,ptin:_GlitchIntensity,varname:node_3404,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:465,x:33212,y:33320,varname:node_465,prsc:2|A-369-R,B-452-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:704,x:32737,y:32931,ptovrint:False,ptlb:Glitch,ptin:_Glitch,varname:node_8273,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:719,x:33714,y:33143,varname:node_719,prsc:2|A-465-OUT,B-2-RGB,C-720-OUT;n:type:ShaderForge.SFN_Vector1,id:720,x:33572,y:33400,varname:node_720,prsc:2,v1:6;n:type:ShaderForge.SFN_Vector3,id:761,x:33517,y:33297,varname:node_761,prsc:2,v1:1,v2:1,v3:2;n:type:ShaderForge.SFN_Vector1,id:771,x:33738,y:33327,varname:node_771,prsc:2,v1:0.5;proporder:2-242-378-704-3-320-452;pass:END;sub:END;*/

Shader "_PlayHead_/pistes/glitched_low_01" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _CircuitsColor ("CircuitsColor", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "black" {}
        _Glitch ("Glitch", 2D) = "white" {}
        _Cubemap ("Cubemap", Cube) = "_Skybox" {}
        _Pulse ("Pulse", Range(0.5, 1)) = 1
        _GlitchIntensity ("GlitchIntensity", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform samplerCUBE _Cubemap;
            uniform float4 _CircuitsColor;
            uniform float _Pulse;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _GlitchIntensity;
            uniform sampler2D _Glitch; uniform float4 _Glitch_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
////// Emissive:
                float4 node_3811 = _Time + _TimeEditor;
                float2 node_388 = (i.uv0+node_3811.g*float2(0,0.5));
                float4 node_9026 = tex2D(_Glitch,TRANSFORM_TEX(node_388, _Glitch));
                float node_465 = (node_9026.r*_GlitchIntensity);
                float2 node_391 = (i.uv0+node_465*float2(1,1));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_391, _MainTex));
                float3 emissive = ((texCUBE(_Cubemap,viewReflectDirection).rgb*_MainTex_var.b)+max((_Color.rgb*_MainTex_var.r),(_CircuitsColor.rgb*_MainTex_var.g*_Pulse))+(node_465*_Color.rgb*6.0));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Self-Illumin/Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
