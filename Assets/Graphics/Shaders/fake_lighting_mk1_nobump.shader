// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Mobile/Bumped Diffuse,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:False,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07843138,fgcg:0.07843138,fgcb:0.07843138,fgca:1,fgde:0.003,fgrn:30,fgrf:40,ofsf:0,ofsu:0,f2p0:True;n:type:ShaderForge.SFN_Final,id:56,x:34718,y:33034,varname:node_56,prsc:2|emission-293-OUT;n:type:ShaderForge.SFN_NormalVector,id:57,x:30944,y:32389,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector4Property,id:58,x:30961,y:32746,ptovrint:False,ptlb:LightDirection1,ptin:_LightDirection1,varname:node_8749,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:-0.5,v2:0.866,v3:0,v4:0;n:type:ShaderForge.SFN_Distance,id:60,x:31540,y:32186,varname:node_60,prsc:2|A-57-OUT,B-58-XYZ;n:type:ShaderForge.SFN_Multiply,id:61,x:31840,y:32154,varname:node_61,prsc:2|A-62-OUT,B-60-OUT;n:type:ShaderForge.SFN_Vector1,id:62,x:31641,y:32097,varname:node_62,prsc:2,v1:0.5;n:type:ShaderForge.SFN_FragmentPosition,id:65,x:30551,y:33568,varname:node_65,prsc:2;n:type:ShaderForge.SFN_Append,id:68,x:30766,y:33568,varname:node_68,prsc:2|A-65-X,B-65-Y;n:type:ShaderForge.SFN_Normalize,id:70,x:30965,y:33568,varname:node_70,prsc:2|IN-68-OUT;n:type:ShaderForge.SFN_Distance,id:76,x:31580,y:33340,varname:node_76,prsc:2|A-58-XYZ,B-70-OUT;n:type:ShaderForge.SFN_Subtract,id:79,x:31848,y:33320,varname:node_79,prsc:2|A-234-OUT,B-76-OUT;n:type:ShaderForge.SFN_Multiply,id:81,x:33668,y:32931,varname:node_81,prsc:2|A-287-OUT,B-289-OUT,C-177-RGB;n:type:ShaderForge.SFN_Clamp01,id:89,x:32008,y:33320,varname:node_89,prsc:2|IN-79-OUT;n:type:ShaderForge.SFN_Vector4Property,id:100,x:30961,y:32948,ptovrint:False,ptlb:LightDirection2,ptin:_LightDirection2,varname:node_4393,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5,v2:0.866,v3:0,v4:0;n:type:ShaderForge.SFN_Vector4Property,id:102,x:30961,y:33159,ptovrint:False,ptlb:LightDirection3,ptin:_LightDirection3,varname:node_1618,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_OneMinus,id:155,x:32074,y:32260,varname:node_155,prsc:2|IN-61-OUT;n:type:ShaderForge.SFN_OneMinus,id:171,x:32196,y:33385,varname:node_171,prsc:2|IN-89-OUT;n:type:ShaderForge.SFN_Color,id:177,x:33437,y:33201,ptovrint:False,ptlb:LightColor,ptin:_LightColor,varname:node_8851,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:179,x:33422,y:33471,ptovrint:False,ptlb:BounceColor,ptin:_BounceColor,varname:node_8352,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_RemapRange,id:180,x:33429,y:33789,varname:node_180,prsc:2,frmn:0.75,frmx:0,tomn:0,tomx:0.5|IN-289-OUT;n:type:ShaderForge.SFN_Multiply,id:181,x:33646,y:33620,varname:node_181,prsc:2|A-179-RGB,B-180-OUT;n:type:ShaderForge.SFN_Max,id:185,x:33980,y:33288,varname:node_185,prsc:2|A-81-OUT,B-181-OUT;n:type:ShaderForge.SFN_Slider,id:186,x:32043,y:32763,ptovrint:False,ptlb:Light1,ptin:_Light1,varname:node_1718,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:187,x:32388,y:33300,varname:node_187,prsc:2|A-186-OUT,B-89-OUT;n:type:ShaderForge.SFN_Distance,id:190,x:31580,y:33503,varname:node_190,prsc:2|A-100-XYZ,B-70-OUT;n:type:ShaderForge.SFN_Subtract,id:192,x:31847,y:33529,varname:node_192,prsc:2|A-234-OUT,B-190-OUT;n:type:ShaderForge.SFN_Clamp01,id:194,x:32008,y:33529,varname:node_194,prsc:2|IN-192-OUT;n:type:ShaderForge.SFN_Multiply,id:196,x:32388,y:33543,varname:node_196,prsc:2|A-198-OUT,B-194-OUT;n:type:ShaderForge.SFN_Slider,id:198,x:31971,y:32856,ptovrint:False,ptlb:Light2,ptin:_Light2,varname:node_4735,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Distance,id:205,x:31540,y:32352,varname:node_205,prsc:2|A-57-OUT,B-100-XYZ;n:type:ShaderForge.SFN_Multiply,id:207,x:31840,y:32347,varname:node_207,prsc:2|A-62-OUT,B-205-OUT;n:type:ShaderForge.SFN_OneMinus,id:211,x:32050,y:32440,varname:node_211,prsc:2|IN-207-OUT;n:type:ShaderForge.SFN_Add,id:214,x:33026,y:32379,varname:node_214,prsc:2|A-244-OUT,B-246-OUT,C-260-OUT;n:type:ShaderForge.SFN_Add,id:215,x:32880,y:33646,varname:node_215,prsc:2|A-187-OUT,B-196-OUT,C-254-OUT;n:type:ShaderForge.SFN_OneMinus,id:228,x:32174,y:33601,varname:node_228,prsc:2|IN-194-OUT;n:type:ShaderForge.SFN_Vector1,id:234,x:31657,y:33254,varname:node_234,prsc:2,v1:1.85;n:type:ShaderForge.SFN_Slider,id:239,x:32043,y:32944,ptovrint:False,ptlb:Light3,ptin:_Light3,varname:node_5732,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Distance,id:241,x:31569,y:33693,varname:node_241,prsc:2|A-102-XYZ,B-70-OUT;n:type:ShaderForge.SFN_Distance,id:243,x:31540,y:32528,varname:node_243,prsc:2|A-57-OUT,B-102-XYZ;n:type:ShaderForge.SFN_Multiply,id:244,x:32518,y:32088,varname:node_244,prsc:2|A-61-OUT,B-186-OUT;n:type:ShaderForge.SFN_Multiply,id:246,x:32517,y:32357,varname:node_246,prsc:2|A-207-OUT,B-198-OUT;n:type:ShaderForge.SFN_Subtract,id:248,x:31831,y:33741,varname:node_248,prsc:2|A-234-OUT,B-241-OUT;n:type:ShaderForge.SFN_Clamp01,id:250,x:31992,y:33741,varname:node_250,prsc:2|IN-248-OUT;n:type:ShaderForge.SFN_OneMinus,id:252,x:32161,y:33827,varname:node_252,prsc:2|IN-250-OUT;n:type:ShaderForge.SFN_Multiply,id:254,x:32388,y:33767,varname:node_254,prsc:2|A-239-OUT,B-250-OUT;n:type:ShaderForge.SFN_Multiply,id:256,x:31840,y:32514,varname:node_256,prsc:2|A-62-OUT,B-243-OUT;n:type:ShaderForge.SFN_OneMinus,id:258,x:32050,y:32607,varname:node_258,prsc:2|IN-256-OUT;n:type:ShaderForge.SFN_Multiply,id:260,x:32517,y:32614,varname:node_260,prsc:2|A-256-OUT,B-239-OUT;n:type:ShaderForge.SFN_Multiply,id:263,x:32621,y:32224,varname:node_263,prsc:2|A-155-OUT,B-281-OUT;n:type:ShaderForge.SFN_Multiply,id:265,x:32618,y:32482,varname:node_265,prsc:2|A-211-OUT,B-283-OUT;n:type:ShaderForge.SFN_Multiply,id:267,x:32601,y:32762,varname:node_267,prsc:2|A-258-OUT,B-285-OUT;n:type:ShaderForge.SFN_Multiply,id:269,x:32517,y:33427,varname:node_269,prsc:2|A-281-OUT,B-171-OUT;n:type:ShaderForge.SFN_Multiply,id:271,x:32529,y:33647,varname:node_271,prsc:2|A-283-OUT,B-228-OUT;n:type:ShaderForge.SFN_Multiply,id:273,x:32529,y:33892,varname:node_273,prsc:2|A-285-OUT,B-252-OUT;n:type:ShaderForge.SFN_Slider,id:281,x:31971,y:33041,ptovrint:False,ptlb:Light4,ptin:_Light4,varname:node_765,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:283,x:32053,y:33127,ptovrint:False,ptlb:Light5,ptin:_Light5,varname:node_1200,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:285,x:31942,y:33226,ptovrint:False,ptlb:Light6,ptin:_Light6,varname:node_7493,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Add,id:287,x:33026,y:32539,varname:node_287,prsc:2|A-214-OUT,B-263-OUT,C-265-OUT,D-267-OUT;n:type:ShaderForge.SFN_Add,id:289,x:32880,y:33792,varname:node_289,prsc:2|A-215-OUT,B-269-OUT,C-271-OUT,D-273-OUT;n:type:ShaderForge.SFN_Color,id:292,x:33994,y:32954,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_889,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:293,x:34347,y:33179,varname:node_293,prsc:2|A-292-RGB,B-185-OUT;n:type:ShaderForge.SFN_Tex2d,id:566,x:34347,y:32947,ptovrint:False,ptlb:BumpMap,ptin:_BumpMap,varname:node_4323,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;proporder:292-177-179-186-198-239-281-283-285-58-100-102-566;pass:END;sub:END;*/

Shader "_PlayHead_/fake_lighting_mk1_nobump" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _LightColor ("LightColor", Color) = (0.5,0.5,0.5,1)
        _BounceColor ("BounceColor", Color) = (0.5,0.5,0.5,1)
        _Light1 ("Light1", Range(0, 1)) = 1
        _Light2 ("Light2", Range(0, 1)) = 0
        _Light3 ("Light3", Range(0, 1)) = 0
        _Light4 ("Light4", Range(0, 1)) = 0
        _Light5 ("Light5", Range(0, 1)) = 0
        _Light6 ("Light6", Range(0, 1)) = 0
        _LightDirection1 ("LightDirection1", Vector) = (-0.5,0.866,0,0)
        _LightDirection2 ("LightDirection2", Vector) = (0.5,0.866,0,0)
        _LightDirection3 ("LightDirection3", Vector) = (1,0,0,0)
        _BumpMap ("BumpMap", 2D) = "bump" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 2.0
            uniform float4 _LightDirection1;
            uniform float4 _LightDirection2;
            uniform float4 _LightDirection3;
            uniform float4 _LightColor;
            uniform float4 _BounceColor;
            uniform float _Light1;
            uniform float _Light2;
            uniform float _Light3;
            uniform float _Light4;
            uniform float _Light5;
            uniform float _Light6;
            uniform float4 _Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float node_62 = 0.5;
                float node_61 = (node_62*distance(i.normalDir,_LightDirection1.rgb));
                float node_207 = (node_62*distance(i.normalDir,_LightDirection2.rgb));
                float node_256 = (node_62*distance(i.normalDir,_LightDirection3.rgb));
                float node_234 = 1.85;
                float2 node_70 = normalize(float2(i.posWorld.r,i.posWorld.g));
                float node_89 = saturate((node_234-distance(_LightDirection1.rgb,node_70)));
                float node_194 = saturate((node_234-distance(_LightDirection2.rgb,node_70)));
                float node_250 = saturate((node_234-distance(_LightDirection3.rgb,node_70)));
                float node_289 = (((_Light1*node_89)+(_Light2*node_194)+(_Light3*node_250))+(_Light4*(1.0 - node_89))+(_Light5*(1.0 - node_194))+(_Light6*(1.0 - node_250)));
                float3 emissive = (_Color.rgb*max(((((node_61*_Light1)+(node_207*_Light2)+(node_256*_Light3))+((1.0 - node_61)*_Light4)+((1.0 - node_207)*_Light5)+((1.0 - node_256)*_Light6))*node_289*_LightColor.rgb),(_BounceColor.rgb*(node_289*-0.6666667+0.5))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Mobile/Bumped Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
