﻿/************************************************************************************

Filename    :   AutoNavigationController.cs
Content     :   AutoNavigationController class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/

using UnityEngine;
using System.Collections;

public class AutoNavigationController : MonoBehaviour, INavigationController
{
	// Control State
	PlayHead.PlayHeadControlState controlState = PlayHead.PlayHeadControlState.auto;

	// Manual speed determination
	public	float desiredSpeed = 0f;
	public	bool overrideSpeed = false;

	// Direction
	public Vector3 motionDirection = new Vector3(0, 0, 1);

	// BPM speed
	public int bpm = 128;
	public int beatSize = 4;
	float	bpmSpeed = 0f; 
	float	speed = 0f;

	// Cache
	Transform tr;

	void	Awake	()
	{
		tr = transform;
		computeSpeed();	// Speed
	}

	// Speed
	void computeSpeed()
	{
		bpmSpeed = ((float)bpm / 60f) * (float)beatSize;
		if (overrideSpeed)
			speed = desiredSpeed;
		else
			speed = bpmSpeed;
	}


	#region Interface (very basic)

	public	void	launch	()
	{
		computeSpeed();
	}

	public void move	()
	{
		tr.position += motionDirection * speed * Time.deltaTime;
	}

	public	void	stop	()
	{

	}

	public	void	pause	()
	{

	}

	public	void	setSpeed	(float _speed, float delay = 0f)
	{
		if (delay > 0f)
			StartCoroutine(gradualSpeedEvolution(_speed, delay));
		else
			speed = _speed;
	}

	IEnumerator gradualSpeedEvolution(float desiredSpeed, float delay)
	{
		float startSpeed = speed;
		for (float timer = 0f; timer < delay; timer += Time.deltaTime)
		{
			speed = Mathf.Lerp(startSpeed, desiredSpeed, timer / delay);
			yield return null;
		}
		speed = desiredSpeed;
	}

	public	float	getSpeed	()
	{
		return speed;
	}

	public	void	resetSpeed	()
	{
		if (overrideSpeed)
			speed = desiredSpeed;
		else
			speed = bpmSpeed;
	}

	public	PlayHead.PlayHeadControlState getControlStateCondition	()
	{
		return controlState;
	}

	public	void	attachToPhase	(Phase phase)
	{

	}
	#endregion
}
