﻿/************************************************************************************

Filename    :   FreeNavigationController.cs
Content     :   FreeNavigationController class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;

public class FreeNavigationController : MonoBehaviour, INavigationController
{
	PlayHead.PlayHeadControlState controlState = PlayHead.PlayHeadControlState.free;

	// ghost
	public Transform ghostTransform;
	public float desiredSpeed = 10f;
	float speed = 0f;

	// 2D lateral motion
	public float timeBetweenLookAndRotation = 0.2f; // In some way, this is the reactivity
	public Vector2 lateralSpeed = new Vector2(20f,20f);
	public Vector2 minimum = new Vector2(-50, 0);
	public Vector2 maximum = new Vector2(50f, 50f);

	// Head look
	public	float maxAngle = 90f;
	float horizontalAngle = 0f;
	float verticalAngle = 0f;

	// Cache
	Transform playHeadTransform;

	void	Awake	()
	{
		playHeadTransform = transform;
		speed = desiredSpeed;
	}

	/* ----------------
	 * Interface
	 * ---------------- */

	#region

	public void launch()
	{
		
	}

	public	void move	()
	{
		compute2DMotion();
		if(ghostTransform != null)
			playHeadTransform.position = ghostTransform.position;
		lateral2DMotion();
	}

	public void stop()
	{
		
	}

	public	void	pause	()
	{

	}

	public	void	setSpeed	(float _speed, float delay = 0f)
	{
		/*
		if (delay > 0f)
			StartCoroutine(gradualSpeedEvolution(_speed, delay));
		else
			speed = _speed;
		 * */
	}

	/*
	IEnumerator		gradualSpeedEvolution	(float desiredSpeed, float delay)
	{
		float startSpeed = speed;
		for(float timer =0f; timer < delay; timer += Time.deltaTime)
		{
			speed = Mathf.Lerp(startSpeed, desiredSpeed, timer / delay);
			yield return null;
		}
		speed = desiredSpeed;
	}
	 * */

	public	float	getSpeed	()
	{
		return speed;
	}

	public	void	resetSpeed	()
	{
		speed = desiredSpeed;
	}

	public PlayHead.PlayHeadControlState getControlStateCondition()
	{
		return controlState;
	}

	#endregion

	public void attachToPhase	(Phase phase)
	{
		ghostTransform = phase.ghost;
	}

	/* ----------------
	 * 2D lateral motion
	 * ---------------- */

	#region

	void lateral2DMotion()
	{
		// Move
		Vector3 motion = playHeadTransform.position
			+ playHeadTransform.right * (horizontalAngle / maxAngle) * lateralSpeed.x * Time.deltaTime
			+ playHeadTransform.up * (-verticalAngle / maxAngle) * lateralSpeed.y * Time.deltaTime;
		
		motion.x = Mathf.Min(Mathf.Max(motion.x, minimum.x), maximum.x);
		motion.y = Mathf.Min(Mathf.Max(motion.y, minimum.y), maximum.y);
		playHeadTransform.position = motion;
	}

	void compute2DMotion	()
	{
		computeHeadDirectionOnXZPlane();
		computeHeadDirectionOnZYPlane();
	}

	void	computeHeadDirectionOnXZPlane	()
	{
		Vector3 playerHeadDirectionOnXZ = PlayerController.instance.eye.forward;
		playerHeadDirectionOnXZ.y = 0;
		playerHeadDirectionOnXZ.Normalize();

		// Compute angle
		horizontalAngle = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(playHeadTransform.forward, playerHeadDirectionOnXZ));
		Vector3 normal = Vector3.Cross(playHeadTransform.forward, playerHeadDirectionOnXZ);
		normal.Normalize();

		// Find the sign
		float fCross = Vector3.Dot(playHeadTransform.up, normal);
		horizontalAngle = fCross * horizontalAngle;
	}

	void computeHeadDirectionOnZYPlane()
	{
		Vector3 playerHeadDirectionOnZY = PlayerController.instance.eye.forward;
		playerHeadDirectionOnZY.x = 0;
		playerHeadDirectionOnZY.Normalize();

		// Compute angle
		verticalAngle = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(playHeadTransform.forward, playerHeadDirectionOnZY));
		Vector3 normal = Vector3.Cross(playHeadTransform.forward, playerHeadDirectionOnZY);
		normal.Normalize();

		// Find the sign
		float fCross = Vector3.Dot(playHeadTransform.right, normal);
		verticalAngle = fCross * verticalAngle;
	}

	#endregion
}
