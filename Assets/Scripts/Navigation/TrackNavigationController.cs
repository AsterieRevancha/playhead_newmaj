﻿/************************************************************************************

Filename    :   TrackNavigationController.cs
Content     :   TrackNavigationController class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// TrackNavigationController is the main song navigation controller. It follows an animation
/// bound to a given phase, and modifies its speed & progression.
/// </summary>
public class TrackNavigationController : MonoBehaviour, INavigationController
{
	#region Track states & actions

	/// <summary>
	/// The animation can be followed differently
	/// </summary>
	public enum TrackPlayState
	{
		playing = 0,
		paused = 1,
		rewinding = 2,
		fastforwarding = 3,
		all = 4
	}
	public TrackPlayState actualState;

	public enum TrackPlayAction
	{
		play = 0,
		pause = 1,
		rewind = 2,
		fastforward = 3,
		jump = 4,
		loop = 5,

		count = 6
	}

	// State mask & speeds
	public Dictionary<TrackPlayAction, bool> actionPermissions	= new Dictionary<TrackPlayAction, bool>();
	public Dictionary<TrackPlayAction, bool> actionsUnlocked	= new Dictionary<TrackPlayAction, bool>();
	Dictionary<TrackPlayState, float> stateSpeeds				= new Dictionary<TrackPlayState, float>();

	// Callbacks
	private enum TransitionType
	{
		start = 0,
		end = 1
	}
	public	delegate	void	TransitionModeFunction	();
	Dictionary<TrackPlayAction, Dictionary<TransitionType, TransitionModeFunction>> transitions = new Dictionary<TrackPlayAction, Dictionary<TransitionType, TransitionModeFunction>>();

	void	configure	()
	{
		stateSpeeds[TrackPlayState.playing] = 1f;
		stateSpeeds[TrackPlayState.paused] = 0f;
		stateSpeeds[TrackPlayState.rewinding] = -2f;
		stateSpeeds[TrackPlayState.fastforwarding] = 2f;

		// Permissions are updated real-time depending on the state
		actionPermissions[TrackPlayAction.rewind]		= true;
		actionPermissions[TrackPlayAction.pause]		= true;
		actionPermissions[TrackPlayAction.fastforward]	= true;
		actionPermissions[TrackPlayAction.play]			= false;
		actionPermissions[TrackPlayAction.jump]			= true;
		actionPermissions[TrackPlayAction.loop]			= true;

		// Locks allow the actions to be only available at a certain point
		actionsUnlocked[TrackPlayAction.rewind]			= true;
		actionsUnlocked[TrackPlayAction.pause]			= true;
		actionsUnlocked[TrackPlayAction.fastforward]	= true;
		actionsUnlocked[TrackPlayAction.play]			= true;
		actionsUnlocked[TrackPlayAction.jump]			= false; // no jump
		actionsUnlocked[TrackPlayAction.loop]			= false; // no loop

		// Transition functions
		for(TrackPlayAction i = 0; i < TrackPlayAction.count; i++)
		{
			transitions[i] = new Dictionary<TransitionType, TransitionModeFunction>();
			transitions[i][TransitionType.end]		= null;
			transitions[i][TransitionType.start]	= null;
		}
		transitions[TrackPlayAction.play][TransitionType.start] = startPlayMode;
		transitions[TrackPlayAction.pause][TransitionType.start] = startPauseMode;
		transitions[TrackPlayAction.fastforward][TransitionType.start] = startFastForwardMode;
		transitions[TrackPlayAction.rewind][TransitionType.start] = startRewindMode;
		transitions[TrackPlayAction.loop][TransitionType.start] = startLoop;

		transitions[TrackPlayAction.fastforward][TransitionType.end] = stopFastForwardMode;
		transitions[TrackPlayAction.rewind][TransitionType.end] = stopRewindMode;
	}

	public bool isPreparingAJump = false; // PlayHead is going to jump soon
	public bool isLooping = false; // PlayHead is locked in a jumping loop

	// Motion speed & temporary variable
	float speed;

	#endregion

	#region Jumping & looping variables

	// Jump tool
	public	JumpGate	jumpGate;

	// Position in the song
	int actualTime;
	Phase actualPhase;

	// Jump & loop
	SoundGate jumpTargetGate;
	SoundGate jumpOriginGate;
	Phase jumpTargetPhase;
	Phase jumpOriginPhase;
	Hypermeasure jumpTargetHyperMeasure;
	Hypermeasure jumpOriginHyperMeasure;

	int timeBeforeNextGate;
	bool hasJumped; //PlayHead has jumped
	bool hasChangedPhase; // Whether or not playhead changed phase while jumping

	#endregion

	// Cache
	SoundGate							targetGate = null;
	Transform							tr;
	PlayHeadTrackNavigationInterface	trackNavigationInterface;

	void	Awake	()
	{
		tr = transform;
		trackNavigationInterface = PlayHead.instance.trackNavigationInterface;
		configure();
	}

	#region Interface
		 
	public	void	launch	()
	{
		// Lock control panel
		tr.parent = actualPhase.ghost;
	}

	public	void	move	()
	{

	}

	public	void	stop	()
	{
		tr.parent = null;

		// Destro any existing loop
		if(isLooping)
			stopLoop();

		// Relaunch sound
		updateTime();
		//SoundManager.instance.setTimelinePosition(actualTime);
		//SoundManager.instance.pause(false);

		jumpGate.unload ();

		// Stop any behaviors
		if (transitions[(TrackPlayAction)actualState][TransitionType.end] != null)
			transitions[(TrackPlayAction)actualState][TransitionType.end] ();

		// Set default as playing
		actualState = TrackPlayState.playing;
	}

	public	void	pause	()
	{
		startPauseMode();
	}

	public void setSpeed	(float _speed, float delay=0f)
	{
		if (delay > 0f)
			StartCoroutine(gradualSpeedEvolution(_speed, delay));
		else
		{
			speed = _speed;
			actualPhase.modifyMotionAnimation(speed);
		}
	}

	IEnumerator gradualSpeedEvolution	(float desiredSpeed, float delay)
	{
		float startSpeed = speed;
		for (float timer = 0f; timer < delay; timer += Time.deltaTime)
		{
			speed = Mathf.Lerp(startSpeed, desiredSpeed, timer / delay);
			actualPhase.modifyMotionAnimation(speed);
			yield return null;
		}

		speed = desiredSpeed;
		actualPhase.modifyMotionAnimation(speed);
	}

	public	float	getSpeed	()
	{
		return speed;
	}

	public	void	resetSpeed	()
	{
		speed = stateSpeeds[actualState];
	}

	public	PlayHead.PlayHeadControlState	getControlStateCondition	()
	{
		return PlayHead.PlayHeadControlState.track;
	}

	public	bool	isActionAvailable	(TrackPlayAction desiredAction)
	{
		return actionsUnlocked[desiredAction] && actionPermissions[desiredAction];
	}

	public	bool	isActiveState	(TrackPlayAction desiredAction)
	{
		switch(desiredAction)
		{
			case TrackPlayAction.fastforward:
				return actualState == TrackPlayState.fastforwarding;
			case TrackPlayAction.loop:
				return isLooping;
			case TrackPlayAction.pause:
				return actualState == TrackPlayState.paused;
			case TrackPlayAction.play:
				return actualState == TrackPlayState.playing;
			case TrackPlayAction.rewind:
				return actualState == TrackPlayState.rewinding;
			default:
				return false;
		}
	}

	#endregion

	#region Phase management

	public	void	leavePhase	()
	{
		trackNavigationInterface.OnExitPhase();
		jumpGate.unload ();
	}

	public void attachToPhase	(Phase phase, SoundGate gate = null)
	{
		// Move playhead to new phase
		actualPhase = phase;
		tr.parent = phase.ghost;
		tr.localPosition = Vector3.zero;

		hasChangedPhase = true;

		// Update navigaton bar
		trackNavigationInterface.OnEnterPhase(phase);

		// Sync at specific time
		if(gate != null)
			syncToGate(gate);

		// Get animations
		TrackManager.instance.retrievePhaseAnimations (phase);
		TrackManager.instance.beginPulse (); // Sync pulse with phase animations
	}

	public Phase getActualPhase()
	{
		return actualPhase;
	}

	public float getTimeBeforeNextGate()
	{
		SoundGate tmp = null;
		if (speed > 0f)
			tmp = actualPhase.getNextGate(actualTime);
		else
			tmp = actualPhase.getPrecGate(actualTime);

		return Mathf.Abs(tmp.timePosition - actualTime) * 0.001f;
	}

	#endregion

	#region Speed & sync

	/// <summary>
	/// Get actual progression of the playhead.
	/// In the case where a transition just occured, phase & time are not updated.
	/// CAREFUL !! It is an *approximation* of the actual time.
	/// </summary>
	void updateTime()
	{
		// Actual time has already been computed during transition
		if (hasJumped)
			hasJumped = false;
		else
		{
			float progression = actualPhase.getAnimationProgression();

			// If animation is set to Clamp Forever, normalized time will increase/decrease after 1/0
			// We need to clamp the values between 0 & 1
			// Verify that afterward progression will not suffer an offset !
			progression = Mathf.Max(0f, Mathf.Min(1f, progression));

			actualTime = Mathf.RoundToInt(actualPhase.startTime + progression * actualPhase.lengthTime);
		}
	}

	public	void	syncToGate	(SoundGate gate)
	{
		actualTime = gate.timePosition;
		actualPhase.playAnimationAtTime(actualTime);
	}

	public	void	syncToTime	(int timeposition)
	{
		actualTime = timeposition;
		actualPhase.playAnimationAtTime(actualTime);
	}

	void planifyMotionVariation(float desiredSpeed)
	{
		// Compute time to use for interpolation (in ms)
		if (desiredSpeed > 0f)
			targetGate = actualPhase.getNextGate(actualTime);
		else
			targetGate = actualPhase.getPrecGate(actualTime);

		timeBeforeNextGate = Mathf.Abs(targetGate.timePosition - actualTime);

		// Launch decc/acceleration
		StartCoroutine(gradualSpeedEvolution(desiredSpeed,timeBeforeNextGate * 0.001f));
	}

	#endregion 

	#region Interface for navigation

	/// <summary>
	/// Note: The first state change after phase switch must reset the boolean to false.
	/// State changing differs during this first switch
	/// </summary>
	/// <param name="desiredAction"></param>
	/// <param name="forceAction"></param>
	public void changeState	(TrackPlayAction desiredAction, bool forceAction = false)
	{
		if (!forceAction && (!actionPermissions[desiredAction] || !actionsUnlocked[desiredAction]))
			return;

		updateTime();

		// Past state
		if(actualState != TrackPlayState.playing)
			SoundManager.instance.setTimelinePosition(actualTime);

		// Loop does not replace precedent action, no need to erase it
		if (desiredAction != TrackPlayAction.loop
		    && transitions[(TrackPlayAction)actualState][TransitionType.end] != null
		    && desiredAction != (TrackPlayAction)actualState)
			transitions[(TrackPlayAction)actualState][TransitionType.end]();

		// Apply effect of action
		transitions[desiredAction][TransitionType.start]();

		if (hasChangedPhase)
			hasChangedPhase = false;
	}

	public void completeJump()
	{
		hasJumped = true;

		// Destroy/hide precedent elements ONLY if the playhead is not looping
		if (!isLooping)
			cleanAfterJump();

		// Restore which state the playhead was in
		restoreStateAfterJump();
	}

	public	void	lockAction	(TrackPlayAction desiredAction, bool status)
	{
		actionsUnlocked[desiredAction] = !status;
	}

	// Temporary
	public	void	hideAction	(TrackPlayAction desiredAction, bool status)
	{
		actionPermissions[desiredAction] = !status;
	}

	#endregion

	#region PlayHead Mode Starters

	void restoreStateAfterJump()
	{
		updateTime();
	}

	public void startFastForwardMode	()
	{
		SoundManager.instance.pauseSong (true);
		planifyMotionVariation(stateSpeeds[TrackPlayState.fastforwarding]);
		actualPhase.playAnimationAtTime(actualTime);
		actualState = TrackPlayState.fastforwarding;

        // Display state on playhead
        PlayHead.instance.feedbackController.showNavigationState(TrackPlayState.fastforwarding, isLooping);

		// Feedback
		SoundManager.instance.launchSound("FastForward");

		// Jumping, alongside the rest, is authorized
		actionPermissions[TrackPlayAction.fastforward] = false;
		actionPermissions[TrackPlayAction.play] = true;
		actionPermissions[TrackPlayAction.pause] = true;
		actionPermissions[TrackPlayAction.rewind] = true;
		actionPermissions[TrackPlayAction.loop] = !isPreparingAJump || isLooping;
		actionPermissions[TrackPlayAction.jump] =!isPreparingAJump || !isLooping;
	}

	public	void	stopFastForwardMode	()
	{
		SoundManager.instance.stopSound("FastForward");
	}

	public void startRewindMode()
	{
		// Update motion
		SoundManager.instance.pauseSong (true);
		planifyMotionVariation(stateSpeeds[TrackPlayState.rewinding]);
		actualPhase.playAnimationAtTime(actualTime);
		actualState = TrackPlayState.rewinding;

        // Display state on playhead
        PlayHead.instance.feedbackController.showNavigationState(TrackPlayState.rewinding, isLooping);

		// Feedback
		SoundManager.instance.launchSound("Rewind");

		// Only play, pause & fastforward are allowed from this state
		actionPermissions[TrackPlayAction.fastforward] = true;
		actionPermissions[TrackPlayAction.play] = true;
		actionPermissions[TrackPlayAction.pause] = true;
		actionPermissions[TrackPlayAction.rewind] = false;
		actionPermissions[TrackPlayAction.loop] = false;
		actionPermissions[TrackPlayAction.jump] = false;
	}

	public	void	stopRewindMode	()
	{
		SoundManager.instance.stopSound("Rewind");
	}

	public void startPauseMode	()
	{

		SoundManager.instance.pauseSong(true);

		planifyMotionVariation(stateSpeeds[TrackPlayState.paused]);
		actualState = TrackPlayState.paused;

        // Display state on playhead
        PlayHead.instance.feedbackController.showNavigationState(TrackPlayState.paused,isLooping);

		// Feedback
		SoundManager.instance.launchSound("Pause");

		// Only play, rewind & fastforward are allowed from this state
		actionPermissions[TrackPlayAction.fastforward] = true;
		actionPermissions[TrackPlayAction.play] = true;
		actionPermissions[TrackPlayAction.pause] = false;
		actionPermissions[TrackPlayAction.rewind] = true;
		actionPermissions[TrackPlayAction.loop] = false;
		actionPermissions[TrackPlayAction.jump] = false;
	}

	public void startPlayMode	()
	{
		// Unpause motino & music
		SoundManager.instance.pauseSong (false);
		setSpeed(stateSpeeds[TrackPlayState.playing]);
		actualPhase.playAnimationAtTime(actualTime);
		actualState = TrackPlayState.playing;

        // Display state on playhead
        PlayHead.instance.feedbackController.showNavigationState(TrackPlayState.playing, isLooping);

		// Dirty hack: play must not be played after being attached to a phase
		if(!hasChangedPhase)
			SoundManager.instance.launchSound("Play");

		// Jumping, alongside the rest, is authorized
		actionPermissions[TrackPlayAction.fastforward] = true;
		actionPermissions[TrackPlayAction.play] = false;
		actionPermissions[TrackPlayAction.pause] = true;
		actionPermissions[TrackPlayAction.rewind] = true;
		actionPermissions[TrackPlayAction.loop] = !isPreparingAJump || isLooping;
		actionPermissions[TrackPlayAction.jump] = false;
	}

	#endregion

	#region Permission management

	public	void	blockForwardMotion	()
	{
		actionPermissions[TrackPlayAction.fastforward] = false;
		actionPermissions[TrackPlayAction.play] = false;
		actionPermissions[TrackPlayAction.pause] = false;
		actionPermissions[TrackPlayAction.rewind] = true;
		actionPermissions[TrackPlayAction.loop] = false;
		actionPermissions[TrackPlayAction.jump] = false;
	}

	public void blockBackwardMotion()
	{
		actionPermissions[TrackPlayAction.fastforward] = true;
		actionPermissions[TrackPlayAction.play] = true;
		actionPermissions[TrackPlayAction.pause] = false;
		actionPermissions[TrackPlayAction.rewind] = false;
		actionPermissions[TrackPlayAction.loop] = false;
		actionPermissions[TrackPlayAction.jump] = false;
	}

	#endregion

	#region Jump control

	/*
	public void prepareJumpDestinationPhase(int timelinePosition, string phaseName = "")
	{
		// Search phase
		if (phaseName.Equals(""))
		{
			phaseName = TravelManager.instance.getPhaseName(timelinePosition);
			jumpTargetPhase = TravelManager.instance.getExistingPhase(phaseName);
		}
		else
			jumpTargetPhase = TravelManager.instance.getExistingPhase(phaseName);

		// If not existing, load it
		if (jumpTargetPhase == null)
			StartCoroutine(asyncPrepareJumpDestinationPhase(phaseName, timelinePosition));
		else
			prepareJumpDestinationGate(jumpTargetPhase, timelinePosition);
	}*/

	/*
	public void prepareJumpDestinationGate(Phase _targetPhase, int _jumpTargetTime)
	{
		// Get targets
		jumpTargetPhase = _targetPhase;
		jumpTargetGate = jumpTargetPhase.getGateByTime(_jumpTargetTime);
		jumpTargetHyperMeasure = jumpTargetGate.getHypermeasure();

		// Show measures (if not in a loop, which in this case would be useless)
		// Sound transition requires the index of the measure in the phase
		int index = jumpTargetPhase.getHypermeasureIndex(jumpTargetHyperMeasure);
		jumpTargetPhase.OnJumpOnHyperMeasure(index, true);
		SoundManager.instance.setProgression((float)index + 1);

		// Verify if targetPhase is != from actual one.
		// That would requires special operations
		if (!actualPhase.name.Equals(jumpTargetPhase.name))
			hasChangedPhase = true;

		// Update actual timing, and with it
		updateTime();
		// Search for the first gate of closest hypermeasure
		// OR the last gate of actual hypermeasure in the special case where we are at the end
		Hypermeasure nextHypermeasure = actualPhase.getNextHypermeasure(actualTime);
		if (nextHypermeasure != null)
			jumpOriginGate = nextHypermeasure.getGateByIndex(0);
		else
			jumpOriginGate = actualPhase.getActualHypermeasure(actualTime).getLastGate();

		jumpOriginGate.prepareJump(jumpTargetGate);

		// Remember origin phase
		jumpOriginPhase = actualPhase;
		jumpOriginHyperMeasure = jumpOriginGate.getHypermeasure();

		// Before jumping, no backward state are allowed
		isPreparingAJump = true;
		actionPermissions[TrackPlayAction.rewind] = false;
	}
*/

	/// <summary>
	/// Show/hide hypermeasures OR destroy precedent phase after a jump.
	/// </summary>
	public void cleanAfterJump()
	{
		// If the playhead jumped to another phase, destroy old one (if there was one).
		if (hasChangedPhase)
		{
			if (jumpOriginPhase != null)
				Destroy(jumpOriginPhase.gameObject);
			hasChangedPhase = false;
		}
		else
		{
			// Hide original hypermeasure, and unlock it
			int index = jumpOriginPhase.getHypermeasureIndex(jumpOriginHyperMeasure);
			jumpOriginPhase.OnJumpOnHyperMeasure(index, false);
			jumpOriginPhase.UnlockAreaHypermeasure(jumpOriginHyperMeasure);
		}

		/* ------------ */
		// Note: If origin & destination are close, the locks serve to avoid hiding destination

		jumpTargetPhase.UnlockAreaHypermeasure(jumpTargetHyperMeasure); // Safely unlock destination
		SoundManager.instance.setProgression(0); // Keep the song moving bysetting its jumping parameter to zero
		isPreparingAJump = false; // Unlock the playhead backward motion
	}

	/*
	IEnumerator asyncPrepareJumpDestinationPhase(string phaseName, int timeline)
	{
		// Wait for phase
		AsyncOperation async = Application.LoadLevelAdditiveAsync(phaseName);
		yield return async;

		// Prepare jump to phase
		jumpTargetPhase = TravelManager.instance.getExistingPhase(phaseName);
		prepareJumpDestinationGate(jumpTargetPhase, timeline);
	}
	*/
	#endregion

	#region Loop control

	public void startLoop()
	{
		if(isLooping)
		{
			stopLoop();
			return;
		}

		updateTime();
		isLooping = true;

        // Show looping symbol
        PlayHead.instance.feedbackController.showNavigationState(actualState, isLooping);

		// Jump origin is the next hypermeasure
		// While the destination is the present one
		jumpTargetPhase = actualPhase;
		jumpOriginPhase = actualPhase;
		jumpOriginHyperMeasure = jumpTargetPhase.getNextHypermeasure(actualTime);
		jumpTargetHyperMeasure = jumpTargetPhase.getActualHypermeasure(actualTime);

		// Link both together
		jumpTargetGate = jumpTargetHyperMeasure.getGateByIndex(0);

		if(jumpOriginHyperMeasure == null)
		{
			jumpOriginHyperMeasure = jumpTargetHyperMeasure;
			jumpOriginGate = jumpOriginHyperMeasure.getLastGate();
		}
		else
			jumpOriginGate = jumpOriginHyperMeasure.getGateByIndex(0);

		// Direct FMOD to jump & loop at this hypermeasure
		int index = jumpTargetPhase.getHypermeasureIndex(jumpTargetHyperMeasure);
		SoundManager.instance.setProgression(index + 1);
		jumpTargetPhase.OnJumpOnHyperMeasure(index, true); // Display target measure, if it already had begun to disappear

		// Preapre a looping jump
		jumpGate.configure (jumpOriginGate, jumpTargetGate, true);

		// While looping, no backward state are allowed
		//actionPermissions[TrackPlayAction.rewind] = false;
		actionPermissions[TrackPlayAction.loop] = true;
		actionPermissions[TrackPlayAction.jump] = false;
		
	}

	public	void	stopLoop	()
	{
		updateTime();
		isLooping = false;
		SoundManager.instance.setProgression(0);

        // Hide looping symbol
        PlayHead.instance.feedbackController.showNavigationState(actualState, isLooping);

		jumpGate.disable ();

		// Unlock looping measure
		jumpTargetPhase.UnlockAreaHypermeasure(jumpTargetHyperMeasure);

		// Restore normal backward motion
		//actionPermissions[TrackPlayAction.rewind] = true;
		actionPermissions[TrackPlayAction.loop] = true;
	}

	#endregion

	#region Debug

#if DEBUG
	private Rect trackNavigation = new Rect(0, 200, 400f, 200f);
	private string navigationDebug = "";

	void OnGUI()
	{
		navigationDebug = "ActualState: " + actualState.ToString() + " \n";

		navigationDebug += "Permissions:\n";

		for (int i = 0; i <= (int)TrackPlayAction.loop; i++)
		{
			navigationDebug += ((TrackPlayAction)i).ToString() + " is "
				+ (actionPermissions[(TrackPlayAction)i] ? "Permitted" : "Forbidden")
				+ " and " + (actionsUnlocked[(TrackPlayAction)i] ? "Unlocked" : "Locked")
				+ " \n";
		}

		GUI.TextArea(trackNavigation, navigationDebug);
	}
#endif

	#endregion
}
