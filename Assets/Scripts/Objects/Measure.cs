﻿/************************************************************************************

Filename    :   Measure.cs
Content     :   Measure class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Static beatblock. Containing unique behaviors and colliders to detect
 * interaction with PlayHead. Linked to a timeline start and a length.
 * @author Kevin Wagrez
 */
[AddComponentMenu("Scripts/PlayHead/Elements/Measure")]
public class Measure : TimeBlock
{
	public SoundGate measureGate; // Entry of the measure is represented by a sound gate
	public string[] tracks; // a measure can be linked with several tracks
	
	//Transform animationHost; // where to summon animations

	protected	override	void init	()
	{
		//animationHost = transform;
		if (autoSync)
		{
			if (measureGate == null)
				measureGate = GetComponentInChildren<SoundGate>();
			measureGate.timePosition = startTime;
		}
		//measureGate.bindToMeasure(this);
	}

	public override	void OnEnterGate	(SoundGate _)
	{
		/*
		if (PlayHead.instance.trackNavigationController.actualState == TrackNavigationController.TrackPlayState.fastforwarding
			|| PlayHead.instance.trackNavigationController.actualState == TrackNavigationController.TrackPlayState.playing)
		{
			for (int i=0; i < tracks.Length; i++)
			{
				if(TrackManager.instance.trackStates[tracks[i]] == TrackManager.TrackState.active)
					TrackManager.instance.summonAnimation (animationHost,tracks[i]);
			}
		}
		 * */
	}


}
