﻿/************************************************************************************

Filename    :   SoundGate.cs
Content     :   SoundGate class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Scripts/PlayHead/Triggerables/SoundGate")]
public class SoundGate : MonoBehaviour
{

	public int timePosition;
	Phase			ownerPhase;

	public void bindToPhase(Phase phase)
	{
		ownerPhase = phase;
	}

	public Phase getPhase()
	{
		return ownerPhase;
	}

	#region Synchronization

	public	float	getTimeBeforeCrossing	()
	{
		return (timePosition - (ownerPhase.startTime + ownerPhase.getAnimationProgression() * ownerPhase.lengthTime)) *0.001f;
	}

	public void syncMotionAnimation()
	{
		ownerPhase.playAnimationAtTime(timePosition);
	}

	/*
	public	void	syncMusicTimeline	()
	{
		SoundManager.instance.setTimelinePosition(timePosition);
	}
	 * */

	#endregion

	/*
	#region Jump

	public void prepareJump	(SoundGate _jumpDestination, bool _looping = false)
	{
		isLooping = _looping;
		jumpDestination = _jumpDestination;
		callbacks[(int)TrackNavigationController.TrackPlayState.playing] += launchJump;
		callbacks[(int)TrackNavigationController.TrackPlayState.fastforwarding] += launchJump;
	}

	public	void	removeJump	()
	{
		callbacks[(int)TrackNavigationController.TrackPlayState.playing] -= launchJump;
		callbacks[(int)TrackNavigationController.TrackPlayState.fastforwarding] -= launchJump;
		isLooping = false;
	}

	void	launchJump	()
	{
		if (!isLooping)
		{
			callbacks[(int)TrackNavigationController.TrackPlayState.playing] -= launchJump;
			callbacks[(int)TrackNavigationController.TrackPlayState.fastforwarding] -= launchJump;
		}

		// Computes how far the playhead was on the animation track
		float offset = getTimeBeforeCrossing();
		Invoke("jump", offset);
	}

	void jump	()
	{
		PlayHead.instance.trackNavigationController.completeJump();
		jumpDestination.syncMotionAnimation();
	}

	#endregion
	 * */

	void OnDrawGizmos()
	{
		Gizmos.DrawWireCube(transform.position, new Vector3(3f, 3f, 0.1f));
	}

}
