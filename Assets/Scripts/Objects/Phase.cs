﻿/************************************************************************************

Filename    :   Phase.cs
Content     :   Phase class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Scripts/PlayHead/Elements/Phase")]
public class Phase : TimeBlock
{
	// Content
	public Hypermeasure[] hyperMeasures;
	public Transform ghost;

	// Time markers
	Dictionary<int, SoundGate> gateTableByTime = new Dictionary<int, SoundGate>();
	List<SoundGate> gatesInventorySortedByTime = new List<SoundGate>();
	Dictionary<int, Hypermeasure> hypermeasureTableByTime = new Dictionary<int, Hypermeasure>();

	// Motion animation management
	AnimationState motionAnimation = null;

	// Tracks
	public	AnimatedTrack[]	tracks;

	#region General

	protected	override	void init()
	{
		// Automatically set timings based on the order of specified measures
		if (autoSync)
		{
			int pathTime = 0;
			for (int i = 0; i < hyperMeasures.Length; i++)
			{
				hyperMeasures[i].startTime = startTime + pathTime;
				hyperMeasures[i].autoSync = true;
				pathTime += hyperMeasures[i].lengthTime;

				hypermeasureTableByTime[hyperMeasures[i].startTime] = hyperMeasures[i];
			}
		}

		// Animation & phase
		if(GetComponent<Animation>().GetClipCount() > 0)
			motionAnimation = GetComponent<Animation>()[GetComponent<Animation>().clip.name];

		//TravelManager.instance.addPhase(this);
	}

	void Start()
	{
		SoundGate[] gates = GetComponentsInChildren<SoundGate>();
		for (int i = 0; i < gates.Length; i++)
		{
			gateTableByTime[gates[i].timePosition] = gates[i];
			gates[i].bindToPhase(this);

			// Insert gate in a sorted inventory
			if (gatesInventorySortedByTime.Count > 0)
			{
				for (int j = 0; j < gatesInventorySortedByTime.Count; j++)
				{
					if (gatesInventorySortedByTime[j].timePosition > gates[i].timePosition)
					{
						gatesInventorySortedByTime.Insert(j, gates[i]);
						break;
					}
					else if (j == gatesInventorySortedByTime.Count - 1)
					{
						gatesInventorySortedByTime.Add(gates[i]);
						break;
					}
				}
			}
			else
				gatesInventorySortedByTime.Add(gates[i]);
		}

		// Show the first element
		/*
		for (int i = 0; i <= TravelManager.instance.farClip && i < hyperMeasures.Length; i++)
		{
			hyperMeasures[i].show(true);
		}
		 * */

		// Hide therest
		/*
		for (int i = TravelManager.instance.farClip + 1; i < hyperMeasures.Length; i++)
		{
			hyperMeasures[i].show(false);
		}
		 * */
	}

	#endregion

	#region Animation synchronization

	public void playAnimationAtTime	(int gateStart)
	{
		// Sync
		motionAnimation.time = (gateStart - startTime) * 0.001f;
		for (int i=0; i < tracks.Length; i++)
			tracks[i].setTime (motionAnimation.time);

		GetComponent<Animation>().Play (motionAnimation.name);
	}

	public void modifyMotionAnimation(float speed)
	{
		motionAnimation.speed = speed;
		for (int i=0; i < tracks.Length; i++)
			tracks[i].setSpeed (speed);
	}

	public float getAnimationProgression()
	{
		return motionAnimation.normalizedTime;
	}

	#endregion

	#region Gate management

	public SoundGate getGateByTime(int timePosition)
	{
		return gateTableByTime[timePosition];
	}

	/*
	public SoundGate getGateByIndex(int index)
	{
		return gatesInventorySortedByTime[index];
	}
	*/
	public SoundGate getNextGate(int timePosition)
	{
		for (int i = 0; i < gatesInventorySortedByTime.Count; i++)
		{
			if (gatesInventorySortedByTime[i].timePosition > timePosition + 200)
				return gatesInventorySortedByTime[i];
		}
		
		return null;
	}

	public SoundGate getPrecGate(int timePosition)
	{
		for (int i = 0; i < gatesInventorySortedByTime.Count; i++)
		{
			if (gatesInventorySortedByTime[i].timePosition > timePosition + 200)
				return gatesInventorySortedByTime[i - 1];
		}

		// Return the second last gate (never the last-last !)
		// That is because we must be stuck at the very last gate
		return gatesInventorySortedByTime[gatesInventorySortedByTime.Count - 2];
	}

	/*
	public SoundGate[] getPathToGate(int timePosition)
	{
		// Number of gates to collect
		int maxIndex = gatesInventorySortedByTime.Count;
		for (int i = 0; i < gatesInventorySortedByTime.Count; i++)
			if (gatesInventorySortedByTime[i].timePosition > timePosition)
			{
				maxIndex = i;
				break;
			}

		return gatesInventorySortedByTime.GetRange(0, maxIndex).ToArray();
	}
	 * */

	#endregion

	#region Hypermeasure management
	/*
	public	Hypermeasure	getHypermeasureByTime	(int timePosition)
	{
		return hypermeasureTableByTime[timePosition];
	}
	 * */
	/*
	public Hypermeasure getHypermeasureByIndex(int index)
	{
		return hyperMeasures[index];
	}
	*/

	public Hypermeasure getNextHypermeasure(int timePosition)
	{
		if (hypermeasureTableByTime.ContainsKey(timePosition))
		{
			return hypermeasureTableByTime[timePosition];
		}
		else
		{
			for (int i = 0; i < hyperMeasures.Length; i++)
			{
				if (hyperMeasures[i].startTime > timePosition)
					return hyperMeasures[i];
			}
		}

		return null;
	}

	public Hypermeasure getActualHypermeasure(int timePosition)
	{
		if (hypermeasureTableByTime.ContainsKey(timePosition))
		{
			return hypermeasureTableByTime[timePosition];
		}
		else
		{
			for (int i = 0; i < hyperMeasures.Length; i++)
				if (hyperMeasures[i].startTime > timePosition)
					return hyperMeasures[i - 1];
		}

		return hyperMeasures[hyperMeasures.Length - 1];
	}

	public	int	getHypermeasureIndex	(Hypermeasure measure)
	{
		for (int i = 0; i < hyperMeasures.Length; i++)
			if (hyperMeasures[i].GetInstanceID() == measure.GetInstanceID())
				return i;
		return -1;
	}

	#endregion

	#region DEPRECATED

	public	void	UnlockAreaHypermeasure	(Hypermeasure originMeasure)
	{
		/*
		originMeasure.isLocked = false;
		int indexHyMeasure = getHypermeasureIndex(originMeasure);

		// Look for next measure to show, if it is not locked
		for (int i = indexHyMeasure - TravelManager.instance.farClip; i <= indexHyMeasure + TravelManager.instance.farClip; i++)
			if (i >= 0 && i < hyperMeasures.Length)
				hyperMeasures[i].isLocked = false;
		 * */
	}

	public	void	OnJumpOnHyperMeasure (int indexMeasure, bool bOrigin)
	{
		// Look adjacent measures to show/hide
		/*
		for (int i = indexMeasure - TravelManager.instance.farClip; i <= indexMeasure + TravelManager.instance.farClip; i++)
		{
			if (i >= 0 && i < hyperMeasures.Length && !hyperMeasures[i].isLocked)
			{
				hyperMeasures[i].show(bOrigin);
				hyperMeasures[i].isLocked = true;
			}
		}
		 * */
	}

	public	override	void	OnEnterGate	(SoundGate gateEntered)
	{
		/*
		int indexHyMeasure = getHypermeasureIndex(gateEntered.getHypermeasure());

		int max = indexHyMeasure + TravelManager.instance.farClip;
		int min = indexHyMeasure - TravelManager.instance.farClip;

		// Look for next measure to show
		if (PlayHead.instance.trackNavigationController.actualState.Equals(TrackNavigationController.TrackPlayState.rewinding))
		{
			if (min >= 0 && !hyperMeasures[min].isLocked)
				hyperMeasures[min].show(true);

			if (max < hyperMeasures.Length && !hyperMeasures[max].isLocked)
				hyperMeasures[max].show(false);
		}
		else
		{
			if (max < hyperMeasures.Length && !hyperMeasures[max].isLocked)
				hyperMeasures[max].show(true);

			if (min >= 0 && !hyperMeasures[min].isLocked)
				hyperMeasures[min].show(false);
		}*/
	}

	#endregion

	#region Events for track navigation

	/*
	public	void	OnStartMotion	()
	{
		PlayHead.instance.trackNavigationController.OnStart();
	}

	public	void	OnEndMotion	()
	{
		PlayHead.instance.trackNavigationController.OnEnd();
	}
	*/
	#endregion
}


