﻿/************************************************************************************

Filename    :   JumpGate.cs
Content     :   JumpGate class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;

public class JumpGate : MonoBehaviour
{
    // Gates
    public Transform outGateTransform;
    Transform inGateTransform;

    // Sound effects
    public string loopSoundEffect = "LoopPhase1";

    // Effects
    public Renderer inGate;
    public Renderer outGate;
    public GameObject teleporter; // for animation

    // 
	bool		isLoopGate = false;
	SoundGate	jumpOrigin;
	SoundGate	jumpDestination;
	Collider	gateCollider;


	void	Awake	()
	{
        inGate.enabled = false;
        outGate.enabled = false;
		inGateTransform = transform;
		gateCollider = GetComponent<Collider>();
	}

	public void configure (SoundGate origin, SoundGate destination, bool _looping = false)
	{
		gateCollider.enabled = true;
        inGate.enabled = true;
        outGate.enabled = true;

        teleporter.SetActive(true);

		isLoopGate		= _looping;

		jumpOrigin		= origin;
		jumpDestination = destination;

		inGateTransform.position	= jumpOrigin.transform.position;
		inGateTransform.parent		= jumpOrigin.transform;

        outGateTransform.position   = jumpDestination.transform.position;
        outGateTransform.parent     = jumpDestination.transform;
	}

	public	void	disable	()
	{
		gateCollider.enabled = false;

        inGate.enabled = false;
        outGate.enabled = false;
        teleporter.SetActive(false);
	}

	public	void	unload	()
	{
		inGateTransform.parent = PlayHead.instance.transform;
        outGateTransform.parent = inGateTransform;
	}

	void	OnTriggerEnter	(Collider other)
	{
		float offset = jumpOrigin.getTimeBeforeCrossing ();

        // Jump sound
        SoundManager.instance.launchSound(loopSoundEffect);

		Invoke ("jump", offset / PlayHead.instance.trackNavigationController.getSpeed());
	}

	void jump ()
	{
		PlayHead.instance.trackNavigationController.completeJump ();

        // Animation is rewinded according to desired progression
		jumpDestination.syncMotionAnimation ();

		if (!isLoopGate)
		{
			gateCollider.enabled = false;
			inGateTransform.parent = PlayHead.instance.transform;
            outGateTransform.parent = inGateTransform;
            gameObject.SetActive(false);
		}
	}

	void	OnDrawGizmos	()
	{
		Gizmos.DrawWireSphere (transform.position, 1f);
	}
}
