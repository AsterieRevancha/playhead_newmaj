﻿/************************************************************************************

Filename    :   Hypermeasure.cs
Content     :   Hypermeasure class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Phase structure, containing multiple blocks. Has an entry and an exit.
 * Also contains a motion animation for the playhead to follow.
 * @author Kevin Wagrez
 */
[AddComponentMenu("Scripts/PlayHead/Elements/Hypermeasure")]
public class Hypermeasure : TimeBlock
{
	// A locked hypermeasure cannot be hidden, or shown, before being released
	public	bool isLocked = false;
	public	Measure[] measures; // Sub-structures containing tracks

	List<SoundGate> gates = new List<SoundGate>(); // Special triggers to mark delimitations between measures

	protected	override	void	init	()
	{
		if (autoSync)
		{
			int pathTime = 0;
			for (int i = 0; i < measures.Length; i++)
			{
				measures[i].startTime = startTime + pathTime;
				pathTime += measures[i].lengthTime;
			}
		}

		SoundGate[] childGates = GetComponentsInChildren<SoundGate>();
		for (int i = 0; i < childGates.Length; i++)
		{
			//childGates[i].bindToHypermeasure(this);
			gates.Add(childGates[i]);
		}

	}

	public	void	show	(bool bValue)
	{
		gameObject.SetActive(bValue);
	}

	#region Gate fetching

	public	SoundGate	getGateByIndex	(int index)
	{
		return gates[index];
	}

	public	SoundGate getLastGate	()
	{
		return gates[gates.Count-1];
	}

	#endregion
}
