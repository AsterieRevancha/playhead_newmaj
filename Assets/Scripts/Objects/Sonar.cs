﻿/************************************************************************************

Filename    :   Sonar.cs
Content     :   Sonar class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;

public class Sonar : MonoBehaviour, ILookable
{
    public Renderer sonar;
    public Renderer sonarWave;

    // Length 
    public float tubeLength;

    // Pulse
    public AnimationCurve pulseAnimation;

    // Read
    public AnimationCurve openAnimation;
    public AnimationCurve waveAnimation;

    bool isReading = false;

    // State
    enum ActivationState
    {
        active,
        inactive
    }
    ActivationState state= ActivationState.inactive;

    // Shortcuts
    int activationID;
    int readID;
    int faderID;
    int pulseID;
    int tubeLengthID;

    // Sound
    public string sonarSound;

    void    Awake   ()
    {
        activationID = Shader.PropertyToID("_Activation");
        readID = Shader.PropertyToID("_Lecture1");
        faderID = Shader.PropertyToID("_Fader1");
        pulseID = Shader.PropertyToID("_Pulse");
        tubeLengthID = Shader.PropertyToID("_TubeShortening");

        sonar.material.SetFloat(tubeLengthID, tubeLength);
    }

    void    Start   ()
    {
        StartCoroutine(pulse());
    }

	public  void    OnLookAt    (float time)
    {
        if (state == ActivationState.active)
            return;

        // Stop pulse
        StopAllCoroutines();

        // Activate
        StartCoroutine(activate());
        
        state = ActivationState.active;
    }

    /// <summary>
    /// Re-trigger activation once the playhead passes through
    /// </summary>
    public  void    OnCollision ()
    {
        if (state == ActivationState.active && !isReading)
        {
            StartCoroutine(activate());
        }
    }

    IEnumerator pulse ()
    {
        while(true)
        {
            for(float timer = 0f; timer < 1f; timer +=Time.deltaTime)
            {
                sonar.material.SetFloat(pulseID, pulseAnimation.Evaluate(timer));
                yield return null;
            }
        }
    }

    IEnumerator activate ()
    {
        // Launch sonar random sound
        SoundManager.instance.launchSound(sonarSound);

        // Avoid any concurrent activations
        isReading = true;

        // Animation must be adapted to speed of playhead
        for (float timer = 0f; timer < 1f; timer += Time.deltaTime * PlayHead.instance.trackNavigationController.getSpeed())
        {
            float value = openAnimation.Evaluate(timer);

            // Open sonar
            sonar.material.SetFloat(activationID, value);
            sonarWave.material.SetFloat(faderID, value);

            // Feel the Wave
            sonarWave.material.SetFloat(readID, waveAnimation.Evaluate(timer));

            yield return null;
        }

        // Now sonar is free to get activated again
        isReading = false;
    }

}
