﻿/************************************************************************************

Filename    :   IOFeedback_ChangeMaterialOnState.cs
Content     :   IOFeedback_ChangeMaterialOnState class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;

[AddComponentMenu("Scripts/PlayHead/Feedbacks/IO/Change material on state")]
public class IOFeedback_ChangeMaterialOnState : IOFeedback
{
	[System.Serializable]
	public struct ParameterToState
	{
		public InteractiveObject.IOState desiredState;
		public string parameterName;
		public float value;
	}
	public ParameterToState[]	parametersToChange;
	public Material				objectMaterial;

	void OnEnable ()
	{
		for(int i=0; i<parametersToChange.Length; i++)
		{
			if (owner.actualState == parametersToChange[i].desiredState)
				objectMaterial.SetFloat(parametersToChange[i].parameterName, parametersToChange[i].value);
		}
	}
}

