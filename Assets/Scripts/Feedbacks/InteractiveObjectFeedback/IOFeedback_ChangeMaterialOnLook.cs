﻿/************************************************************************************

Filename    :   IOFeedback_changeMaterialOnLook.cs
Content     :   IOFeedback_changeMaterialOnLook class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;

[AddComponentMenu("Scripts/PlayHead/Feedbacks/IO/Change material on look")]
public class IOFeedback_ChangeMaterialOnLook : IOFeedback
{
	public bool		instantEffect = false;
	public string	parameterName = "";
	public bool		applyParameterWhileLocked = false;
	public bool		useInstance = false;

	public Renderer buttonRenderer;

	void Update()
	{
		if (owner.isFrozen())
			return;

		if(instantEffect)
		{
			if (owner.getTimeLookedAt() > 0f)
				modifyMaterial(1f);
			else
				modifyMaterial(0f);
		}
		else if (!owner.objectIsLocked() || applyParameterWhileLocked)
			modifyMaterial(owner.getTimeLookedAt() / owner.getMaxTime());
	}

	void OnDisable()
	{
		modifyMaterial(0f);
	}

	void	modifyMaterial	(float value)
	{
		if (useInstance)
			buttonRenderer.material.SetFloat(parameterName, value);
		else
			buttonRenderer.sharedMaterial.SetFloat(parameterName, value);
	}

}
