﻿/************************************************************************************

Filename    :   IOFeedback_FreezeAndHidePlayHeadButton.cs
Content     :   IOFeedback_FreezeAndHidePlayHeadButton class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;

[AddComponentMenu("Scripts/PlayHead/Feedbacks/IO/Freeze playhead button")]
public class IOFeedback_FreezeAndHidePlayHeadButton : IOFeedback
{
	public	TrackNavigationController.TrackPlayAction	desiredAction;
	public	InteractiveObject							button;
	public	Material									buttonMaterial;
	public	string										availabilityParameter = "_Activable";
	public	string										activeParameter = "_Activation";

	// Lock menu button if state is not available
	void OnEnable	()
	{
		if (PlayHead.instance == null)
			return;

		if (PlayHead.instance.trackNavigationController.isActionAvailable(desiredAction))
		{
			button.freezeObject(!PlayHead.instance.trackNavigationController.actionPermissions[desiredAction]);

			if(PlayHead.instance.trackNavigationController.actionPermissions[desiredAction])
				buttonMaterial.SetFloat(availabilityParameter,1f);
		}
		else
		{
			if (PlayHead.instance.trackNavigationController.isActiveState(desiredAction))
			{
				buttonMaterial.SetFloat(activeParameter, 1f);
			}
			else
				buttonMaterial.SetFloat(availabilityParameter, 0.33f);

			button.freezeObject(true);
			
		}
	}

}
