﻿/************************************************************************************

Filename    :   PulseAnimator.cs
Content     :   PulseAnimator class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;

public class PulseAnimator : MonoBehaviour {

	public string trackName;

	// Animation sync
	public string			pulseAnimationName;
	public Animation		pulseAnimation;
	public AnimationState	pulseAnimationState;

	// Animated value
	public float			pulse;

	void Awake ()
	{
        reload();
	}

    public  void    reload  ()
    {
        pulseAnimationState = pulseAnimation[pulseAnimationName];
    }

	void Update()
	{
		if (pulseAnimation.isPlaying)
			pulseAnimationState.speed = PlayHead.instance.trackNavigationController.getSpeed();
	}

	public	bool	isPlaying	()
	{
		return pulseAnimation.isPlaying;
	}

    /// <summary>
    /// Called only phase 5 when we switch the descend pulse for a quicker one
    /// </summary>
    /// <param name="desiredPulseName"></param>
    public  void    forceSwitchPulse    (string desiredPulseName)
    {
        AnimationState precAnimation = pulseAnimationState;
        pulseAnimationState = pulseAnimation[desiredPulseName];

        if (pulseAnimation.isPlaying)
        {
            pulseAnimation.Stop(pulseAnimationName);
            pulseAnimationState.time = precAnimation.time % pulseAnimationState.length;
            pulseAnimationState.speed = precAnimation.speed;
            pulseAnimation.Play(desiredPulseName);
        }
        
    }

	public	void	beginPulse	(float startTime = 0f)
	{
        pulseAnimation.Play(pulseAnimationState.name);
		pulseAnimationState.normalizedTime = startTime;
		pulseAnimationState.speed = PlayHead.instance.trackNavigationController.getSpeed();
	}

	public	void	forceNormalizedTime	(float time)
	{
		pulseAnimationState.normalizedTime = time;
	}

	public void setSpeed	(float speed)
	{
		pulseAnimationState.speed = 1f;
	}

	public void play()
	{
        pulseAnimation.Play(pulseAnimationState.name);
	}

	public	void	stop	()
	{
		pulseAnimation.Stop();
	}
}
