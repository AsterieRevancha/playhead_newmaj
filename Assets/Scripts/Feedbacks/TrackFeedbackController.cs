﻿/************************************************************************************

Filename    :   TrackFeedbckController.cs
Content     :   TrackFeedbackController class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;

public class TrackFeedbackController : MonoBehaviour
{
	public	string		trackName;

	float				activationLevel = 0f;

	// Animations
	public	AnimationState	phaseTrackAnimation; // Set for each phase
	public	Animation	feedbackAnimation;
	AnimationState		focusAnimationState;

	public	string		activationAnimationName;
	public	string		focusAnimationName;
	public	string		idleAnimationName;
    public  string      eraseAnimationName;

    // Loading bar
    public TrackLoadingBar gauge;

	// Sound
    public  string      volumeParameter = "";
	public	string		activationProcessSound	= "Waiting";
	public	string		activationEndParameter	= "Load"; // Parameter to switch one it turns off
	public	string		focusSound				= "Focus";
	public  string		designerTrackDialog		= "TrackName";

    // Lights
    public  string      objectiveParameterName = "_lightX";
    public  Gradient    flashLight;

    // Lightning effect
    public int lightningIndex;

    // Glitch
    public LegacyColorAnimatorForMaterial materialAnimator;

	// Pulse
	public	PulseAnimator	pulseController;

	public	Material[]		glowMaterials;
	public	Material[]		gateMaterials; // Gate material
	public	Material[]		trackMaterials; // Track material
	public	string[]		lightParameters; // PlayHead lights
    public  string[]        lightColors; // PlayHead light colors
	public	string			glowParameter		= "_Apparition";
	public	string			pulseParameterName	= "_Pulse"; // Pulse parameter

	// IDs cache
	int[]	lightParametersIDs;
    int[]   lightColorIDs;
	int	pulseParameterID;
	int glowParameterID;
	int lightParameterID;
	int activationFlashID;
    int objectiveLightID;
    int colorID;
    int circuitsID;
    int glitchID;


	void Awake ()
	{
		pulseParameterID	= Shader.PropertyToID (pulseParameterName);
		glowParameterID		= Shader.PropertyToID (glowParameter);
		lightParameterID	= Shader.PropertyToID ("_Light");
		activationFlashID	= Shader.PropertyToID ("_ActivationFlash");
        objectiveLightID    = Shader.PropertyToID(objectiveParameterName);
        colorID             = Shader.PropertyToID("_Color");
        circuitsID          = Shader.PropertyToID("_CircuitsColor");
        glitchID            = Shader.PropertyToID("_GlitchIntensity");

		lightParametersIDs = new int[lightParameters.Length];
        lightColorIDs = new int[lightColors.Length];
        for (int i = 0; i < lightParameters.Length; i++)
        {
            lightParametersIDs[i] = Shader.PropertyToID(lightParameters[i]);
            lightColorIDs[i] = Shader.PropertyToID(lightColors[i]);
        }

		feedbackAnimation = GetComponent<Animation>();
		focusAnimationState = feedbackAnimation[focusAnimationName];
		reload ();
	}

	public		void	reload	()
	{
        // Pulsar must also be reloaded
        pulseController.reload();

        // Retrieve original track material
        materialAnimator.targetMaterial = trackMaterials[0];

        // Put materials to idle state
		feedbackAnimation.Play(idleAnimationName);

        // Switch loading bar back to normal
        gauge.switchToState(TrackLoadingBar.LoadingState.Loading);

        // Turn off lights
		for (int i=0; i < gateMaterials.Length; i++)
			gateMaterials[i].SetFloat (lightParameterID, 0f);
		for (int i=0; i < lightParameters.Length; i++)
			PlayHead.instance.feedbackController.modifyLightIntensity (lightParametersIDs[i], 0f);
	}

	void	OnDestroy	()
	{
		feedbackAnimation.Play(idleAnimationName);
	}

	void	Update	()
	{
		if (pulseController.isPlaying ())
			for (int i = 0; i < trackMaterials.Length; i++)
				trackMaterials[i].SetFloat (pulseParameterID, pulseController.pulse);
	}

	#region Main functions

	public	void	onBeginLook	(bool previsualize = true)
	{
		SoundManager.instance.launchSound(focusSound);
		SoundManager.instance.stopSound(activationProcessSound);
		SoundManager.instance.setParameter(activationProcessSound,activationEndParameter,0f);

        // Add sound previsualization
        if (previsualize)
            SoundManager.instance.setSongParameter(volumeParameter, 0.5f);

		// Compute progression
		activationLevel = TrackManager.instance.getActivationLevel(trackName);
		focusAnimationState.normalizedTime = activationLevel;

        gauge.changesLevel(activationLevel, true);

		// Show pulse
		for (int i=0; i < lightParameters.Length; i++ )
			PlayHead.instance.feedbackController.modifyLightIntensity (lightParametersIDs[i], pulseController.pulse);

		feedbackAnimation.Play(focusAnimationName);
		focusAnimationState.speed = 0f;

		if (glowMaterials != null)
			StartCoroutine (fadeGlowTo (1f));
	}

	public	void	onLook	()
	{
		// Compute progression
		activationLevel = TrackManager.instance.getActivationLevel(trackName);
		focusAnimationState.normalizedTime = activationLevel;

        // Indicator fills up
        gauge.changesLevel(activationLevel,true);

		// Glow becomes whiter
		for (int i=0; i < glowMaterials.Length; i++)
			glowMaterials[i].SetFloat (activationFlashID, activationLevel);

		// Show pulse
		for (int i=0; i < lightParameters.Length; i++)
			PlayHead.instance.feedbackController.modifyLightIntensity (lightParametersIDs[i], pulseController.pulse);
	}

    /// <summary>
    /// Updates focus animation
    /// </summary>
	public	void	onNotLook	()
	{
		// Compute progression
		activationLevel = TrackManager.instance.getActivationLevel (trackName);
		focusAnimationState.normalizedTime = activationLevel;

        // Indicator empties
        gauge.changesLevel(activationLevel,false);

		// Glow becomes darker
        StartCoroutine(fadeGlowTo(0f));
		for (int i=0; i < glowMaterials.Length; i++)
			glowMaterials[i].SetFloat (activationFlashID, activationLevel);

		// Turn off the lights
		for (int i=0; i < lightParameters.Length; i++)
			PlayHead.instance.feedbackController.modifyLightIntensity (lightParametersIDs[i], 0f);

	}

	#endregion

	#region Feedbacks

	public	void	initiateActivation	(float delay)
	{
		SoundManager.instance.launchSound (activationProcessSound);

		// Glow will fade
		StartCoroutine (fadeGlowTo (0f));

		// Lights will flash
		StartCoroutine (flashLights (delay));

        // Fade gauge
        gauge.changesLevel(1f, false);
	}

	public	void	activate	()
	{
        // Process waiting sound is closed
		SoundManager.instance.setParameter (activationProcessSound, activationEndParameter,1f);
        SoundManager.instance.setSongParameter(volumeParameter, 0.8f); // Sound at maximum volume

        // Designer acknowledges activation
		DesignerManager.instance.speak(designerTrackDialog,false);

		// Turn on the lights on the playhead
		for (int i=0; i < lightParameters.Length; i++)
			PlayHead.instance.feedbackController.modifyLightIntensity (lightParametersIDs[i], 1f);

        // Launch lightning ! 
        PlayHead.instance.feedbackController.launchLightning(lightningIndex);

		feedbackAnimation.Play(activationAnimationName);
		for (int i=0; i < gateMaterials.Length; i++)
			gateMaterials[i].SetFloat (lightParameterID, 1f);

        PlayHead.instance.feedbackController.validateObjective(objectiveLightID);
	}

    /// <summary>
    /// Switches legacy animator target materials with glitched materials
    /// </summary>
    public  void    switchAnimatorToMaterial   (int index)
    {
        materialAnimator.targetMaterial = trackMaterials[index];
    }


    /// <summary>
    /// Sync glitched material & standard material
    /// </summary>
    public  void    sync    ()
    {
        trackMaterials[1].SetColor(colorID, trackMaterials[0].GetColor(colorID));
        trackMaterials[1].SetColor(circuitsID, trackMaterials[0].GetColor(circuitsID));
        trackMaterials[1].SetFloat(glitchID, 0f);
    }

    public  void    erase   ()
    {
        // Turn off the colors
        feedbackAnimation.Play(eraseAnimationName);

        // Glow will fade
        StartCoroutine(fadeGlowTo(0f));

        // Stop the pulse
        pulseController.stop();

        // No more gauge
        gauge.changesLevel(0f, false);

        // Sound off
        SoundManager.instance.setSongParameter(volumeParameter, 0f);

        // No light on the playhead
        for (int i = 0; i < lightParameters.Length; i++)
            PlayHead.instance.feedbackController.modifyLightIntensity(lightParametersIDs[i], 0f);

        // Launch lightning ! 
        PlayHead.instance.feedbackController.launchLightning(lightningIndex);

        PlayHead.instance.feedbackController.cancelObjective(objectiveLightID);
    }

    /// <summary>
    /// While Track is malfunctioning
    /// </summary>
    public  void    malfunction ()
    {
        gauge.switchToState(TrackLoadingBar.LoadingState.Unloading);
        switchAnimatorToMaterial(0);
    }

	public	void	repair	()
	{
        // Launch lightning ! 
        PlayHead.instance.feedbackController.launchLightning(lightningIndex);

		// Turn on the lights on the playhead
		for (int i=0; i < lightParameters.Length; i++)
			PlayHead.instance.feedbackController.modifyLightIntensity (lightParametersIDs[i], 1f);

		feedbackAnimation.Play (activationAnimationName);
		for (int i=0; i < gateMaterials.Length; i++)
			gateMaterials[i].SetFloat (lightParameterID, 1f);
	}

	public	void	beginPulse	()
	{
		if(phaseTrackAnimation != null)
			pulseController.beginPulse (phaseTrackAnimation.normalizedTime);
	}

	#endregion

	#region Coroutines

	IEnumerator		fadeGlowTo	(float value)
	{
		float startValue = glowMaterials[0].GetFloat (glowParameterID);
		for(float timer = 0f; timer < 0.2f; timer += Time.deltaTime)
		{
			for (int i=0; i < glowMaterials.Length; i++ )
				glowMaterials[i].SetFloat (glowParameterID, Mathf.Lerp (startValue, value, timer / 0.2f));
			yield return null;
		}
	}

	IEnumerator		flashLights	(float delay)
	{
		for(float timer = 0f; timer < delay; timer += Time.deltaTime)
		{
			for (int i=0; i < lightParameters.Length; i++)
				PlayHead.instance.feedbackController.modifyLightIntensity (lightParametersIDs[i], Mathf.Lerp (0f, 1f, timer / delay));
			yield return null;
		}

        for(float timer = 0f; timer < 0.5f; timer += Time.deltaTime)
        {
            for (int i = 0; i < lightParameters.Length; i++)
                PlayHead.instance.feedbackController.modifyLightColor(lightColorIDs[i], flashLight.Evaluate(timer / 0.5f));
            yield return null;
        }
	}


	#endregion
}
