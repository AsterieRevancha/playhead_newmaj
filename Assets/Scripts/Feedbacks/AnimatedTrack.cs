﻿/************************************************************************************

Filename    :   AnimatedTrack.cs
Content     :   AnimatedTrack class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimatedTrack : MonoBehaviour
{
	// Offset for local animation
	public	float		startOffset;

	public	string		trackName;
	public	Phase		ownerPhase;

	public	Animation	motionAnimation;
	public	Animation	localAnimation;

	public	Renderer[]	trackRenderers;

	//	External access is required to sync pulse
	public	AnimationState		localAnimationState;
	public	AnimationState		motionAnimationState;

	void	Awake	()
	{
		localAnimationState = localAnimation[localAnimation.clip.name];
		motionAnimationState = motionAnimation[motionAnimation.clip.name];
	}

	public	void	stop	()
	{
        for (int i = 0; i < trackRenderers.Length; i++)
            trackRenderers[i].enabled = false;

		motionAnimation.Stop ();
		localAnimation.Stop ();
	}

	public	void	play	()
	{
		for (int i=0; i < trackRenderers.Length; i++)
			trackRenderers[i].enabled = true;

		localAnimation.Play ();
		motionAnimation.Play ();

		// Sync
		motionAnimationState.time	= ownerPhase.getAnimationProgression () * motionAnimationState.length;
		motionAnimationState.speed	= PlayHead.instance.trackNavigationController.getSpeed ();
		localAnimationState.time	= startOffset + motionAnimationState.time % localAnimationState.length;
		localAnimationState.speed	= 0f;
	}

	public	void	setTime	(float time)
	{
		motionAnimationState.time = time;
		localAnimationState.time = startOffset + motionAnimationState.time % localAnimationState.length;
	}

	public	void	setSpeed	(float speed)
	{
		motionAnimationState.speed = speed;
		localAnimationState.speed = 0f;
	}

	void	Update	()
	{
		if(motionAnimation.isPlaying)
			localAnimationState.time = startOffset + motionAnimationState.time % localAnimationState.length;
	}

}
