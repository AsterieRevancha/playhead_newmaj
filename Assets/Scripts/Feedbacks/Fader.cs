﻿/************************************************************************************

Filename    :   Fader.cs
Content     :   Fader class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;

public class Fader : MonoBehaviour
{
	public enum FadeType
	{
		FadeIn,
		FadeOut
	}

    public enum FadeColor
    {
        white,
        black
    }

    public enum FadeZone
    {
        all,
        environment
    }

    // Fader meshes
	public	Renderer	allFader;
    public  Renderer    envFader;

    // Fades
    public  Gradient    whiteFade;
    public  Gradient    blackFade;

	public	void	parallelFade	(float delay, FadeType desiredFade, FadeZone zone = FadeZone.all, FadeColor col = FadeColor.black)
	{
        StartCoroutine(fade(delay, desiredFade, col, zone));
	}

    public IEnumerator asyncFade    (float delay, FadeType desiredFade, FadeZone zone = FadeZone.all, FadeColor col = FadeColor.black)
	{
        yield return StartCoroutine(fade(delay, desiredFade, col, zone));
	}

	IEnumerator	fade	(float delay, FadeType type, FadeColor col, FadeZone zone)
	{
        // Gradient
        Gradient faderGradient = null;
        switch(col)
        {
            case FadeColor.black:
                faderGradient = blackFade;
                break;
            case FadeColor.white:
                faderGradient = whiteFade;
                break;
        }

        // Choose Renderer
        Renderer desiredRenderer = null;
        switch(zone)
        {
            case FadeZone.all:
                desiredRenderer = allFader;
                break;
            case FadeZone.environment:
                desiredRenderer = envFader;
                break;
        }

        // Choose start values
        float start = 0f, target = 0f;
        switch(type)
        {
            case FadeType.FadeIn:
                start = 0f;
                target = 1f;
                desiredRenderer.enabled = true;
                break;

            case FadeType.FadeOut:
                start = 1f;
                target = 0f;
                break;
        }

        // Configure
        desiredRenderer.sharedMaterial.color = faderGradient.Evaluate(start);

        // Timer
		float startTime = Time.realtimeSinceStartup;
		float endOfLoop = Time.realtimeSinceStartup + delay;

        while (Time.realtimeSinceStartup < endOfLoop)
        {
            float progress = (Time.realtimeSinceStartup - startTime) / delay;
            desiredRenderer.sharedMaterial.color = faderGradient.Evaluate(Mathf.Lerp(start, target, progress));
            yield return null;
        }
        desiredRenderer.sharedMaterial.color = faderGradient.Evaluate(target);

        switch (type)
        {
            case FadeType.FadeIn:
                break;

            case FadeType.FadeOut:
                desiredRenderer.enabled = false;
                break;
        }
	}
}
