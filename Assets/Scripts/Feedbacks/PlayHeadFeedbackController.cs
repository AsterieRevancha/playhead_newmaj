﻿/************************************************************************************

Filename    :   PlayHeadFeedbackController.cs
Content     :   PlayHeadFeedbackController class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayHeadFeedbackController : MonoBehaviour
{
    // Material control
	public Material cockpitMainMaterial;
    public Material cockpitBaseMaterial;
	public Material cockpitEffectsMaterial;
    public Material cockpitObjectiveMaterial;
    public Material cockpitNavigationStateMaterial;

    // Lightning effect renderers; Found by using indexes stored in track controllers
    public Renderer[] lightningEffects;
    public string lightningSound = "Lightning";

    // shortcuts
	int	glowID;
    int numberID;
    int alphaID;
    int colorID;
    int lightningOpacityID;

    // Objective indicator
    int objectivesAccomplished = 0;

    // Flash gradient
    public Gradient flashColor;
    public AnimationCurve lightningAnimation;

    // Navigation
    Dictionary<TrackNavigationController.TrackPlayState, Vector2> offsets = new Dictionary<TrackNavigationController.TrackPlayState, Vector2>();
    

	void	Awake	()
	{
		glowID      = Shader.PropertyToID   ("_GlowAnim");
        numberID    = Shader.PropertyToID   ("_Number");
        alphaID     = Shader.PropertyToID   ("_Alpha");
        colorID     = Shader.PropertyToID   ("_Color");
        lightningOpacityID = Shader.PropertyToID("_Opacity");

        offsets[TrackNavigationController.TrackPlayState.playing] = new Vector2(0f, 1f);
        offsets[TrackNavigationController.TrackPlayState.rewinding] = new Vector2(1.25f, -1f);
        offsets[TrackNavigationController.TrackPlayState.fastforwarding] = new Vector2(0.5f, 1f);
        offsets[TrackNavigationController.TrackPlayState.paused] = new Vector2(-0.25f, 1f);

		reload ();
	}

	public	void	reload	()
	{
		cockpitMainMaterial.SetFloat    (glowID, 0f);
        cockpitBaseMaterial.SetFloat    (glowID, 0f);

        objectivesAccomplished = 0;
        cockpitObjectiveMaterial.SetFloat(numberID, objectivesAccomplished);
	}

	#region Glow

	public	void	glow	()
	{
		StartCoroutine (glow (0.5f));
	}

	IEnumerator		glow	(float delay)
	{
		for (float timer = delay; timer > 0f; timer -= Time.deltaTime)
		{
			cockpitMainMaterial.SetFloat (glowID, timer);
            cockpitBaseMaterial.SetFloat (glowID, timer);
			yield return null;
		}
	}

	#endregion

	public	void	modifyLightIntensity	(int parameter, float value)
	{
		cockpitMainMaterial.SetFloat (parameter, value);
		cockpitEffectsMaterial.SetFloat (parameter, value);
	}

    public  void    modifyLightColor    (int parameter, Color value)
    {
        cockpitMainMaterial.SetColor(parameter, value);
        cockpitEffectsMaterial.SetColor(parameter, value);
    }

    public  void    launchLightning (int index)
    {
        lightningEffects[index].enabled = true;
        StartCoroutine(lightning(index));

        SoundManager.instance.launchSound(lightningSound);
    }

    public  void    cancelObjective     (int parameter)
    {
        objectivesAccomplished--;

        //  Increment number of objectives
        cockpitObjectiveMaterial.SetFloat(parameter, 0f);
        cockpitObjectiveMaterial.SetFloat(numberID, objectivesAccomplished);

        // Flash !
        StartCoroutine(flash());
    }

    public  void    validateObjective   (int parameter)
    {
        objectivesAccomplished++;

        //  Increment number of objectives
        cockpitObjectiveMaterial.SetFloat(parameter, 1f);
        cockpitObjectiveMaterial.SetFloat(numberID, objectivesAccomplished);

        // Flash !
        StartCoroutine(flash());
    }

    public  void    showObjectiveCounter    (bool bValue, int newValue = 0)
    {
        objectivesAccomplished = newValue;
        cockpitObjectiveMaterial.SetFloat(numberID, objectivesAccomplished);
        cockpitObjectiveMaterial.SetFloat(alphaID, bValue ? 1f : 0f);
    }

    public  void    showNavigationState (TrackNavigationController.TrackPlayState state, bool isLooping)
    {
        Vector2 offset = Vector2.zero;
        offset.x = offsets[state].x;
        offset.y = isLooping ? -0.75f : 0f;

        if (offsets[state].y < 0f)
            cockpitNavigationStateMaterial.SetTextureScale("_MainTex", new Vector2(-1f, 1f));
        else
            cockpitNavigationStateMaterial.SetTextureScale("_MainTex", new Vector2(1f, 1f));

        cockpitNavigationStateMaterial.SetTextureOffset("_MainTex", offset);
    }

    IEnumerator flash   ()
    {
        for (float timer = 0f; timer < 0.5f; timer += Time.deltaTime)
        {
            cockpitObjectiveMaterial.SetColor(colorID, flashColor.Evaluate(timer / 0.5f));
            yield return null;
        }
    }

    IEnumerator lightning   (int index)
    {
        for( float timer = 0f; timer < 1f; timer += Time.deltaTime)
        {
            lightningEffects[index].material.SetFloat(lightningOpacityID, lightningAnimation.Evaluate(timer));
            yield return null;
        }
        lightningEffects[index].enabled = false;
    }

}
