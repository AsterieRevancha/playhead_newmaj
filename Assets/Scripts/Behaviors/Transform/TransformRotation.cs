﻿using UnityEngine;

public class TransformRotation: MonoBehaviour
{
	public Vector3 rotationToApply;
	Transform tr;

	void Awake	()
	{
		tr = transform;
	}

	void	Update	()
	{
		tr.Rotate (rotationToApply * Time.deltaTime, Space.World);
	}

}
