﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomRotate : MonoBehaviour
{
	public	float	angleStep;
	public	Vector3	axis;

	Transform	tr;
	int			maxTurns;

	// Special, rotate the blocks
	Renderer	blockRenderer;

	// Materials to assign
	public	List<Material>			materials;
	Dictionary<int, Material>		materialsForRotation;

	void	Awake	()
	{
		axis.Normalize();

		// Shortcuts
		tr				=	transform;
		blockRenderer	=	GetComponent<Renderer>();

		// Maximum number of turns
		maxTurns	=	Mathf.FloorToInt(360f	/	angleStep);

		// Link materials with turns
		materialsForRotation	=	new Dictionary<int, Material>();
		for(int i=0; i<maxTurns; i++)
			materialsForRotation[i] = materials[i];

	}

	void	OnEnable	()
	{
		int	iTurns	=	UnityEngine.Random.Range(0,maxTurns);

		// Renderer spec 
		blockRenderer.material	=	materialsForRotation[Mathf.Abs(iTurns)];

		//Set rotation
		tr.rotation	=	Quaternion.Euler( axis * angleStep * iTurns);
	}
}
