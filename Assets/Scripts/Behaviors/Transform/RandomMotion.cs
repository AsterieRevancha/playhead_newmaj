﻿using UnityEngine;
using System.Collections;

public class RandomMotion : MonoBehaviour
{
	public	Vector3		range;
	public	float		delayBetweenChanges	=	2f;
	public	bool		isLocal;

	Transform	tr;
	Rigidbody	rb;

	Vector3	axis;
	Quaternion	rot;

	Vector3	startPosition;

	float		timer	=	0f;


	// Use this for initialization
	void Awake ()
	{
		tr = transform;
		rb = GetComponent<Rigidbody>();

		startPosition = tr.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
		timer += Time.deltaTime;

		if(timer > delayBetweenChanges)
		{
			axis	=	new Vector3(UnityEngine.Random.Range(-range.x,range.x),UnityEngine.Random.Range(-range.y,range.y),UnityEngine.Random.Range(-range.z,range.z));
			rot = Quaternion.AngleAxis(90*Time.deltaTime,axis);
			timer = 0f;
		}

		if(isLocal)
			rb.MovePosition(Vector3.Lerp(tr.parent.position,tr.parent.position + rot* axis,timer / delayBetweenChanges));
		else
			rb.MovePosition(Vector3.Lerp(rb.position,startPosition + rot* axis,timer / delayBetweenChanges));
	}
}
