﻿using UnityEngine;
using System.Collections;

public class Feedback_LookAtObject : MonoBehaviour
{
	public Transform target;

	Transform tr;

	void Awake	()
	{
		tr = transform;
	}

	// Use this for initialization
	void Update()
	{
		tr.LookAt(target);
	}

}
