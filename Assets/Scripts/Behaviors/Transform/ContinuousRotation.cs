﻿using UnityEngine;
using System.Collections;

public class ContinuousRotation : MonoBehaviour
{
	public float angularSpeed = 20f;
	public Vector3 axis = new Vector3(0,0,1);
	
	Transform tr;

	void Awake ()
	{
		tr = transform;
	}
	
	void Update ()
	{
		tr.Rotate(axis, angularSpeed * Time.deltaTime);
	}
}
