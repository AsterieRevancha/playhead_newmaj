﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Very simple behavior aimed at making an object oves forward
 */ 
public class MoveForward : MonoBehaviour
{
    public	float		speed = 0f;
	public	Vector3		axis = new Vector3(0, 0, 1);

	Transform	tr	=	null;
	float		pathMade = 0f;

	void	Awake	()
	{
		tr = transform;
	}

	void	Update	()
	{
		pathMade += Time.deltaTime;
		tr.position = Vector3.Lerp(tr.position, tr.position + axis * speed * Time.deltaTime, pathMade);
	}
}