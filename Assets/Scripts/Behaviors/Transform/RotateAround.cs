﻿using UnityEngine;
using System.Collections;

public class RotateAround : MonoBehaviour
{

	public	Transform	pivot;
	public	Vector3		axis;
	public	float		angularSpeed	=	2f;

	// cache shortcuts
	Transform	tr;

	void	Awake	()
	{
		tr = transform;
	}
	
	// Update is called once per frame
	void Update ()
	{
		tr.RotateAround(pivot.position,axis,angularSpeed*Time.deltaTime);
	}
}
