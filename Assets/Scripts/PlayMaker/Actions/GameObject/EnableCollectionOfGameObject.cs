﻿/************************************************************************************

Filename    :   EnableCollectionOfGameObject.cs
Content     :   EnableCollectionOfGameObject class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.GameObject)]
	[Tooltip("Activates/deactivates a collection of Game Object. Use this to hide/show areas, or enable/disable many Behaviours at once.")]
	public class EnableCollectionOfGameObject : FsmStateAction
	{
		[RequiredField]
        [Tooltip("The GameObjects to activate/deactivate.")]
        public	FsmGameObject[] gameObjects;

		public	FsmBool	useCounterForGameObjects;
		public	FsmInt	amountOfGameObjectsToActivate;
		
		[RequiredField]
        [Tooltip("Check to activate, uncheck to deactivate Game Object.")]
        public FsmBool activate;
		
		public override void OnEnter()
		{
			int sizeOfActivation = gameObjects.Length;
			if (useCounterForGameObjects.Value)
				sizeOfActivation = Mathf.Min(amountOfGameObjectsToActivate.Value,gameObjects.Length);

			for (int i=0; i < sizeOfActivation; i++)
				gameObjects[i].Value.SetActive (activate.Value);

			Finish();
		}

    }
}