﻿/************************************************************************************

Filename    :   SetPlayHeadAttachedPhase.cs
Content     :   SetPlayHeadAttachedPhase class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory (ActionCategory.GameLogic)]
	[Tooltip ("Attach the playhead to the given phase")]
	public class SetPlayHeadAttachedPhase : FsmStateAction
	{
		[Tooltip ("Soundgate to which attach the playhead.")]
		public SoundGate soundGate;

		[Tooltip ("The phase which the playhead will go through.")]
		public Phase desiredPhase;

		[Tooltip ("The start mode of the playhead in the phase.")]
		public TrackNavigationController.TrackPlayAction desiredStartMode;

		public	FsmBool		syncSound;

		public override void OnEnter ()
		{
			// Prepare track navigation
			PlayHead.instance.trackNavigationController.attachToPhase (desiredPhase, soundGate);

			// Launch track navigation
			PlayHead.instance.setNavigationControlTo (PlayHead.PlayHeadControlState.track);

			// Put the playhead on track
			PlayHead.instance.trackNavigationController.changeState (desiredStartMode, true);

			// Sync sound
			if (syncSound.Value)
				SoundManager.instance.setTimelinePosition (soundGate.timePosition);

			Finish ();
		}
	}
}