﻿/************************************************************************************

Filename    :   AnimateMaterialColorGradient.cs
Content     :   AnimateMaterialColorGradient class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.AnimateVariables)]
	public class AnimateMaterialColorGradient : FsmStateAction
	{
        public Color colorStart;
        public Color colorEnd;
        public Material materialToAnimate;
        public FsmString parameterName;
        public FsmFloat duration;

        public FsmBool pingPong;
        public FsmInt iterations;

        float timer = 0f;
			
		public override void OnEnter()
		{
			base.OnEnter();
            timer = 0f;
		}

		public override void OnUpdate()
		{
			base.OnUpdate();

            timer += Time.deltaTime;

            materialToAnimate.SetColor(parameterName.Value, Color.Lerp(colorStart,colorEnd,timer / duration.Value));

            if (timer > duration.Value)
            {
                if (pingPong.Value && iterations.Value > 0)
                {
                    iterations.Value--;
                    timer = 0f;
                    Color tmp = colorEnd;
                    colorEnd = colorStart;
                    colorStart = tmp;
                    return;
                }
                Finish();
            }
		}
	}
}