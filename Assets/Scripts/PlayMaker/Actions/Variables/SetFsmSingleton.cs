﻿/************************************************************************************

Filename    :   SetFsmSingleton.cs
Content     :   SetFsmSingleton class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.StateMachine)]
	[Tooltip("Set the value of a Game Object Variable in this FSM. Accept null reference")]
	public class SetFsmSingleton : FsmStateAction
	{
		[RequiredField]
		[UIHint(UIHint.FsmGameObject)]
        [Tooltip("The variable to set.")]
		public FsmGameObject variableGameObject;

		public	enum SingletonType
		{
			player = 0,
			playhead = 1,
			designer = 2
		}

        [Tooltip("Which singleton to use.")]
		public SingletonType setValue;

		public override void OnEnter()
		{
			switch(setValue)
			{
				case SingletonType.designer:
					variableGameObject.Value = DesignerManager.instance.gameObject;
					break;
				case SingletonType.player:
					variableGameObject.Value = PlayerController.instance.gameObject;
					break;
				case SingletonType.playhead:
					variableGameObject.Value = PlayHead.instance.gameObject;
					break;
				default:
					return;
			}
			Finish();
		}
	}
}