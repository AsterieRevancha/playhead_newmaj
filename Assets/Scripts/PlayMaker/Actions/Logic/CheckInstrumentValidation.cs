﻿/************************************************************************************

Filename    :   CheckInstrumentValidation.cs
Content     :   CheckInstrumentValidation class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory (ActionCategory.GameLogic)]
	[Tooltip ("Check validation of instruments")]
	public class CheckInstrumentValidation : FsmStateAction
	{
		[Tooltip ("Instruments to check.")]
		public	FsmString[]	tracksToCheck;

        public TrackManager.TrackState desiredState = TrackManager.TrackState.active;

		public	FsmEvent	successEvent;
		public	FsmEvent	failureEvent;

		[Tooltip ("More than half the conditions are met.")]
		public	FsmEvent	nearlyCompleteEvent;

		[Tooltip ("Less than half the conditions are met.")]
		public	FsmEvent	begunEvent;

		public override void OnEnter ()
		{
			bool	isValid = true;
			int		iCounter = tracksToCheck.Length;

			for (int i = 0; i < tracksToCheck.Length; i++)
			{
                if (!(TrackManager.instance.trackStates[tracksToCheck[i].Value] == desiredState))
				{
					isValid = false;
					iCounter--;
				}
			}

			if (isValid)
				Fsm.Event (successEvent);
			else
			{
				if (iCounter == 0)
					Fsm.Event (failureEvent);
				else if(iCounter > tracksToCheck.Length / 2)
					Fsm.Event (nearlyCompleteEvent);
				else
					Fsm.Event (begunEvent);

			}

			Finish ();
		}
	}
}
