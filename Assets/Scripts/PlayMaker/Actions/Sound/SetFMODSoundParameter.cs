﻿/************************************************************************************

Filename    :   SetFMODSoundParameter.cs
Content     :   SetFMODSoundParameter class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory (ActionCategory.Audio)]
	[Tooltip ("Set FMOD sound parameter(s) value(s)")]
	public class SetFMODSoundParameter : FsmStateAction
	{
		[Tooltip ("Sound effect owner of parameters.")]
		public	FsmString	soundName;

		[Tooltip ("Parameters to be set.")]
		public	FsmString[] parameterName;

		[Tooltip ("Value to set the parameters with.")]
		public	FsmFloat	parameterValue;

		public override void Reset ()
		{
			soundName = null;
			parameterName = null;
			parameterValue = null;
		}

		public override void OnEnter ()
		{
			if(soundName.Value == "")
				for (int i = 0; i < parameterName.Length; i++)
					SoundManager.instance.setSongParameter (parameterName[i].Value, parameterValue.Value);
			else
				for (int i = 0; i < parameterName.Length; i++)
					SoundManager.instance.setParameter (soundName.Value, parameterName[i].Value, parameterValue.Value);

			Finish ();
		}


		public override void OnExit ()
		{

		}

		public override string ErrorCheck ()
		{
			return parameterName != null ? null : "Sound parameters missing";
		}
	}
}