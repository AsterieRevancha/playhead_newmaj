﻿/************************************************************************************

Filename    :   PlayMakerInteractiveObject.cs
Content     :   PlayMakerInteractiveObject class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu ("Scripts/PlayHead/PlayMaker/Triggerables/PlayMaker Interactive Object")]
public class PlayMakerInteractiveObject : MonoBehaviour, ILookable
{
	public	PlayMakerFSM	fsm;
	public	Collider		thisCollider;

	public float	activationTimeRequired = 1f;
	public float	delayBeforeDecay = 0.3f;
	public	bool	repeatable	=	false;
	public	bool	colliderRemovedAfterActivation = false;
	public	bool	timerDecay = false;

	// Keep track of the time
	public enum TimeModality
	{
		online,
		offline
	}
	public TimeModality timeModality;

	// Time computation
	public delegate float DeltaTimeMethod();
	private DeltaTimeMethod getDeltaTime;

	// Note: Time.realtimeSinceStartup is immune to TimeScale
	private		float	lastTimeOfUpdate = 0f;
	private		float	deltaTime = 0f;
	protected	float	timer		=	0f;
	protected	float	lastLook = 0f;

	// State-machine
	public enum IOState
	{
		active = 0, // IO can be activated
		locked = 1, // IO has been validated
		frozen = 2 // IO cannot be activated
	}
	public IOState actualState = IOState.active;

	void	Awake	()
	{
		// Computes delta time
		switch (timeModality)
		{
			case TimeModality.offline:
				getDeltaTime = getOfflineDeltaTime;
				break;
			case TimeModality.online:
				getDeltaTime = getOnlineDeltaTime;
				break;
		}
	}

	/* ----------------
	 * Time management
	 * ---------------- */

	float	getOfflineDeltaTime	()
	{
		float delta =  Time.realtimeSinceStartup - lastTimeOfUpdate;
		lastTimeOfUpdate = Time.realtimeSinceStartup;
		return delta;
	}

	float getOnlineDeltaTime()
	{
		return Time.deltaTime;
	}

	/* ----------------
	 * Interaction
	 * ---------------- */

	#region

	public	virtual	void	OnLookAt	(float timeSpent)
	{
		if (actualState.Equals(IOState.frozen))
			return;

		lastLook	= Time.realtimeSinceStartup;

		fsm.SendEvent ("OnLookAt");

		if(actualState.Equals(IOState.active))
			timer		+=	timeSpent;
	}

	protected	virtual	void	Update	()
	{
		if (actualState == IOState.frozen)
			return;

		// Generic method to get delta time
		deltaTime = getDeltaTime();

		if (actualState.Equals(IOState.active))
		{
			// Activation test
			if(timer > activationTimeRequired)
			{
				actualState = IOState.locked;
				fsm.SendEvent ("OnActivation");
				if (colliderRemovedAfterActivation)
					thisCollider.enabled = false;
			}
			else if(timer > 0f && timerDecay && Time.realtimeSinceStartup - lastLook > delayBeforeDecay)
				timer -= deltaTime;
		}
		else if (actualState.Equals(IOState.locked))
		{
			if(repeatable && Time.realtimeSinceStartup - lastLook > delayBeforeDecay)
			{
				timer -= deltaTime;

				if (timer < 0f)
				{
					timer = 0f;
					actualState = IOState.active;
					fsm.SendEvent ("OnDeactivation");

					if (colliderRemovedAfterActivation)
						thisCollider.enabled = true;
				}
			}
		}
	}

	void	OnDisable	()
	{
		reset();
	}

	void	OnEnable	()
	{
		thisCollider.enabled = true;
		lastTimeOfUpdate = Time.realtimeSinceStartup;
	}

	public	void	lockObject (bool bValue)
	{
		if (bValue)
			actualState = IOState.locked;
		else
			actualState = IOState.active;
	}

	public	void	freezeObject	(bool bValue)
	{
		if (bValue)
			actualState = IOState.frozen;
		else
			actualState = IOState.active;
	}

	public	void	reset	()
	{
		timer = 0f;
		if(actualState == IOState.locked)
			actualState = IOState.active;
		lastLook = 0f;
	}

	public	bool	objectIsLocked	()
	{
		return actualState == IOState.locked;
	}

	public	bool	isFrozen	()
	{
		return actualState == IOState.frozen;
	}

	public	float	getTimeLookedAt	()
	{
		return timer;
	}

	public	float	getMaxTime	()
	{
		return activationTimeRequired;
	}

#endregion


	void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere(transform.position, 0.1f);
	}
}
