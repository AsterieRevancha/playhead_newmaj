﻿/************************************************************************************

Filename    :   SpectrumAnalyzer.cs
Content     :   SpectrumAnalyzer class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/

#define UNSAFE

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using FMOD.Studio;

public class SpectrumAnalyzer : MonoBehaviour
{
	// Spectrum configuration
	public 	FMOD.DSP_FFT_WINDOW         windowType;
	public 	int                         windowSize = 64;
	public	float						updateFrequency = 0.1f;

	// Variables computed from those given
	float						resolution;

	// FMOD internal Variables
	FMOD.System 				fmodSystem = null;

	// Arrays
	List<string>									soundNames			= new List<string>();
	Dictionary<string, FMOD.ChannelGroup>			channelsOpened		= new Dictionary<string,FMOD.ChannelGroup>();
	Dictionary<string, FMOD.DSP>					dspCreated			= new Dictionary<string,FMOD.DSP>();
	Dictionary<string, FMOD.DSP_PARAMETER_FFT>		spectrumPtrs		= new Dictionary<string, FMOD.DSP_PARAMETER_FFT>();
	Dictionary<string, float>						dominantFrequency	= new Dictionary<string,float>();

	// Specturm data free to access
	Dictionary<string, float[]>						spectrumData		= new Dictionary<string, float[]>();
	Dictionary<string, float>						updateTimes			= new Dictionary<string, float>();
	
	void	Awake	()
	{
		FMOD_StudioSystem.instance.System.getLowLevelSystem(out fmodSystem);

		// Compute useful variables
		resolution = 22050f / windowSize;
	}
	
	public	void	createAnalyzer	(string eventName, FMOD.Studio.EventInstance	eventInstance)
	{
		soundNames.Add(eventName);

		// Allocate space for a WINDOWSIZE float array to receive spectrum frequency data
		windowSize 		= 	Mathf.Max(Mathf.Min(windowSize,8192),0);
		spectrumData[eventName] 	=	new float[windowSize];
		dominantFrequency[eventName]	=	0f;
		updateTimes[eventName] = 0f;

		// Retrieve the Channel groupe and create the DSP
		FMOD.ChannelGroup	thisChannelGroup	=	null;
		FMOD.Studio.UnityUtil.ERRCHECK(eventInstance.getChannelGroup(out thisChannelGroup));
		channelsOpened[eventName] = thisChannelGroup;
		
		// Create a FFT DSP
		FMOD.DSP	thisFFT;
		fmodSystem.createDSPByType(FMOD.DSP_TYPE.FFT, out thisFFT);
		dspCreated[eventName]	=	thisFFT;

		// Configure
		thisFFT.setParameterInt((int)FMOD.DSP_FFT.WINDOWSIZE, windowSize);
		thisFFT.setParameterInt((int)FMOD.DSP_FFT.WINDOWTYPE, windowType.GetHashCode());
		
		// Add it to the channel group
		thisChannelGroup.addDSP(FMOD.CHANNELCONTROL_DSP_INDEX.HEAD,thisFFT);

		// Construct managed ptr
		spectrumPtrs[eventName] = new FMOD.DSP_PARAMETER_FFT();
	}

	unsafe void	unsafeSpectrumRetrieval	(string eventName)
	{
		System.IntPtr ptr = System.IntPtr.Zero;
		uint size = 0;
		
		// Retrieve spectrum data
		if (FMOD.Studio.UnityUtil.ERRCHECK(
			dspCreated[eventName].getParameterData((int)FMOD.DSP_FFT.SPECTRUMDATA, out ptr, out size)))
		{
			spectrumPtrs[eventName] = (FMOD.DSP_PARAMETER_FFT)Marshal.PtrToStructure(ptr, typeof(FMOD.DSP_PARAMETER_FFT));

			if (spectrumPtrs[eventName].length > 0)
			{
				// Get the spectrum
				System.IntPtr[] unsafeSpectrum = spectrumPtrs[eventName].unsafeSpectrum;

				for (int i = 0; i < windowSize; i++)
				{
					float* r = (float*)unsafeSpectrum[0].ToPointer() + i;
					float* l = (float*)unsafeSpectrum[1].ToPointer() + i;
					spectrumData[eventName][i] = (*r + *l) / 2;
				}
			}
		}
	}

	/*
	void spectrumRetrieval(string eventName)
	{
		System.IntPtr ptr = System.IntPtr.Zero;
		uint size = 0;
		
		// Retrieve spectrum data
		if (FMOD.Studio.UnityUtil.ERRCHECK(
			dspCreated[eventName].getParameterData((int)FMOD.DSP_FFT.SPECTRUMDATA, out ptr, out size)))
		{
			spectrumPtrs[eventName] = (FMOD.DSP_PARAMETER_FFT)Marshal.PtrToStructure(ptr, typeof(FMOD.DSP_PARAMETER_FFT));

			// Channels
			if (spectrumPtrs[eventName].length > 0)
			{
				float[] left = spectrumPtrs[eventName].spectrum[0];
				float[] right = spectrumPtrs[eventName].spectrum[1];

				for (int i = 0; i < windowSize; i++)
				{
					spectrumData[eventName][i] = (right[i] + left[i]) / 2;
				}
			}
		}
		
		// Retrieve dominant frequency
		float temp;
		FMOD.Studio.UnityUtil.ERRCHECK(
			dspCreated[eventName].getParameterFloat((int)FMOD.DSP_FFT.DOMINANT_FREQ,
		                                        out temp
		                                        ));
		dominantFrequency[eventName] = temp;
	}
	 * */
	
	public	float	getDominantFrequency	(string	eventName)
	{
		return dominantFrequency[eventName];
	}

	public float[]	getSpectrum (string eventName)
	{
		if (Time.realtimeSinceStartup - updateTimes[eventName] > updateFrequency)
		{
			updateTimes[eventName] = Time.realtimeSinceStartup;
			unsafeSpectrumRetrieval(eventName);
		}

		return spectrumData[eventName];
	}

	public	bool	containsEvent	(string eventName)
	{
		return soundNames.Contains(eventName);
	}

	public	float	getMeanVolumeForFrequencyRange	(string eventName, int minFrequency, int maxFrequency)
	{
		if (Time.realtimeSinceStartup - updateTimes[eventName] > updateFrequency)
		{
			updateTimes[eventName] = Time.realtimeSinceStartup;
			unsafeSpectrumRetrieval(eventName);
		}

		int iMin = Mathf.RoundToInt((float)minFrequency / resolution);
		int iMax = Mathf.RoundToInt((float)maxFrequency / resolution);

		float fMeanVolume = 0f;
		for (int i = iMin; i <= iMax; i++)
			fMeanVolume += spectrumData[eventName][i];
		fMeanVolume /= windowSize;

		return fMeanVolume;
	}
}
