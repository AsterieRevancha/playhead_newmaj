﻿/************************************************************************************

Filename    :   TravelManager.cs
Content     :   Travel Manager class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

[AddComponentMenu("Scripts/PlayHead/Manager/TravelManager")]
public class TravelManager : Singleton<TravelManager>
{
	// Clipping of Hypermeasures
	//public int farClip = 3;

	[System.Serializable]
	public struct PhaseMap
	{
		public string phaseName;
		public int start;
		public int end;
	}

	public PhaseMap[]		map;
	//Phase[]					phasesInventoryByTime;
	//Dictionary<string, Phase>	phasesInventoryByName = new Dictionary<string, Phase>();

	protected override void init ()
	{

	}

	/*

	protected	override	void	init	()
	{

	}

	#region Phase management

	/// <summary>
	/// 
	/// </summary>
	/// <param name="newPhase"></param>
	public	void	addPhase	(Phase newPhase)
	{
		for (int i = 0; i < phasesInventoryByTime.Length; i++)
			if (phasesInventoryByTime[i].startTime > newPhase.startTime)
			{
				phasesInventoryByTime.Insert(i, newPhase);
				return;
			}
		phasesInventoryByTime.Add(newPhase);
		phasesInventoryByName[newPhase.name] = newPhase;
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="oldPhase"></param>
	public void removePhase(Phase oldPhase)
	{
		phasesInventoryByTime.Remove(oldPhase);
		phasesInventoryByName.Remove(oldPhase.name);
	}

	/// <summary>
	/// Retrieve the phase containing the desired position in the timeline
	/// </summary>
	/// <param name="timePosition">Desired position in time</param>
	/// <returns></returns>
	public Phase getExistingPhaseContainingTimeline(int timePosition)
	{
		for (int i = 0; i < phasesInventoryByTime.Count; i++)
			if (phasesInventoryByTime[i].startTime > timePosition)
				return phasesInventoryByTime[i - 1];
			else if (phasesInventoryByTime[i].startTime + phasesInventoryByTime[i].lengthTime > timePosition)
				return phasesInventoryByTime[i];
		return null;
	}

	/// <summary>
	/// Searches inventory for existing phase
	/// </summary>
	/// <param name="phaseName"></param>
	/// <returns></returns>
	public Phase getExistingPhase(string phaseName)
	{
		if (phasesInventoryByName.ContainsKey(phaseName))
			return phasesInventoryByName[phaseName];
		else
			return null;
	}


	public	string	getPhaseName	(int timelinePosition)
	{
		for (int i = 0; i < map.Count; i++)
		{
			if (map[i].start <= timelinePosition && map[i].end > timelinePosition)
			{
				return map[i].phaseName;
			}
		}
		return "";
	}
	*/

}
