﻿/************************************************************************************

Filename    :   GameManager.cs
Content     :   Game Manager class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
	// PlayMaker
	public	PlayMakerFSM	fsm;

	// GameState
	private	enum GameState
	{
		absent,
		build,
		launched,
		paused,
		reloading
	}
	GameState	actualState	=	GameState.absent;

	//Common
	public GameMenu gameMenu;

	#region Main functions

	protected	override	void init()
	{

	}

	IEnumerator	Start	()
	{
		yield return StartCoroutine(asyncBuild());
		yield return StartCoroutine(asyncLaunch());
	}

	IEnumerator	asyncBuild	()
	{
		actualState = GameState.paused;

		SoundManager.instance.build();
		yield return null;

		DesignerManager.instance.build ();
		yield return null;

		actualState = GameState.build;
	}

	IEnumerator asyncLaunch	()
	{
		actualState = GameState.paused;

		// Is there anything here ?
		SoundManager.instance.launch();

		actualState = GameState.launched;

		yield return null;
	}

	public	void pause	(bool bValue)
	{
		if (actualState == GameState.reloading)
			return;

		SoundManager.instance.pause(bValue);
		DesignerManager.instance.pause(bValue);

		// Freeze animations & updates
		if (!bValue)
		{
			actualState = GameState.launched;
			Time.timeScale = 1f;
		}
		else
		{
			actualState = GameState.paused;
			Time.timeScale = 0f;
		}
	}

	void	reload	()
	{
		fsm.SendEvent ("Reload");
		TrackManager.instance.reload();
		DesignerManager.instance.reload();
		PlayHead.instance.reload();
		gameMenu.reload();
		SoundManager.instance.reload();
		PlayerController.instance.reload();
		MaterialManager.instance.reload ();
	}

	void	unload	()
	{
		DesignerManager.instance.unload();
		TrackManager.instance.unload();
		PlayerController.instance.unload();
		SoundManager.instance.unload ();
	}

	public	void	reloadGame	(float fadeTime = 3f, float waitTime = 1f)
	{
		pause	(true);
		actualState = GameState.reloading;

		unload();

		// Protect Oculus player from beign destroyed
		GameObject	oculusPlayer	=	GameObject.FindGameObjectWithTag(Tags.player);
		oculusPlayer.transform.parent = null;

		// Launch laoding during fade
        StartCoroutine(asyncReload(fadeTime, waitTime));
	}

	public bool isReloading()
	{
		return actualState == GameState.reloading;
	}

	#endregion


	IEnumerator	asyncReload	(float fadeTime = 3f, float waitTime = 1f)
	{
		// Wait the fader
        yield return StartCoroutine(PlayerController.instance.fader.asyncFade(fadeTime, Fader.FadeType.FadeIn));

		// Unpause manager & reload all other managers
		actualState = GameState.paused;

        // Wait
        float startTimeForWaiting = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup - startTimeForWaiting < waitTime)
        {
            yield return null;
        }

		// Launch loading
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(0); // Application.LoadLevelAsync(0);
		yield return asyncLoad;

        yield return StartCoroutine(PlayerController.instance.fader.asyncFade(fadeTime, Fader.FadeType.FadeOut));

		pause(false);
		reload();
	}
}
