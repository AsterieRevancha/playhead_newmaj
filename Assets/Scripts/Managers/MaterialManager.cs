﻿/************************************************************************************

Filename    :   MaterialManager.cs
Content     :   Material Manager class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/

using UnityEngine;
using System.Collections;

public class MaterialManager : Singleton<MaterialManager>
{
	[System.Serializable]
	public struct ParameterDesc
	{
		public	string	parameterName;
		public	float	initValue;
	}

	[System.Serializable]
	public struct ColorDesc
	{
		public	string	parameterName;
		public	Color	initColor;
	}

	[System.Serializable]
	public struct MaterialDesc
	{
		public	Material		material;
		public	ParameterDesc[]	parameters;
		public	ColorDesc[]		colors;
	}

	public	MaterialDesc[]	materials;

	protected	override	void	init	()
	{
		resetMaterialParameters ();
	}

	public	void	reload	()
	{
		resetMaterialParameters ();
	}

	void	resetMaterialParameters	()
	{
		for (int i=0; i < materials.Length; i++)
		{
			for (int j=0; j < materials[i].parameters.Length; j++)
				materials[i].material.SetFloat (materials[i].parameters[j].parameterName, materials[i].parameters[j].initValue);

			for (int j=0; j < materials[i].colors.Length; j++)
				materials[i].material.SetColor (materials[i].colors[j].parameterName, materials[i].colors[j].initColor);
		}
	}
	
}
