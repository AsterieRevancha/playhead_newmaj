﻿/************************************************************************************

Filename    :   SceneManager.cs
Content     :   Scene Manager class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayheadSceneManager : MonoBehaviour
{
	public GameObject playerPrefab;
	public GameObject playHeadPrefab;

	public bool	forcePositionIfExisting = false; // On some levels, starting position are obligatory
	public PlayHead.PlayHeadControlState startControlState = PlayHead.PlayHeadControlState.track;

	// Positions in space wheere to spawn
	public Transform playerSpawnPoint;
	public Transform designerVoiceSpawnPoint;

	private Vector3 playerPositionInPlayHead = new Vector3(0, 0, 5f);
	private GameObject player;
	private GameObject playHead;

	// Phase present in the scene
	public Phase phaseInScene;

	// Tracks that should be available
	public List<string> activeTracks;

	void	Awake	()
	{
		// Spawn playHead
		playHead = GameObject.FindGameObjectWithTag(Tags.playHead);
		if(playHead == null)
		{
			playHead = Instantiate(playHeadPrefab, playerSpawnPoint.position + playerPositionInPlayHead, playerSpawnPoint.rotation) as GameObject;
			
			// Careful, player contains all the gamemanagers which requires the playhead
			player = Instantiate(playerPrefab, playerSpawnPoint.position, playerSpawnPoint.rotation) as GameObject;
			player.transform.parent = playHead.transform;

			playHead.GetComponent<PlayHead>().setNavigationControlTo(startControlState);

			// I guess environments without playhead require a little sync in time
			if (phaseInScene != null)
			{
				Invoke("syncTimeline", 0.5f);
			}

			// Enable instruments
			//Invoke ("syncTracks", 0.5f);
		}
		else
		{
			// Spawn player at origin
			player = GameObject.FindGameObjectWithTag(Tags.player);
			if (player == null)
			{
				player = Instantiate(playerPrefab, playerSpawnPoint.position, playerSpawnPoint.rotation) as GameObject;
				player.transform.parent = playHead.transform;

				// Enable instruments
				//Invoke ("syncTracks", 0.5f);
			}
			else if (forcePositionIfExisting)
			{
				player.transform.position = playerSpawnPoint.position;
				player.transform.rotation = playerSpawnPoint.rotation;
			}
		}

		// Spawn designer voice
		if (designerVoiceSpawnPoint != null)
			DesignerManager.instance.spawnVoice(designerVoiceSpawnPoint);
		else
			DesignerManager.instance.spawnVoice(GameObject.FindGameObjectWithTag(Tags.designerVoice).transform);

	}

	/* ----------------
	 * Delayed actions
	* ---------------- */

	// Wait for soundmanager & gameManager to build themselves
	void	syncTimeline	()
	{
		SoundManager.instance.setTimelinePosition(phaseInScene.startTime);
		SoundManager.instance.pause(false);
	}

	void	syncTracks	()
	{
		TrackManager.instance.retrievePhaseAnimations (phaseInScene);
		for (int i = 0; i < activeTracks.Count; i++)
			TrackManager.instance.setTrackStatus(activeTracks[i],TrackManager.TrackState.active,false);
	}

}
