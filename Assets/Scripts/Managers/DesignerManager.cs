﻿/************************************************************************************

Filename    :   DesignerManager.cs
Content     :   DesignerManager class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;

/**
 * DesignerManager manages the behavior of the narrator/AI hovering around the player.
 * @author Kevin Wagrez
 */ 
public class DesignerManager : Singleton<DesignerManager>
{
	// 
	bool		isSpeaking	=	false;

	[System.Serializable]
	public struct DesignerWord
	{
		public string dialogId;
		public FMODAsset soundAsset;
	}
	public DesignerWord[] dialogs;

	// Store voice effects
	string									dialogBeingPlayed = "";
	Dictionary<string,	EventInstance>		dialogEvents			= new Dictionary<string, EventInstance>();
	Dictionary<string,	EventDescription>	dialogDescs				= new Dictionary<string, EventDescription>();

	public	Transform		voiceHolder = null;
	Transform[]				voiceBars;

	//public	Transform		upFace;
	//public	Transform		downFace;

    // Vision
    public      Material    designerMaterial;
    int transitionID;

	// Analyzer
	public SpectrumAnalyzer spectrumAnalyzer = null;
	public float			updateFrequency = 0.05f;
	float[]					spectrum;

	float					maxScale	=	0.01f;
	float					scaleTimer = 0f;

	public	int				barNumbers = 7;

	#region Building process

	protected	override	void	init	()
	{
        transitionID = Shader.PropertyToID("_Transition");

		// Build voice
		// TODO: do that somewhere else ?
		voiceBars = new Transform[barNumbers];
		for (int i = 0; i < barNumbers; i++)
			voiceBars[i] = voiceHolder.GetChild(i);
		voiceHolder.gameObject.SetActive(false);

		// Link with spectrum analyzer
		spectrumAnalyzer = GetComponent<SpectrumAnalyzer> ();
	}

	public	void	retrieveEventFromAsset	(int index, string eventName, FMODAsset asset)
	{
		dialogEvents[eventName] = FMOD_StudioSystem.instance.GetEvent(asset.path);

		// Description
		EventDescription desc = null;
		dialogEvents[eventName].getDescription(out desc);
		dialogDescs[eventName] = desc;
	}

	// TODO: avoid special cases !
	public void build()
	{
		// Bind events & descs with strings
		for (int i = 0; i < dialogs.Length; i++)
			retrieveEventFromAsset (i, dialogs[i].dialogId, dialogs[i].soundAsset);

		InvokeRepeating ("spectrumUpdate", updateFrequency, updateFrequency);
		StartCoroutine (spectrumUpdateBars ());
		Invoke ("preloadSpectrums", 2f);
	}

	void preloadSpectrums ()
	{
		// Bind events & descs with strings
		for (int i = 0; i < dialogs.Length; i++)
		{
			dialogEvents[dialogs[i].dialogId].start ();
			spectrumAnalyzer.createAnalyzer (dialogs[i].dialogId, dialogEvents[dialogs[i].dialogId]);
			dialogEvents[dialogs[i].dialogId].stop (STOP_MODE.IMMEDIATE);
		}
	}

	#endregion

	public	void	pause	(bool bValue)
	{
		isSpeaking = !bValue;
		for (int i = 0; i < dialogs.Length; i++ )
			dialogEvents[dialogs[i].dialogId].setPaused(bValue);
	}

	public	void	unload	()
	{
		for (int i = 0; i < dialogs.Length; i++)
			dialogEvents[dialogs[i].dialogId].stop(STOP_MODE.IMMEDIATE);

		StopAllCoroutines();
		CancelInvoke();

		isSpeaking = false;
		voiceHolder.gameObject.SetActive(false);

		voiceHolder.transform.parent = transform;
	}

	public	void	reload	()
	{
		InvokeRepeating("spectrumUpdate", updateFrequency, updateFrequency);
		StartCoroutine(spectrumUpdateBars());
	}

	#region Spectrum management

	void spectrumUpdate	()
	{
        if(dialogBeingPlayed != "")
            dialogEvents[dialogBeingPlayed].set3DAttributes(UnityUtil.to3DAttributes(voiceHolder.position));

		if (!isSpeaking)
			return;

		// If actual word has a spectrum analyzer, plays it
		if (spectrumAnalyzer.containsEvent(dialogBeingPlayed) && !isPaused(dialogBeingPlayed))
		{
			scaleTimer = 0f;
			spectrum = spectrumAnalyzer.getSpectrum(dialogBeingPlayed);
		}

	}

	IEnumerator	spectrumUpdateBars	()
	{
		Vector3[] barScales = new Vector3[voiceBars.Length];
		for (int i = 0; i < voiceBars.Length; i++)
			barScales[i] = voiceBars[i].localScale;

		while (true)
		{
			while (!isSpeaking || !spectrumAnalyzer.containsEvent(dialogBeingPlayed) || isPaused(dialogBeingPlayed) || spectrum == null)
				yield return null;

			float maximumBarScale = 0f;
			for (int i = 0; i < voiceBars.Length; i++)
			{
				barScales[i].z = Mathf.Lerp(barScales[i].z, maxScale * spectrum[i] * (100 + i * i), scaleTimer/updateFrequency);
				voiceBars[i].localScale = barScales[i];
				if (barScales[i].z > maximumBarScale)
					maximumBarScale = barScales[i].z;
			}

            // Transition
            designerMaterial.SetFloat(transitionID, 3f * maximumBarScale);

			// Position up & down triangles
			//upFace.localPosition = new Vector3 (upFace.localPosition.x, 0.15f + maximumBarScale / 2f, upFace.localPosition.z);
			//downFace.localPosition = new Vector3 (downFace.localPosition.x, -0.15f +  -maximumBarScale / 2f, downFace.localPosition.z);

			scaleTimer += Time.deltaTime;
			yield return isSpeaking;
		}
		
	}

	#endregion

	#region Voice

	public	void	spawnVoice	(Transform spawnPosition, bool resetScale = false)
	{
		voiceHolder.transform.position = spawnPosition.position;
		voiceHolder.transform.rotation = spawnPosition.rotation;
		voiceHolder.transform.parent = spawnPosition;

        if (resetScale)
            voiceHolder.transform.localScale = Vector3.one;
	}

	public	bool	exists	(string eventName)
	{
		return dialogEvents.ContainsKey(eventName);
	}

	public	bool	isPaused	(string eventName)
	{
		bool bValue = false;
		dialogEvents[eventName].getPaused(out bValue);
		return bValue;
	}
	
	public	void	speak	(string	eventName, bool displayDesigner = true)
	{
		// Check if designer's speaking
		if (isSpeaking && dialogBeingPlayed != "")
			dialogEvents[dialogBeingPlayed].stop (STOP_MODE.IMMEDIATE);

		dialogBeingPlayed = eventName;
		dialogEvents[eventName].set3DAttributes(UnityUtil.to3DAttributes(voiceHolder.position));
		dialogEvents[eventName].start();

		// Activate voice Holder
        if (displayDesigner)
        {
            voiceHolder.gameObject.SetActive(true);
            isSpeaking = true;

            // Stop any stopSpeakingFunction
            CancelInvoke("stopSpeaking");

            // Shut designer
            int msDelay = 0;
            dialogDescs[eventName].getLength(out msDelay);
            Invoke("stopSpeaking", msDelay * 0.001f);
        }
	}

	public	void	stopSpeaking	()
	{
		isSpeaking = false;
		dialogBeingPlayed = "";
		voiceHolder.gameObject.SetActive(false);
	}

#endregion

	#region Debug

#if DEBUG
	Rect designerArea = new Rect(0, 100, 200f, 100f);
	string designerDebug = "";

	void OnGUI()
	{
		designerDebug = "Actual sound: " + dialogBeingPlayed + " \n";
		designerDebug += "Spectrum contains sound: " + spectrumAnalyzer.containsEvent(dialogBeingPlayed).ToString() + " \n";
		designerDebug += "Is designer speaking: " + isSpeaking.ToString() + " \n";
		if (exists(dialogBeingPlayed))
			designerDebug += "Sound is paused: " +  (!isPaused(dialogBeingPlayed)).ToString() + " \n";
		GUI.TextArea(designerArea, designerDebug);
	}
#endif

	#endregion
}

