﻿/************************************************************************************

Filename    :   InputManager.cs
Content     :   Input Manager class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputManager : Singleton<InputManager>
{
	#region Inputs

	[System.Serializable]
	public struct InputMapping
	{
		public string button;
		public GameInput correspondingInput;
	}
	public InputMapping[]	userDefinedMappings;
	Dictionary<int, string> mappings = new Dictionary<int, string>();

	public float			longPressTime = 0.75f;
	public float			doubleTapTime = 0.25f;
	public float			restBetweenInputs = 0.2f;

	public enum GameInput
	{
		back = 0,
		select = 1,

		count = 2
	}

	public enum InputType
	{
		tap = 0,
		doubleTap = 1,
		longPress = 2,
		release = 3,

		count = 4
	}

	Dictionary<int, bool>	buttonsPressed = new Dictionary<int, bool>();
	Dictionary<int, float>	buttonDownTime = new Dictionary<int, float>();

	#endregion

	#region Callbacks

	Dictionary<int, Dictionary<int, List<CallableBehavior>>> callbacks = new Dictionary<int, Dictionary<int, List<CallableBehavior>>>();

	#endregion

	protected	override	void	init	()
	{
		// Init callbacks
		for (int i = 0; i < (int)GameInput.count; i++)
		{
			buttonsPressed[i] = false;
			buttonDownTime[i] = 0f;

			callbacks[i] = new Dictionary<int, List<CallableBehavior>>();
			for (int j = 0; j < (int)InputType.count; j++)
				callbacks[i][j] = new List<CallableBehavior>(); ;
		}

		// Sort mappings
		for (int i = 0; i < userDefinedMappings.Length; i++)
			mappings[(int)userDefinedMappings[i].correspondingInput] = userDefinedMappings[i].button;
	}

	public	void	addCallback	(GameInput desiredInput, InputType desiredInputType, CallableBehavior callback)
	{
		callbacks[(int)desiredInput][(int)desiredInputType].Add(callback);
	}

	public	void	Update	()
	{
		for (int i = 0; i < (int)GameInput.count; i++)
		{
			if (buttonsPressed[i])
			{
				if (Input.GetButton(mappings[i]) && (Time.realtimeSinceStartup - buttonDownTime[i]) > longPressTime)
				{
					applyBehaviors(i, (int)InputType.longPress);
				}
				else if (Input.GetButtonUp(mappings[i]))
				{
					buttonsPressed[i] = false;
					applyBehaviors(i, (int)InputType.release);
				}
			}
			else if (Input.GetButtonDown(mappings[i]))
			{
				if (Time.realtimeSinceStartup - buttonDownTime[i] < doubleTapTime)
				{
					applyBehaviors(i, (int)InputType.doubleTap);
				}
				else
					applyBehaviors(i, (int)InputType.tap);

				buttonDownTime[i] = Time.realtimeSinceStartup;
				buttonsPressed[i] = true;
			}
		}
	}

	void applyBehaviors(int input, int inputType)
	{
		int i = 0;
		while (i < callbacks[input][inputType].Count)
		{
			if (callbacks[input][inputType][i].canBeUsed())
			{
				callbacks[input][inputType][i].Call();
				i++;
			}
			else
			{
				callbacks[input][inputType].RemoveAt(i);
			}
			
		}
	}
}
