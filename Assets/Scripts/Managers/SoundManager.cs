﻿/************************************************************************************

Filename    :   SoundManager.cs
Content     :   Sound Manager class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;

/**
 * SoundManager is the interface between the game and every FMOD-actions.
 * It must create, manage, advance, stop and clean sound events. Also it must
 * report the actual timeline to other managers to keep the game in sync with
 * the music.
 * @author Kevin Wagrez
 */ 
public class SoundManager : Singleton<SoundManager>
{
	// Launch trigger
	bool						isActive	=	false;

	#region Sound effects

	[System.Serializable]
	public struct SoundEffect
	{
		public string effectName;
		public FMODAsset asset;
		public string[] parameters;
		public bool localized;
	}
	public SoundEffect[] soundEffects;
	Dictionary<string, FMOD.Studio.EventInstance> soundEffectsTable = new Dictionary<string, EventInstance>();

	#endregion

	#region Main song

	// Song, with all of its parameters and asset
	public		SoundEffect		song;
	FMOD.Studio.EventInstance	songEvent;

	// Bank of parameters
	Dictionary<string, float> songParametersValues = new Dictionary<string, float>();


	float	_progression	=	0f;
	public	float	progression
	{
		get{return _progression;}
		private	set{ _progression = value; }
	}

	int _timeline = 0;
	public	int		timeline
	{
		get { return _timeline; }
		private set { _timeline = value; }
	}

	bool	progressionUpdated	=	false;
	public bool		isBuilt = false;
	#endregion

	#region SoundFade
	
	public	enum SoundFadeType
	{
	    fadeIn,
		fadeOut
	}

	#endregion

	protected	override	void	init	()
	{

	}

	public	void	build	()
	{
		songEvent = FMOD_StudioSystem.instance.GetEvent(song.asset.path);
		songEvent.setParameterValue("Progression_Part1", progression);

		// Create sound effects
		for (int i = 0; i < soundEffects.Length; i++)
			createEvent(soundEffects[i].asset, soundEffects[i].effectName);
		isBuilt = true;
	}

	public	void	launch	()
	{
		songEvent.start();
		songEvent.setPaused(true);
		isActive = false;
	}

	public	void	unload	()
	{
		songEvent.stop (STOP_MODE.ALLOWFADEOUT);

		for (int i = 0; i < song.parameters.Length; i++)
			songEvent.setParameterValue (song.parameters[i], 0f);

		for (int i = 0; i < soundEffects.Length; i++)
		{
			stopSound (soundEffects[i].effectName);
			for (int j = 0; j < soundEffects[i].parameters.Length; j++)
				setParameter (soundEffects[i].effectName, soundEffects[i].parameters[j], 0f);
		}
	}

	public	void	reload	()
	{
		launch ();
	}

	public	void	pause	(bool bValue)
	{
		FMOD.ChannelGroup	channelGroup;
		songEvent.getChannelGroup (out channelGroup);
		channelGroup.setPaused (bValue);

		// Sound effects
		for (int i = 0; i < soundEffects.Length; i++)
		{
			soundEffectsTable[soundEffects[i].effectName].getChannelGroup (out channelGroup);
			channelGroup.setPaused (bValue);
		}
		isActive	=	!bValue;
	}

	public	void	pauseSong	(bool bValue)
	{
		// Pause song but do not depaus
		songEvent.setPaused (bValue);
		isActive = !bValue;
	}


	void	FixedUpdate	()
	{
		if(!isActive)
			return;

		songEvent.getTimelinePosition(out _timeline);

		if(progressionUpdated)
		{
			songEvent.setParameterValue("Progression_Part1", progression);
			progressionUpdated = false;
		}
	}

	#region Event management

	public void createEvent(FMODAsset asset, string eventName)
	{
		soundEffectsTable[eventName] = FMOD_StudioSystem.instance.GetEvent(asset);
	}

	public bool isEventCreated(string eventName)
	{
		return soundEffectsTable.ContainsKey(eventName);
	}

	public FMOD.Studio.EventInstance getSongEvent()
	{
		return songEvent;
	}

	#endregion

	#region Parameter setting

	public	void	setProgression	(float newProgression)
	{
		progression = newProgression;
		progressionUpdated = true;
	}

	public	void	setSongParameter	(string parameterName, float value)
	{
		songEvent.setParameterValue(parameterName,value);
		songParametersValues[parameterName] = value;
	}

	public void		setParameter	(string soundName, string parameterName, float value)
	{
		soundEffectsTable[soundName].setParameterValue(parameterName, value);
	}

	public float getParameter(string parameterName)
	{
		return songParametersValues[parameterName];
	}

	public	int	getTimelinePosition	()
	{
		int index = 0;
		songEvent.getTimelinePosition(out index);
		return index;
	}

	public	void	setTimelinePosition	(int newPosition)
	{
		songEvent.setTimelinePosition(newPosition);
		timeline = newPosition;
	}

	public	void	moveAudioSource	(Vector3 newPosition)
	{
        // Move song with playhead
		songEvent.set3DAttributes(UnityUtil.to3DAttributes(newPosition));

        // Move non-localized audio sources with the playhead
		for (int i = 0; i < soundEffects.Length; i++)
		{
			if (!soundEffects[i].localized)
				soundEffectsTable[soundEffects[i].effectName].set3DAttributes(UnityUtil.to3DAttributes(newPosition));
		}
	}

	public	void	moveEventSource	(string eventName, Vector3 newPosition)
	{
		soundEffectsTable[eventName].set3DAttributes(UnityUtil.to3DAttributes(newPosition));
	}

	public	void	launchSound	(string eventName)
	{
		soundEffectsTable[eventName].start();
	}

	public	void	stopSound	(string eventName, bool fade = false)
	{
		if(fade)
			soundEffectsTable[eventName].stop(STOP_MODE.ALLOWFADEOUT);
		else
			soundEffectsTable[eventName].stop(STOP_MODE.IMMEDIATE);
	}

	public	void	launchDelayedSound	(string eventName, float delay)
	{
		StartCoroutine(manageSoundLifetime(eventName,delay));
	}

	public	void	launchDelayedSoundWithParameter	(string sound, float delay, string parameter)
	{
		launchSound(sound);
		StartCoroutine(manageSoundParameterActivation(sound,delay,parameter));
	}

	IEnumerator		manageSoundParameterActivation	(string sound, float delay, string parameter)
	{
		yield return new WaitForSeconds(delay);
		setParameter(sound,parameter,1f);
	}

	IEnumerator		manageSoundLifetime	(string sound, float delay)
	{
		launchSound(sound);
		yield return new WaitForSeconds(delay);
		stopSound(sound);
	}

	IEnumerator		fadeSound	(SoundFadeType	desiredFade, string soundName, float delay)
	{
		switch(desiredFade)
		{
			case SoundFadeType.fadeIn:
				soundEffectsTable[soundName].setVolume(0f);
				soundEffectsTable[soundName].start();

				for (float timer = 0f; timer < delay; timer += Time.deltaTime)
				{
					soundEffectsTable[soundName].setVolume(timer / delay);
					yield return null;
				}
				soundEffectsTable[soundName].setVolume(1f);

				break;

			case SoundFadeType.fadeOut:

				for (float timer = 0f; timer < delay; timer += Time.deltaTime)
				{
					soundEffectsTable[soundName].setVolume(1 - timer / delay);
					yield return null;
				}

				soundEffectsTable[soundName].setVolume(0f);
				soundEffectsTable[soundName].stop(STOP_MODE.IMMEDIATE);
				break;
		}


	}

	#endregion


	#region Debug

#if DEBUG
	private Rect soundArea = new Rect(0, 0, 200f, 100f);
	private string soundDebug = "";

	void OnGUI()
	{
		soundDebug = "Timeline: " + getTimelinePosition() + " \n"; ;
		GUI.TextArea(soundArea, soundDebug);
	}
#endif

	#endregion
}
