﻿/************************************************************************************

Filename    :   TrackManager.cs
Content     :   Track Manager class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class TrackManager : Singleton<TrackManager>
{
	#region Parameters

	[System.Serializable]
	public struct SongTrack
	{
		public string trackName;
		public string volumeParameter;
		public float desiredActivationTime;
		public float timeBeforeDecay;
		public GameObject feedbackController;
	}

	// Song description
	public	SongTrack[]				songDesc;
	Dictionary<string, SongTrack>	trackDesc = new Dictionary<string, SongTrack>(); // Private cache for quick song access

	// Track states
	public enum TrackState
	{
		interactive = 0, // Can be activated
		active = 1, // Plays normally
		broken = 2, // Glitched
		repaired = 3, // Plays normally
        malfunctioning = 4, // Plays weirdly
        erased = 5 // Not playing anymore
	}
	public Dictionary<string, TrackState> trackStates = new Dictionary<string, TrackState>();

	// Activation is possible
	public  bool tracksCanBeActivated = true;

	// Quick access dict for feedback controllers
	Dictionary<string, TrackFeedbackController> feedbackControllersTable = new Dictionary<string,TrackFeedbackController>();

	// Mixers
	public	MixTableController mixTableController;

	// Phase Animation
	AnimatedTrack[]		animationTracks = null;

	// Normalized timers
	Dictionary<string, float>	activationLevels = new Dictionary<string, float>();
	Dictionary<string, float>	lastLook = new Dictionary<string, float>();

	// Memory for delayed activation
	string	trackToActivate;

	#endregion

	#region Main general

	protected	override	void	init	()
	{
		mixTableController = PlayHead.instance.GetComponentInChildren<MixTableController> ();

		// Build pool & init track system	
		for (int i = 0; i < songDesc.Length; i++)
		{
			string trackName = songDesc[i].trackName;

			// Begin tracks inactive & at zero
			trackStates[trackName] = TrackState.interactive;
			activationLevels[trackName] = 0f;
			lastLook[trackName] = 0f;

			// Cache quick access
			trackDesc[trackName] = songDesc[i];
			feedbackControllersTable[trackName] = songDesc[i].feedbackController.GetComponent<TrackFeedbackController>();
		}
	}

	/// <summary>
	/// Retrieve animated objects to the pool, to be able to keep them to the next level.
	/// </summary>
	public	void	unload	()
	{
		for (int i = 0; i < songDesc.Length; i++)
			SoundManager.instance.setSongParameter(songDesc[i].volumeParameter, 0f);
	}

	public	void	reload	()
	{
		// Build pool & init track system	
		for (int i = 0; i < songDesc.Length; i++)
		{
			trackStates[songDesc[i].trackName] = TrackState.interactive;
			activationLevels[songDesc[i].trackName] = 0f;
			feedbackControllersTable[songDesc[i].trackName].reload();
		}

        // It is necessary to retrieve the mixtable since it is destroyed with the PlayHead
		mixTableController = PlayHead.instance.GetComponentInChildren<MixTableController>();
	}

	#endregion

	#region Animations

	public	PulseAnimator	getPulseAnimator	(string trackName)
	{
		return feedbackControllersTable[trackName].pulseController;
	}

	/// <summary>
	/// Link tracks with phase-specific animations for sync
	/// </summary>
	/// <param name="newPhase"></param>
	public	void	retrievePhaseAnimations	(Phase newPhase)
	{
        animationTracks = newPhase.tracks;
		for (int i=0; i < newPhase.tracks.Length; i++)
		{
			feedbackControllersTable[newPhase.tracks[i].trackName].phaseTrackAnimation = newPhase.tracks[i].localAnimationState;

            if (trackStates[newPhase.tracks[i].trackName] == TrackState.active
                || trackStates[newPhase.tracks[i].trackName] == TrackState.repaired)
                startOrStopReadAnimation(newPhase.tracks[i].trackName);
		}
	}

	public	void	beginPulse	()
	{
		for (int i = 0; i < songDesc.Length; i++)
		{
			feedbackControllersTable[songDesc[i].trackName].beginPulse();
		}
	}

    public  void    switchPulse (string trackName, string pulseAnimationName)
    {
        feedbackControllersTable[trackName].pulseController.forceSwitchPulse(pulseAnimationName);
    }

	public	void	startOrStopReadAnimation	(string trackName, bool bValue = true)
	{
        if (animationTracks == null)
            return;

		// Play animation on tracks
		for (int i=0; i < animationTracks.Length; i++)
		{
			if (animationTracks[i].trackName == trackName)
			{
                if (bValue)
				    animationTracks[i].play ();
                else
                    animationTracks[i].stop();
				break;
			}
		}
	}

    public  void    startOrStopAllReadAnimations   (bool bValue)
    {
        // Play animation on tracks
        for (int i = 0; i < animationTracks.Length; i++)
        {
            if(bValue)
                animationTracks[i].play();
            else
                animationTracks[i].stop();
        }
    }

	#endregion

	#region Track Interaction

	public	void	showMixingInterface	(bool bValue)
	{
		mixTableController.showMixingInterface(bValue);
	}

	public	bool	OnLookAt	(string trackName, float timeSpent)
	{
		// Discard looks when playhead is not playing & during interaction lock after activation
		if (PlayHead.instance.trackNavigationController.actualState != TrackNavigationController.TrackPlayState.playing
			|| !tracksCanBeActivated)
			return false;

		// Interactive track behavior
        if (trackStates[trackName] == TrackState.interactive)
        {

            // Increase time
            activationLevels[trackName] += timeSpent / trackDesc[trackName].desiredActivationTime;

            if(activationLevels[trackName] > 1f)
            {
                // Track State
                trackStates[trackName] = TrackState.active;
                activationLevels[trackName] = 1f;

                // Prepare delayed activation
                trackToActivate = trackName;
                float activationDelayToNextGate = PlayHead.instance.trackNavigationController.getTimeBeforeNextGate();

                // Initiate activation
                PlayerController.instance.eyeController.activateReticle();
                feedbackControllersTable[trackToActivate].initiateActivation(activationDelayToNextGate);
                tracksCanBeActivated = false;

                // Activation will be confirmed in DELAY
                Invoke("activationEffect", activationDelayToNextGate);

                return true;
            }
            else
            {
                if (Time.realtimeSinceStartup - lastLook[trackName] > trackDesc[trackName].timeBeforeDecay)
                    feedbackControllersTable[trackName].onBeginLook();
                else
                    feedbackControllersTable[trackName].onLook();
            }

        }
        // Malfunctioning track
        // Looking at them decreases their activation level
        else if (trackStates[trackName] == TrackState.malfunctioning)
        {
            activationLevels[trackName] -= timeSpent / trackDesc[trackName].desiredActivationTime;

            // Desactivate track
            if(activationLevels[trackName] < 0f)
            {
                trackStates[trackName] = TrackState.erased;
                activationLevels[trackName] = 0f;

                feedbackControllersTable[trackName].erase();
                startOrStopReadAnimation(trackName, false);

                return true;
            }
            else
            {
                if (Time.realtimeSinceStartup - lastLook[trackName] > trackDesc[trackName].timeBeforeDecay)
                    feedbackControllersTable[trackName].onBeginLook(false);
                else
                    feedbackControllersTable[trackName].onLook();
            }
        }

		// Remember this look
		lastLook[trackName] = Time.realtimeSinceStartup;

		return false;
	}

	void	Update	()
	{
		// TODO: verify if there some redundant calls
		for (int i = 0; i < songDesc.Length; i++)
		{
            if(Time.realtimeSinceStartup - lastLook[songDesc[i].trackName] > songDesc[i].timeBeforeDecay)
            {
                // 2 cases
                if ((trackStates[songDesc[i].trackName] == TrackState.interactive && activationLevels[songDesc[i].trackName] > 0.01f )
                    || (trackStates[songDesc[i].trackName] == TrackState.malfunctioning && activationLevels[songDesc[i].trackName] < 1f)
                    )
				{
                    // Behavior depends of state
                    switch(trackStates[songDesc[i].trackName])
                    {
                        case TrackState.malfunctioning:
                            activationLevels[songDesc[i].trackName] += Time.deltaTime / songDesc[i].desiredActivationTime;
                            feedbackControllersTable[songDesc[i].trackName].onNotLook(); // Apply evolution of
                            break;

                        case TrackState.interactive:
                            activationLevels[songDesc[i].trackName] -= Time.deltaTime / songDesc[i].desiredActivationTime;
                            feedbackControllersTable[songDesc[i].trackName].onNotLook(); // Apply evolution of activation level
                            SoundManager.instance.setSongParameter(trackDesc[songDesc[i].trackName].volumeParameter, 0f);// Remove sound previsualization
                            break;
                    }
					
				}
			}
		}
	}

	/// <summary>
	/// Invoked
	/// </summary>
	void	activationEffect	()
	{

		feedbackControllersTable[trackToActivate].activate (); // Specific feedbacks from playhead & track
		startOrStopReadAnimation (trackToActivate); // Play animation on tracks

        PlayHead.instance.feedbackController.glow();

		Invoke ("restoreActivation", 2f);
	}

	void	restoreActivation	()
	{
		tracksCanBeActivated = true;
	}

	public	float	getActivationLevel	(string trackName)
	{
		return activationLevels[trackName];
	}

	public	float	getLastTimeOfLook	(string trackName)
	{
		return lastLook[trackName];
	}

	public	void	setActivationLevel (string trackName, float level)
	{
		OnLookAt(trackName,level);
	}

    public  void    syncGlitchMaterial  ()
    {
        for (int i = 0; i < songDesc.Length; i++)
		{
            feedbackControllersTable[songDesc[i].trackName].sync();
        }
    }

    /// <summary>
    /// I don't remember why applyFeedbacks is so important...
    /// That would be better to apply these choices inside a trackfeedbackcontroller instead of there
    /// </summary>
    /// <param name="trackName"></param>
    /// <param name="desiredStatus"></param>
    /// <param name="applyFeedbacks"></param>
	public	void	setTrackStatus	(string trackName, TrackState desiredStatus, bool applyFeedbacks = true)
	{
		trackStates[trackName] = desiredStatus;

		switch (desiredStatus)
		{
			// Reset active signal & downlight the volume 
			case TrackState.broken:
				SoundManager.instance.setSongParameter (trackDesc[trackName].volumeParameter, 0.8f);
                startOrStopReadAnimation(trackName, false); // Stop any read animation
				break;
            case TrackState.repaired:
			case TrackState.active:
				activationLevels[trackName] = 1f;
				startOrStopReadAnimation (trackName); // Read is Online again
				SoundManager.instance.setSongParameter (trackDesc[trackName].volumeParameter, 0.8f);
				break;
			default:
				break;
		}

		if (!applyFeedbacks)
			return;

		switch(desiredStatus)
		{
			// Reset active signal & downlight the volume 
			case TrackState.broken:
				feedbackControllersTable[trackName].reload ();
                feedbackControllersTable[trackName].switchAnimatorToMaterial  (1); // Careful ! it changes the material to animate !
				break;
			case TrackState.active:
				feedbackControllersTable[trackName].activate ();
				break;
			case TrackState.repaired:
				feedbackControllersTable[trackName].repair ();
				break;
            case TrackState.malfunctioning:
                feedbackControllersTable[trackName].malfunction();
                break;
			default:
				break;
		}
	}

	#endregion

	#region Debug

#if DEBUG
	private Rect trackManager = new Rect(0, 400, 400f, 200f);
	private string trackDebug = "";

	void OnGUI()
	{
		trackDebug = "Active tracks \n";
		for (int i = 0; i < songDesc.Length; i++)
		{
			trackDebug += songDesc[i].trackName + " is " + trackStates[songDesc[i].trackName].ToString() + "\n";
			trackDebug += "Pulse is at " + feedbackControllersTable[songDesc[i].trackName].pulseController.pulseAnimationState.normalizedTime 
				+ " with speed " + feedbackControllersTable[songDesc[i].trackName].pulseController.pulseAnimationState.speed + "\n";
		}

		GUI.TextArea(trackManager, trackDebug);
	}
#endif

	#endregion
}
