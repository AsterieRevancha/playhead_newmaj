﻿/************************************************************************************

Filename    :   PlayHead.cs
Content     :   PlayHead class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Scripts/PlayHead/Managers/PlayHead")]
public class PlayHead : Singleton<PlayHead>
{
	//
	public enum PlayHeadControlState
	{
		auto = 0, // slowly moving forward
		track = 1, // Attached to animation
		free = 2, // Free motion
		still = 3
	}
	public PlayHeadControlState controlState = PlayHeadControlState.still;

	// Interfaces
	public GameObject controlBar;
	public PlayHeadTrackNavigationInterface trackNavigationInterface;

	// Navigation controllers
	public	Dictionary<PlayHeadControlState, INavigationController> navigationControllers = new Dictionary<PlayHeadControlState, INavigationController>();
	public	Component[] navigationControllersUsable;

	// Feedbacks
	public PlayHeadFeedbackController feedbackController;

	// Specific: track navigation controller, which is needed by many other elements
	public TrackNavigationController trackNavigationController;

	protected	override	void	init	()
	{
		for (int i = 0; i < navigationControllersUsable.Length; i++)
		{
			INavigationController controller = navigationControllersUsable[i] as INavigationController;
			navigationControllers[controller.getControlStateCondition()] = controller;
		}
	}

	/* ----------------
	 * Main update
	 * ---------------- */

	void	Update	()
	{
		// Move playhead
		if(controlState != PlayHeadControlState.still)
		{
			SoundManager.instance.moveAudioSource(PlayerController.instance.transform.position);
			navigationControllers[controlState].move();
		}
	}

	public	void	reload	()
	{
		trackNavigationInterface.reload();
	}

	/* ----------------
	 * Navigation control interface
	 * ---------------- */

	#region

	public void setNavigationControlTo	(PlayHeadControlState desiredControl)
	{
		if(controlState != PlayHeadControlState.still)
			navigationControllers[controlState].stop();

		controlState = desiredControl;

		if (controlState != PlayHeadControlState.still)
			navigationControllers[controlState].launch();
	}

	public float getSpeed()
	{
		return navigationControllers[controlState].getSpeed();
	}

	public	void setSpeed	(float _speed)
	{
		navigationControllers[controlState].setSpeed(_speed);
	}

	public	void	pause	(bool bValue)
	{
		navigationControllers[controlState].pause();
	}

	#endregion


}
