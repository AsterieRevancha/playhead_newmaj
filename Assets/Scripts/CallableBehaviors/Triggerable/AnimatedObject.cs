﻿/************************************************************************************

Filename    :   AnimatedObject.cs
Content     :   AnimatedObject class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Scripts/PlayHead/Triggerables/Animated Object")]
public class AnimatedObject : CallableBehaviorWrapper
{
	public enum AnimationEvent
	{
		OnStart = 0,
		OnComplete = 1
	}

	[System.Serializable]
	public class AnimationEventBinding : CallbackBinding
	{
		public AnimationEvent desiredEvent;
		public override int getId() { return (int)desiredEvent; }
	}
	public List<AnimationEventBinding> bindings;

	protected override void init()
	{
		callbacks[(int)AnimationEvent.OnStart] = null;
		callbacks[(int)AnimationEvent.OnComplete] = null;
	}

	protected override int getBindingCount()
	{
		return bindings.Count;
	}

	protected override CallbackBinding getBinding(int index)
	{
		return bindings[index];
	}

	/* --------
	 * 
	 * -------- */

	public void OnStart()
	{
		if (callbacks[(int)AnimationEvent.OnStart] != null)
			callbacks[(int)AnimationEvent.OnStart]();
	}

	public	void	OnComplete	()
	{
		if (callbacks[(int)AnimationEvent.OnComplete] != null)
			callbacks[(int)AnimationEvent.OnComplete]();
	}

}
