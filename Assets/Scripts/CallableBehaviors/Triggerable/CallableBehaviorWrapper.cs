﻿/************************************************************************************

Filename    :   CallableBehaviorWrapper.cs
Content     :   CallableBehaviorWrapper class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class CallbackBinding
{
	// Function to be called
	public CallableBehavior behavior;
	public CallableBehavior getBehavior() { return behavior; }

	// Identifier for enums
	public abstract int getId();
}

public	abstract class CallableBehaviorWrapper : MonoBehaviour
{
	protected	Dictionary<int, CallableBehavior.Callback> callbacks = new Dictionary<int, CallableBehavior.Callback>();
	protected	Dictionary<int, List<CallableBehavior>> behaviors = new Dictionary<int, List<CallableBehavior>>();

	int bindingsCount = 0;

	void Start()
	{
		init();

		// Bind callbacks
		bindingsCount = getBindingCount();
		for (int i = 0; i < bindingsCount; i++)
		{
			callbacks[getBinding(i).getId()] += getBinding(i).getBehavior().Call;

			// Store also callable behavior
			if (!behaviors.ContainsKey(getBinding(i).getId()))
				behaviors[getBinding(i).getId()] = new List<CallableBehavior>();
			behaviors[getBinding(i).getId()].Add(getBinding(i).getBehavior());
		}
	}

	protected	void	verifyAvailability	()
	{
		for (int i = 0; i < bindingsCount; i++)
		{
			for (int j = 0; j < behaviors[getBinding(i).getId()].Count; j++ )
			{
				if (!behaviors[getBinding(i).getId()][j].canBeUsed())
				{
					callbacks[getBinding(i).getId()] -= behaviors[getBinding(i).getId()][j].Call;
					behaviors[getBinding(i).getId()].RemoveAt(j);
					i--;
					break;
				}
			}
				
		}
	}

	protected abstract void init(); // Must init callbacks for enumerators
	protected abstract int getBindingCount();
	protected abstract CallbackBinding getBinding(int index);

	//	Useful to see connections
	void	OnDrawGizmos	()
	{
		// Bind callbacks
		int bindingCount = getBindingCount();
		for (int i = 0; i < bindingCount; i++)
		{
			if (getBinding(i).getBehavior() != null)
				Gizmos.DrawLine(transform.position, getBinding(i).getBehavior().transform.position);
		}
	}
}
