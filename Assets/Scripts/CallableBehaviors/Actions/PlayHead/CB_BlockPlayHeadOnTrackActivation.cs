﻿/************************************************************************************

Filename    :   CB_BlockPlayHeadOnTrackActivation.cs
Content     :   CB_BlockPlayHeadOnTrackActivation class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Scripts/PlayHead/CallableBehaviors/PlayHead/Block track unless tracks are active")]
public class CB_BlockPlayHeadOnTrackActivation : CallableBehavior
{
	[System.Serializable]
	public struct TrackCondition
	{
		public string trackName;
		public TrackManager.TrackState desiredState;
	}
	public List<TrackCondition> conditions;

	// Success behaviors
	public CallableBehavior[] OnSuccess;
	public CallableBehavior[] OnFailure;

	public override void OnCall	()
	{
		// Verify coniditons
		bool isValid = true;
		for (int i = 0; i < conditions.Count; i++)
			if (!(TrackManager.instance.trackStates[conditions[i].trackName] == conditions[i].desiredState))
				isValid = false;

		if (isValid)
		{
			for(int i=0; i<OnSuccess.Length;i++)
			{
				if (OnSuccess[i].canBeUsed())
					OnSuccess[i].Call();
			}
		}
		else
		{
			for (int i = 0; i < OnFailure.Length; i++)
			{
				if (OnFailure[i].canBeUsed())
					OnFailure[i].Call();
			}
		}
	}
}
