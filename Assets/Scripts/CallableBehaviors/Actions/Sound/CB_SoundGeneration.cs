﻿/************************************************************************************

Filename    :   CB_SoundGeneration.cs
Content     :   CB_SoundGeneration class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Scripts/PlayHead/CallableBehaviors/Sound/Generate sound")]
public class CB_SoundGeneration : CallableBehavior
{
	public Transform		soundSource;

	// Collection of sounds
	public string			soundName;
	public List<FMODAsset>	sounds;

	// Selection of sound
	int				rangeSounds		= 0;
	List<string>	accessorNames	= new List<string>();

	void	Awake	()
	{
		// Create the sounds associated to the object
		for (int i = 0; i < sounds.Count; i++)
		{
			string accessor = soundName + "_" + i;
			accessorNames.Add(accessor);
			if (!SoundManager.instance.isEventCreated(accessor))
				SoundManager.instance.createEvent(sounds[i], accessor);
		}

		rangeSounds = sounds.Count;
	}

	public override void OnCall	()
	{
		// Launch one sound in random
		int i = UnityEngine.Random.Range(0, rangeSounds);
		SoundManager.instance.launchSound(accessorNames[i]);
		SoundManager.instance.moveEventSource(accessorNames[i], soundSource.position);
	}
}
