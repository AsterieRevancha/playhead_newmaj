﻿/************************************************************************************

Filename    :   LegacyColorAnimatorForMaterial.cs
Content     :   LegacyColorAnimatorForMaterial class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class LegacyColorAnimatorForMaterial : MonoBehaviour
{
	// Parameters
	public Color color0;
	public Color color1;
	public Color color2;
	public Color color3;

	// Equivalents
	public	string[]	parameterNames;
	int[]				parameterIDs;

	// Target material
	public Material targetMaterial;

	// Animation
	public Animation animationControl;

	public delegate Color GetParameter ();
	private GetParameter[] getParameters;

	void Awake()
	{
		buildDelegates();
		// Seek IDs
		parameterIDs = new int[parameterNames.Length];
		for (int i=0; i < parameterNames.Length; i++)
			parameterIDs[i] = Shader.PropertyToID (parameterNames[i]);
	}

	void Update()
	{
		if (animationControl.isPlaying)
			delegateUpdate();
	}

	void delegateUpdate()
	{
		for (int i = 0; i < parameterIDs.Length; i++)
			targetMaterial.SetColor (parameterIDs[i], getParameters[i] ());
	}

	void buildDelegates()
	{
		getParameters = new GetParameter[parameterNames.Length];
		for (int i = 0; i < parameterNames.Length; i++)
		{
			MethodInfo info = this.GetType().GetMethod("getParameter" + i);
			getParameters[i] = System.Delegate.CreateDelegate (typeof (GetParameter), this, info) as GetParameter;
		}
	}

	#region
	public Color getParameter0()
	{
		return color0;
	}

	public Color getParameter1()
	{
		return color1;
	}

	public Color getParameter2()
	{
		return color2;
	}

	public Color getParameter3()
	{
		return color3;
	}


	#endregion
}
