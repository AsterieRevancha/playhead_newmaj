﻿/************************************************************************************

Filename    :   Singleton.cs
Content     :   Singleton class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;

public abstract	class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	// Instance
	protected static T _self;
	protected int count = 0;
	public static T instance
	{
		get
		{
			if (_self != null)
				return _self;
			else
			{
				// Try a search
				// Note: this will be specific to each child
				_self = factory();
				return _self;
			}
		}
	}

	/**
	 * We consider the possibility that the singleton has been instantiated
	 * by an external call. In this case, the count has not been incremented.
	 */
	void Awake()
	{
		// Singleton already existing
		if (_self != null && count > 0)
		{
			// To mark the fact that this singleton MUST NOT destroy the instance
			count = 2;
			Destroy(this.gameObject);
			return;
		}
		// Singleton has already been searched by external call
		else if (_self != null)
		{
			count = 1;
			init();
		}
		// There was no singleton
		else
		{
			count = 1;
			_self = getSelf();
			init();
		}
	}

	/**
	 * Do all of the initialization stuff only the real singleton should do
	 */ 
	protected abstract void init();

	/**
	 * Set the singleton to null.
	 * Else decreases the number of elements
	 */
	protected virtual void OnDestroy()
	{
		if (_self != null && count == 1)
			_self = null;
		else
			count = 1;
	}

	/**
	 * It *might* require to be overriden, but that should do the trick
	 * The T we wish to return is atually the singleton itself
	 */
	protected T getSelf()
	{
		return GetComponent<T>();
	}

	/**
	 * Factory
	 * Used to find the locations of the various managers
	 * TODO: Allow factory to fail ! Indeed, sometimes the singleton does not
	 * exist yet, and *CANNOT* exist yet
	 */
	protected static T factory()
	{
		// Designer should always exist
		if (typeof(T).Equals(typeof(DesignerManager)))
		{
			return GameObject.FindGameObjectWithTag(Tags.designer).GetComponent<DesignerManager>() as T;
		}
		/* These managers are all located on a gamemanager gameobject
		 * They might not be present all the time
		 * */
		else if (typeof(T).Equals(typeof(GameManager)))
		{
			GameObject go = GameObject.FindGameObjectWithTag(Tags.gameManager);
			if (go != null)
				return go.GetComponent<GameManager>() as T;
			else
				return null;
		}
		else if (typeof(T).Equals(typeof(SoundManager)))
		{
			GameObject go = GameObject.FindGameObjectWithTag(Tags.gameManager);
			if (go != null)
				return go.GetComponent<SoundManager>() as T;
			else
				return null;
		}
		else if (typeof(T).Equals(typeof(HashIDs)))
		{
			GameObject go = GameObject.FindGameObjectWithTag(Tags.gameManager);
			if (go != null)
				return go.GetComponent<HashIDs>() as T;
			else
				return null;
		}
		else if (typeof(T).Equals(typeof(TravelManager)))
		{
			GameObject go = GameObject.FindGameObjectWithTag(Tags.gameManager);
			if (go != null)
				return go.GetComponent<TravelManager>() as T;
			else
				return null;
		}
		else if (typeof(T).Equals(typeof(TrackManager)))
		{
			GameObject go = GameObject.FindGameObjectWithTag(Tags.gameManager);
			if (go != null)
				return go.GetComponent<TrackManager>() as T;
			else
				return null;
		}
		/* Located in player
		 * May not be there
		 * */
		else if (typeof(T).Equals(typeof(PlayHead)))
		{
			GameObject go = GameObject.FindGameObjectWithTag(Tags.playHead);
			if (go != null)
				return go.GetComponent<PlayHead>() as T;
			else
				return null;
		}
		else if (typeof(T).Equals(typeof(PlayerController)))
		{
			GameObject go = GameObject.FindGameObjectWithTag(Tags.player);
			if (go != null)
				return go.GetComponent<PlayerController>() as T;
			else
				return null;
		}
		else
			return default(T);

	}

}
