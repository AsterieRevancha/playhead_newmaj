﻿/************************************************************************************

Filename    :   ScreenShooter.cs
Content     :   ScreenShooter class
Created     :   November 1, 2014
Authors     :   Guillaume Bertinet

Copyright   :   Copyright 2014, Guillaume Bertinet. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;

public class ScreenShooter : MonoBehaviour {

	public string Shortcut = "s";
	public int SizeMultiplier = 1;
	public string ScreenName = "Screenshot";
	public int ScreenNumber = 0;

	void Update () {
		if(Input.GetKeyDown(Shortcut))
		{
			Application.CaptureScreenshot(ScreenName+ScreenNumber+".png", SizeMultiplier);
			//Debug.Log("Screen target number "+ScreenNumber+" shot.");
			ScreenNumber += 1;
		}
	}
}
