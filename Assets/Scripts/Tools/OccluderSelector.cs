﻿/************************************************************************************

Filename    :   OccluderSelector.cs
Content     :   OccluderSelector class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/
using UnityEngine;
using System.Collections;

public class OccluderSelector : MonoBehaviour
{
	public	GameObject[]	objects;
	int		objectIndex		= 0;

	bool	selectButtonPressed;
	float	selectButtonTime		= 0.5f;
	float	selectButtonDownTime	= 0f;
	string	selectButtonID			= "Select";

	void	Awake	()
	{
		objects[objectIndex].SetActive (true);
	}

	void	Update	()
	{

		if (selectButtonPressed)
		{

			if (Input.GetButton (selectButtonID))
			{

				if (Time.realtimeSinceStartup - selectButtonDownTime > selectButtonTime)
				{
					objects[objectIndex].SetActive (false);
					objectIndex = (objectIndex + 1) % objects.Length;

					objects[objectIndex].SetActive (true);
					selectButtonPressed = false;
					return;
				}

			}

			else if (Input.GetButtonUp (selectButtonID))
			{
				selectButtonPressed = false;
			}
		}

		else if (Input.GetButtonDown (selectButtonID))

		{
			selectButtonPressed = true;
			selectButtonDownTime = Time.realtimeSinceStartup;
		}

	}

}
