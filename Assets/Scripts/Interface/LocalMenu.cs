﻿/************************************************************************************

Filename    :   LocalMenu.cs
Content     :   LocalMenu class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LocalMenu : MonoBehaviour
{
	public bool							timerDecay				= false;
	public float						timeBeforeVanish		= 6f;

	public bool							orientTowardPlayer		= false;
	public List<InteractiveObject>		buttons;
	public GameMenu						owner;
	public CallableBehavior.Callback	buttonCallback;

	// Animations
	public	Animation					animationController		= null;
	public	string						showAnimationName;
	public	string						hideAnimationName;

	#region Logic

	public	void	reset	()
	{
		buttonCallback = null;
		for (int i = 0; i < buttons.Count; i++)
			buttons[i].reset();
	}

	public void resetOtherButtons	(InteractiveObject selected)
	{
		int selectedIndex = buttons.IndexOf(selected);
		for (int i = 0; i < buttons.Count; i++)
		{
			if (i != selectedIndex)
				buttons[i].reset();
		}
	}

	public	bool	launch	()
	{
		if (buttonCallback != null)
		{
			buttonCallback();
			return true;
		}
		else return false;
	}

	#endregion

	#region Animations

	public void show	(bool bValue)
	{
		if(bValue)
		{
			gameObject.SetActive (true);
			if(animationController != null)
				animationController.Play (showAnimationName);
		}
		else
		{
			if(animationController != null && gameObject.activeSelf)
			{
				animationController.Play (hideAnimationName);
				StartCoroutine (hideAfterAnimation ());
			}
			else
			{
				gameObject.SetActive (false);
			}
		}
	}

	IEnumerator		hideAfterAnimation	()
	{
		animationController.Play(hideAnimationName);
		while(animationController.isPlaying)
			yield return null;
		gameObject.SetActive (false);
	}

	#endregion
}
