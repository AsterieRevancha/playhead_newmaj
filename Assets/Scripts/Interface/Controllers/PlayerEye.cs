﻿/************************************************************************************

Filename    :   PlayerEye.cs
Content     :   PlayerEye class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;

public class PlayerEye : EyeController
{

    float cptActivationButton = 0f;

	protected override	void applyEyeEffect	(RaycastHit eyeRayHit)
	{
		// Object might be looked upon
		ILookable io = eyeRayHit.collider.GetComponent(typeof(ILookable)) as ILookable;
		if (io != null)
		{
			io.OnLookAt(deltaTime);
			return;
		}

		// Object might be controlled
		IControlable ic = eyeRayHit.collider.GetComponent(typeof(IControlable)) as IControlable;
		if (ic != null)
		{
			ic.OnLookAt(eyeRayHit);
			return;
		}
	}

    

	protected	virtual	void	Update	()
	{
		Vector3		eye				= eyeOrigin.position;
		Vector3		eyeDirection	= eyeOrigin.forward;
		RaycastHit	eyeRayHit;

		deltaTime = Time.realtimeSinceStartup - timeOfLastUpdate; // Compute delta time

		// Look with your cybernetic eye !
		// But only those objects you can interact with (actually, there should ONLY be interactive objects)
		if(Physics.Raycast(eye, eyeDirection, out eyeRayHit,farSight,1 << LayerMask.NameToLayer("InteractiveObjects")))
		{
			onInteractiveObject = true;
			applyEyeEffect(eyeRayHit);

            if (eyeRayHit.collider.tag == Tags.button)
            {
                //Debug.Log("Reticle in Button object");
                cptActivationButton += 0.01f;
                reticleRenderer.sharedMaterial.SetFloat("_Activation", cptActivationButton);
            }

			if(eyeRayHit.collider.tag == Tags.track)
			{
				InteractiveTrackArea track = eyeRayHit.collider.GetComponent<InteractiveTrackArea>();
				reticleRenderer.sharedMaterial.SetFloat ("_Activation", TrackManager.instance.getActivationLevel (track.trackName));

                // Move impact object to collider
                if (!impactIndicatorRenderer.enabled)
                {
                    // Show object
                    if (PlayHead.instance.trackNavigationController.actualState == TrackNavigationController.TrackPlayState.playing
                        && TrackManager.instance.tracksCanBeActivated
                        && (TrackManager.instance.trackStates[track.trackName] == TrackManager.TrackState.interactive
                        || TrackManager.instance.trackStates[track.trackName] == TrackManager.TrackState.malfunctioning)
                        )
                    {
                        impactIndicatorTransform.position = eyeRayHit.point;
                        impactIndicatorTransform.LookAt(impactIndicatorTransform.position + eyeRayHit.normal);
                        impactIndicatorRenderer.enabled = true;

                        // Speed is dependant on the track state
                        if (TrackManager.instance.trackStates[track.trackName] == TrackManager.TrackState.malfunctioning)
                        {
                            impactIndicatorRenderer.sharedMaterial.SetFloat("_Speed", 5f);
                        }
                        else if (TrackManager.instance.trackStates[track.trackName] == TrackManager.TrackState.interactive)
                        {
                            impactIndicatorRenderer.sharedMaterial.SetFloat("_Speed", -5f);
                        }
                    }


                }
                else if (impactIndicatorRenderer.enabled)
                {

                    // Position imapct objetc to correct position
                    if (PlayHead.instance.trackNavigationController.actualState == TrackNavigationController.TrackPlayState.playing
                        && TrackManager.instance.tracksCanBeActivated
                        && (TrackManager.instance.trackStates[track.trackName] == TrackManager.TrackState.interactive
                        || TrackManager.instance.trackStates[track.trackName] == TrackManager.TrackState.malfunctioning)
                        )
                    {
                        impactIndicatorTransform.position = eyeRayHit.point;
                        impactIndicatorTransform.LookAt(impactIndicatorTransform.position + eyeRayHit.normal);
                    }
                    // Hide impact object
                    else
                    {
                        impactIndicatorRenderer.enabled = false;
                    }
                }
			}

			reticleRenderer.sharedMaterial.SetFloat ("_Hover", 1f);
 
		}
		else
		{
            cptActivationButton = 0;
			reticleRenderer.sharedMaterial.SetFloat ("_Hover", 0f);
			reticleRenderer.sharedMaterial.SetFloat ("_Activation", 0f);

            if (impactIndicatorRenderer.enabled)
            {
                impactIndicatorRenderer.enabled = false;
            }
		}

		// Store
		timeOfLastUpdate	=	Time.realtimeSinceStartup;
	}

	
}
