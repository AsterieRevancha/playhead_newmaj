﻿/************************************************************************************

Filename    :   PlayerController.cs
Content     :   PlayerController class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;

public class PlayerController : Singleton<PlayerController>
{
	// Various cameras
	public	GameObject	desktopRoot;
	public	GameObject	stereoRoot;

	public	Transform	desktopEye;
	public	Transform	stereoEye;

	// Head
	public	GameObject		selectedRoot;
	public	Transform		eye;
	public	EyeController	eyeController;

	// Fader tool
	public	Fader		fader;
	public	float		fadeStartTime = 1f;

	public	PlayerAssistant		helper;

    // Computation shortcuts
    Transform playerOrigin;

	// Debug
	public bool	isStereo = false;

	protected	override	void	init	()
	{
		// System preference
		Application.targetFrameRate = 60;

        // Shortcut
        playerOrigin = transform;

		// Keep player & fmod alive
		DontDestroyOnLoad (FMOD_StudioSystem.instance.gameObject);
		DontDestroyOnLoad (this.gameObject);

		// Oculus or Desktop
		if (isStereo)
		{
			stereoRoot.SetActive(true);
			desktopRoot.SetActive(false);
			selectedRoot = stereoRoot;
			eye = stereoEye;

			GameManager.instance.gameMenu.OVRPlatformMenuIsOn = true;
		}
		else
		{
			stereoRoot.SetActive(false);
			desktopRoot.SetActive(true);
			selectedRoot = desktopRoot;
			eye = desktopEye;

			GameManager.instance.gameMenu.OVRPlatformMenuIsOn = false;
		}

		eyeController = eye.GetComponent<EyeController> ();
	}

	IEnumerator	Start	()
	{
		eye.gameObject.SetActive(false);
        yield return StartCoroutine(fader.asyncFade(fadeStartTime,Fader.FadeType.FadeOut));
		eye.gameObject.SetActive(true);
	}

	public	void	unload	()
	{
		eye.gameObject.SetActive(false);
	}

	public void		reload	()
	{
		eye.gameObject.SetActive(true);
	}

    /// <summary>
    /// Only for Yaw & pitch
    /// </summary>
    void    Update  ()
    {
        // Compute rotation
        Vector3 eyeMotion = Quaternion.FromToRotation(playerOrigin.forward, eye.forward).eulerAngles;

        SoundManager.instance.setSongParameter("Yaw", eyeMotion.y);
        SoundManager.instance.setSongParameter("Pitch", eyeMotion.x);

        //Debug.Log(eyeMotion);
    }
}
