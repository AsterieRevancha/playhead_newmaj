﻿/************************************************************************************

Filename    :   EyeContoller.cs
Content     :   EyeController class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;

public abstract class EyeController : MonoBehaviour
{
	public	Transform	eyeOrigin;

	public	bool		hideReticleAtStart	= false;
	bool				reticleIsShown		= true; 

	// General charcteristics
	public	float		farSight			= 10f; // Avoid the user to activate very long
	public	float		reticleFollowSpeed	= 10f; // m/s

	// Reticle
	public	Transform	reticleTransform;
	public	Renderer	reticleRenderer;

    // Impact
    public  Transform   impactIndicatorTransform;
    public  Renderer    impactIndicatorRenderer;

	// Reticle configuration
	protected	float	fixedDistance		= 2f;
	protected	bool	onInteractiveObject = false;
	protected	Vector3	desiredPosition		= Vector3.zero;

	// Offline sight requires to manually compute delta-time
	protected float timeOfLastUpdate = 0f;
	protected float deltaTime = 0f;

	#region Show/hide reticle

	public void showReticle	(bool bValue)
	{
		reticleRenderer.enabled = bValue;
		reticleIsShown = bValue;
	}

	/// <summary>
	/// Reticle is present in menu but must be shown/hidden only depending on the game state
	/// </summary>
	/// <param name="bValue"></param>
	public	void	showReticleInMenu	(bool bValue)
	{
		if(bValue)
			reticleRenderer.enabled = true;
		else
			reticleRenderer.enabled = reticleIsShown;
	}

	void OnLevelWasLoaded (int index)
	{
		showReticle (!hideReticleAtStart);
	}

	#endregion

    #region Impact

    public  void    changeSpeedImpactParticles  (float newSpeed)
    {
        impactIndicatorRenderer.sharedMaterial.SetFloat("_Speed", newSpeed);
    }

    #endregion

    #region Reticle feedbacks

    public	void	activateReticle	()
	{
		StartCoroutine (manageReticleActivation ());
	}

	IEnumerator		manageReticleActivation	()
	{
		for(float timer = 0f; timer < 0.5f; timer += Time.deltaTime)
		{
			reticleRenderer.sharedMaterial.SetFloat ("_ActivationFlash", timer);
			yield return null;
		}

		for (float timer = 0.5f; timer > 0f; timer -= Time.deltaTime)
		{
			reticleRenderer.sharedMaterial.SetFloat ("_ActivationFlash", timer);
			yield return null;
		}
	}

	#endregion

	protected abstract	void applyEyeEffect	(RaycastHit eyeRayHit);
}
