﻿/************************************************************************************

Filename    :   PlayerAssistant.cs
Content     :   PlayerAssistant class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAssistant : MonoBehaviour
{
	// Help display
	public	Material	helpDisplayMaterial;
	public	Material	helpIconMaterial;
	public	GameObject	helpIcon;
	public	GameObject	helpMessageDisplay;
	public	GameObject	helpMask;

	// Objective display
	public	Material	objectiveDisplayMaterial;
	public	GameObject	objectiveDisplay;
	public	GameObject	objectiveMask;

    // Sounds
    public string tipSound = "Tip";
    public string objectiveSound = "Objective";

	Vector2	offset = new Vector2(0,0);

	// Complex index system 
	Dictionary<int, float>	objectiveOffsets	=	new Dictionary<int,float>();
	Dictionary<int, float>	helpOffsets			=	new Dictionary<int, float> ();

	#region Timely display

	void	Awake	()
	{
		objectiveOffsets[0] = 0.163f; // phase 1
		objectiveOffsets[1] = -0.163f; // phase 2
		objectiveOffsets[2] = 0f; // phase 3

		helpOffsets[0] = 0f; // Touchpad
		helpOffsets[1] = -0.165f; // Look aroudn you
		helpOffsets[2] = -0.28f; // sliders
	}

	public void displayHelp (int index, float delay)
	{
		gameObject.SetActive (true);

		// turn off objective
		objectiveDisplay.SetActive (false);
		objectiveMask.SetActive (false);
		helpIcon.SetActive (true);
		helpMessageDisplay.SetActive (true);
		helpMask.SetActive (true);

		offset.y = helpOffsets[index];
		helpDisplayMaterial.SetTextureOffset ("_MainTex", offset);

		helpIconMaterial.SetTextureOffset ("_MainTex", new Vector2 (index % 2 * 0.5f, Mathf.RoundToInt (index / 2f) * 0.5f));

        // Launch sound
        SoundManager.instance.launchSound(tipSound);
		
		Invoke ("close", delay);
	}

	public	void	displayObjective	(int index, float delay)
	{
		gameObject.SetActive (true);

		// Turn off help
		objectiveDisplay.SetActive (true);
		objectiveMask.SetActive (true);
		helpIcon.SetActive (false);
		helpMessageDisplay.SetActive (false);
		helpMask.SetActive (false);

		offset.y = objectiveOffsets[index];
		objectiveDisplayMaterial.SetTextureOffset ("_MainTex", offset);

        // Sound
        SoundManager.instance.launchSound(objectiveSound);

		Invoke ("close", delay);
	}

	public	void	close	()
	{
		gameObject.SetActive (false);
	}

	#endregion

}
