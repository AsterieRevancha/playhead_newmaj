﻿/************************************************************************************

Filename    :   ParameterSlider.cs
Content     :   ParameterSlider class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;

[AddComponentMenu("Scripts/PlayHead/Triggerables/Parameter Slider")]
public class ParameterSlider : MonoBehaviour,	IControlable
{
	// Sound parameters
	public	string		parameterName;
	public	string		volumeParameter;
	public	string		trackName;

	// State
	public	bool		lockable		= true;
	public	bool		correct			= false;
	bool				isInteractive	= false;

	// Curve
	public	AnimationCurve	glitchBehavior;
	public	Material		glitchMaterial;
    public  Material        sliderMaterial;

	// Slider
	public Transform	sliderStart;
	public Transform	sliderStop;
	public Vector3		sliderDirection = new Vector3(0, 0, 1);
    public Gradient     sliderColor;

    // Focus
    public Gradient sliderAreaColor;

    // Lights
    public string objectiveParameterName = "_lightX";

    // Sound
    public string soundToPlay = "ValidMix";

	// Memory
	public	float		activationTime = 1.5f;
	public	float		decayDelay = 0.2f;
	float				lastLook = 0f;
	float				lastParameterValue = 1f;
	float				lastParameterChange = 0f;
	float				desiredParameterValue = 0f;

	// Computation cache
	float				sliderLength = 0f;
	Vector3				sliderVector;
	Renderer			sliderRenderer;
	Collider			sliderCollider;

	// IDs for faster access
	int		glitchParameterID;
	int		sliderPositionID;
	int		sliderIntensityID;
    int     sliderGlitchID;
    int     sliderColorID;
    int sliderAreaColorID;
    int     objectiveLightID;
	int		faderID;

	void	Awake	()
	{
		glitchParameterID	= Shader.PropertyToID ("_GlitchIntensity");
		sliderPositionID	= Shader.PropertyToID ("_SliderPosition");
		sliderIntensityID	= Shader.PropertyToID ("_SliderIntensity");
        sliderGlitchID      = Shader.PropertyToID ("_Glitch");
        sliderColorID       = Shader.PropertyToID ("_SliderColor");
        sliderAreaColorID = Shader.PropertyToID("_Color");
        objectiveLightID    = Shader.PropertyToID(objectiveParameterName);
		faderID				= Shader.PropertyToID ("_Fader");

		sliderRenderer	= GetComponent<Renderer>();
		sliderCollider	= GetComponent<Collider>();
		sliderVector	= sliderStop.position - sliderStart.position;
		sliderLength	= sliderVector.magnitude;
	}

	void OnEnable ()
	{
		// Init value to glitch intensity
		desiredParameterValue = glitchMaterial.GetFloat (glitchParameterID);
        sliderMaterial.SetFloat(sliderGlitchID, desiredParameterValue * 0.1f);

		correct = false;
		sliderRenderer.enabled = true;

        StartCoroutine(fadeTo(-0.01f, true));
	}

	public void OnLookAt	(RaycastHit hit)
	{
		if (GameManager.instance.gameMenu.playerIsInMenu() || 
			!(PlayHead.instance.trackNavigationController.actualState == TrackNavigationController.TrackPlayState.playing))
			return;

		// Memorize when slider was last seen
		lastLook = Time.realtimeSinceStartup;

		// Project where the eye points at on the sldier itself
		Vector3 projection = Vector3.Project(hit.point - sliderStart.position, sliderVector);

		if (!isInteractive)
		{
			isInteractive = true;
			//StartCoroutine (fadeTo (1f, true));
            StartCoroutine(changeColorTo(0f, 1f));
			SoundManager.instance.setSongParameter (volumeParameter, 1f);
		}

		float projectionAngle = Vector3.Dot (projection, sliderVector);
		float progressionThroughSlider = projection.magnitude / sliderLength;

		// Ignore < 0 & > 1
		if (projectionAngle >= 0f && progressionThroughSlider < 1.01f)
		{
			// Get the value of the glitch at given position in slider
			desiredParameterValue = glitchBehavior.Evaluate (progressionThroughSlider);

            // Slider material evolution
            sliderMaterial.SetFloat(sliderGlitchID, desiredParameterValue * 0.1f);
            sliderMaterial.SetFloat(sliderPositionID, progressionThroughSlider - 0.5f);
            sliderMaterial.SetColor(sliderColorID, sliderColor.Evaluate(desiredParameterValue));
			glitchMaterial.SetFloat (glitchParameterID, desiredParameterValue); // changes glitch on tracks
		}
	}

	void	Update	()
	{

		if (isInteractive)
		{
			if (Time.realtimeSinceStartup - lastLook > decayDelay)
			{
				SoundManager.instance.setSongParameter (volumeParameter, 0.8f);
				//StartCoroutine (fadeTo (0f, false));
                StartCoroutine(changeColorTo(1f, 0f));
				isInteractive = false;
				return;
			}

			// If value has changed, memorize time of change
			if (Mathf.Abs (lastParameterValue - desiredParameterValue) > 0.05f)
			{
				lastParameterValue = desiredParameterValue;
				lastParameterChange = Time.realtimeSinceStartup;
			}
			else
			{
				// If it has been a long time, validate change
				if (Time.realtimeSinceStartup - lastParameterChange > activationTime && desiredParameterValue < 0.05f && lockable)
				{
					complete ();
				}
				SoundManager.instance.setSongParameter (parameterName, desiredParameterValue);
			}

			// Change slider intensity
			float intensity = Mathf.Min (1f, (0.2f + (Time.realtimeSinceStartup - lastParameterChange)));
            sliderMaterial.SetFloat(sliderIntensityID, intensity / activationTime);
		}
	}

	void	complete	()
	{
		sliderCollider.enabled = false;
		//sliderRenderer.enabled = false;

        StartCoroutine(fadeTo(-2f, false));

        // Launch validation sound
        SoundManager.instance.launchSound(soundToPlay);

		TrackManager.instance.setTrackStatus (trackName, TrackManager.TrackState.repaired); // Feedbacks
		PlayHead.instance.feedbackController.glow (); // Lights are on

		glitchMaterial.SetFloat (glitchParameterID, 0f);

		// Track has been repaied
		isInteractive = false;
		correct = true;

        // Increment objective
        PlayHead.instance.feedbackController.validateObjective(objectiveLightID);
	}

	IEnumerator		fadeTo	(float value, bool status)
	{
        float startValue = sliderMaterial.GetFloat(faderID);
		if (status)
			sliderRenderer.enabled = true;

		for(float timer = 0f; timer < 0.5f; timer += Time.deltaTime)
		{
            sliderMaterial.SetFloat(faderID, Mathf.Lerp(startValue, value, timer / 0.5f));
			yield return null;
		}
		if(!status)
			sliderRenderer.enabled = false;
	}

    IEnumerator     changeColorTo   (float start, float end)
    {
        float value = start;
        sliderMaterial.SetColor(sliderAreaColorID, sliderAreaColor.Evaluate(start));

        for (float timer = 0f; timer < 0.5f; timer += Time.deltaTime)
        {
            value = Mathf.Lerp(start, end, timer * 2f);
            sliderMaterial.SetColor(sliderAreaColorID, sliderAreaColor.Evaluate(value));
            yield return null;
        }

        sliderMaterial.SetColor(sliderAreaColorID, sliderAreaColor.Evaluate(end));
    }
}
