﻿/************************************************************************************

Filename    :   GameMenu.cs
Content     :   GameMenu class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Can be summoned during gameplay. Freeze the timescale and all managers.
 * Allows level reloading, quit toward Oculus Home and OVR sensor reset.
 * @author Kevin Wagrez
 */ 
[AddComponentMenu("Scripts/PlayHead/Menu/GameMenu")]
public class GameMenu : MonoBehaviour
{
	public LocalMenu playHeadControlBar;
	public LocalMenu mainMenuObject;
	public LocalMenu mapMenuObject;

	public	bool	OVRPlatformMenuIsOn = false;

	public enum MenuType
	{
		playHeadMenu,
		main,
		songMap,
		none
	}
	MenuType displayedMenu = MenuType.none;

	// menu authorizations
	Dictionary<MenuType, bool> menuPermissions = new Dictionary<MenuType, bool>();

	// Menu
	bool			isActive = true;

	// Interaction modalities
	public	float			doubleTapTime	=	0.25f;
	public	float			longPressTime	=	0.75f;
	public	string			selectButtonID	=	"Select";
	public string			backButtonID	=	"Back";

	// Management of buttons
	bool			selectButtonPressed = false;
	float			selectButtonDownTime = 0f;
	bool			backButtonPressed	=	false;
	float			backButtonDownTime	=	0f;

	// Self-recovery
	float			timeOfSwitch		=	0f;
	float			delayOfRecovery		=	1f;

	// Self-orientation
	Transform		menuRoot;

	#region Main functions

	void	Awake	()
	{
		menuRoot = transform;

		menuPermissions[MenuType.main] = true; // Always, always !
		menuPermissions[MenuType.songMap] = false; // Unfeatured for now
		menuPermissions[MenuType.playHeadMenu] = false; // Only available while attached to playhead 

		// Search for playhead menu
		// REQUIRES PlayHead to be present
		playHeadControlBar = PlayHead.instance.controlBar.GetComponent<LocalMenu>();
		playHeadControlBar.owner = this;
		playHeadControlBar.show(false);

		// Bind main menu
		if (!OVRPlatformMenuIsOn)
		{
			mainMenuObject.owner = this;
			mainMenuObject.show (false);
		}

		// Bind map menu, if present
		if(mapMenuObject != null)
		{
			mapMenuObject.owner = this;
			mapMenuObject.show(false);
		}
	}

	/// <summary>
	/// The playhead menu may have been destroyed during restart
	/// </summary>
	public	void	reload ()
	{
		// Search for playhead menu
		// REQUIRES PlayHead to be present
		playHeadControlBar = PlayHead.instance.controlBar.GetComponent<LocalMenu>();
		playHeadControlBar.owner = this;
		playHeadControlBar.show(false);
	}

	void	Update	()
	{
		if(!isActive)
		{
			if((Time.realtimeSinceStartup - timeOfSwitch) > delayOfRecovery)
				isActive = true;
			else 
				return;
		}

		switch (displayedMenu)
		{
			case MenuType.playHeadMenu:
				playheadMenu();
				break;

			case MenuType.none:
				invisibleMenu();
				break;

			case MenuType.main:
				mainMenu();
				break;

			case MenuType.songMap:
				mapMenu();
				break;

			default:
				break;
		}
	
	}

	#endregion

	#region Menu inputs

	/// <summary>
	/// 
	/// </summary>
	void	invisibleMenu	()
	{
		if (backButtonPressed)
		{
#if MOONLIGHT
			if (Input.GetKey(KeyCode.Escape))
#else
			if (Input.GetButton(backButtonID))
#endif
			{
				// Oculus menu short-circuit this
				if (OVRPlatformMenuIsOn)
					return;

				if (menuPermissions[MenuType.main] && (Time.realtimeSinceStartup - backButtonDownTime) > longPressTime)
				{
					showMenu(MenuType.main, true);
					return;
				}
				 
			}
#if MOONLIGHT
			else if (Input.GetKeyUp(KeyCode.Escape))
#else
			else if (Input.GetButtonUp(backButtonID))
#endif
			{
				backButtonPressed = false;
			}
		}
#if MOONLIGHT
		else if (Input.GetKeyDown(KeyCode.Escape))
#else
		else if (Input.GetButtonDown(backButtonID))
#endif
		{
			backButtonPressed = true;
			backButtonDownTime = Time.realtimeSinceStartup;
		}

		if (selectButtonPressed)
		{
			if (Input.GetButton(selectButtonID))
			{
				if (menuPermissions[MenuType.playHeadMenu] && (Time.realtimeSinceStartup - selectButtonDownTime) > doubleTapTime)
				{
					showMenu (MenuType.playHeadMenu, true);
					SoundManager.instance.launchSound ("PopUpMenu");
				}
			}
			else if (Input.GetButtonUp(selectButtonID))
			{
				selectButtonPressed = false;
			}
		}
		else if (Input.GetButtonDown(selectButtonID))
		{
			selectButtonPressed = true;
			selectButtonDownTime = Time.realtimeSinceStartup;
		}
	}

	/// <summary>
	/// 
	/// </summary>
	void	mapMenu	()
	{
#if MOONLIGHT
		if (Input.GetKeyDown(KeyCode.Escape))
#else
		if (Input.GetButtonDown(backButtonID))
#endif
		{
			showMenu(MenuType.songMap,false);
		}
		else if(Input.GetButtonDown(selectButtonID))
		{
			if (mapMenuObject.launch())
				showMenu(MenuType.songMap,false);
		}
	}

	void	playheadMenu	()
	{
		if (Input.GetButtonUp(selectButtonID))
		{
			playHeadControlBar.launch();
			showMenu(MenuType.playHeadMenu,false);
			SoundManager.instance.launchSound ("PopOutMenu");
		}
	}

	void	mainMenu	()
	{
		if(backButtonPressed)
		{
#if MOONLIGHT
			if (Input.GetKey(KeyCode.Escape))
#else
			if (Input.GetButton(backButtonID))
#endif
			{
				if((Time.realtimeSinceStartup-backButtonDownTime) > longPressTime)
					showMenu(MenuType.main,false);
			}
#if MOONLIGHT
			else if (Input.GetKeyUp(KeyCode.Escape))
#else
			else if (Input.GetButtonUp(backButtonID))
#endif
			{
				backButtonPressed = false;
				if ((Time.realtimeSinceStartup - backButtonDownTime) <= doubleTapTime)
					mainMenuObject.reset();
			}
		}
#if MOONLIGHT
		else if (Input.GetKeyDown(KeyCode.Escape))
#else
		else if (Input.GetButtonDown(backButtonID))
#endif
		{
			backButtonPressed	=	true;
			backButtonDownTime	=	Time.realtimeSinceStartup;
		}
		// A button is selected
		else if(Input.GetButtonDown(selectButtonID))
		{
			if(mainMenuObject.launch())
				showMenu(MenuType.main,false);
		}
	}

	#endregion

	#region Show a menu

	public	void	showMenu	(MenuType desiredMenu, bool bValue)
	{
		switch(desiredMenu)
		{
			/* PlayHead menu, only available after link playhead-player */
			case MenuType.playHeadMenu:
				if (playHeadControlBar == null)
					return;
				playHeadControlBar.show(bValue);
				break;
			case MenuType.main:
				GameManager.instance.pause(bValue);
				if(mainMenuObject != null)
					mainMenuObject.show(bValue);
				break;
		}

		PlayerController.instance.eyeController.showReticleInMenu (bValue);
		resetButtons(); // Reset buttons
		displayedMenu = bValue ? desiredMenu : MenuType.none; // Chane menu or revert to none
		timeOfSwitch = Time.realtimeSinceStartup; // Keep the menu inactive for a moment

		// Orientation
		Vector3 originalEulerRotation = menuRoot.rotation.eulerAngles;
		menuRoot.rotation = Quaternion.Euler(originalEulerRotation.x,PlayerController.instance.eye.rotation.eulerAngles.y,originalEulerRotation.z);
	}

	void	resetButtons	()
	{
		selectButtonPressed = false;
		selectButtonDownTime = 0f;
		backButtonPressed = false;
		backButtonDownTime = 0f;

		if(playHeadControlBar!=null)
			playHeadControlBar.reset();
		if(mainMenuObject != null)
			mainMenuObject.reset();
		if (mapMenuObject != null)
			mapMenuObject.reset();
	}
	#endregion

	#region Local bindings with sub-menus

	/// <summary>
	/// When the player reconnects with the playhead, he receives a new menu.
	/// For now, only the PlayerHead menu is taken into account
	/// </summary>
	/// <param name="desiredMenu"></param>
	/// <param name="menuObject"></param>
	public	void	attach	(GameObject menuObject, MenuType desiredMenu = MenuType.playHeadMenu)
	{
		switch(desiredMenu)
		{
			case MenuType.playHeadMenu:
				playHeadControlBar = menuObject.GetComponent<LocalMenu>();
				break;
			default:
				break;
		}
	}

	public	void	unlockMenu	(MenuType desiredMenu, bool status)
	{
		menuPermissions[desiredMenu] = status;
	}

	public	bool	playerIsInMenu	()
	{
		return displayedMenu != MenuType.none;
	}

	#endregion
}
