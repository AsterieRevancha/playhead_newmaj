﻿/************************************************************************************

Filename    :   PlayHeadTrackNavigationInterface.cs
Content     :   PlayHeadTrackNavigationInterface class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Scripts/PlayHead/Interaction/Navigation map")]
public class PlayHeadTrackNavigationInterface : MonoBehaviour
{
	// Tracks
	public	string [] monitoredTracks;
	public	string [] activityParameters;

	// Phase
	Phase actualPhase;
	List<float> gateQuartiles = new List<float>();

	void	Awake	()
	{
		reload();
	}

	public	void	reload	()
	{
		for (int i = 0; i < monitoredTracks.Length; i++)
		{
			GetComponent<Renderer>().sharedMaterial.SetFloat(activityParameters[i],0f);
		}
	}

	void	OnEnable	()
	{
		for(int i=0; i<monitoredTracks.Length; i++)
			GetComponent<Renderer>().sharedMaterial.SetFloat(activityParameters[i], Mathf.Min(1f,TrackManager.instance.getActivationLevel(monitoredTracks[i])));
	}

	void	updateProgression	()
	{
		if(actualPhase != null)
		{ 
			GetComponent<Renderer>().sharedMaterial.SetFloat("_CursorPosition", actualPhase.getAnimationProgression());
		}
	}

	public	void	OnExitPhase	()
	{
		actualPhase = null;
		CancelInvoke();
		gateQuartiles.Clear();
	}

	public	void	OnEnterPhase	(Phase newPhase)
	{
		actualPhase = newPhase;

		if (newPhase.gameObject.name == "phase_1")
			GetComponent<Renderer>().sharedMaterial.SetFloat("_Phase", 0f);
		else
			GetComponent<Renderer>().sharedMaterial.SetFloat("_Phase", 1f);

		CancelInvoke();
		InvokeRepeating("updateProgression", 0.1f, 0.1f);

		gateQuartiles.Clear();

		// Find new quartiles
		for (int i = 0; i < actualPhase.hyperMeasures.Length; i++)
			gateQuartiles.Add((float)(actualPhase.hyperMeasures[i].startTime - actualPhase.startTime) / (float)actualPhase.lengthTime);
	}

}
