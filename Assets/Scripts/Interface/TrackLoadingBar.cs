﻿/************************************************************************************

Filename    :   TrackLoadingBar.cs
Content     :   TrackLoadingBar class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;

public class TrackLoadingBar : MonoBehaviour
{
    public  Renderer gaugeRenderer;

    public string loadingSound = "XLoadingBar";
    public string unloadingSound = "XUnloadingBar";

    // State
    public  enum LoadingState
    {
        Loading,
        Unloading
    }
    public  LoadingState state = LoadingState.Loading;

    // Fade
    enum FadeType
    {
        FadeIn,
        FadeOut,
        None
    }
    FadeType fadeType = FadeType.None;

    // IDs
    int gaugeID;
    int faderID;

    void    Awake   ()
    {
        gaugeID = Shader.PropertyToID("_Jauge");
        faderID = Shader.PropertyToID("_Fader");
        gaugeRenderer.enabled = false;
    }

    public  void    switchToState   (LoadingState newState)
    {
        state = newState;
    }

    public  void    changesLevel  (float level, bool positive = true)
    {
        gaugeRenderer.sharedMaterial.SetFloat(gaugeID, level);

        // Show gauge if fading & new level
        if ((positive && !gaugeRenderer.enabled) || (positive && fadeType == FadeType.FadeOut))
        {
            fade(FadeType.FadeIn);
            manageSoundFromState(true);
        }

        // Hide gauge
        if (fadeType == FadeType.None && !positive)
        {
            fade(FadeType.FadeOut);
            manageSoundFromState(false);
        }
    }

    void    manageSoundFromState    (bool active)
    {
        if(state == LoadingState.Loading)
        {
            if (active)
                SoundManager.instance.launchSound(loadingSound);
            else
                SoundManager.instance.stopSound(loadingSound);
        }
        else
        {
            if (active)
                SoundManager.instance.launchSound(unloadingSound);
            else
                SoundManager.instance.stopSound(unloadingSound);
        }

    }

    void    fade    (FadeType desiredFade)
    {
        StopAllCoroutines();
        fadeType = desiredFade;
        StartCoroutine(asyncFade(desiredFade));
    }

    IEnumerator asyncFade    (FadeType desiredFade)
    {
        if (desiredFade == FadeType.FadeIn)
            gaugeRenderer.enabled = true;

        for(float timer = 0f; timer <1f; timer += 2 * Time.deltaTime)
        {
            gaugeRenderer.sharedMaterial.SetFloat(faderID, (desiredFade == FadeType.FadeIn) ? -2f + timer * 2f : timer * -2f);
            yield return null;
        }

        if (desiredFade == FadeType.FadeOut)
            gaugeRenderer.enabled = false;

        fadeType = FadeType.None;
    }

}
