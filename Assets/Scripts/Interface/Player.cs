﻿/************************************************************************************

Filename    :   Player.cs
Content     :   Player class
Created     :   November 1, 2014
Authors     :   Kevin Wagrez

Copyright   :   Copyright 2014, Kevin Wagrez. All Rights reserved.
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files, to deal in the Software
without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

************************************************************************************/


using UnityEngine;
using System.Collections;

public class ObjectiveInterface : MonoBehaviour
{
	public	Material	objectiveDisplayMaterial;
	public	GameObject	objectiveDisplay;

	Vector2	offset = new Vector2(0,0);
	float stepSize = 1f / 3f;

	#region Timely display

	public	void	displayObjective	(int index, float delay)
	{
		gameObject.SetActive (true);

		offset.y = index * stepSize;
		objectiveDisplayMaterial.SetTextureOffset ("_MainTex", offset);

		Invoke ("close", delay);
	}

	void	close	()
	{
		gameObject.SetActive (false);
	}

	#endregion

	public	void	show	(bool bValue)
	{
		gameObject.SetActive (bValue);
	}

	public	void	setPhaseObjective	(int index)
	{
		offset.y = index * stepSize;
		objectiveDisplayMaterial.SetTextureOffset ("_MainTex", offset);
	}
}
